<?php

namespace App\Helper;

use App\Models\Notificaciones;
use App\Models\UserNotificacion;
use App\Models\AdminNotificacion;
use App\Models\Admin;
use App\Models\User;

class Notificacion
{
    
    public static function instance()
    {
        return new Notificacion();
    }
    
    ///////////////////////////////////////NOTIFICACIONES DEL ADMIN
    
    public function store($data, $modulo){
        $admins = Admin::all();
        $notificacion = Notificaciones::create($data);
        
        $value['FK_id_notificaciones'] = $notificacion->id_notificaciones;
        foreach ($admins as $admin){
            $value['FK_id_useradmin'] = $admin->id_useradmin;
            switch ($modulo) {
                case 'usuarios': $admin->FK_id_adminrol == 1 ? AdminNotificacion::create($value) : ""; break;
                case 'noticia': $admin->p_noticias ? AdminNotificacion::create($value) : ""; break;
                /* case 'trivia': $admin->p_trivia ? AdminNotificacion::create($value) : ""; break;
                case 'premio': $admin->p_premios ? AdminNotificacion::create($value) : ""; break;
                case 'producto': $admin->p_productos ? AdminNotificacion::create($value) : ""; break;
                case 'capacitacion': $admin->p_capacitaciones ? AdminNotificacion::create($value) : ""; break; */
            }
        }
    }

    ///////////////////////////////////////NOTIFICACIONES DE USUARIO NORMAL

    public function storeClient($data){
        $users = User::all();
        $notificacion = Notificaciones::create($data);
        
        $value['FK_id_notificaciones'] = $notificacion->id_notificaciones;
        foreach ($users as $user){
            $value['FK_id_user'] = $user->id;
            UserNotificacion::create($value);
        }
    }

    public function storeUniqueClient($data){
        $notificacion = Notificaciones::create($data);
        
        $value['FK_id_notificaciones'] = $notificacion->id_notificaciones;
        $value['FK_id_user'] = $data["user"];
        UserNotificacion::create($value);
    }
}

?>