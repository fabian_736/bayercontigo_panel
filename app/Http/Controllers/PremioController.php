<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

//MODELOS
use App\Models\UserHistorial;
use App\Models\Premio;
use App\Models\PremioCanjeado;
use App\Models\PremioCaracteristicas;
use App\Models\PremioCategoria;
use App\Models\PremioEntrega;

//IMPORTACION
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\PremioImport;
use App\Imports\TestImport;

//Helper o Usables
use App\Helper\Notificacion;
use Intervention\Image\Facades\Image;

class PremioController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'imagen.required' => 'La imagen es requerida',
        'imagenUD.required' => 'La imagen es requerida',
        'min' => 'El dato debe ser mayor a 0',
        'numeric' => 'El dato debe ir en formato númerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
        'max' => 'El dato no debe ser mayor a 255 carácteres',
        'descripcion.max' => 'El dato no debe ser mayor a 65535 carácteres',
        'categoria.max' => 'El nombre de la categoría no debe exceder de 255 carácteres',
        'categoriaUD.max' => 'El nombre de la categoría no debe exceder de 255 carácteres',
        'entrega.max' => 'El dato no debe ser mayor a 255 carácteres',
        'marca.max' => 'El dato no debe ser mayor a 255 carácteres',
        'foto.max' => 'La imagen no puede ser mayor a 2Mb',
        'imagen.max' => 'La imagen no puede ser mayor a 2Mb',
        'imagenUD.max' => 'La imagen no puede ser mayor a 2Mb',
        'cedula.digits_between' => 'La cedula debe tener entra 4 y 20 dígitos',
    ];

    //Validaciones - Datos que viajaran como tal
    public $validaciones = [
        'nombre' => 'required|string|max:255', 
        'marca' => 'required|string|max:255', 
        'descripcion' => 'required|string|max:65535',
        'entrega' => 'required|string|max:255',
        'stock' => 'required|numeric|min:0',
        'puntos' => 'required|numeric|min:0',
        'imagen' => 'required|file|mimes:jpg,jpeg,png|max:2100',
        'FK_categoria' => 'required|numeric|min:0',
        'caracteristicas.*' => 'string|max:255',
        //Es Opcional
        'distribuidor' => 'nullable|string|max:255',
        'id_producto_dist' => 'nullable|numeric|min:0',
        'id_brand_dist' => 'nullable|numeric|min:0',
    ];

    //Recoger imagen del premio | categoria
    public function getImage($filename = null){
        
        if($filename){
            $exist = Storage::disk('public')->exists('premios/'.$filename);
            if(!$exist) //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('nodisponible.png');
            else
                $file = Storage::disk('public')->get('premios/'.$filename);
        }
        else
            $file = Storage::disk('public')->get('nodisponible.png');

        return new Response($file, 200);
    }

    //Compresor de Imagen
    public function imagecompress($image){
        $img = Image::make(storage_path('app/public/'.$image))->resize(1200, 1200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save();
        $img->destroy();
    }

    //Pantalla inicial al darle al sidebar
    public function index(){
        $premios = Premio::orderBy('created_at', 'desc')->take(3)->get();
        $canjeados = PremioCanjeado::orderBy('created_at', 'desc')->take(3)->get();
        
        //PARA CREAR PREMIO
        $categorias = PremioCategoria::all();

        return view('premios.index',compact('premios','canjeados','categorias'));
    }

    //Apartado visual de las categorias
    public function categorias(){
        $categorias = PremioCategoria::all();
        return view('premios.categorias',compact('categorias'));
    }

    //Apartado visual de los premios en total
    public function premios(){
        $premios = Premio::all();
        
        //PARA CREAR PREMIO
        $categorias = PremioCategoria::all();

        return view('premios.list',compact('premios','categorias'));
    }

    //Apartado visual de los canjeos en total
    public function canjeos($estado = null){
        if($estado)
            $canjeados = PremioCanjeado::where('estado',$estado)->orderBy('created_at', 'desc')->get();
        else
            $canjeados = PremioCanjeado::orderBy('created_at', 'desc')->get();
        
        return view('canjeados.list',[
            "canjeados" => $canjeados,
            "estado" => $estado
        ]);
    }

    //Tomar Datos de Entrega de Premio Canjeado
    public function entrega($id_premio){
        $canjeado = PremioCanjeado::findOrFail($id_premio);
        return view('canjeados.entrega', compact('canjeado'));
    }

    //Apartado visual de la ventana de edición de premio
    public function editPremio($id_premio){
        $premio = Premio::findOrFail($id_premio);

        //PARA EDITAR PREMIO
        $categorias = PremioCategoria::all();

        //Vista para la edición de datos
        return view('premios.edit',compact('premio', 'categorias'));
    }

    //Apartado visual de la ventana de edición de premio canjeado
    public function editCanjeado($id_canjeado){
        $canjeado = PremioCanjeado::findOrFail($id_canjeado);

        //Vista para la edición de datos
        return view('canjeados.edit',compact('canjeado'));
    }

    //Apartado visual del destroy de un premio
    public function deletePremio($id_premio){
        $premio = Premio::findOrFail($id_premio);
        return view('premios.confirm', compact('premio'));
    }

    ///////////////////////////////////////SECCION DE IMPORTACIÓN MASIVA///////////////////////////

    //Apartado visual Inicial para cargar premios con importación
    public function importIndex(){
        return view('premios.import_index');
    }

    //Metodo que lee el form enviado y manda la tabla de imports #TODO puede ser borrable
    public function importForm(Request $request){

        $import = array();
        $header = array();
        
        foreach( $request->nombre as $key => $nombre ) {
            $usuario = [];
            $usuario["nombre"] = $nombre;
            $usuario["apellido"] = $request->apellido[$key];
            $usuario["segmento"] = $request->segmento[$key];
            $usuario["telefono"] = $request->telefono[$key];
            $usuario["correo"] = $request->correo[$key];
            $usuario["especialidad"] = $request->especialidad[$key];
            array_push($import, $usuario);
        }

        return view ('premios.import_tabla', compact('import', 'header'));
    }

    //Metodo que lee el archivo excel y manda la tabla de imports
    public function importCsv(Request $request){
        
        $validate = Validator::make($request->all(), [
            'file' => 'required|file|mimes:xls,xlsx,csv',
        ], [
            'required' => 'El dato es requerido',
            'file' => 'El dato debe llegar como un archivo',
            'mime' => 'El archivo debe llegar en formato xls, xlsx, csv',
        ]);

        if($validate->fails()){
            return redirect()->route('premio.import')->withErrors($validate);     
        }

        $validaciones = $this->validaciones;
        $validaciones["telefono"] = 'required|max:15';
        unset($validaciones["total_estrellas"]);
        unset($validaciones["estado"]);

        //Categorias
        $categorias = PremioCategoria::all();

        try{
            $collection = Excel::toCollection(new PremioImport(auth()->user()->id_useradmin,$validaciones), $request->file);
            $import = array();
            $header = array();
            
            !array_key_exists("nombre",$collection[0][0]->toArray()) ? array_push($header, "nombre") : "";
            !array_key_exists("marca",$collection[0][0]->toArray()) ? array_push($header, "marca") : "";
            !array_key_exists("descripcion",$collection[0][0]->toArray()) ? array_push($header, "descripcion") : "";
            !array_key_exists("entrega",$collection[0][0]->toArray()) ? array_push($header, "entrega") : "";
            !array_key_exists("stock",$collection[0][0]->toArray()) ? array_push($header, "stock") : "";
            !array_key_exists("puntos",$collection[0][0]->toArray()) ? array_push($header, "puntos") : "";
            !array_key_exists("distribuidor",$collection[0][0]->toArray()) ? array_push($header, "distribuidor") : "";
            !array_key_exists("producto_id",$collection[0][0]->toArray()) ? array_push($header, "producto_id") : "";
            !array_key_exists("marca_id",$collection[0][0]->toArray()) ? array_push($header, "marca_id") : "";

            foreach ($collection[0] as $usuario){
                array_push($import, $usuario);
            }
            
            return view ('premios.import_tabla', compact('import', 'header','categorias'));

            //Excel::import(new ClientImport(auth()->user()->id_useradmin,$validaciones), $request->file);
            
            //Vista de que el import fue correcto
            //return redirect()->route('cliente.index')->with('create', 'Usuarios Correctamente Importados');
        }catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $errores = $e->failures();
            return view ('premios.import_index', compact('errores'));
       }
    }

    //Tabla de importados | creo que esta ruta ya no la utilizo pero bueno #TODO pendiente
    public function tabla_import(){
        return view('premios.import_tabla');
    }

    //Store Import de Premios
    public function importStore(Request $request){
        
        $validate = Validator::make($request->all(), [
            'nombre.*' => 'required|string|max:255',
            'marca.*' => 'required|string|max:255',
            'descripcion.*' => 'required|string|max:65535',
            'entrega.*' => 'required|string|max:255',
            'stock.*' => 'required|string|max:255',
            'puntos.*' => 'required|string|max:255',
            'distribuidor.*' => 'required|string|max:255',
            'producto_id.*' => 'required|string|max:255',
            'marca_id.*' => 'required|string|max:255',
        ], $this->validaciones);

        $header = array();
        $import = array();
        
        foreach( $request->nombre as $key => $nombre ) {
            $premio = [];
            $premio["nombre"] = $nombre;
            $premio["marca"] = $request->marca[$key];
            $premio["descripcion"] = $request->descripcion[$key];
            $premio["entrega"] = $request->entrega[$key];
            $premio["stock"] = $request->stock[$key];
            $premio["puntos"] = $request->puntos[$key];
            $premio["distribuidor"] = $request->distribuidor[$key];
            $premio["producto_id"] = $request->distribuidor[$key] != "N/A" ? $request->id_producto_dist[$key] : 0;
            $premio["marca_id"] = $request->distribuidor[$key] != "N/A" ? $request->id_brand_dist[$key] : 0;
            array_push($import, $premio);
        }

        if($validate->fails()){
            $errors = $validate->errors();
            //Categorias
            $categorias = PremioCategoria::all();
            return view ('premios.import_tabla', compact('import', 'header', 'categorias', 'errors'));
        }
        
        //Proceso completo ahora si de Importar
        DB::beginTransaction();
        try{
            foreach ($import as $premio){
                Premio::create([
                    'nombre' => $premio['nombre'],
                    'marca' => $premio['marca'],
                    'descripcion' => $premio['descripcion'],
                    'entrega' => $premio['entrega'],
                    'stock' => $premio["stock"],
                    'puntos' => $premio['puntos'],
                    'distribuidor' => $premio['distribuidor'],
                    'id_producto_dist' => $premio['producto_id'],
                    'id_brand_dist' => $premio['marca_id'],
                    'FK_categoria' => $request->categoria
                ]);
            }
            DB::commit();
            return redirect()->route('premio.import')->with('create', 'Los premios fueron creados satisfactoriamente');
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()->route('premio.import')->with('delete', 'Los premios no pudieron ser creados: '.$e->getMessage());
        }
    }

    //////////////////////SECCIÓN DEL CUD (CREATE, UPDATE Y DELETE)////////////////////

    //Store de Premio
    public function store(Request $request){
        
        $validaciones = $this->validaciones;

        if($request->tipo != "no"){
            unset($validaciones["imagen"]); 
        }

        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), $validaciones, $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $data = $request->all();
            
            if($request->tipo == "no"){
                $data["distribuidor"] = "N/A";
                $data["id_producto_dist"] = null;
                $data["id_brand_dist"] = null;
            }
            else{
                $validate = Validator::make($request->all(), [
                    'imagen_quantum' => 'required|string|max:255', 
                    'distribuidor' => 'required|string|max:255',
                    'id_producto_dist' => 'required|numeric|min:0',
                    'id_brand_dist' => 'numeric|min:0',
                ], $this->mensajes_error);

                if($validate->fails()){
                    return Redirect::back()->withErrors($validate)->withInput();
                }
            }
            
            if($request->tipo == "no"){
                if($request->hasFile('imagen')){
                    $data["imagen"] = $request->file('imagen')->store('premios', 'public');
                    $this->imagecompress($data["imagen"]);
                }
            }
            else{
                $data["imagen"] = $request->imagen_quantum;
            }

            $premio = Premio::create($data);

            //Grabamos las caracteristicas
            if($request->has('caracteristicas')){
                foreach ($request->caracteristicas as $caracteristica) {
                    $caract = New PremioCaracteristicas();
                    $caract->caracteristica = $caracteristica;
                    $caract->FK_id_premio = $premio->id_premio;
                    $caract->save();
                }
            }

            /* $data['accion'] = 'Inserción';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
            $data['FK_id_premios'] = $premio->id_premio;
            
            PremioCambio::create($data); */

            $data = [
                "nombre" => "Store de Premio",
                "tipo" => "Insert",
                "descripcion" => "Creación de Premio: ".$premio->nombre,
            ];

            Notificacion::instance()->store($data, "premio");

            DB::commit();
            return Redirect::back()->with('create', 'Premio Registrado');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('delete', 'Error al añadir premio: '.$e->getMessage());
        }

    }

    //Store de Categoria
    public function storeCategoria(Request $request){
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'categoria' => 'required|string|max:255',
                'imagen' => 'required|file|mimes:jpg,jpeg,png|max:2100',
            ], $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $data = $request->all();

            if($request->hasFile('imagen')){
                $data["imagen"] = $request->file('imagen')->store('premios', 'public');
                $this->imagecompress($data["imagen"]);
            }

            PremioCategoria::create($data);

            DB::commit();
            return Redirect::back()->with('create', 'Categoría Registrada Exitosamente');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('delete', 'Error al añadir Categoría: '.$e->getMessage());
        }
    }

    //Store de Canje de Premio


    //Update de Premio
    public function update(Request $request, $id_premio){
        
        $validaciones = $this->validaciones;
        if(!$request->hasFile('imagen') && $request->iBefore){
            $validaciones["imagen"] = 'file|mimes:jpg,jpeg,png|max:2100';
        }

        if($request->tipo != "no"){
            unset($validaciones["imagen"]); 
        }

        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), $validaciones, $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();      
            }

            $premio = Premio::findOrFail($id_premio);
            $data = $request->all();
            
            if($request->tipo == "no"){
                $data["distribuidor"] = "N/A";
                $data["id_producto_dist"] = null;
                $data["id_brand_dist"] = null;
            }
            else{
                $validate = Validator::make($request->all(), [
                    'imagen_quantum' => 'required|string|max:255', 
                    'distribuidor' => 'required|string|max:255',
                    'id_producto_dist' => 'required|numeric|min:0',
                    'id_brand_dist' => 'numeric|min:0',
                ], $this->mensajes_error);

                if($validate->fails()){
                    return Redirect::back()->withErrors($validate)->withInput();
                }
            }

            if($request->has('distribuidor')){
                if($data["distribuidor"] != "N/A"){
                    Storage::delete(['public/'.$premio->imagen]);
                    $data["imagen"] = $request->imagen_quantum;
                }
                else{
                    if($request->hasFile('imagen')){
                        Storage::delete(['public/'.$premio->imagen]);
                        $data["imagen"] = $request->file('imagen')->store('premios', 'public');
                        $this->imagecompress($data["imagen"]);
                    }
                }
            }

            //Grabamos las caracteristicas
            if($request->has('caracteristicas')){
                PremioCaracteristicas::where('FK_id_premio', $premio->id_premio)->delete();
                foreach ($request->caracteristicas as $caracteristica) {
                    $caract = New PremioCaracteristicas();
                    $caract->caracteristica = $caracteristica;
                    $caract->FK_id_premio = $premio->id_premio;
                    $caract->save();
                }
            }
            else{
                PremioCaracteristicas::where('FK_id_premio', $premio->id_premio)->delete();
            }

            $premio->update($data);

            /* $data['accion'] = 'Update';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
            $data['FK_id_premios'] = $id_premio;
            
            PremioCambio::create($data); */

            $data = [
                "nombre" => "Update de Premio",
                "tipo" => "Update",
                "descripcion" => "Edición de Premio: ".$premio->nombre,
            ];

            Notificacion::instance()->store($data, "premio");

            DB::commit();
            return Redirect::back()->with('edit', 'Premio Actualizado Exitosamente!!');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'Fallo al editar premio: '.$e->getMessage());
        }
    }

    //Update de Categoria
    public function updateCategoria(Request $request){
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'categoriaUD' => 'required|string|max:255',
                'imagenUD' => 'file|mimes:jpg,jpeg,png|max:2100',
            ], $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();      
            }
            
            $data = $request->all();
            $categoria = PremioCategoria::findOrFail($data["id_categoria"]);
            $categoria->categoria = $data["categoriaUD"];

            if($request->hasFile('imagenUD')){
                Storage::delete(['public/'.$categoria->imagen]);
                $data["imagenUD"] = $request->file('imagenUD')->store('premios', 'public');
                $this->imagecompress($data["imagenUD"]);
                $categoria->imagen = $data["imagenUD"];
            }

            $categoria->save();

            DB::commit();
            return Redirect::back()->with('edit', 'Categoría Actualizada Exitosamente');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'Error al editar Categoría: '.$e->getMessage());
        }
    }

    //Update de Premio Canjeado (datos de entrega y canje)
    public function updateCanjeado(Request $request, $id_canjeado){
            
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'estado' => 'required|string|max:255',
                'tiempo' => 'required|string|max:255',
            ], $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();      
            }
            
            $canjeado = PremioCanjeado::findOrFail($id_canjeado);
            $canjeado->entrega->update($request->all());

            switch ($request->input('estado')) {
                case 'pendiente': $mensaje = "Tu canje ha sido recibido y esta en proceso de entrega"; break;
                case 'empacando': $mensaje = "Tu premio esta siendo empacado para su entrega"; break;
                case 'saliendo': $mensaje = "Tu premio esta en camino para su entrega"; break;
                case 'entregado': $mensaje = "Tu premio ha sido entregado"; break;
            }

            //Notificación para los clientes
            $dataClient = [
                "nombre" => '¡NUEVO STATUS '.strtoupper($canjeado->nombre).' CANJEADO A '.strtoupper($canjeado->entrega->estado).'! 👨🎁',
                "tipo" => "Premio",
                "descripcion" => $mensaje,
                "user" => $canjeado->FK_id_user,
            ];

            Notificacion::instance()->storeUniqueClient($dataClient);

            //Correo de Notificación al cliente tambien
            /* $information = new \stdClass();
            $information->asunto = 'Estatus de Premio Canjeado '. $canjeado->nombre;
            $information->descripcion = $mensaje;
            $information->entrega = $canjeado->premio->entrega;
            $information->nombre = $canjeado->cliente->nombre." ".$canjeado->cliente->apellido;
            $information->premio = $canjeado->nombre;
            $information->cantidad = $canjeado->cantidad;
            $information->total_estrellas = $canjeado->total_estrellas;
            $information->fecha_redencion = $canjeado->fecha_redencion;
            $information->referencia = $canjeado->referencia;
            
            try{
                Mail::to($canjeado->cliente->correo)->queue(new PremioEmail($information));
            }
            catch(\Exception $e){
                "";
            } */

            DB::commit();
            if($request->input('estado') != 'entregado')
                return Redirect::back()->with('edit', 'Estatus y Datos de Entrega Actualizado Exitosamente!');
            else
                return redirect()->route('canjeado.index')->with('edit', 'Estatus y Datos de Entrega Actualizado Exitosamente!');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'Fallo al Editar los Datos: '.$e->getMessage());
        }
    }

    //Delete Premio
    public function destroy($id_premio){
        
        DB::beginTransaction();
        try{
            $premio = Premio::find($id_premio);

            if(!$premio)
                return redirect()->route('premio.index')->with('edit', 'El Premio no fue encontrado');

            /* foreach ($premio->gestionCambio as $cambios){
                $cambios->FK_id_premios = null;
                $cambios->update();
            } */

            PremioCaracteristicas::where('FK_id_premio', $premio->id_premio)->delete();
            PremioCanjeado::where('FK_id_premio', $premio->id_premio)->update(['FK_id_premio' => null]);

            if($premio->imagen){
                Storage::delete(['public/'.$premio->imagen]);
            }

            $premio->delete();

            /* $data = $premio->toArray();
            $data['accion'] = 'Delete';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
            
            PremioCambio::create($data); */

            $data = [
                "nombre" => "Delete de Premio",
                "tipo" => "Delete",
                "descripcion" => "Eliminación de Premio: ".$premio->nombre,
            ];

            Notificacion::instance()->store($data, "premio");

            DB::commit();
            return redirect()->route('premio.listado')->with('delete', 'Premio Eliminado Exitosamente!');
        }catch(\Exception $e){
            DB::rollback();
            return Redirect::back()->with('error', 'Fallor al eliminar premio: '.$e->getMessage());
        }
    }

    //Delete Categoria
    public function destroyCategoria($id_categoria){
        
        DB::beginTransaction();
        try{
            $categoria = PremioCategoria::find($id_categoria);

            if(!$categoria)
                return response()->json([
                    'message' => "La Categoria no se encuentra registrada, porfavor intenta más tarde"
                ]);

            //Arreglo de que los premios bajo dicha categoría queden sin categoría y deban ser arreglados
            foreach($categoria->premios as $premio){
                $premio->FK_categoria = null;
                $premio->save();
            }

            if($categoria->imagen){
                Storage::delete(['public/'.$categoria->imagen]);
            }

            $categoria->delete();

            DB::commit();
            return response()->json([
                'delete' => true,
                'message' => "La Categoría se ha eliminado exitosamente"
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => "Error al eliminar Categoría, intentelo más tarde"
            ]);
        }
    }
}
