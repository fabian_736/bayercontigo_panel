<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\LoginController as DefaultLoginController;

class LoginController extends DefaultLoginController
{
    protected $redirectTo = '/portal';
        
    public function __construct(){
        $this->middleware('guest:admin')->except('logout');
    }
    
    public function form_login(){
        return view('modules.auth.form_login');
    }
    
    public function username(){
        return 'email';
    }

    protected function guard(){
        return Auth::guard('admin');
    }
}
