<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

//Modelos
use App\Models\Admin;
use App\Models\Rol;
use App\Models\AdminNotificacion;

//Helper
use App\Helper\Notificacion;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
        'min' => 'El dato debe ser mayor a 8 carácteres',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'email' => 'El dato debe ir en formato de correo (ejemplo@correo.com)',
        'unique' => 'El dato enviado ya se encuentra registrado',
        'same' => 'La contraseña no coincide con la de confirmación',
        'regex' => 'La contraseña debe contener mayuscula, números y caracteres especiales',
        'avatar.max' => 'La imagen no puede ser mayor a 2Mb',
        'boolean' => 'El dato debe viajar como booleano (1 o 0)',
    ];

    //Validaciones - Datos que viajaran como tal
    public $validaciones = [
        'nombre' => 'required|string|max:50', 
        'apellido' => 'required|string|max:50',
        'email' => 'required|string|email|max:50|unique:bc_useradmin,email',
        'estado' => 'required|boolean',
        'password' => [
            'required',
            'string',
            'min:8',
            'max:50',
            'regex:/[a-z]/',      // must contain at least one lowercase letter
            'regex:/[A-Z]/',      // must contain at least one uppercase letter
            'regex:/[0-9]/',      // must contain at least one digit
            'regex:/[@$!%*#?&+-]/',
        ],
        'FK_id_adminrol' => 'required',
    ];

    //Compresor de Imagen
    public function imagecompress($image){
        $img = Image::make(storage_path('app/public/'.$image))->resize(1200, 1200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save();
        $img->destroy();
    }

    //Chequiamos password si se asemeja al correo
    public function checkPassword($check){
        //Chequiamos si es el correo y el password son iguales
        $check["email"] = explode(' ',strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check["email"])));
        $check["password"] = strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check["password"]));
        
        if(stripos($check["password"],$check['email'][0]) !== false)
            return true;
        else
            return false;
    }

    //Listado de Admins
    public function listadmin(){
        $admins = Admin::where("FK_id_adminrol","!=",1)->get();
        $rol = Rol::all();

        return view('admin.listadmin',compact('admins','rol'));
    }

    //Enviar Vista para el registro de un admin nuevo
    public function create(){
        $rol = Rol::all();
        return view('admin.create', compact('rol'));
    }

    //Guardado de un Admin
    public function store(Request $request){
    
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), $this->validaciones, $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $data = $request->all();

            if($this->checkPassword($data))
                return Redirect::back()->with('error', 'El password se asemeja al correo');

            $data["password"] = Hash::make($request->input('password'));

            //GRABAMOS LOS PERMISOS
                if($data["FK_id_adminrol"] == 2){
                    $data["p_store"] = $request->has('p_store') ? 1 : 0;
                    $data["p_update"] = $request->has('p_update') ? 1 : 0;
                    $data["p_destroy"] = $request->has('p_destroy') ? 1 : 0;
                }

                if($data["FK_id_adminrol"] == 3){
                    $data["p_store"] = 0;
                    $data["p_update"] = 0;
                    $data["p_destroy"] = 0;
                }

                if($data["FK_id_adminrol"] == 2 || $data["FK_id_adminrol"] == 3){
                    $data["p_productos"] = $request->has('p_productos') ? 1 : 0;
                    $data["p_premios"] = $request->has('p_premios') ? 1 : 0;
                    $data["p_facturas"] = $request->has('p_facturas') ? 1 : 0;
                    $data["p_noticias"] = $request->has('p_noticias') ? 1 : 0;
                    $data["p_capacitaciones"] = $request->has('p_capacitaciones') ? 1 : 0;
                    $data["p_trivia"] = $request->has('p_trivia') ? 1 : 0;
                }
            //FIN GRABAMOS LOS PERMISOS

            $admin = Admin::create($data);

            $data = [
                "nombre" => "Creación de Usuario",
                "tipo" => "Insert",
                "descripcion" => "Creación nuevo Usuario Admin: ".$admin->nombre." ".$admin->apellido,
            ];

            Notificacion::instance()->store($data, "usuarios");

            DB::commit();
            return Redirect::back()->with('create', 'Usuario Registrado Exitosamente');
        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            return Redirect::back()->with('error', 'El Usuario no pudo ser creado');
        }
    }

    //Vista para confirmar si de verdad quiere eliminar el admin
    public function confirm($id_useradmin){
        $admin = Admin::findOrFail($id_useradmin);
        return view('admin.confirm', compact('admin'));
    }

    //Eliminar un Admin
    public function destroy($id_useradmin){
        
        DB::beginTransaction();
        try{
            $admin = Admin::find($id_useradmin);

            if(!$admin)
                return redirect()->route('admin.listadmin')->with('edit', 'El Usuario no se encuentra registrado');

            if($admin->avatar){
                Storage::delete(['public/'.$admin->avatar]);
            }

            AdminNotificacion::where('FK_id_useradmin', $id_useradmin)->delete();

            $admin->delete();

            $data = [
                "nombre" => "Delete de Usuario",
                "tipo" => "Delete",
                "descripcion" => "Eliminado Usuario Admin: ".$admin->nombre." ".$admin->apellido,
            ];

            Notificacion::instance()->store($data, "usuarios");

            DB::commit();
            return redirect()->route('admin.listadmin')->with('create', 'Usuario Eliminado');
        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'El Usuario no pudo ser Eliminado');
        }
    }

    //Enviar la Vista para editar un Admin
    public function edit($id_useradmin){
        $admin = Admin::findOrFail($id_useradmin);
        $rol = Rol::all();

        return view('admin.edit', compact('admin', 'rol'));
    }

    //Update de los datos de un admin
    public function update(Request $request, $id_useradmin){
        
        $validaciones = $this->validaciones;
        unset($validaciones['password']);
        $validaciones['email'] = 'required|string|email|max:50|unique:bc_useradmin,email,'.$id_useradmin.',id_useradmin';

        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), $validaciones, $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $admin = Admin::findOrFail($id_useradmin);
            $data = $request->all();

            //GRABAMOS LOS PERMISOS
                if($data["FK_id_adminrol"] == 2){
                    $data["p_store"] = $request->has('p_store') ? 1 : 0;
                    $data["p_update"] = $request->has('p_update') ? 1 : 0;
                    $data["p_destroy"] = $request->has('p_destroy') ? 1 : 0;
                }

                if($data["FK_id_adminrol"] == 3){
                    $data["p_store"] = 0;
                    $data["p_update"] = 0;
                    $data["p_destroy"] = 0;
                }

                if($data["FK_id_adminrol"] == 2 || $data["FK_id_adminrol"] == 3){
                    $data["p_productos"] = $request->has('p_productos') ? 1 : 0;
                    $data["p_premios"] = $request->has('p_premios') ? 1 : 0;
                    $data["p_facturas"] = $request->has('p_facturas') ? 1 : 0;
                    $data["p_noticias"] = $request->has('p_noticias') ? 1 : 0;
                    $data["p_capacitaciones"] = $request->has('p_capacitaciones') ? 1 : 0;
                    $data["p_trivia"] = $request->has('p_trivia') ? 1 : 0;
                }
            //FIN GRABAMOS LOS PERMISOS

            $admin->update($data);

            $data = [
                "nombre" => "Update de Usuario",
                "tipo" => "Update",
                "descripcion" => "Edición de Usuario Admin: ".$admin->nombre." ".$admin->apellido,
            ];

            Notificacion::instance()->store($data, "usuarios");

            DB::commit();
            return Redirect::back()->with('edit', 'El Usuario ha sido editado exitosamente');
        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            return Redirect::back()->with('error', 'El Usuario no pudo ser editado');
        }
    }
}
