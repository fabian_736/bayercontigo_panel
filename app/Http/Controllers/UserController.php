<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

//Modelos
use App\Models\User;
use App\Models\UserPassword;
use App\Models\Paciente;
use App\Models\Profesional;
use App\Models\PacientePatologia;
use App\Models\ProfesionalInsignia;
use App\Models\Patologia;
use App\Models\UserNotificacion;

//Import
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\PremioImport;
use App\Imports\TestImport;

//Helper
use App\Helper\Notificacion;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg',
        'min' => 'El dato debe ser mayor a 8 carácteres',
        'max' => 'El dato no debe ser mayor a 255 carácteres',
        'email' => 'El dato debe ir en formato de correo (ejemplo@correo.com)',
        'unique' => 'El dato enviado ya se encuentra registrado',
        'same' => 'La contraseña no coincide con la de confirmación',
        'regex' => 'La contraseña debe contener mayuscula, números y caracteres especiales',
        'avatar.max' => 'La imagen no puede ser mayor a 2Mb',
        'boolean' => 'El dato debe viajar como booleano (1 o 0)',
        'password.min' => 'La contraseña debe ser mayor a 8 caracteres',
    ];

    //Validaciones - Datos que viajaran como tal
    public $validaciones = [
        /*DATOS LOGIN*/
        'email' => 'required|string|email|max:255|unique:users,email',
        'password' => [
            'required',
            'string',
            'min:8',
            'max:50',
            'regex:/[a-z]/',      // must contain at least one lowercase letter
            'regex:/[A-Z]/',      // must contain at least one uppercase letter
            'regex:/[0-9]/',      // must contain at least one digit
            'regex:/[@$!%*#?&+-]/',
        ],
        'estado' => 'required|boolean',
        'pais' => 'required|string',
        'tipo' => 'required',
        /*DATOS DEL USUARIO*/
        'nombre' => 'required|string|max:255', 
        'apellido' => 'required|string|max:255',
        //'identificacion' => 'required|numeric|unique:users,identificacion',
        'telefono' => 'required|string|max:255',
        'direccion' => 'required|string|max:255',
        'especialidad' => 'required|string|max:255',
        'descripcion' => 'required|string|max:65535',
    ];

    //Compresor de Imagen
    public function imagecompress($image){
        $img = Image::make(storage_path('app/public/'.$image))->resize(1200, 1200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save();
        $img->destroy();
    }

    //Chequiamos password si se asemeja al correo
    public function checkPassword($check){
        //Chequiamos si es el correo y el password son iguales
        $check["email"] = explode(' ',strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check["email"])));
        $check["password"] = strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check["password"]));
        
        if(stripos($check["password"],$check['email'][0]) !== false)
            return true;
        else
            return false;
    }

    //Listado de Usuarios
    public function listuser(){
        $usuarios = User::all();
        $patologias = Patologia::all();

        return view('usuarios.listuser',compact('usuarios','patologias'));
    }

    //Ver los Datos del Usuario (paciente o profesional)
    public function getDatos($tipo, $id){
        
        if($tipo == "paciente")
            $datos = Paciente::findOrFail($id);
        else
            $datos = Profesional::findOrFail($id);
        
        return view('usuarios.datos', compact('datos','tipo'));
    }

    //Tomar detallado del usuario lo que ha hecho
    public function getDetail($id_cliente){
        $user = User::findOrFail($id_cliente);

        return view('usuarios.detail', compact('user'));
    }

    //Recoger imagen del usuario
    public function getImage($filename = null){

        if($filename){
            $exist = Storage::disk('public')->exists('uploads/'.$filename);
            if(!$exist) //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('nodisponible.png');
            else
                $file = Storage::disk('public')->get('uploads/'.$filename);
        }
        else
            $file = Storage::disk('public')->get('nodisponible.png');

        return new Response($file, 200);
    }

    //Enviar Vista para el registro de un usuario nuevo
    public function create(){
        $patologias = Patologia::all();
        return view('usuarios.create', compact('patologias'));
    }

    //Enviar la Vista para editar un Usuario
    public function edit($id){
        $user = User::findOrFail($id);
        $patologias = Patologia::all();

        if($user->tipo == "paciente")
            $datos = $user->paciente;
        else
            $datos = $user->profesional;

        return view('usuarios.edit', compact('user','datos', 'patologias'));
    }
    
    //Vista para confirmar si de verdad quiere eliminar el Usuario
    public function confirm($id){
        $user = User::findOrFail($id);

        if($user->tipo == "paciente")
            $datos = $user->paciente;
        else
            $datos = $user->profesional;

        return view('usuarios.confirm', compact('user','datos'));
    }

    //Guardado de un Usuario
    public function store(Request $request){
        
        $validaciones = $this->validaciones;

        if($request->tipo == "paciente")
            $validaciones['identificacion'] = 'required|numeric|unique:bc_paciente,identificacion';
        else
            $validaciones['identificacion'] = 'required|numeric|unique:bc_profesional,identificacion';

        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), $validaciones, $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $data = $request->all();

            if($this->checkPassword($data))
                return Redirect::back()->with('error', 'El password se asemeja al correo');

            $data["password"] = Hash::make($request->input('password'));
            
            $user = User::create($data);

            //Grabamos el Paciente o Profesional
                $data["FK_id_user"] = $user->id;
                $request->tipo == "paciente" ? 
                    $udatos = Paciente::create($data) : 
                    $udatos = Profesional::create($data);

            //Grabamos el historial de passwords
                $history = new UserPassword();
                $history->password   = Hash::make($request->input('password'));
                $history->registro   = date('Y-m-d');
                $history->FK_id_user = $user->id;
                $history->save();

            //GRABAMOS LAS PATOLOGIAS SI TIENE
                if($request->tipo == "paciente" && $request->has('patologias')){
                    foreach($request->patologias as $patologia){
                        $ppa = new PacientePatologia();
                        $ppa->FK_id_paciente = $udatos->id_paciente;
                        $ppa->FK_id_patologia = $patologia;
                        $ppa->save();
                    }
                }
            //FIN GRABAMOS LAS PATOLOGIAS

            $data = [
                "nombre" => "Creación de Usuario",
                "tipo" => "Insert",
                "descripcion" => "Creación nuevo Usuario ".$request->tipo.": ".$request->nombre." ".$request->apellido,
            ];

            Notificacion::instance()->store($data, "usuarios");

            DB::commit();
            return Redirect::back()->with('create', 'Usuario Registrado Exitosamente');
        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    //Eliminar un Usuario
    public function destroy($id){
        
        DB::beginTransaction();
        try{
            $user = User::find($id);

            if(!$user)
                return redirect()->route('user.listuser')->with('edit', 'El Usuario no se encuentra registrado');

            if($user->avatar){
                Storage::delete(['public/'.$user->avatar]);
            }

            UserNotificacion::where('FK_id_user', $id)->delete();

            //Eliminamos su relación de patologias o insignias
            if($user->tipo == "paciente"){
                $datos = $user->paciente;
                PacientePatologia::where('FK_id_paciente', $datos->id_paciente)->delete();
            }
            else{
                $datos = $user->profesional;
                ProfesionalInsignia::where('FK_id_profesional', $datos->id_profesional)->delete();
            }

            $datos->delete();
            $user->delete();

            $data = [
                "nombre" => "Delete de Usuario ".$datos->nombre." ".$datos->apellido,
                "tipo" => "Delete",
                "descripcion" => "Usuario Eliminado por Admin: ".auth()->user()->nombre." ".auth()->user()->apellido,
            ];

            Notificacion::instance()->store($data, "usuarios");

            DB::commit();
            return redirect()->route('user.listuser')->with('create', 'Usuario Eliminado');
        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'El Usuario no pudo ser Eliminado');
        }
    }

    //Update de los datos de un usuario
    public function update(Request $request, $id){
        
        $user = User::findOrFail($id);

        $validaciones = $this->validaciones;
        $validaciones['email'] = 'required|string|email|max:255|unique:users,email,'.$id.',id';
        $validaciones['password'] = [
            'string',
            'min:8',
            'max:50',
            'nullable'
        ];

        if($request->tipo == "paciente")
            $validaciones['identificacion'] = 'required|numeric|unique:bc_paciente,identificacion,'.$user->paciente->id_paciente.',id_paciente';
        else
            $validaciones['identificacion'] = 'required|numeric|unique:bc_profesional,identificacion,'.$user->profesional->id_profesional.',id_profesional';

        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), $validaciones, $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $data = $request->all();

            if($request->input('password')){
                //Chequiamos si no estamos repitiendo el mismo password
                if (Hash::check($request->input('password'), $user->password)) {
                    return Redirect::back()->with('error', 'El nuevo password es igual al anterior');
                }

                //Chequiamos que no se asemeje al correo
                if($this->checkPassword($data))
                    return Redirect::back()->with('error', 'El password se asemeja al correo');

                //Revisamos si el password no es el mismo de los ultimos 3 utilizados
                foreach ($user->historialpass->take(3) as $pass) {
                    if(Hash::check($request->input('password'), $pass->password))
                        return Redirect::back()->with('error', 'La contraseña a editar ya fue utilizada en las ultimas 3 oportunidades');
                }

                //Grabamos el historial de passwords
                $history = new UserPassword();
                $history->password   = Hash::make($request->input('password'));
                $history->registro   = date('Y-m-d');
                $history->FK_id_user = $user->id;
                $history->save();

                $data["password"] = Hash::make($request->input('password'));
            }
            else{
                unset($data['password']);
            }

            if($request->hasFile('avatar')){
                Storage::delete(['public/'.$user->avatar]);
                $data["avatar"] = $request->file('avatar')->store('uploads', 'public');
                $this->imagecompress($data["avatar"]);
            }
            
            //Hacemos Update de los datos indivudales del Paciente o Profesional
                $data["FK_id_user"] = $user->id;
                if($request->tipo == "paciente"){  
                    $paciente = $user->paciente;
                    $paciente ?
                        $paciente->update($data) :
                        $paciente = Paciente::create($data);
                }
                else{ 
                    $profesional = $user->profesional;
                    $profesional ?
                        $profesional->update($data) :
                        $profesional = Profesional::create($data);
                }

            //GRABAMOS LAS PATOLOGIAS SI TIENE
                if($request->tipo == "paciente" && $request->has('patologias')){
                    PacientePatologia::where('FK_id_paciente', $paciente->id_paciente)->delete();
                    foreach($request->patologias as $patologia){
                        $ppa = new PacientePatologia();
                        $ppa->FK_id_paciente = $paciente->id_paciente;
                        $ppa->FK_id_patologia = $patologia;
                        $ppa->save();
                    }
                }
            //FIN GRABAMOS LAS PATOLOGIAS

            $user->update($data);

            $data = [
                "nombre" => "Update de Usuario",
                "tipo" => "Update",
                "descripcion" => "Edición de Usuario ".$request->tipo.": ".$request->nombre." ".$request->apellido,
            ];

            Notificacion::instance()->store($data, "usuarios");

            DB::commit();
            return Redirect::back()->with('edit', 'El Usuario ha sido editado exitosamente');
        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    ///////////////////////////////////////SECCION DE IMPORTACIÓN MASIVA///////////////////////////

    //Apartado visual Inicial para cargar premios con importación
    public function importIndex(){
        return view('usuarios.import_index');
    }

    //Metodo que lee el form enviado y manda la tabla de imports #TODO puede ser borrable
    public function importForm(Request $request){

        $import = array();
        $header = array();
        
        foreach( $request->nombre as $key => $nombre ) {
            $usuario = [];
            $usuario["nombre"] = $nombre;
            $usuario["apellido"] = $request->apellido[$key];
            $usuario["segmento"] = $request->segmento[$key];
            $usuario["telefono"] = $request->telefono[$key];
            $usuario["correo"] = $request->correo[$key];
            $usuario["especialidad"] = $request->especialidad[$key];
            array_push($import, $usuario);
        }

        return view ('usuarios.import_tabla', compact('import', 'header'));
    }

    //Metodo que lee el archivo excel y manda la tabla de imports
    public function importCsv(Request $request){
        
        $validate = Validator::make($request->all(), [
            'file' => 'required|file|mimes:xls,xlsx,csv',
        ], [
            'required' => 'El dato es requerido',
            'file' => 'El dato debe llegar como un archivo',
            'mime' => 'El archivo debe llegar en formato xls, xlsx, csv',
        ]);

        if($validate->fails()){
            return redirect()->route('user.import')->withErrors($validate);     
        }

        $validaciones = $this->validaciones;
        $validaciones["telefono"] = 'required|max:15';
        unset($validaciones["estado"]);

        try{
            $collection = Excel::toCollection(new PremioImport(auth()->user()->id_useradmin,$validaciones), $request->file);
            $import = array();
            $header = array();
            
            !array_key_exists("email",$collection[0][0]->toArray()) ? array_push($header, "email") : "";
            !array_key_exists("pais",$collection[0][0]->toArray()) ? array_push($header, "pais") : "";
            !array_key_exists("tipo",$collection[0][0]->toArray()) ? array_push($header, "tipo") : "";
            !array_key_exists("nombre",$collection[0][0]->toArray()) ? array_push($header, "nombre") : "";
            !array_key_exists("apellido",$collection[0][0]->toArray()) ? array_push($header, "apellido") : "";
            !array_key_exists("direccion",$collection[0][0]->toArray()) ? array_push($header, "direccion") : "";
            !array_key_exists("identificacion",$collection[0][0]->toArray()) ? array_push($header, "identificacion") : "";
            !array_key_exists("telefono",$collection[0][0]->toArray()) ? array_push($header, "telefono") : "";
            !array_key_exists("especialidad",$collection[0][0]->toArray()) ? array_push($header, "especialidad") : "";

            foreach ($collection[0] as $usuario){
                array_push($import, $usuario);
            }
            
            return view ('usuarios.import_tabla', compact('import', 'header'));
        }catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $errores = $e->failures();
            return view ('usuarios.import_index', compact('errores'));
       }
    }

    //Tabla de importados | creo que esta ruta ya no la utilizo pero bueno #TODO pendiente
    public function tabla_import(){
        return view('usuarios.import_tabla');
    }

    //Store Import de Premios
    public function importStore(Request $request){
        
        $validate = Validator::make($request->all(), [
            'email.*' => 'required|string|distinct|email|max:255|unique:users,email',
            'pais.*' => 'required|string|max:255',
            'tipo.*' => 'required|string|max:255',
            'nombre.*' => 'required|string|max:255',
            'apellido.*' => 'required|string|max:255',
            'direccion.*' => 'required|string|max:65535',
            'identificacion.*' => 'required|string|distinct|max:255',
            'telefono.*' => 'required|string|max:255',
            'especialidad.*' => 'required|string|max:255',
        ], $this->validaciones);
        
        $header = array();
        $import = array();

        foreach( $request->nombre as $key => $nombre ) {
            $usuario = [];
            $usuario["email"] = $request->email[$key];
            $usuario["pais"] = $request->pais[$key];
            $usuario["tipo"] = $request->tipo[$key];
            $usuario["nombre"] = $nombre;
            $usuario["apellido"] = $request->apellido[$key];
            $usuario["direccion"] = $request->direccion[$key];
            $usuario["identificacion"] = $request->identificacion[$key];
            $usuario["telefono"] = $request->telefono[$key];
            $usuario["especialidad"] = $request->especialidad[$key];
            array_push($import, $usuario);
        }

        if($validate->fails()){
            $errors = $validate->errors();
            return view ('usuarios.import_tabla', compact('import', 'header', 'errors'));
        }
        
        //Proceso completo ahora si de Importar
        DB::beginTransaction();
        try{
            foreach ($import as $usuario){
                $caracteres_permitidos = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $password = substr(str_shuffle($caracteres_permitidos), 0, 8); 

                //Creación de codigo para verificación de email

                $user = User::create([
                    'pais' => $usuario['pais'],
                    'email' => $usuario['email'],
                    'password' => Hash::make($password),
                    'tipo' => $usuario['tipo'],
                    'fecha_cambio' => date('Y-m-d'),
                ]);

                if(strtolower($usuario['tipo']) == "paciente"){
                    Paciente::create([
                        'nombre' => $usuario['nombre'],
                        'apellido' => $usuario['apellido'],
                        'direccion' => $usuario['direccion'],
                        'descripcion' => null,
                        'telefono' => $usuario['telefono'],
                        'identificacion' => $usuario['identificacion'],
                        'especialidad' => $usuario['especialidad'],
                        'FK_id_user' => $user->id
                    ]);
                }
                else{
                    Profesional::create([
                        'nombre' => $usuario['nombre'],
                        'apellido' => $usuario['apellido'],
                        'direccion' => $usuario['direccion'],
                        'descripcion' => null,
                        'telefono' => $usuario['telefono'],
                        'identificacion' => $usuario['identificacion'],
                        'especialidad' => $usuario['especialidad'],
                        'FK_id_user' => $user->id
                    ]);
                }

                UserPassword::create([
                    'password' => Hash::make($password),
                    'registro' => date('Y-m-d'),
                    'FK_id_user' => $user->id
                ]);

                //ENVIO DEL CORREO
                /* $information = new \stdClass();
                $information->asunto = 'Bienvenido a DentApp';
                $information->nombre = $usuario["nombre"]." ".$usuario["apellido"];
                $information->correo = $usuario['correo'];
                $information->contraseña = $password;

                try{
                    Mail::to($usuario['correo'])->queue(new Welcome($information));
                }
                catch(\Exception $e){
                    "";
                } */
            }
            DB::commit();
            return redirect()->route('user.import')->with('create', 'Los Usuarios fueron creados satisfactoriamente');
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()->route('user.import')->with('delete', 'Los Usuarios no pudieron ser creados: '.$e->getMessage());
        }
    }
}
