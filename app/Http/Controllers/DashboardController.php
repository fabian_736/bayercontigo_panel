<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

//Modelos
use App\Models\Admin;

//Helper
use App\Helper\Notificacion;
use Intervention\Image\Facades\Image;

class DashboardController extends Controller
{   

    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
        'min' => 'El dato debe ser mayor a 8 carácteres',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'email' => 'El dato debe ir en formato de correo (ejemplo@correo.com)',
        'unique' => 'El dato enviado ya se encuentra registrado',
        'same' => 'La contraseña no coincide con la de confirmación',
        'regex' => 'La contraseña debe contener mayuscula, números y caracteres especiales',
        'avatar.max' => 'La imagen no puede ser mayor a 2Mb',
    ];

    //Chequiamos password si se asemeja al correo
    public function checkPassword($check){
        //Chequiamos si es el correo y el password son iguales
        $check["email"] = explode(' ',strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check["email"])));
        $check["password"] = strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check["password"]));
        
        if(stripos($check["password"],$check['email'][0]) !== false)
            return true;
        else
            return false;
    }

    //Compresor de Imagen
    public function imagecompress($image){
        $img = Image::make(storage_path('app/public/'.$image))->resize(1200, 1200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save();
        $img->destroy();
    }
    
    public function index()
    {  
        return view('modules.dashboard.index');
    }

    //Recoger imagen del usuario
    public function getImage($filename = null){

        if($filename){
            $exist = Storage::disk('public')->exists('uploads/'.$filename);
            if(!$exist) //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('avatar.png');
            else
                $file = Storage::disk('public')->get('uploads/'.$filename);
        }
        else
            $file = Storage::disk('public')->get('avatar.png');

        return new Response($file, 200);
    }

    public function perfil(){
        $admin = auth()->user();
        return view('modules.dashboard.perfil', compact('admin')) ;
    }

    public function update(Request $request){
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'avatar' => 'file|mimes:jpg,jpeg,png|max:2100',
                'password' => [
                    'string',
                    'min:8',
                    'max:50',
                    'regex:/[a-z]/',      // must contain at least one lowercase letter
                    'regex:/[A-Z]/',      // must contain at least one uppercase letter
                    'regex:/[0-9]/',      // must contain at least one digit
                    'regex:/[@$!%*#?&+-]/',
                    'nullable'
                ],
                'confirmacion' => 'string|same:password|nullable',
            ], $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate);
            }

            $admin = Admin::findOrFail(auth()->user()->id_useradmin);
            $data = $request->all();
            $data["email"] = $admin->email;

            //Chequiamos si no estamos repitiendo el mismo password
            if (Hash::check($request->input('password'), $admin->password)) {
                return Redirect::back()->with('error', 'El nuevo password es igual al anterior');
            }

            //Chequiamos que no se asemeje al correo
            if($request->input('password')){
                if($this->checkPassword($data))
                    return Redirect::back()->with('error', 'El password se asemeja al correo');
            }
            
            if($request->hasFile('avatar')){
                Storage::delete(['public/'.$admin->avatar]);
                $data["avatar"] = $request->file('avatar')->store('uploads', 'public');
                $this->imagecompress($data["avatar"]);
            }

            if($request->input('password'))
                $data["password"] = Hash::make($request->input('password'));
            else
                $data["password"] = $admin->password;
            
            $admin->update($data);

            $data = [
                "nombre" => "Perfil de Admin",
                "tipo" => "Update",
                "descripcion" => "Edición de Perfil Admin: ".$request->nombre." ".$request->apellido,
            ];

            Notificacion::instance()->store($data, "usuarios");

            DB::commit();
            return Redirect::back()->with('edit', 'El Perfil ha sido editado exitosamente');
        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            return Redirect::back()->with('error', 'El Perfil no pudo ser editado');
        }
    }
}
