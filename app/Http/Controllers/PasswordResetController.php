<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

//MODELOS
use App\Models\Admin;
use App\Models\User;
use App\Models\PasswordReset;
use App\Models\UserPassword;

//USABLES
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;

class PasswordResetController extends Controller
{
    public $message_error = [
        'email' => 'El correo debe ser una dirección de correo valida.',
        'required' => 'El dato es requerido',
        'same' => 'La confirmación y la contraseña deben coincidir',
        'min' => 'El dato debe llevar minimo 8 caracteres',
        'max' => 'El dato debe llevar maximo 8 caracteres',
        'regex' => 'La contraseña debe contener mayuscula, números y caracteres especiales',
    ];

    public function forgot(){
        return view('modules.auth.forgot');
    }

    public function create(Request $request, $tipo = null){

        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'email' => 'required|string|email',
            ], $this->message_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate);  
            }

            if($tipo == "admin")
                $user = Admin::where('email', $request->email)->first();
            elseif($tipo == "cliente")
                $user = User::where('email', $request->email)->first();
            
            if (!$user)
                return Redirect::back()->with('error', 'El correo no se encuentra registrado dentro de bayer contigo');

            $passwordReset = PasswordReset::updateOrCreate(
                ['email' => $user->email],
                [
                    'tipo' => $tipo,
                    'email' => $user->email,
                    'token' => Str::random(60)
                ]
            );

            if ($user && $passwordReset)
                $user->notify(new PasswordResetRequest($passwordReset->token, $passwordReset->tipo));

            DB::commit();
            return Redirect::back()->with('edit', 'Se envio un correo con el link para resetear su contraseña. Tienes 30 min para cambiar la misma');
        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            return Redirect::back()->with('error', $e->getMessage());
        }
    }
    
    public function find($token, $tipo = null){

        $passwordReset = PasswordReset::where('token', $token)->first();

        if (!$passwordReset){
            if($tipo == 'admin')
                return redirect()->route('login')->with('message', 'El token de recuperación de clave es invalido'); 
            else{
                $mensaje = 'El token de recuperación de clave no existe';
                return view('modules.auth.reset',compact('passwordReset','mensaje'));
            }
        }

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(30)->isPast()) {
            $passwordReset->delete();
            if($tipo == 'admin')
                return redirect()->route('login')->with('message', 'El token de recuperación de clave ya se encuentra expirado'); 
            else{
                $mensaje = 'El token de recuperación de clave ya se encuentra expirado';
                return view('modules.auth.reset',compact('passwordReset','mensaje'));
            }
        }

        return view('modules.auth.reset',compact('passwordReset'));
    }
    
    //Chequiamos password si se asemeja al correo
    public function checkPassword($check){
        //Chequiamos si es el correo y el password son iguales
        $check["correo"] = explode(' ',strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check["correo"])));
        $check["password"] = strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check["password"]));
        
        if(stripos($check["password"],$check['correo'][0]) !== false)
            return true;
        else
            return false;
    }

    public function reset(Request $request){

        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'email' => 'required|string|email',
                'password' => [
                    'string',
                    'min:8',
                    'max:50',
                    'regex:/[a-z]/',      // must contain at least one lowercase letter
                    'regex:/[A-Z]/',      // must contain at least one uppercase letter
                    'regex:/[0-9]/',      // must contain at least one digit
                    'regex:/[@$!%*#?&+-]/',
                    'required'
                ],
                'password_confirmation' => 'required|string|same:password',
                'token' => 'required',
                'tipo' => 'required',
            ], $this->message_error);

            $tipo = $request->tipo;

            if($validate->fails()){
                return Redirect::back()->withErrors($validate);  
            }

            $passwordReset = PasswordReset::where('token', $request->token)->first();

            if (!$passwordReset){
                if($tipo == 'admin')
                    return redirect()->route('login')->with('message', 'El token de recuperación de clave es invalido'); 
                else{
                    $mensaje = 'El token de recuperación de clave no existe';
                    return view('modules.auth.reset',compact('passwordReset','mensaje'));
                }
            }

            if($tipo == "admin")
                $user = Admin::where('email', $request->email)->first();
            else
                $user = User::where('email', $request->email)->first();
            
            if (!$user){
                return Redirect::back()->with('edit', 'No se encontro ningun usuario bajo este correo electrónico.'); 
            }

            //Chequiamos si no estamos repitiendo el mismo password
            if (Hash::check($request->input('password'), $user->password)) {
                return Redirect::back()->with('edit', 'El nuevo password es igual al anterior'); 
            }

            //Chequiamos que no se asemeje al correo
            $data["correo"] = $request->email;
            $data["password"] = $request->password;
            if($this->checkPassword($data)){
                return Redirect::back()->with('edit', 'El password se asemeja al correo, por favor utiliza otro');
            }

            //Revisamos si el password no es el mismo de los ultimos 3 utilizados
            if($tipo != 'admin'){
                foreach ($user->historialpass->take(3) as $pass) {
                    if(Hash::check($request->input('password'), $pass->password))
                        return Redirect::back()->with('edit', 'La contraseña a editar ya fue utilizada en las ultimas 3 contraseñas utilizadas');
                }
            }

            //Grabamos el historial de passwords
            if($tipo != 'admin'){
                $history = new UserPassword();
                $history->password   = Hash::make($request->input('password'));
                $history->registro   = date('Y-m-d');
                $history->FK_id_user = $user->id;
                $history->save();
            }
            
            //Update de los datos de login
            $user->password = Hash::make($request->input('password'));
            $tipo != 'admin' ? $user->cambiar = 0 : "";
            $tipo != 'admin' ? $user->fecha_cambio = date('Y-m-d') : "";
            $user->update();

            $passwordReset->delete();

            DB::commit();
            if($tipo == 'admin')
                return redirect()->route('login')->with('edit', 'Se modifico el password satisfactoriamente.');
            else{
                $mensaje = 'Se modifico el password satisfactoriamente.';
                $passwordReset = null;
                return view('modules.auth.reset',compact('passwordReset','mensaje'));
            }
        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            return Redirect::back()->with('error', $e->getMessage());
        }
    }
}
