<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class QuantumController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'min' => 'El dato debe ser mayor a 0',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'descripcion.max' => 'El dato no debe ser mayor a 65535 carácteres',
        'entrega.max' => 'El dato no debe ser mayor a 255 carácteres',
        'marca.max' => 'El dato no debe ser mayor a 255 carácteres',
        'foto.max' => 'La imagen no puede ser mayor a 2Mb',
        'cedula.digits_between' => 'La cedula debe tener entra 4 y 20 dígitos',
    ];

    ////////////////////////////////////////////////API QUANTUM//////////////////

    //Solicitar Marcas
    public function QuantumBrands(){
        $url = "http://wsrest.activarpromo.com/api/getbrands.json";

        $response = Http::withHeaders([
            'user' => 'pmarketingtest',
            'token' => 'AUE5-A*dkL87@',
            'Content-Type' => 'application/json',
        ])->post($url);
        
        //Evaluamos los resultados
        if($response->successful()){
            $data = $response->object()->response;
            if($data->error){
                $codigo = substr($data->message, 1);
                $codigo = substr($codigo, 0, strpos($codigo, ']'));
                return response()->json([
                    'status' => 'error',
                    'codigo' => $codigo,
                    'message' => $data->message,
                ], 200);
            }
            else{
                return response()->json([
                    'status' => 'success',
                    'message' => "Marcas Comerciales de Quantum",
                    'marcas' => $data->message,
                ], 200);
            }
        }
        else if($response->clientError()){
            return response()->json([
                'status' => 'error',
                'message' => 'No se pudo hacer la conexión con la API de Quantum, intentelo más tarde',
            ], 400);
        }
        else{
            return response()->json([
                'status' => 'error',
                'message' => 'Error en el Servidor de la API de Quantum, intentelo más tarde',
            ], 503);
        }
    }

    //Solicitar Productos
    public function QuantumProducts($id_brand){
        $url = "http://wsrest.activarpromo.com/api/getproducts.json";

        $response = Http::withHeaders([
            'user' => 'pmarketingtest',
            'token' => 'AUE5-A*dkL87@',
            'Content-Type' => 'application/json',
        ])->post($url, [
            'brand_id' => $id_brand
        ]);

        //Evaluamos los resultados
        if($response->successful()){
            $data = $response->object()->response;
            if($data->error){
                $codigo = substr($data->message, 1);
                $codigo = substr($codigo, 0, strpos($codigo, ']'));
                return response()->json([
                    'status' => 'error',
                    'codigo' => $codigo,
                    'message' => $data->message,
                ], 200);
            }
            else{
                return response()->json([
                    'status' => 'success',
                    'message' => "Productos de la Marca Seleccionada",
                    'premios' => $data->message,
                ], 200);
            }
        }
        else if($response->clientError()){
            return response()->json([
                'status' => 'error',
                'message' => 'No se pudo hacer la conexión con la API de Quantum, intentelo más tarde',
            ], 400);
        }
        else{
            return response()->json([
                'status' => 'error',
                'message' => 'Error en el Servidor de la API de Quantum, intentelo más tarde',
            ], 503);
        }
    }

    //Solicitar Departamentos
    public function QuantumBrandDepartment($id_brand){
        $url = "http://wsrest.activarpromo.com/api/getdeps.json";

        $response = Http::withHeaders([
            'user' => 'pmarketingtest',
            'token' => 'AUE5-A*dkL87@',
            'Content-Type' => 'application/json',
        ])->post($url, [
            'brand_id' => $id_brand
        ]);

        //Evaluamos los resultados
        if($response->successful()){
            $data = $response->object()->response;
            if($data->error){
                $codigo = substr($data->message, 1);
                $codigo = substr($codigo, 0, strpos($codigo, ']'));
                return response()->json([
                    'status' => 'error',
                    'codigo' => $codigo,
                    'message' => $data->message,
                ], 200);
            }
            else{
                return response()->json([
                    'status' => 'success',
                    'message' => "Departamentos de la Marca Seleccionada",
                    'departamentos' => $data->message,
                ], 200);
            }
        }
        else if($response->clientError()){
            return response()->json([
                'status' => 'error',
                'message' => 'No se pudo hacer la conexión con la API de Quantum, intentelo más tarde',
            ], 400);
        }
        else{
            return response()->json([
                'status' => 'error',
                'message' => 'Error en el Servidor de la API de Quantum, intentelo más tarde',
            ], 503);
        }
    }

    //Solicitar Ciudades
    public function QuantumBrandCities($id_brand, $id_department){
        $url = "http://wsrest.activarpromo.com/api/getcities.json";

        $response = Http::withHeaders([
            'user' => 'pmarketingtest',
            'token' => 'AUE5-A*dkL87@',
            'Content-Type' => 'application/json',
        ])->post($url, [
            'brand_id' => $id_brand,
            'dep_id' => $id_department
        ]);

        //Evaluamos los resultados
        if($response->successful()){
            $data = $response->object()->response;
            if($data->error){
                $codigo = substr($data->message, 1);
                $codigo = substr($codigo, 0, strpos($codigo, ']'));
                return response()->json([
                    'status' => 'error',
                    'codigo' => $codigo,
                    'message' => $data->message,
                ], 200);
            }
            else{
                return response()->json([
                    'status' => 'success',
                    'message' => "Ciudades de la Marca Seleccionada",
                    'ciudades' => $data->message,
                ], 200);
            }
        }
        else if($response->clientError()){
            return response()->json([
                'status' => 'error',
                'message' => 'No se pudo hacer la conexión con la API de Quantum, intentelo más tarde',
            ], 400);
        }
        else{
            return response()->json([
                'status' => 'error',
                'message' => 'Error en el Servidor de la API de Quantum, intentelo más tarde',
            ], 503);
        }
    }

    //Solicitar Sitios de Localización del Producto
    public function QuantumBrandSites($id_brand, $id_department, $id_city){
        $url = "http://wsrest.activarpromo.com/api/getcities.json";

        $response = Http::withHeaders([
            'user' => 'pmarketingtest',
            'token' => 'AUE5-A*dkL87@',
            'Content-Type' => 'application/json',
        ])->post($url, [
            'brand_id' => $id_brand,
            'dep_id' => $id_department,
            'city_id' => $id_city,
        ]);

        //Evaluamos los resultados
        if($response->successful()){
            $data = $response->object()->response;
            if($data->error){
                $codigo = substr($data->message, 1);
                $codigo = substr($codigo, 0, strpos($codigo, ']'));
                return response()->json([
                    'status' => 'error',
                    'codigo' => $codigo,
                    'message' => $data->message,
                ], 200);
            }
            else{
                return response()->json([
                    'status' => 'success',
                    'message' => "Sitios de la Marca Seleccionada",
                    'sitios' => $data->message,
                ], 200);
            }
        }
        else if($response->clientError()){
            return response()->json([
                'status' => 'error',
                'message' => 'No se pudo hacer la conexión con la API de Quantum, intentelo más tarde',
            ], 400);
        }
        else{
            return response()->json([
                'status' => 'error',
                'message' => 'Error en el Servidor de la API de Quantum, intentelo más tarde',
            ], 503);
        }
    }

    //Redención del Premio
    public function QuantumRedeem(Request $request, $id_premio){
        /*
        //Me deben viajar los siguientes datos por POST
        //'cantidad', //se envia al canjear
        //'receptor', //se envia al canjear
        //'telefono', //se envia al canjear
        //'direccion', //se envia al canjear
        //'adicional', //se envia al canjear

        /*  Datos a enviar por Request ejemplo para Quantum
            "brand_id":"7",
            "product_id":"12",
            "site_id":"4260",
            "user_data":{
                "email":"andresramirez2025@gmail.com",
                "name":"Andres Ramirez",
                "birthdate":"1996-06-28",
                "id":"22652343",
                "cellphone": "04127942183"
            } 
        */

        //URL de Quantum
        $url = "http://wsrest.activarpromo.com/api/redeem.json";

        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'cantidad' => 'required|numeric|min:0',
                'receptor' => 'required|string|max:50', 
                'telefono' => 'required|string|max:255',
                'cedula' => 'numeric|digits_between:4,20',
                'direccion' => 'string|max:255',
                'adicional' => 'string|max:255',

                //VALIDACIONES PARA QUANTUM
                'brand_id' => 'required',
                'product_id' => 'required',
                'birthdate' => 'required',
                'email' => 'required|email',
            ], $this->mensajes_error);

            if($validate->fails()){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error formato de datos recibidos',
                    'errores' => $validate->errors()
                ], 203);
            }

            $response = Http::withHeaders([
                'user' => 'pmarketingtest',
                'token' => 'AUE5-A*dkL87@',
                'Content-Type' => 'application/json',
            ])->post($url, [
                'brand_id' => $request->brand_id,
                'product_id' => $request->product_id,
                'site_id' => $request->site_id,
                'user_data' => [
                    'email' => $request->email,
                    'name' => $request->receptor,
                    'birthdate' => $request->birthdate,
                    'id' => $request->cedula,
                    'cellphone' => $request->telefono
                ],
            ]);
            
            //Evaluamos los resultados
            if($response->successful()){
                $data = $response->object()->response;
                if($data->error){
                    $codigo = substr($data->message, 1);
                    $codigo = substr($codigo, 0, strpos($codigo, ']'));
                    return response()->json([
                        'status' => 'error',
                        'codigo' => $codigo,
                        'message' => $data->message,
                    ], 200);
                }
                else{
                    $referencia = $data->trxid;
                    $factura = $data->url;
                }
            }
            else if($response->clientError()){
                return response()->json([
                    'status' => 'error',
                    'message' => 'No se pudo hacer la conexión con la API de Quantum, intentelo más tarde',
                ], 400);
            }
            else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error en el Servidor de la API de Quantum, intentelo más tarde',
                ], 503);
            }

            DB::commit();
            return response()->json([
                'status' => 'success',
                'message' => 'Premio Canjeado Exitosamente',
            ], 200);

        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al almacenar redención de premio',
                'errores' => $e->getMessage()
            ], 500);
        }
    }

    //Ver Disponibilidad del Producto
    public function QuantumPreCheck(Request $request){
        
        /*  Datos a enviar por Request ejemplo site_id si no tiene que viaje NULL
            "brand_id":"7",
            "product_id":"11",
            "site_id":"4260",
            "user_data":{
                "email":"andresramirez2025@gmail.com"
            }
        */

        $url = "http://wsrest.activarpromo.com/api/preredeem.json";

        //OBLIGATORIO PARA TODOS
        //brand_id, product_id y email

        $response = Http::withHeaders([
            'user' => 'pmarketingtest',
            'token' => 'AUE5-A*dkL87@',
            'Content-Type' => 'application/json',
        ])->post($url, [
            'brand_id' => $request->brand_id,
            'product_id' => $request->product_id,
            'site_id' => $request->site_id,
            'user_data' => [
                'email' => $request->user_data["email"],
            ],
        ]);
        
        //Evaluamos los resultados
        if($response->successful()){
            $data = $response->object()->response;
            if($data->error){
                $codigo = substr($data->message, 1);
                $codigo = substr($codigo, 0, strpos($codigo, ']'));
                return response()->json([
                    'status' => 'error',
                    'codigo' => $codigo,
                    'message' => $data->message,
                    'disponible' => false,
                ], 200);
            }
            else{
                return response()->json([
                    'status' => 'success',
                    'message' => "El Producto esta dispnible",
                    'disponible' => true,
                ], 200);
            }
        }
        else if($response->clientError()){
            return response()->json([
                'status' => 'error',
                'message' => 'No se pudo hacer la conexión con la API de Quantum, intentelo más tarde',
            ], 400);
        }
        else{
            return response()->json([
                'status' => 'error',
                'message' => 'Error en el Servidor de la API de Quantum, intentelo más tarde',
            ], 503);
        }
    }

    //Verificar Redenciones por TRXID y Correo
    public function QuantumVerify(Request $request){
        
        /*  Datos a enviar por Request ejemplo site_id si no tiene que viaje NULL
            "trxid":"91f4ea5964fd8f9dd8c2f16b78806a39",
            "email":"andresramirez2025@gmail.com"
        */

        $url = "http://wsrest.activarpromo.com/api/verify.json";

        //OBLIGATORIO PARA TODOS
        //trxid y email

        $response = Http::withHeaders([
            'user' => 'pmarketingtest',
            'token' => 'AUE5-A*dkL87@',
            'Content-Type' => 'application/json',
        ])->post($url, [
            'trxid' => $request->trxid,
            'email' => $request->email,
        ]);
        
        //Evaluamos los resultados
        if($response->successful()){
            $data = $response->object()->response;
            if($data->error){
                $codigo = substr($data->message, 1);
                $codigo = substr($codigo, 0, strpos($codigo, ']'));
                return response()->json([
                    'status' => 'error',
                    'codigo' => $codigo,
                    'message' => $data->message,
                    'disponible' => false,
                ], 200);
            }
            else if(!$data->message){
                return response()->json([
                    'status' => 'error',
                    'message' => "El código enviado no tiene una relación de un canje existente",
                ], 200);
            }
            else{
                return response()->json([
                    'status' => 'success',
                    'message' => "La información del Canjeo se retuvo exitosamente",
                    'datos' => $data->message,
                ], 200);
            }
        }
        else if($response->clientError()){
            return response()->json([
                'status' => 'error',
                'message' => 'No se pudo hacer la conexión con la API de Quantum, intentelo más tarde',
            ], 400);
        }
        else{
            return response()->json([
                'status' => 'error',
                'message' => 'Error en el Servidor de la API de Quantum, intentelo más tarde',
            ], 503);
        }
    }
    
    //Recoger documento de Redencion (Bauche)
    public function QuantumDocument($filename = null){
        
        $url = "https://apitest.activarpromo.com/productos/viewpdf/";

        // $filename = 91f4ea5964fd8f9dd8c2f16b78806a39

        try{
            $content = Response(file_get_contents($url.$filename), 200, [ 
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="'.$filename.'"'
            ]);
            
            $archivo = storage_path().'/app/public/testing/'.$filename.".pdf";
            file_put_contents($archivo, $content);

            //Eliminamos el archivo
            unlink($archivo);

            return $content;
        }
        catch(\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => 'Mensaje de Error: '.$e->getMessage(),
            ], 503);
        }
            
    }

    //Store del Bauche de Canje para el envio de correo
    public function QuantumSaveDocument($filename = null){
        $url = "https://apitest.activarpromo.com/productos/viewpdf/";

        // $filename = 91f4ea5964fd8f9dd8c2f16b78806a39

        try{
            $content = Response(file_get_contents($url.$filename), 200, [ 
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="'.$filename.'"'
            ]);
            
            //El archivo esta grabado
            $archivo = storage_path().'/app/public/testing/'.$filename.".pdf";
            file_put_contents($archivo, $content);

            return $archivo;
        }
        catch(\Exception $e){
            return null;
        }
    }
}
