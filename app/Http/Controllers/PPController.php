<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

//Modelos
use App\Models\Paciente;
use App\Models\Profesional;
use App\Models\PacientePatologia;
use App\Models\ProfesionalInsignia;
use App\Models\Patologia;
use App\Models\Producto;
use App\Models\Insignia;
use App\Models\CapacitacionUser;
use App\Models\CuestionarioUser;
use App\Models\RespuestaOpcion;

//Helper
use App\Helper\Notificacion;
use Intervention\Image\Facades\Image;

class PPController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg',
        'min' => 'El dato debe ser mayor a 8 carácteres',
        'max' => 'El dato no debe ser mayor a 255 carácteres',
        'email' => 'El dato debe ir en formato de correo (ejemplo@correo.com)',
        'unique' => 'El dato enviado ya se encuentra registrado',
        'same' => 'La contraseña no coincide con la de confirmación',
        'regex' => 'La contraseña debe contener mayuscula, números y caracteres especiales',
        'avatar.max' => 'La imagen no puede ser mayor a 2Mb',
        'boolean' => 'El dato debe viajar como booleano (1 o 0)',
        'password.min' => 'La contraseña debe ser mayor a 8 caracteres',
    ];

    //Validaciones - Datos que viajaran como tal
    public $validaciones = [
        /*DATOS LOGIN*/
        'email' => 'required|string|email|max:255|unique:users,email',
        'password' => [
            'required',
            'string',
            'min:8',
            'max:50',
            'regex:/[a-z]/',      // must contain at least one lowercase letter
            'regex:/[A-Z]/',      // must contain at least one uppercase letter
            'regex:/[0-9]/',      // must contain at least one digit
            'regex:/[@$!%*#?&+-]/',
        ],
        'estado' => 'required|boolean',
        'pais' => 'required|string',
        'tipo' => 'required',
        /*DATOS DEL USUARIO*/
        'nombre' => 'required|string|max:255', 
        'apellido' => 'required|string|max:255',
        //'identificacion' => 'required|numeric|unique:users,identificacion',
        'telefono' => 'required|string|max:255',
        'direccion' => 'required|string|max:255',
        'especialidad' => 'required|string|max:255',
        'descripcion' => 'required|string|max:65535',
    ];

    //Compresor de Imagen
    public function imagecompress($image){
        $img = Image::make(storage_path('app/public/'.$image))->resize(1200, 1200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save();
        $img->destroy();
    }

    //Recoger imagen del producto
    public function getImageProducto($filename = null){
        
        if($filename){
            $exist = Storage::disk('public')->exists('productos/'.$filename);
            if(!$exist) //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('nodisponible.png');
            else
                $file = Storage::disk('public')->get('productos/'.$filename);
        }
        else
            $file = Storage::disk('public')->get('nodisponible.png');

        return new Response($file, 200);
    }

    //Recoger imagen de insignia
    public function getImageInsignia($filename = null){
        
        if($filename){
            $exist = Storage::disk('public')->exists('insignias/'.$filename);
            if(!$exist) //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('nodisponible.png');
            else
                $file = Storage::disk('public')->get('insignias/'.$filename);
        }
        else
            $file = Storage::disk('public')->get('nodisponible.png');

        return new Response($file, 200);
    }

    //////////////////////////////APARTADO DE READ VISUALES////////////////////////

    //visuales de patologias
    public function patologias(){
        $patologias = Patologia::all();
        return view('patologias.list',compact('patologias'));
    }

    //ajax paciente bajo dicha patologia
    public function getPacientes($id_patologia = null){
        $patologia = Patologia::find($id_patologia);

        if(!$patologia)
            return response()->json([
                'message' => "La Patología no se encuentra registrada, porfavor intenta más tarde"
            ]);

        //Recojo la tabla de enlace
        $listados = PacientePatologia::where('FK_id_patologia', $id_patologia)->get();
        
        //Reviso si tiene pacientes
        if($listados->isEmpty())
            return response()->json([
                'message' => "No tiene pacientes bajo dicha patología"
            ]);

        //Creo la variable con los pacientes
        $pacientes = collect();
        foreach($listados as $listado){
            $nombre = $listado->paciente->nombre." ".$listado->paciente->apellido.", C.I: ".$listado->paciente->identificacion;
            $pacientes->add($nombre);
        }

        return response()->json([
            'pacientes' => $pacientes,
            'message' => "Pacientes bajo dicha patología"
        ]);
    }

    //visuales de productos
    public function productos(){
        $productos = Producto::all();
        return view('productos.list',compact('productos'));
    }

    //visuales de insignias
    public function insignias(){
        $insignias = Insignia::all();
        return view('insignias.list',compact('insignias'));
    }

    //ajax paciente bajo dicha insignia
    public function getProfesionales($id_insignia = null){
        /* $patologia = Patologia::find($id_patologia);

        if(!$patologia)
            return response()->json([
                'message' => "La Patología no se encuentra registrada, porfavor intenta más tarde"
            ]);

        //Recojo la tabla de enlace
        $listados = PacientePatologia::where('FK_id_patologia', $id_patologia)->get();
        
        //Reviso si tiene pacientes
        if($listados->isEmpty())
            return response()->json([
                'message' => "No tiene pacientes bajo dicha patología"
            ]);

        //Creo la variable con los pacientes
        $pacientes = collect();
        foreach($listados as $listado){
            $nombre = $listado->paciente->nombre." ".$listado->paciente->apellido.", C.I: ".$listado->paciente->identificacion;
            $pacientes->add($nombre);
        }

        return response()->json([
            'pacientes' => $pacientes,
            'message' => "Pacientes bajo dicha patología"
        ]); */
    }

    //////////////////////////APARTADO CE CUD (CREATE UPDATE DELETE)//////////////////////////////

    //Store de Patología
    public function storePatologia(Request $request){
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'nombre' => 'required|string|max:255',
            ], $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $data = $request->all();
            Patologia::create($data);

            DB::commit();
            return Redirect::back()->with('create', 'Patología Registrada Exitosamente');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('delete', 'Error al añadir Patología: '.$e->getMessage());
        }
    }

    //Store de Producto
    public function storeProducto(Request $request){
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'nombre' => 'required|string|max:255',
                'descripcion' => 'required|string|max:65535',
                'imagen' => 'required|file|mimes:jpg,jpeg,png|max:2100',
            ], $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $data = $request->all();

            if($request->hasFile('imagen')){
                $data["imagen"] = $request->file('imagen')->store('productos', 'public');
                $this->imagecompress($data["imagen"]);
            }

            Producto::create($data);

            DB::commit();
            return Redirect::back()->with('create', 'Producto Registrado Exitosamente');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('delete', 'Error al añadir Producto: '.$e->getMessage());
        }
    }

    //update de Patologia
    public function updatePatologia(Request $request){
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'patologiaUD' => 'required|string|max:255',
                'id_patologia' => 'required',
            ], $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();      
            }
            
            $data = $request->all();
            $patologia = Patologia::findOrFail($data["id_patologia"]);
            $patologia->nombre = $data["patologiaUD"];
            $patologia->save();

            DB::commit();
            return Redirect::back()->with('edit', 'Patología Actualizada Exitosamente');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'Error al editar Patología: '.$e->getMessage());
        }
    }

    //Update de Producto
    public function updateProducto(Request $request){
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'nombreUD' => 'required|string|max:255',
                'descripcionUD' => 'required|string|max:65535',
                'imagenUD' => 'file|mimes:jpg,jpeg,png|max:2100',
            ], $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();      
            }
            
            $data = $request->all();
            $producto = Producto::findOrFail($data["id_producto"]);
            $producto->nombre = $data["nombreUD"];
            $producto->descripcion = $data["descripcionUD"];

            if($request->hasFile('imagenUD')){
                Storage::delete(['public/'.$producto->imagen]);
                $data["imagenUD"] = $request->file('imagenUD')->store('productos', 'public');
                $this->imagecompress($data["imagenUD"]);
                $producto->imagen = $data["imagenUD"];
            }

            $producto->save();

            DB::commit();
            return Redirect::back()->with('edit', 'Producto Actualizado Exitosamente');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'Error al editar Producto: '.$e->getMessage());
        }
    }

    //Delete Patología
    public function destroyPatologia($id_patologia = null){
        
        DB::beginTransaction();
        try{
            $patologia = Patologia::find($id_patologia);

            if(!$patologia)
                return response()->json([
                    'message' => "La Patología no se encuentra registrada, porfavor intenta más tarde"
                ]);

            //Elimino el enlace de dicha patologia con los distintos usuarios
            PacientePatologia::where('FK_id_patologia', $id_patologia)->delete();

            //Elimino o Vuelvo null las capacitaciones bajo esta patologia
            foreach ($patologia->capacitaciones as $capacitacion) {
                $cuestionario = $capacitacion->cuestionario;

                if($cuestionario){
                    //Elmino Respuestas Opciones y Preguntas
                    foreach ($cuestionario->preguntas as $pregunta){
                        foreach ($pregunta->respuestas as $respuesta){
                            Storage::delete(['public/'.$respuesta->imagen]);
                            $respuesta->delete();
                        }
                        RespuestaOpcion::where('FK_id_pregunta', $pregunta->id_pregunta)->delete();
                        $pregunta->delete();
                    }

                    //Dejo el cliente cuestionario con la información y pongo nula la relación
                    CuestionarioUser::where('FK_id_cuestionario', $cuestionario->id_cuestionario)->
                                        update(['FK_id_cuestionario' => null]);

                    $cuestionario->delete();
                }
            
                foreach ($capacitacion->entrenamientos as $entrenamiento){
                    CapacitacionUser::where('FK_id_capacitacion_documento', $entrenamiento->id_capacitacion_documento)->delete();
                    Storage::delete(['public/'.$entrenamiento->documento]);
                    Storage::delete(['public/'.$entrenamiento->video]);
                    $entrenamiento->delete();
                }

                $capacitacion->delete();
            }

            $patologia->delete();

            DB::commit();
            return response()->json([
                'delete' => true,
                'message' => "La Patología se ha eliminado exitosamente"
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => "Error al eliminar Patología, intentelo más tarde"
            ]);
        }
    }

    //Delete Producto
    public function destroyProducto($id_producto){
        
        DB::beginTransaction();
        try{
            $producto = Producto::find($id_producto);

            if(!$producto)
                return response()->json([
                    'message' => "La Categoria no se encuentra registrada, porfavor intenta más tarde"
                ]);

            //Elimino o Vuelvo null las capacitaciones bajo esta patologia
            foreach ($producto->capacitaciones as $capacitacion) {
                $cuestionario = $capacitacion->cuestionario;

                if($cuestionario){
                    //Elmino Respuestas Opciones y Preguntas
                    foreach ($cuestionario->preguntas as $pregunta){
                        foreach ($pregunta->respuestas as $respuesta){
                            Storage::delete(['public/'.$respuesta->imagen]);
                            $respuesta->delete();
                        }
                        RespuestaOpcion::where('FK_id_pregunta', $pregunta->id_pregunta)->delete();
                        $pregunta->delete();
                    }

                    //Dejo el cliente cuestionario con la información y pongo nula la relación
                    CuestionarioUser::where('FK_id_cuestionario', $cuestionario->id_cuestionario)->
                                        update(['FK_id_cuestionario' => null]);

                    $cuestionario->delete();
                }
            
                foreach ($capacitacion->entrenamientos as $entrenamiento){
                    CapacitacionUser::where('FK_id_capacitacion_documento', $entrenamiento->id_capacitacion_documento)->delete();
                    Storage::delete(['public/'.$entrenamiento->documento]);
                    Storage::delete(['public/'.$entrenamiento->video]);
                    $entrenamiento->delete();
                }

                $capacitacion->delete();
            }

            if($producto->imagen){
                Storage::delete(['public/'.$producto->imagen]);
            }

            $producto->delete();

            DB::commit();
            return response()->json([
                'delete' => true,
                'message' => "El producto se ha eliminado exitosamente"
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => "Error al eliminar Producto, intentelo más tarde"
            ]);
        }
    }
}
