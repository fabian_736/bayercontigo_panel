<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RutasController extends Controller
{

    public function form_login(){
        return view('modules.auth.form_login');
    }

    public function forgot(){
        return view('modules.auth.forgot');
    }

    public function index()
    {  
        return view('modules.dashboard.index');
    }

    public function perfil()
    {  
        return view('modules.dashboard.perfil');
    }

    public function facturas()
    {  
        return view('facturas.index');
    }

    public function listclientfacturas()
    {  
        return view('facturas.listclientfacturas');
    }

    public function listfacturas()
    {  
        return view('facturas.listfacturas');
    }

    public function capacitaciones()
    {  
        return view('capacitaciones.index');
    }

    public function informativo()
    {  
        return view('informativo.index');
    }

    public function listbanner()
    {  
        return view('informativo.listbanner');
    }

    public function listnoticias()
    {  
        return view('informativo.listnoticias');
    }

    public function noticias()
    {  
        return view('noticias.index');
    }

    public function listnoticia()
    {  
        return view('noticias.listnoticias');
    }

    public function premios()
    {  
        return view('premios.index');
    }

    public function listpremios()
    {  
        return view('premios.listpremios');
    }

    public function listcanjeados()
    {  
        return view('premios.listcanjeados');
    }

    public function niveles()
    {  
        return view('premios.niveles');
    }

    public function nivelesdetalles()
    {  
        return view('premios.nivelesdetalles');
    }

    public function reportes()
    {  
        return view('reportes.index');
    }

    public function filtros()
    {  
        return view('reportes.filtros');
    }
}
