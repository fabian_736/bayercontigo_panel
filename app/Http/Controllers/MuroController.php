<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

//MODELOS
use App\Models\Muro;
use App\Models\MuroComentario;
use App\Models\MuroLike;

//Helper o Usables
use App\Helper\Notificacion;
use Intervention\Image\Facades\Image;

class MuroController extends Controller
{

    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'file' => 'El dato debe llegar como un archivo',
        'mimes' => 'El archivo debe llegar en formato png, jpg, o jpeg',
        'imagen.max' => 'La imagen no puede ser mayor a 2Mb',
        'boolean' => 'Debes enviar el dato como true (1) o false (0)',
        'max' => 'El dato no debe ser mayor a 255 carácteres',
        'contenido.max' => "El dato no debe ser mayor a 65535 carácteres",
    ];

    //Validaciones - Datos que viajaran como tal
    public $validaciones = [
        'contenido' => 'required|string|max:65535',
        'comentario' => 'required|string|max:255',
        'imagen' => 'file|mimes:jpg,jpeg,png|max:2100',
        'erase' => 'boolean',
        'tipo' => 'required',
        'FK_id_useradmin' => 'required',
    ];

    //Compresor de Imagen, PDF o Video
    public function imagecompress($image, $extension){
        if($extension != 'pdf' && $extension != 'mp4'){
            $img = Image::make(storage_path('app/public/'.$image))->resize(1200, 1200, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save();
            $img->destroy();
        }
    }

    //Recoger imagen del muro si tiene
    public function getImage($filename = null){
        
        if($filename){
            $exist = Storage::disk('public')->exists('muros/'.$filename);
            if(!$exist) //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('nodisponible.png');
            else
                $file = Storage::disk('public')->get('muros/'.$filename);
        }
        else
            $file = Storage::disk('public')->get('nodisponible.png');

        return new Response($file, 200);
    }

    public function index(){
        $muros = Muro::where('FK_id_useradmin', auth()->user()->id_useradmin)->orderBy('created_at', 'desc')->get();
        return view('muros.index',compact('muros'));
    }

    public function list(){
        $muros = Muro::where('FK_id_useradmin','!=', auth()->user()->id_useradmin)->orderBy('created_at', 'desc')->get();
        return view('muros.list',compact('muros'));
    }

    //Listado de Muro Cambiados (trazabilidad)
    public function listcambios(){
        //$noticias = NoticiaCambio::orderBy('created_at', 'desc')->get();

        //Vista de la lista de premios cambios
        //return view('premio.index',compact('cambios'));
    }

    //Detallado del Muro (Modal de comentarios)
    public function detail($id){
        $muro = Muro::findOrFail($id);

        return view('muros.detail',compact('muro'));
    }

    //Comentarios del Muro
    public function comentarios($id){
        $muro = Muro::findOrFail($id);

        return view('muros.detail',compact('muro'));
    }

    //Enviar Vista para el registro de un muro
    public function create(){
        /* $premios = Premio::orderBy('nombre', 'asc')->get();

        return view('premio.control',compact('premios')); */
    }
    
    //Enviar la Vista para editar un Muro
    public function edit($id){
        $muro = Muro::findOrFail($id);

        return view('muros.edit',compact('muro'));
    }

    //Vista para confirmar si de verdad quiere eliminar el Muro
    public function confirm($id_muro){
        $muro = Muro::findOrFail($id_muro);
        return view('muros.confirm', compact('muro'));
    }

    //Guardado de un MURO
    public function store(Request $request){
        
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), $this->validaciones, $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();
            }
            
            $data = $request->all();
            
            if($request->hasFile('imagen')){
                $data["imagen"] = $request->file('imagen')->store('muros', 'public');
                $this->imagecompress($data["imagen"], $request->file('imagen')->extension());
            }

            $muro = Muro::create($data);
            
            /* GRABAMOS TRAZABILIDAD */
                /* $data['accion'] = 'Inserción';
                $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
                $data['FK_id_noticia'] = $noticia->id_noticia;
                
                NoticiaCambio::create($data); */

            //Notificación para los admin
                $data = [
                    "nombre" => "Store de Muro",
                    "tipo" => "Insert",
                    "descripcion" => "Creación de Muro: ".$muro->id_muro,
                ];

                Notificacion::instance()->store($data, "noticia");

            DB::commit();
            return Redirect::back()->with('create', 'Muro Registrado Exitosamente');

        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'El Muro no pudo registrarse Exitosamente');
        }

    }

    //Eliminar un MURO
    public function destroy($id_muro){
        
        DB::beginTransaction();
        try{
            $muro = Muro::find($id_muro);

            if(!$muro)
                return redirect()->route('muro.index')->with('edit', 'El muro no fue encontrado');

            MuroComentario::where('FK_id_muro', $muro->id_muro)->delete();
            MuroLike::where('FK_id_muro', $muro->id_muro)->delete();

            if($muro->imagen){
                Storage::delete(['public/'.$muro->imagen]);
            }

            $muro->delete();

            $data = $muro->toArray();
            $data['accion'] = 'Delete';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
            
            /* NoticiaCambio::create($data); */

            $data = [
                "nombre" => "Delete de Muro",
                "tipo" => "Delete",
                "descripcion" => "Eliminación de Muro: ".$muro->id_muro,
            ];

            Notificacion::instance()->store($data, "noticia");

            DB::commit();
            return redirect()->route('muro.index')->with('delete', 'Muro Eliminado Exitosamente');
        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            return Redirect::back()->with('error', 'El Muro no pudo ser eliminado');
        }
    }

    //Update de los datos de un MURO
    public function update(Request $request, $id_muro){
        
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), $this->validaciones, $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();      
            }

            $muro = Muro::findOrFail($id_muro);
            $data = $request->all();
            
            if($request->has('erase')){
                if($request->erase){
                    Storage::delete(['public/'.$muro->imagen]);
                    $data["imagen"] = null;
                }
                else if($request->hasFile('imagen') && !$request->erase){
                    Storage::delete(['public/'.$muro->imagen]);
                    $data["imagen"] = $request->file('imagen')->store('muros', 'public');
                    $this->imagecompress($data["imagen"], $request->file('imagen')->extension(), 'noticia');
                }
            }else{
                if($request->hasFile('imagen')){
                    Storage::delete(['public/'.$muro->imagen]);
                    $data["imagen"] = $request->file('imagen')->store('muros', 'public');
                    $this->imagecompress($data["imagen"], $request->file('imagen')->extension(), 'noticia');
                }
            }

            $muro->update($data);

            /* GRABAMOS TRAZABILIDAD */
                /* $data['accion'] = 'Update';
                $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
                $data['FK_id_noticia'] = $id_noticia;
                
                MuroCambio::create($data); */
            
            //Notificación para los admin
                $data = [
                    "nombre" => "Update de Muro",
                    "tipo" => "Update",
                    "descripcion" => "Edición de Muro: ".$muro->id_muro,
                ];

                Notificacion::instance()->store($data, "noticia");

            DB::commit();
            return Redirect::back()->with('edit', 'Muro Actualizado Exitosamente');

        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'El Muro no pudo ser Editado');
        }
    }

    //APARTADO DE LOS COMENTARIOS TODO SU PROCESO YA POR SEPARADOOOOOO

    //Store un comentario en especifico
    public function storeComment(Request $request, $id_muro){
        
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), [
                'comentario' => 'required|string|max:255',
            ], $this->mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();
            }
            
            $muro = Muro::find($id_muro);
            
            if(!$muro)
                return Redirect::back()->with('error', 'El muro no fue encontrado');

            $comment = new MuroComentario();
            $comment->comentario = $request->comentario;
            $comment->FK_id_useradmin = auth()->user()->id_useradmin;
            $comment->FK_id_muro = $id_muro;
            $comment->save();

            DB::commit();
            return Redirect::back()->with('create', 'Comentario Añadido Exitosamente');
        }catch(\Exception $e){
            DB::rollback();
            return Redirect::back()->with('error', 'El Comentario no pudo ser agregado: '.$e->getMessage());
        }
    }

    //Eliminar un comentario en especifico
    public function destroyComment($id_comment){
        
        DB::beginTransaction();
        try{
            $comment = MuroComentario::find($id_comment);

            if(!$comment)
                return Redirect::back()->with('error', 'El comentario no fue encontrado');

            $comment->delete();

            DB::commit();
            return Redirect::back()->with('delete', 'Comentario Eliminado Exitosamente');
        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            return Redirect::back()->with('error', 'El Comentario no pudo ser eliminado');
        }
    }

    //APARTADO DE LOS LIKE Y DISLIKE
    public function like($id_muro){
        //RECOGO LA ID DEL USUARIO Y INSTANCIO EL OBJEOT DE LIKE
        DB::beginTransaction();
        try{
            $admin = auth()->user()->id_useradmin;

            $like = new MuroLike();

            //REVISO SI YA TIENE LIKE
            $revision = MuroLike::where('FK_id_useradmin', $admin)->where('FK_id_muro', $id_muro)->count();

            $cantidad = MuroLike::where('FK_id_muro', $id_muro)->count();
            
            if($revision == 0){
                //SETTEO LOS VALORES
                $like->FK_id_muro = (int)$id_muro;
                $like->FK_id_useradmin = $admin;

                //GUARDO EL LIKE
                $like->save();

                DB::commit();
                //JSON PORQUE ESTO SE EJECUTA CON AJAX
                return response()->json([
                    'like' => $like,
                    'cantidad' => $cantidad + 1,
                    'message' => "Has dado like con exito"
                ]);
            }
            else{
                //JSON PORQUE ESTO SE EJECUTA CON AJAX
                return response()->json([
                    'message' => "El like ya existe",
                    'cantidad' => $cantidad,
                ]);
            }
        }
        catch(\Exception $e){
            DB::rollback();
            //JSON PORQUE ESTO SE EJECUTA CON AJAX
            return response()->json([
                'error' => "Fallo en el proceso de dar like: ".$e->getMessage()
            ]);
        }
    }

    public function dislike($id_muro){
        DB::beginTransaction();
        try{
            //RECOGO LA ID DEL USUARIO Y INSTANCIO EL OBJEOT DE LIKE
            $admin = auth()->user()->id_useradmin;

            //REVISO SI YA TIENE LIKE
            $like = MuroLike::where('FK_id_useradmin', $admin)->where('FK_id_muro', $id_muro)->first();

            $cantidad = MuroLike::where('FK_id_muro', $id_muro)->count();

            if($like){

                //BORRO EL LIKE
                $like->delete();
                
                DB::commit();
                //JSON PORQUE ESTO SE EJECUTA CON AJAX
                return response()->json([
                    'dislike' => $like,
                    'cantidad' => $cantidad - 1,
                    'message' => 'Has dado dislike correctamente'
                ]);
            }
            else{
                //JSON PORQUE ESTO SE EJECUTA CON AJAX
                return response()->json([
                    'message' => "La persona no posee likes colocados",
                    'cantidad' => $cantidad,
                ]);
            }
        }
        catch(\Exception $e){
            DB::rollback();
            //JSON PORQUE ESTO SE EJECUTA CON AJAX
            return response()->json([
                'error' => "Fallo en el proceso de dar like: ".$e->getMessage(),
            ]);
        }
    }
}
