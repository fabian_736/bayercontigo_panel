<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

//MODELOS
use App\Models\Capacitacion;
use App\Models\CapacitacionDocumento;
use App\Models\CapacitacionUser;
use App\Models\CuestionarioUser;
use App\Models\RespuestaOpcion;
use App\Models\Patologia;
use App\Models\Producto;
use App\Models\User;

//Helper o Usables
use App\Helper\VideoStream;
use App\Helper\Notificacion;
use Intervention\Image\Facades\Image;

//MAILABLE
use App\Mail\CapacitacionEmail;
use Illuminate\Support\Facades\Mail;

class CapacitacionController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'min' => 'El dato debe ser mayor a 0',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mimes' => 'El archivo debe llegar en formato pdf o mp4',
        'max' => 'El dato no debe ser mayor a 255 carácteres',
        'descripcion.max' => 'El dato no debe ser mayor a 65535 carácteres',

        //DOCUMENTOS
        'pdf.*.max' => 'El pdf no puede ser mayor a 2Mb',
        'video.*.max' => 'El video no puede ser mayor a 150Mb',

        //DOCUMENTOS NUEVOS
        'newpdf.*.max' => 'El pdf no puede ser mayor a 2Mb',
        'newvideo.*.max' => 'El video no puede ser mayor a 150Mb',
    ];

    //Validaciones - Datos que viajaran como tal
    public $validaciones = [
        'titulo' => 'required|string|max:255', 
        'descripcion' => 'required|string|max:65535',
        'FK_id_patologia' => 'numeric',
        'FK_id_producto' => 'numeric',

        //DOCUMENTOS
        'pdf.*' => 'file|mimes:pdf|max:'.(2100),
        'video.*' => 'file|mimes:mp4|max:'.(1024 * 150),

        //DOCUMENTOS EN EDIT
        'newpdf.*' => 'file|mimes:pdf|max:'.(2100),
        'newvideo.*' => 'file|mimes:mp4|max:'.(1024 * 150),
    ];

    //Compresor de Imagen o PDF
    public function imagecompress($image, $extension){
        if($extension == 'pdf')
            return "nada";
        else if($extension == 'mp4')
            return "nada";
        else{
            $img = Image::make(storage_path('app/public/'.$image))->resize(1200, 1200, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save();
            $img->destroy();
        }
    }

    //Recoger archivo de la capacitacion
    public function getArchive($filename = null){
        
        $extension = null;

        if($filename){
            $extension = pathinfo($filename)['extension'];

            $exist = Storage::disk('public')->exists('entrenamiento/'.$filename);
            if(!$exist){ //si no existe el file devuelveme el estandar
                $extension = null;
                $file = storage_path('app/public/nodisponible.png');
            }
            else
                $file = storage_path('app/public/entrenamiento/'.$filename);
        }
        else
            $file = storage_path('app/public/nodisponible.png');

        switch ($extension) {
            case 'pdf': $extension = 'application/pdf'; break;
            case 'mp4': $extension = 'mp4'; break;
            default: $extension = 'image'; break;
        }

        if($extension == 'mp4'){
            return view('capacitaciones.stream',compact('filename'));
        }
        else{
            return Response(file_get_contents($file), 200, [ 
                'Content-Type' => $extension,
                'Content-Disposition' => 'inline; filename="'.$file.'"'
            ]);  
        }
    }

    //Recoger preview de la capacitacion
    public function getImage($filename = null){
        
        if($filename){
            $exist = Storage::disk('public')->exists('entrenamiento/'.$filename);
            if(!$exist) //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('usericon.jpg');
            else
                $file = Storage::disk('public')->get('entrenamiento/'.$filename);
        }
        else
            $file = Storage::disk('public')->get('usericon.jpg');

        return new Response($file, 200);
    }

    //Recoger video de ejercicio
    public function getVideo($filename = null){
        try{
            $filePath = storage_path('app/public/entrenamiento/'.$filename);
            $stream = new VideoStream($filePath);
            return response()->stream(function() use ($stream) {
                $stream->start();
            }); 
            //$stream->start();
        }
        catch(\Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al abrir stream: No se encontro el archivo',
                'errores' => $e->getMessage()
            ], 500);
        }
    }  

    //////////////////////////////////////////APARTADO DE LAS VISUALES/////////////////////////////////////

    public function index(){
        $capacitaciones = Capacitacion::orderBy('created_at', 'desc')->paginate(5);
        
        //Producto y Patologias para crear capacitación
        $produtos = Producto::all();
        $patologias = Patologia::all();

        return view ('capacitaciones.index', compact('capacitaciones', 'produtos', 'patologias'));
    }

    public function entrenamientos($id = null){
        $capacitacion = Capacitacion::find($id);

        return view ('capacitaciones.entrenamiento', compact('capacitacion'));
    }

    public function capacitaciones($id_producto = null, $id_patologia = null){

        if($id_producto && $id_producto != 0)
            $capacitaciones = Capacitacion::where('FK_id_producto', $id_producto)->orderBy('created_at', 'desc')->paginate(5);
        elseif($id_patologia)
            $capacitaciones = Capacitacion::where('FK_id_patologia', $id_patologia)->orderBy('created_at', 'desc')->paginate(5);
        else
            $capacitaciones = Capacitacion::orderBy('created_at', 'desc')->paginate(5);
        
        return view ('capacitaciones.listado', compact('capacitaciones', 'id_producto', 'id_patologia'));
    }

    public function capProductos(){
        $productos = Producto::orderBy('created_at', 'desc')->paginate(5);
        
        return view ('capacitaciones.productos', compact('productos'));
    }

    public function capPatologias(){
        $patologias = Patologia::orderBy('created_at', 'desc')->paginate(5);
        
        return view ('capacitaciones.patologias', compact('patologias'));
    }

    public function usuarios(){
        $usuarios = User::all();
        
        return view ('capacitaciones.usuarios', compact('usuarios'));
    }

    //AJAX de detallado de cuestionarios realizados
    public function usuarioDetail($id_usuario = null){
        
        try{
            $cuestionarios = CuestionarioUser::where('FK_id_user', $id_usuario)->with('respuestas')->get();

            if($cuestionarios->count() <= 0)
                return response()->json([
                    'message' => "El Usuario no posee cuestionarios realizados"
                ]);
            
            return response()->json([
                'trivias' => $cuestionarios,
                'message' => "Cuestionarios Realizados del Usuario"
            ]);
        }
        catch(\Exception $e){
            return response()->json([
                'message' => "Error al consultar cuestionarios: ".$e->getMessage()
            ]);
        }
    }

    //Enviar la Vista para editar una Capacitación
    public function edit($id_capacitacion){
        $capacitacion = Capacitacion::findOrFail($id_capacitacion);
        
        //Producto y Patologias para crear capacitación
        $produtos = Producto::all();
        $patologias = Patologia::all();

        return view('capacitaciones.edit',compact('capacitacion', 'produtos', 'patologias'));
    }

    //Enviar a la vista para eliminar una Capacitación
    public function delete($id_capacitacion){
        $capacitacion = Capacitacion::findOrFail($id_capacitacion);
        return view('capacitaciones.confirm', compact('capacitacion'));
    }

    //////////////////////////////////////////APARTADO DEL CUD/////////////////////////////////////

    //Guardado de una capacitacion
    public function store(Request $request){
        
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), $this->validaciones);

            if($validate->fails()){
                return response()->json(['errors'=>$validate->errors()->all()]);
            }

            $data = $request->all();
            
            //Grabamos la Capacitación su Entrenamiento | Reto
            $capacitacion = Capacitacion::create($data);

            /* $data['accion'] = 'Inserción';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
            $data['FK_id_capacitacion'] = $capacitacion->id_capacitacion;
            
            CapacitacionCambio::create($data); */

            if($request->has('FK_id_producto'))
                $texto = "Ejercicio ";
            else
                $texto = "Reto ";

            //Grabamos los PDF y VIDEO del entrenamiento | reto
            if($request->has('pdf') && $request->has('video')){
                for ($i = 1; $i <= count($request->pdf); $i++) { 
                    $ejercicio = new CapacitacionDocumento();
                    $ejercicio->nombre = $texto.$i;
                    //$ejercicio->documento = $request->file('pdf[1]')->store('entrenamiento', 'public');
                    $ejercicio->documento = $request->pdf[$i]->store('entrenamiento', 'public');
                    $ejercicio->video = $request->video[$i]->store('entrenamiento', 'public');
                    $ejercicio->FK_id_capacitacion = $capacitacion->id_capacitacion;
                    $ejercicio->save();
                }
            }

            $data = [
                "nombre" => "Store de Capacitación",
                "tipo" => "Insert",
                "descripcion" => "Creación de Training: ".$capacitacion->titulo,
            ];

            Notificacion::instance()->store($data, "capacitacion");

            //Notificación para los clientes
            $dataClient = [
                "nombre" => '¡NUEVO RETO DE CONOCIMIENTO! 👨‍⚕️🎁',
                "tipo" => "Capacitación",
                "descripcion" => $capacitacion->titulo,
            ];

            Notificacion::instance()->storeClient($dataClient);
            
            /* $title = '¡NUEVO RETO DE CONOCIMIENTO! 👨‍⚕️🎁';
            $text = 'Aprovecha para sumar hasta '.$trivia->puntuacion.' puntos extra y demuestra que tanto sabes.¡CLIC AQUÍ PARA GANAR!';
            //$text = 'Nueva Capacitación disponible: '.$capacitacion->nombre;
            event(new BNotificacion($text, $title)); */

            //ENVIO DEL CORREO DE QUE HAY UNA CAPACITACION A TODOS LOS USUARIOS ACTIVOS
            /* $information = new \stdClass();
            $information->asunto = 'Nueva Capacitación Disponible';
            $information->nombre = $capacitacion->nombre;
            $information->puntuacion = $trivia->puntuacion;
            $this->sendEmails($information); */

            DB::commit();
            return response()->json(['success'=>'Capacitación Registrada de forma exitosa, Completa ahora el Cuestionario']);
            //return Redirect::back()->with('create', 'Capacitación Registrada');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return response()->json(['fallo'=>'La Capacitación no pudo ser Registrada, intentelo más tarde, Error: '+$e->getMessage()]);
            //return Redirect::back()->with('error', 'La Capicitación no pudo ser Registrada');
        }

    }

    //Update de una capacitacion
    public function update(Request $request, $id_capacitacion){
        
        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), $this->validaciones);

            if($validate->fails()){
                return response()->json(['errors'=>$validate->errors()->all()]);
            }

            //return $request;

            $data = $request->all();
            
            //Recojemos el Entrenamiento o Reto
            $capacitacion = Capacitacion::findOrFail($id_capacitacion);

            if($request->has('FK_id_producto')){
                $texto = "Ejercicio ";
                $capacitacion->FK_id_patologia = null;
            }
            else{
                $texto = "Reto ";
                $capacitacion->FK_id_producto = null;
            }

            //Revisamos si hay edición o borrado de VIDEOS O PDF YA EXISTENTES
            for ($i = 1; $i <= count($request->id_docs); $i++) { 
                //ESTAMOS RECORRIENDO UNO POR UNO

                //Evaluemos si va a elimnar la capacitacion
                if($request->delete_doc[$i]){
                    //Eliminamos la capacitacion user 
                    CapacitacionUser::where('FK_id_capacitacion_documento', $request->id_docs[$i])->delete();
                    $ejercicio = CapacitacionDocumento::find($request->id_docs[$i]);
                    Storage::delete(['public/'.$ejercicio->documento]);
                    Storage::delete(['public/'.$ejercicio->video]);
                    $ejercicio->delete();
                }
                //Evaluemos si va a editar algo de la capacitacion
                elseif(isset($request->pdf[$i]) || isset($request->video[$i])){
                    $ejercicio = CapacitacionDocumento::find($request->id_docs[$i]);

                    if(isset($request->pdf[$i])){
                        Storage::delete(['public/'.$ejercicio->documento]);
                        $ejercicio->documento = $request->pdf[$i]->store('entrenamiento', 'public');
                    }

                    if(isset($request->video[$i])){
                        Storage::delete(['public/'.$ejercicio->video]);
                        $ejercicio->video = $request->video[$i]->store('entrenamiento', 'public');
                    }
                    $ejercicio->save();
                }
            }

            //Grabamos los PDF y VIDEO Nuevos del entrenamiento | reto
            if($request->has('addExercise')){
                if($request->addExercise){
                    if($request->has('newpdf') && $request->has('newvideo')){
                        for ($i = 1; $i <= count($request->newpdf); $i++) { 
                            $ejercicio = new CapacitacionDocumento();
                            $ejercicio->nombre = $texto.($i+count($request->id_docs));
                            //$ejercicio->documento = $request->file('pdf[1]')->store('entrenamiento', 'public');
                            $ejercicio->documento = $request->newpdf[$i]->store('entrenamiento', 'public');
                            $ejercicio->video = $request->newvideo[$i]->store('entrenamiento', 'public');
                            $ejercicio->FK_id_capacitacion = $capacitacion->id_capacitacion;
                            $ejercicio->save();
                        }
                    }
                }
            }

            $capacitacion->update($data);

            $data = [
                "nombre" => "Update de Capacitación",
                "tipo" => "Update",
                "descripcion" => "Edición de Capacitación: ".$capacitacion->titulo,
            ];

            Notificacion::instance()->store($data, "capacitacion");

            //Notificación para los clientes
            $dataClient = [
                "nombre" => '¡NUEVO RETO DE CONOCIMIENTO! 👨‍⚕️🎁',
                "tipo" => "Capacitación",
                "descripcion" => $capacitacion->titulo,
            ];

            Notificacion::instance()->storeClient($dataClient);
            
            DB::commit();
            return response()->json(['success'=>'Capacitación Editada de forma exitosa']);
            //return Redirect::back()->with('create', 'Capacitación Registrada');

        }catch(\Exception $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return response()->json(['fallo'=>'La Capacitación no pudo ser Editada, intentelo más tarde, Error: '+$e->getMessage()]);
            //return Redirect::back()->with('error', 'La Capicitación no pudo ser Registrada');
        }

    }

    //Eliminar una Capacitación
    public function destroy($id_capacitacion){
        
        DB::beginTransaction();
        try{
            $capacitacion = Capacitacion::find($id_capacitacion);

            if(!$capacitacion)
                return redirect()->route('capacitacion.index')->with('edit', 'La capacitación no fue encontrada');

            /*CapacitacionCambio::where('FK_id_capacitacion', $capacitacion->id_capacitacion)->
                                update(['FK_id_capacitacion' => null]);*/

            /* ELIMINO EL CUSTIONARIO CON SUS PREGUNTAS Y RESPUESTAS */
                $cuestionario = $capacitacion->cuestionario;

                if($cuestionario){
                    /* TriviaCambio::where('FK_id_trivia', $trivia->id_trivia)->
                                    update(['FK_id_trivia' => null]); */

                    //Elmino Respuestas Opciones y Preguntas
                    foreach ($cuestionario->preguntas as $pregunta){
                        foreach ($pregunta->respuestas as $respuesta){
                            Storage::delete(['public/'.$respuesta->imagen]);
                            $respuesta->delete();
                        }
                        RespuestaOpcion::where('FK_id_pregunta', $pregunta->id_pregunta)->delete();
                        $pregunta->delete();
                    }

                    //Dejo el cliente cuestionario con la información y pongo nula la relación
                    CuestionarioUser::where('FK_id_cuestionario', $cuestionario->id_cuestionario)->
                                        update(['FK_id_cuestionario' => null]);

                    $cuestionario->delete();
                }
            
            foreach ($capacitacion->entrenamientos as $entrenamiento){
                CapacitacionUser::where('FK_id_capacitacion_documento', $entrenamiento->id_capacitacion_documento)->delete();
                Storage::delete(['public/'.$entrenamiento->documento]);
                Storage::delete(['public/'.$entrenamiento->video]);
                $entrenamiento->delete();
            }

            $capacitacion->delete();

            /* $data = $capacitacion->toArray();
            $data['accion'] = 'Delete';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
            
            CapacitacionCambio::create($data); */

            $data = [
                "nombre" => "Delete de Capacitación",
                "tipo" => "Delete",
                "descripcion" => "Eliminación de Capacitación: ".$capacitacion->titulo,
            ];

            Notificacion::instance()->store($data, "capacitacion");

            DB::commit();
            return redirect()->route('capacitacion.index')->with('delete', 'Capacitación Eliminada Exitosamente');
        }catch(\Exception $e){
            DB::rollback();
            return Redirect::back()->with('error', 'La Capacitación no pudo ser eliminada');
        }
    }
}
