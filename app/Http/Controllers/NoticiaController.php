<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

//MODELOS
use App\Models\Noticia;
use App\Models\NoticiaTipo;
use App\Models\NoticiaItem;
use App\Models\NoticiaDirectorio;
use App\Models\Profesional;
use App\Models\NoticiaCambio;
use App\Models\Muro;
use App\Models\ClienteHistorial;

//Helper o Usables
use App\Helper\VideoStream;
use App\Helper\Notificacion;
use Intervention\Image\Facades\Image;

class NoticiaController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'ingredientes.required' => 'Debes Ingresar al Menos un Ingrediente',
        'directorio.required' => 'Debes Ingresar al Menos un Profesional al Directorio si posee la noticia',
        'required' => 'El dato es requerido',
        'min' => 'El dato debe ser mayor a 0',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mimes' => 'El archivo debe llegar en formato png, jpg, jpeg, mp4 o pdf',
        'imagen.mimes' => 'El archivo debe llegar en formato imagen png, jpg, jpeg',
        'max' => 'El dato no debe ser mayor a 255 carácteres',
        'contenido.max' => "El dato no debe ser mayor a 65535 carácteres",
        'boolean' => 'Debes enviar el dato como true (1) o false (0)',
        'imagen.max' => 'La imagen no puede ser mayor a 2Mb',
        'documento.max' => 'El archivo no puede ser mayor a 2Mb',
    ];

    //Validaciones - Datos que viajaran como tal
    public $validaciones = [
        'titulo' => 'required|string|max:255', 
        'categoria' => 'required|string|max:255',
        'descripcion' => 'required|string|max:255',
        'contenido' => 'nullable|string|max:65535',
        'estado' => 'boolean',
        'imagen' => 'file|mimes:jpg,jpeg,png|max:2100',
        'documento' => 'file|mimes:jpg,jpeg,png,pdf,mp4|max:2100',
        'FK_id_tipo' => 'required',
    ];

    //Compresor de Imagen, PDF o Video
    public function imagecompress($image, $extension, $tipo = 'noticia'){
        if($tipo == 'noticia'){
            $width = 400; $height = 500;
        }
        else{
            $width = 600; $height = 280;
        }

        if($extension != 'pdf' && $extension != 'mp4'){
            $img = Image::make(storage_path('app/public/'.$image))->resize($width, $height)->save();
            $img->destroy();
        }
    }

    public function index(){
        $noticias = Noticia::orderBy('created_at', 'desc')->paginate(5);
        $muros = Muro::orderBy('created_at', 'desc')->paginate(5);

        //DATOS PARA EL PROCESO DE CREAR NOTICIA
        $tipos = NoticiaTipo::all();
        $profesionales = Profesional::all();

        return view ('informativo.index', compact('noticias', 'muros', 'tipos', 'profesionales'));
    }

    public function indexNoticia(){
        $noticias = Noticia::orderBy('created_at', 'desc')->paginate(5);
        //DATOS PARA EL PROCESO DE CREAR NOTICIA
        $tipos = NoticiaTipo::all();
        $profesionales = Profesional::all();

        return view ('noticias.index', compact('noticias', 'tipos', 'profesionales'));
    }

    //Recoger archivo de la noticia
    public function getDocument($filename = null){
        
        $extension = null;

        if($filename){
            $extension = pathinfo($filename)['extension'];

            $exist = Storage::disk('public')->exists('noticias/'.$filename);
            if(!$exist){ //si no existe el file devuelveme el estandar
                $extension = null;
                $file = storage_path('app/public/nodisponible.png');
            }
            else
                $file = storage_path('app/public/noticias/'.$filename);
        }
        else
            $file = storage_path('app/public/nodisponible.png');

        switch ($extension) {
            case 'pdf': $extension = 'application/pdf'; break;
            case 'mp4': $extension = 'mp4'; break;
            default: $extension = 'image'; break;
        }
        
        if($extension == 'mp4'){
            return view('noticias.stream',compact('filename'));
        }
        else{
            return Response(file_get_contents($file), 200, [ 
                'Content-Type' => $extension,
                'Content-Disposition' => 'inline; filename="'.$file.'"'
            ]);  
        }
    }

    //Recoger preview de la noticia
    public function getImage($filename = null){
        
        if($filename){
            $exist = Storage::disk('public')->exists('noticias/'.$filename);
            if(!$exist) //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('avatar.png');
            else
                $file = Storage::disk('public')->get('noticias/'.$filename);
        }
        else
            $file = Storage::disk('public')->get('avatar.png');

        return new Response($file, 200);
    }

    
    //Visualizar los videos
    public function StreamVideo($filename = null){
        try{
            $filePath = storage_path('app/public/noticias/'.$filename);
            $stream = new VideoStream($filePath);
            $stream->start();
        }
        catch(\Illuminate\Database\QueryException $e){
            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al abrir stream: No se encontro el archivo',
                'errores' => $e->getMessage()
            ], 500);
        }
    }  

    //Listado de Noticias
    public function list(){
        $noticias = Noticia::orderBy('created_at', 'desc')->get();
        //DATOS PARA EL PROCESO DE CREAR NOTICIA
        $tipos = NoticiaTipo::all();
        $profesionales = Profesional::all();

        return view('noticias.listnoticias',compact('noticias', 'tipos', 'profesionales'));
    }

    //Listado de Noticias Cambiadas (trazabilidad)
    public function listcambios(){
        //$noticias = NoticiaCambio::orderBy('created_at', 'desc')->get();

        //Vista de la lista de premios cambios
        //return view('premio.index',compact('cambios'));
    }

    //Detallado de la Noticia
    public function detail($id){
        $noticia = Noticia::findOrFail($id);

        return view('noticias.detail',compact('noticia'));
    }

    //Enviar Vista para el registro de una noticia
    public function create(){
        /* $premios = Premio::orderBy('nombre', 'asc')->get();

        return view('premio.control',compact('premios')); */
    }
    
    //Enviar la Vista para editar una Noticia
    public function edit($id_noticia){
        $noticia = Noticia::findOrFail($id_noticia);
        //$tipos = NoticiaTipo::all();
        $profesionales = Profesional::all();

        return view('noticias.edit',compact('noticia', 'profesionales'));
    }

    //Vista para confirmar si de verdad quiere eliminar la noticia
    public function confirm($id_noticia){
        $noticia = Noticia::findOrFail($id_noticia);
        return view('noticias.confirm', compact('noticia'));
    }

    //Guardado de una noticia
    public function store(Request $request){
        
        $validaciones = $this->validaciones;
        $mensajes_error = $this->mensajes_error;

        /* VALIDACION SI LLEGA EL DOCUMENTO */
        if($request->hasFile('documento')){
            if($request->file('documento')->extension() == "mp4"){
                $validaciones['documento'] = 'file|mimes:jpg,jpeg,png,pdf,mp4|max:'.(1024 * 150);
                $mensajes_error['documento.max'] = 'El archivo debe ser menor a 150 Mb';
            }
            else{
                $validaciones['documento'] = 'file|mimes:jpg,jpeg,png,pdf,mp4|max:'.(2100);
                $mensajes_error['documento.max'] = 'El archivo debe ser menor a 2 Mb';
            }
        }

        /* VALIDACIONES POR TIPO DE NOTICIA */
        if($request->selectedTipo == "Receta"){
            $validaciones['ingredientes'] = 'required';
        }
        else if($request->selectedTipo == "Salud"){
            if($request->has('portafolio')){
                if($request->portafolio == 1)
                    $validaciones['directorio'] = 'required';
            }
        }

        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), $validaciones, $mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();
            }
            
            $data = $request->all();
            
            if($request->hasFile('documento')){
                $data["documento"] = $request->file('documento')->store('noticias', 'public');
                $this->imagecompress($data["documento"], $request->file('documento')->extension());
            }

            if($request->hasFile('imagen')){
                $data["imagen"] = $request->file('imagen')->store('noticias', 'public');
                $this->imagecompress($data["imagen"], $request->file('imagen')->extension(), 'noticia');
            }

            $noticia = Noticia::create($data);

            /* GRABAR EL DIRECTORIO SI TIENE */
                if($request->selectedTipo == "Salud" && $request->has('directorio')){
                    foreach($request->directorio as $profe){
                        $nitem = new NoticiaDirectorio();
                        $nitem->FK_id_noticia = $noticia->id_noticia;
                        $nitem->FK_id_profesional = $profe;
                        $nitem->save();
                    }
                }

            /* GRABAR EL ITEM SI TIENE */
                if($request->selectedTipo == "Receta" && $request->has('ingredientes')){
                    foreach($request->ingredientes as $ing){
                        $nitem = new NoticiaItem();
                        $nitem->FK_id_noticia = $noticia->id_noticia;
                        $nitem->item = $ing;
                        $nitem->save();
                    }
                }
            
            /* GRABAMOS TRAZABILIDAD */
                /* $data['accion'] = 'Inserción';
                $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
                $data['FK_id_noticia'] = $noticia->id_noticia;
                
                NoticiaCambio::create($data); */

            //Notificación para los admin
                $data = [
                    "nombre" => "Store de Noticia",
                    "tipo" => "Insert",
                    "descripcion" => "Creación de Noticia: ".$noticia->titulo,
                ];

                Notificacion::instance()->store($data, "noticia");

            //Notificación para los clientes
                $dataClient = [
                    "nombre" => $noticia->titulo,
                    "tipo" => "Noticia",
                    "descripcion" => "Nueva Noticia Disponible",
                ];

                Notificacion::instance()->storeClient($dataClient);

                //$text = 'Nueva Noticia disponible: '.$noticia->nombre;
                //event(new BNotificacion($text));

            DB::commit();
            return Redirect::back()->with('create', 'Noticia Registrada Exitosamente');

        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'La Noticia no pudo registrarse Exitosamente');
        }

    }

    //Eliminar una Noticia
    public function destroy($id_noticia){
        
        DB::beginTransaction();
        try{
            $noticia = Noticia::find($id_noticia);

            if(!$noticia)
                return redirect()->route('noticias.index')->with('edit', 'La capacitación no fue encontrada');

            /* NoticiaCambio::where('FK_id_noticia', $noticia->id_noticia)->
                                update(['FK_id_noticia' => null]); */

            if($noticia->documento){
                Storage::delete(['public/'.$noticia->documento]);
            }

            if($noticia->imagen){
                Storage::delete(['public/'.$noticia->imagen]);
            }

            $noticia->delete();

            $data = $noticia->toArray();
            $data['accion'] = 'Delete';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
            
            /* NoticiaCambio::create($data); */

            $data = [
                "nombre" => "Delete de Noticia",
                "tipo" => "Delete",
                "descripcion" => "Eliminación de Noticia: ".$noticia->nombre,
            ];

            Notificacion::instance()->store($data, "noticia");

            DB::commit();
            return redirect()->route('noticias.list')->with('delete', 'Noticia Eliminada Exitosamente');
        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            return Redirect::back()->with('error', 'La Noticia no pudo ser eliminada');
        }
    }

    //Update de los datos de una noticia
    public function update(Request $request, $id_noticia){
        
        $validaciones = $this->validaciones;
        $mensajes_error = $this->mensajes_error;

        /* VALIDACION SI LLEGA EL DOCUMENTO */
        if($request->hasFile('documento')){
            if($request->file('documento')->extension() == "mp4"){
                $validaciones['documento'] = 'file|mimes:jpg,jpeg,png,pdf,mp4|max:'.(1024 * 150);
                $mensajes_error['documento.max'] = 'El archivo debe ser menor a 150 Mb';
            }
            else{
                $validaciones['documento'] = 'file|mimes:jpg,jpeg,png,pdf,mp4|max:'.(2100);
                $mensajes_error['documento.max'] = 'El archivo debe ser menor a 2 Mb';
            }
        }

        /* VALIDACIONES POR TIPO DE NOTICIA */
        if($request->selectedTipo == "Receta"){
            $validaciones['ingredientes'] = 'required';
        }
        else if($request->selectedTipo == "Salud"){
            if($request->has('portafolio')){
                if($request->portafolio == 1)
                    $validaciones['directorio'] = 'required';
            }
        }

        DB::beginTransaction();
        try{
            $validate = Validator::make($request->all(), $validaciones, $mensajes_error);

            if($validate->fails()){
                return Redirect::back()->withErrors($validate)->withInput();      
            }

            $noticia = Noticia::findOrFail($id_noticia);
            $data = $request->all();
            
            if($request->hasFile('documento')){
                Storage::delete(['public/'.$noticia->documento]);
                $data["documento"] = $request->file('documento')->store('noticias', 'public');
                $this->imagecompress($data["documento"], $request->file('documento')->extension());
            }

            if($request->hasFile('imagen')){
                Storage::delete(['public/'.$noticia->imagen]);
                $data["imagen"] = $request->file('imagen')->store('noticias', 'public');
                $this->imagecompress($data["imagen"], $request->file('imagen')->extension(), 'noticia');
            }

            $noticia->update($data);

            /* GRABAR EL DIRECTORIO SI TIENE */
                if($request->selectedTipo == "Salud" && $request->has('directorio')){
                    if($request->has('selectDirect')){
                        if($request->selectDirect){
                            NoticiaDirectorio::where('FK_id_noticia', $noticia->id_noticia)->delete();
                            //UPDATE
                            foreach($request->directorio as $profe){
                                $nitem = new NoticiaDirectorio();
                                $nitem->FK_id_noticia = $noticia->id_noticia;
                                $nitem->FK_id_profesional = $profe;
                                $nitem->save();
                            }
                        }
                    }
                    else{
                        //CREATE
                        foreach($request->directorio as $profe){
                            $nitem = new NoticiaDirectorio();
                            $nitem->FK_id_noticia = $noticia->id_noticia;
                            $nitem->FK_id_profesional = $profe;
                            $nitem->save();
                        }
                    }
                }

            /* GRABAR EL ITEM SI TIENE */
                if($request->selectIngrediente){
                    if($request->selectedTipo == "Receta" && $request->has('ingredientes')){
                        //UPDATE
                        NoticiaItem::where('FK_id_noticia', $noticia->id_noticia)->delete();
                        foreach($request->ingredientes as $ing){
                            $nitem = new NoticiaItem();
                            $nitem->FK_id_noticia = $noticia->id_noticia;
                            $nitem->item = $ing;
                            $nitem->save();
                        }
                    }
                }
            
            /* GRABAMOS TRAZABILIDAD */
                /* $data['accion'] = 'Update';
                $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
                $data['FK_id_noticia'] = $id_noticia;
                
                NoticiaCambio::create($data); */
            
            //Notificación para los admin
                $data = [
                    "nombre" => "Update de Noticia",
                    "tipo" => "Update",
                    "descripcion" => "Edición de Noticia: ".$noticia->nombre,
                ];

                Notificacion::instance()->store($data, "noticia");

            DB::commit();
            return redirect()->route('noticias.list')->with('edit', 'Noticia Actualizada Exitosamente');

        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'La Noticia no pudo ser Editada');
        }
    }
}
