<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPassword extends Model
{
    use HasFactory;

    protected $table='bc_historial_password';
    protected $primaryKey='id_historial_password';

    protected $fillable = [
        'id_historial_password', 
        'password', 
        'registro',
        'FK_id_user',
    ];

    public function cliente(){
        return $this->belongsTo('App\Models\User', 'FK_id_user')->withTrashed();
    }
}
