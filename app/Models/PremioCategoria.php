<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PremioCategoria extends Model
{
    use HasFactory;

    protected $table='bc_premio_categoria';
    protected $primaryKey='id_premio_cat';

    protected $fillable =[
        'id_premio_cat',
        'categoria',
        'imagen',
    ];

    public function premios(){
        return $this->hasMany('App\Models\Premio', 'FK_categoria');
    }
}
