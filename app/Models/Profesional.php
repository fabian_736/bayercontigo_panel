<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profesional extends Model
{
    use HasFactory;

    protected $table='bc_profesional';
    protected $primaryKey='id_profesional';

    protected $fillable =[
        'id_profesional',
        'nombre',
        'apellido',
        'direccion',
        'descripcion',
        'telefono',
        'identificacion',
        'especialidad',
        'FK_id_user',
    ];

    public function user(){
        return $this->belongsTo('App\Models\User', 'FK_id_user');
    }

    public function insignias(){
        return $this->hasMany('App\Models\ProfesionalInsignia', 'FK_id_profesional');
    }
}
