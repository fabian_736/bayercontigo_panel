<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    use HasFactory;

    protected $table='bc_respuesta';
    protected $primaryKey='id_respuesta';

    protected $fillable =[
        'id_respuesta',
        'respuesta',
        'imagen',
        'correcta',
        'FK_id_pregunta',
        'FK_id_opcion',
    ];

    public function pregunta(){
        return $this->belongsTo('App\Models\Pregunta', 'FK_id_pregunta');
    }

    public function opciones(){
        return $this->hasOne('App\Models\RespuestaOpcion', 'FK_id_opcion');
    }
}
