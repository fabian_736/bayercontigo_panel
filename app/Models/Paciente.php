<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    use HasFactory;

    protected $table='bc_paciente';
    protected $primaryKey='id_paciente';

    protected $fillable =[
        'id_paciente',
        'nombre',
        'apellido',
        'direccion',
        'telefono',
        'descripcion',
        'identificacion',
        'especialidad',
        'FK_id_user',
    ];

    public function user(){
        return $this->belongsTo('App\Models\User', 'FK_id_user');
    }

    public function patologias(){
        return $this->hasMany('App\Models\PacientePatologia', 'FK_id_paciente');
    }
}
