<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    protected $table='bc_producto';
    protected $primaryKey='id_producto';

    protected $fillable =[
        'id_producto',
        'nombre',
        'descripcion',
        'imagen',
    ];

    public function capacitaciones(){
        return $this->hasMany('App\Models\Capacitacion', 'FK_id_producto');
    }
}
