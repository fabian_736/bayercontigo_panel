<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Premio extends Model
{
    use HasFactory;

    protected $table='bc_premio';
    protected $primaryKey='id_premio';

    protected $fillable =[
        'id_premio',
        'nombre',
        'marca',
        'descripcion',
        'entrega',
        'stock',
        'puntos',
        'imagen',
        'distribuidor',
        'id_producto_dist',
        'id_brand_dist',
        'FK_categoria',
    ];

    public function canjeos(){
        return $this->hasMany('App\Models\PremioCanjeado', 'FK_id_premio');
    }

    public function caracteristicas(){
        return $this->hasMany('App\Models\PremioCaracteristicas', 'FK_id_premio');
    }

    public function categoria(){
        return $this->belongsTo('App\Models\PremioCategoria', 'FK_categoria');
    }
}
