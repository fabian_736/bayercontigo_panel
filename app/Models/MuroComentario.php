<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MuroComentario extends Model
{
    use HasFactory;

    protected $table='bc_muro_comentario';
    protected $primaryKey='id_muro_comentario';

    protected $fillable =[
        'id_muro_comentario',
        'comentario',
        'FK_id_user',
        'FK_id_useradmin',
        'FK_id_muro',
    ];

    public function creadorUser(){
        return $this->belongsTo('App\Models\User', 'FK_id_user');
    }

    public function creadorAdmin(){
        return $this->belongsTo('App\Models\Admin', 'FK_id_useradmin');
    }

    public function muro(){
        return $this->belongsTo('App\Models\Muro', 'FK_id_muro');
    }
}
