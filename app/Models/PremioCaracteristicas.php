<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PremioCaracteristicas extends Model
{
    use HasFactory;

    protected $table='bc_premio_caracteristica';
    protected $primaryKey='id_caracteristica';

    protected $fillable =[
        'id_caracteristica',
        'caracteristica',
        'FK_id_premio',
    ];

    public function premio(){
        return $this->belongsTo('App\Models\Premio', 'FK_id_premio');
    }
}
