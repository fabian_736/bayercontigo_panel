<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PremioEntrega extends Model
{
    use HasFactory;

    protected $table='bc_premio_entrega';
    protected $primaryKey='id_entrega';

    protected $fillable =[
        'id_entrega',
        'tipo',
        'telefono', //dato de entrega
        'correo', //dato de entrega
        'receptor', //dato de entrega
        'cedula', 
        'direccion', //dato de entrega
        'adicional', //dato de entrega
        'estado',
        'tiempo',
        'FK_id_canjeado',
    ];

    public function canjeo(){
        return $this->belongsTo('App\Models\PremioCanjeado', 'FK_id_canjeado');
    }
}
