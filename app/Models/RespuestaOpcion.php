<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RespuestaOpcion extends Model
{
    use HasFactory;

    protected $table='bc_respuesta_opcion';
    protected $primaryKey='id_opcion';

    protected $fillable =[
        'id_opcion',
        'opcion',
        'FK_id_pregunta',
    ];

    public function pregunta(){
        return $this->belongsTo('App\Models\Pregunta', 'FK_id_pregunta');
    }

    public function respuesta(){
        return $this->hasOne('App\Models\Respuesta', 'FK_id_opcion');
    }
}
