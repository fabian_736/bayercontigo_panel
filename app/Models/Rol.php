<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    use HasFactory;

    protected $table='bc_adminrol';
    protected $primaryKey='id_adminrol';
    protected $fillable =[
        'id_adminrol',
        'nombre',
    ];

    public function admin(){
        return $this->hasMany(Rol::class);
    }
}
