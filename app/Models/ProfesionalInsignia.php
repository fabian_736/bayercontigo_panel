<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfesionalInsignia extends Model
{
    use HasFactory;

    protected $table='bc_pro_insignia';
    protected $primaryKey='id_pro_insignia';

    protected $fillable =[
        'id_pro_insignia',
        'FK_id_profesional',
        'FK_id_insignia',
    ];

    public function profesional(){
        return $this->belongsTo('App\Models\Profesional', 'FK_id_profesional');
    }

    public function insignia(){
        return $this->belongsTo('App\Models\Insignia', 'FK_id_insignia');
    }
}
