<?php

namespace App\Imports;

use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

//MODELOS
use App\Models\Premio;

class PremioImport implements ToCollection, WithValidation, WithHeadingRow
{   
    use Importable;

    public function  __construct($id_useradmin, $validaciones)
    {
        $this->id_useradmin = $id_useradmin;
        $this->validaciones = $validaciones;
    }

    public function collection(Collection  $rows)
    {   
        Validator::make($rows->toArray(), $this->validaciones)->validate();

        foreach ($rows as $row) {
            return new Premio([
                'nombre' => $row['nombre'],
                'marca' => $row['marca'],
                'descripcion' => $row['descripcion'],
                'entrega' => $row['entrega'],
                'stock' => $row['stock'],
                'puntos' => $row['puntos'],
                'distribuidor' => $row['distribuidor'],
                'producto_id' => $row['producto_id'],
                'marca_id' => $row['marca_id'],
            ]);
        }
    }

    public function rules(): array
    {
        return $this->validaciones;
    }
}
