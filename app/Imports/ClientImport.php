<?php

namespace App\Imports;

use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Facades\Hash;

//MODELOS
use App\Models\Cliente;
use App\Models\ClienteLogin;
use App\Models\ClientePassword;

//MAILABLE
use App\Mail\Welcome;
use Illuminate\Support\Facades\Mail;


class ClientImport implements ToCollection, WithValidation, WithHeadingRow
{   
    use Importable;

    public function  __construct($id_useradmin, $validaciones)
    {
        $this->id_useradmin = $id_useradmin;
        $this->validaciones = $validaciones;
    }

    public function collection(Collection  $rows)
    {   
        foreach ($rows as $row) {
            $caracteres_permitidos = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $password = substr(str_shuffle($caracteres_permitidos), 0, 8); 

            $cliente = Cliente::create([
                'nombre' => $row['nombre'],
                'apellido' => $row['apellido'],
                'telefono' => $row['telefono'],
                'correo' => $row['correo'],
                'total_estrellas' => 0,
                'especialidad' => $row['especialidad'],
            ]);

            ClienteLogin::create([
                'email' => $row['correo'],
                'password' => Hash::make($password),
                'estado' => "activo",
                'segmento' => $row['segmento'],
                'fecha_cambio' => date('Y-m-d'),
                'FK_id_cliente' => $cliente->id_cliente
            ]);

            ClientePassword::create([
                'password' => Hash::make($password),
                'registro' => date('Y-m-d'),
                'FK_id_cliente' => $cliente->id_cliente
            ]);

            //ENVIO DEL CORREO
            $information = new \stdClass();
            $information->asunto = 'Bienvenido a DentApp';
            $information->nombre = $row["nombre"]." ".$row["apellido"];
            $information->correo = $row['correo'];
            $information->contraseña = $password;

            Mail::to($row['correo'])->queue(new Welcome($information));
        }
    }

    public function rules(): array
    {
        return $this->validaciones;
    }
}
