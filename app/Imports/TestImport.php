<?php

namespace App\Imports;

use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Facades\Hash;

//MODELOS
use App\Models\Cliente;
use App\Models\ClienteLogin;
use App\Models\ClientePassword;

//MAILABLE
use App\Mail\Welcome;
use Illuminate\Support\Facades\Mail;


class TestImport implements ToCollection, WithValidation, WithHeadingRow
{   
    use Importable;

    public function  __construct($id_useradmin, $validaciones)
    {
        $this->id_useradmin = $id_useradmin;
        $this->validaciones = $validaciones;
    }

    public function collection(Collection  $rows)
    {   
        Validator::make($rows->toArray(), $this->validaciones)->validate();

        foreach ($rows as $row) {
            return new Cliente([
                'nombre' => $row['nombre'],
                'apellido' => $row['apellido'],
                'telefono' => $row['telefono'],
                'correo' => $row['correo'],
                'total_estrellas' => 0,
                'especialidad' => $row['especialidad'],
            ]);
        }
    }

    /* public function model(array $row)
    {   
        return new Cliente([
            'nombre' => $row['nombre'],
            'apellido' => $row['apellido'],
            'telefono' => $row['telefono'],
            'correo' => $row['correo'],
            'total_estrellas' => 0,
            'especialidad' => $row['especialidad'],
        ]);
    } */

    public function rules(): array
    {
        return $this->validaciones;
    }
}
