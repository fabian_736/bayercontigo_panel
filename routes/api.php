<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//CONTROLADORES
use App\Http\Controllers\QuantumController;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/quantum')->group(function(){
    //Apartado de localizar el producto, marca, ciudad etc etc
    Route::middleware('auth:api')->get('/marcas',[QuantumController::class,'QuantumBrands']);
    Route::middleware('auth:api')->get('/productos/{id}', [QuantumController::class, 'QuantumProducts']);
    Route::middleware('auth:api')->get('/departamentos/{id}', [QuantumController::class, 'QuantumBrandDepartment']);
    Route::middleware('auth:api')->get('/ciudades/{id_brand}/{id_department}', [QuantumController::class, 'QuantumBrandCities']);
    Route::middleware('auth:api')->get('/sitios/{id_brand}/{id_department}/{id_city}', [QuantumController::class, 'QuantumBrandSites']);

    //Apartado ya de canje como tal
    Route::middleware('auth:api')->post('/redencion/{id}', [QuantumController::class, 'QuantumRedeem']);
    Route::middleware('auth:api')->post('/existencia', [QuantumController::class, 'QuantumPreCheck']);
    Route::middleware('auth:api')->post('/verificar', [QuantumController::class, 'QuantumVerify']);

    //Llamar una imagen del canje con el PDF
    Route::middleware('auth:api')->get('/documento/{filename?}', [QuantumController::class, 'QuantumDocument']);
}); 