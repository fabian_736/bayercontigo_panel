<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

//CONTROLADORES
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PasswordResetController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\RutasController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\NotificacionAdmin;
use App\Http\Controllers\NoticiaController;
use App\Http\Controllers\MuroController;
use App\Http\Controllers\PremioController;
use App\Http\Controllers\PPController;
use App\Http\Controllers\CapacitacionController;
use App\Http\Controllers\CuestionarioController;
use App\Http\Controllers\QuantumController;


//RUTAS DE LOGIN | LOGOUT
Route::get('/',[LoginController::class,'form_login'])->name('login'); //mostrar el login o dashboard
Route::get('login',[LoginController::class,'form_login']);//->name('login'); //mostrar el login
Route::post('auth',[LoginController::class,'login'])->name('login.auth'); //metodo de login
Route::get('logout',[LoginController::class,'logout'])->name('login.logout'); //metodo de logout

// RUTAS RESET PASSWORD
Route::prefix('/password')->group(function(){
    Route::get('/forgot',[PasswordResetController::class,'forgot'])->name('password.forgot');
    Route::post('/email/{tipo?}',[PasswordResetController::class,'create'])->name('password.email');
    Route::get('/find/{token}/{tipo}', [PasswordResetController::class,'find'])->name('password.find');
    Route::post('/reset', [PasswordResetController::class, 'reset'])->name('password.update');
});

//RUTAS DEL DASHBOARD Y PERFIL
Route::middleware(['auth:admin', 'state'])->get('home',[DashboardController::class,'index'])->name('home');
Route::middleware(['auth:admin', 'state'])->get('portal',[DashboardController::class,'index'])->name('dashboard.index');
Route::prefix('/perfil')->group(function(){
    Route::get('/getImage/{filename?}', [DashboardController::class, 'getImage'])->name('admin.icon');
    Route::middleware(['auth:admin', 'state'])->get('',[DashboardController::class,'perfil'])->name('dashboard.perfil');
    Route::middleware(['auth:admin', 'state'])->post('/update',[DashboardController::class,'update'])->name('update.perfil');
});

//RUTAS DEL ADMIN
Route::prefix('/admin')->group(function(){
    Route::middleware(['auth:admin', 'state'])->get('/list',[AdminController::class,'listadmin'])->name('admin.listadmin');
    Route::middleware(['auth:admin', 'state'])->get('/create',[AdminController::class,'create'])->name('admin.create');
    Route::middleware(['auth:admin', 'state'])->get('/edit/{id}',[AdminController::class,'edit'])->name('admin.edit');
    Route::middleware(['auth:admin', 'state'])->get('/confirm/{id}',[AdminController::class,'confirm'] )->name('admin.confirm');

    Route::middleware(['auth:admin', 'state', 'store'])->post('/store',[AdminController::class,'store'])->name('admin.store');
    Route::middleware(['auth:admin', 'state', 'update'])->post('/update/{id}',[AdminController::class,'update'])->name('admin.update');
    Route::middleware(['auth:admin', 'state', 'destroy'])->delete('/destroy/{id}',[AdminController::class,'destroy'])->name('admin.destroy');
});

//RUTAS DE USUARIOS
Route::prefix('/usuarios')->group(function(){
    Route::middleware(['auth:admin', 'state'])->get('/list',[UserController::class,'listuser'])->name('user.listuser');
    Route::middleware(['auth:admin', 'state'])->get('/datos/{tipo}/{id}',[UserController::class,'getDatos'])->name('user.datos');
    Route::middleware(['auth:admin', 'state'])->get('/detalle/{id}',[UserController::class,'getDetail'])->name('user.detail');
    Route::get('/getImage/{filename?}', [UserController::class, 'getImage'])->name('user.icon');

    Route::middleware(['auth:admin', 'state'])->get('/create',[UserController::class,'create'])->name('user.create');
    Route::middleware(['auth:admin', 'state'])->get('/edit/{id}',[UserController::class,'edit'])->name('user.edit');
    Route::middleware(['auth:admin', 'state'])->get('/confirm/{id}',[UserController::class,'confirm'] )->name('user.confirm');

    Route::middleware(['auth:admin', 'state', 'store'])->post('/store',[UserController::class,'store'])->name('user.store');
    Route::middleware(['auth:admin', 'state', 'update'])->post('/update/{id}',[UserController::class,'update'])->name('user.update');
    Route::middleware(['auth:admin', 'state', 'destroy'])->post('/destroy/{id}',[UserController::class,'destroy'])->name('user.destroy');

    //Apartado de la Importación con excel de Usuarios
    Route::middleware(['auth:admin', 'state'])->get('/import',[UserController::class,'importIndex'])->name('user.import');
    Route::middleware(['auth:admin', 'state'])->get('/import/tabla',[UserController::class,'importTabla'])->name('user.import.tabla');
    Route::middleware(['auth:admin', 'state', 'store'])->post('/import/csv',[UserController::class,'importCsv'])->name('user.import.csv');
    Route::middleware(['auth:admin', 'state', 'store'])->post('/import/form',[UserController::class,'importForm'])->name('user.import.form');
    Route::middleware(['auth:admin', 'state', 'store'])->post('/import/store',[UserController::class,'importStore'])->name('user.import.store');
});

//RUTAS DE CONFIGURACIÓN (PATOLOGIA | PRODUCTOS | INSIGNIAS)
Route::prefix('/configuracion')->group(function(){
    //Apartado de las patologias
    Route::middleware(['auth:admin', 'state'])->get('/patologia/list', [PPController::class, 'patologias'])->name('patologia.listado');
    Route::middleware(['auth:admin', 'state'])->get('/patologia/detail/{id?}',[PPController::class,'getPacientes'])->name('patologia.detail');
    Route::middleware(['auth:admin', 'state', 'store'])->post('/patologia/store',[PPController::class,'storePatologia'])->name('patologia.store');
    Route::middleware(['auth:admin', 'state', 'update'])->post('/patologia/update',[PPController::class,'updatePatologia'])->name('patologia.update');
    Route::middleware(['auth:admin', 'state', 'destroy'])->post('/patologia/delete/{id?}',[PPController::class,'destroyPatologia'])->name('patologia.destroy');

    //Apartado de los productos
    Route::middleware(['auth:admin', 'state'])->get('/producto/list', [PPController::class, 'productos'])->name('producto.listado');
    Route::middleware(['auth:admin', 'state', 'store'])->post('/producto/store',[PPController::class,'storeProducto'])->name('producto.store');
    Route::middleware(['auth:admin', 'state', 'update'])->post('/producto/update',[PPController::class,'updateProducto'])->name('producto.update');
    Route::middleware(['auth:admin', 'state', 'destroy'])->post('/producto/delete/{id?}',[PPController::class,'destroyProducto'])->name('producto.destroy');
    Route::get('/producto/getImage/{filename?}', [PPController::class, 'getImageProducto'])->name('producto.icon');

    //Apartado de las insignias
    Route::middleware(['auth:admin', 'state'])->get('/insignia/list', [PPController::class, 'insignias'])->name('insignia.listado');
    Route::middleware(['auth:admin', 'state'])->get('/insignia/detail/{id}',[PPController::class,'getProfesionales'])->name('insignia.detail');
    Route::middleware(['auth:admin', 'state', 'store'])->post('/insignia/store',[PPController::class,'storeInsignia'])->name('insignia.store');
    Route::middleware(['auth:admin', 'state', 'update'])->post('/insignia/update',[PPController::class,'updateInsignia'])->name('insignia.update');
    Route::middleware(['auth:admin', 'state', 'destroy'])->post('/insignia/delete/{id?}',[PPController::class,'destroyInsignia'])->name('insignia.destroy');
    Route::get('/insignia/getImage/{filename?}', [PPController::class, 'getImageInsignia'])->name('insignia.icon');
});

//RUTAS DE NOTIFICACIONES
Route::prefix('/notificaciones')->group(function(){
    Route::middleware(['auth:admin', 'state'])->get('all',[NotificacionAdmin::class,'all'])->name('notificacion.all');
    Route::middleware(['auth:admin', 'state'])->get('index',[NotificacionAdmin::class,'index'])->name('notificacion.index');
    Route::middleware(['auth:admin', 'state'])->get('/change/{id}',[NotificacionAdmin::class,'marcarLeido'])->name('notificacion.marcar');
    Route::middleware(['auth:admin', 'state'])->get('/marcar',[NotificacionAdmin::class,'marcartodo'])->name('notificacion.marcartodas');
    Route::middleware(['auth:admin', 'state'])->get('/marcartres',[NotificacionAdmin::class,'marcartres'])->name('notificacion.marcartres');

    Route::middleware(['auth:admin', 'state'])->get('/delete/{id}',[NotificacionAdmin::class,'deleteone'])->name('notificacion.delete');
    Route::middleware(['auth:admin', 'state'])->get('/borrar',[NotificacionAdmin::class,'deletetodo'])->name('notificacion.deleteall');
}); 

//RUTAS DE NOTICIAS | MURO | GENERAL
Route::prefix('/informativo')->group(function(){
    //RUTA DEL GENERAL
    Route::middleware(['auth:admin', 'state'])->get('/inicio',[NoticiaController::class,'index'])->name('informativo.index');

    //RUTAS DE NOTICIAS
    Route::middleware(['auth:admin', 'state'])->get('/noticias',[NoticiaController::class,'indexNoticia'])->name('noticias.index');
    Route::middleware(['auth:admin', 'state'])->get('/noticias/list', [NoticiaController::class, 'list'])->name('noticias.list');
    Route::middleware(['auth:admin', 'state'])->get('/noticias/cambios', [NoticiaController::class, 'listcambios'])->name('noticias.cambios');
    Route::middleware(['auth:admin', 'state'])->get('/noticias/detail/{id}',[NoticiaController::class,'detail'])->name('noticias.detail');
    Route::middleware(['auth:admin', 'state'])->get('/noticias/edit/{id}',[NoticiaController::class,'edit'])->name('noticias.edit');
    Route::middleware(['auth:admin', 'state'])->get('/noticias/confirm/{id}',[NoticiaController::class,'confirm'] )->name('noticias.confirm');

    Route::middleware(['auth:admin', 'state', 'store'])->post('/noticias/store',[NoticiaController::class,'store'])->name('noticias.store');
    Route::middleware(['auth:admin', 'state', 'update'])->post('/noticias/update/{id}',[NoticiaController::class,'update'])->name('noticias.update');
    Route::middleware(['auth:admin', 'state', 'destroy'])->delete('/noticias/delete/{id}',[NoticiaController::class,'destroy'])->name('noticias.destroy');

    Route::get('/noticias/getDocument/{filename?}', [NoticiaController::class, 'getDocument'])->name('noticias.document');
    Route::get('/noticias/getImage/{filename?}', [NoticiaController::class, 'getImage'])->name('noticias.image');
    Route::get('/noticias/getVideo/{filename?}',[NoticiaController::class,'StreamVideo'])->name('noticias.video');

    //RUTAS DE MURO
    Route::middleware(['auth:admin', 'state'])->get('/muro',[MuroController::class,'index'])->name('muro.index');
    Route::middleware(['auth:admin', 'state'])->get('/muro/listado',[MuroController::class,'list'])->name('muro.list');
    Route::middleware(['auth:admin', 'state'])->get('/muro/detail/{id}',[MuroController::class,'detail'])->name('muro.detail');
    Route::middleware(['auth:admin', 'state'])->get('/muro/comentarios/{id}',[MuroController::class,'comentarios'])->name('muro.comments');
    Route::middleware(['auth:admin', 'state'])->get('/muro/edit/{id}',[MuroController::class,'edit'])->name('muro.edit');
    Route::middleware(['auth:admin', 'state'])->get('/muro/confirm/{id}',[MuroController::class,'confirm'] )->name('muro.confirm');
    
    Route::middleware(['auth:admin', 'state', 'store'])->post('/muro/store',[MuroController::class,'store'])->name('muro.store');
    Route::middleware(['auth:admin', 'state', 'update'])->post('/muro/update/{id}',[MuroController::class,'update'])->name('muro.update');
    Route::middleware(['auth:admin', 'state', 'destroy'])->delete('/muro/delete/{id}',[MuroController::class,'destroy'])->name('muro.destroy');

    Route::middleware(['auth:admin', 'state'])->post('/comentario/store/{id}',[MuroController::class,'storeComment'])->name('muro.comment.store');
    Route::middleware(['auth:admin', 'state'])->post('/comentario/delete/{id}',[MuroController::class,'destroyComment'])->name('muro.comment.delete');

    Route::get('/muro/getImage/{filename?}', [MuroController::class, 'getImage'])->name('muro.image');
    Route::middleware(['auth:admin', 'state'])->get('/muro/add/like/{id}',[MuroController::class,'like'])->name('muro.like');
    Route::middleware(['auth:admin', 'state'])->get('/muro/add/dislike/{id}',[MuroController::class,'dislike'])->name('muro.dislike');
    Route::get('/muro/add/',[MuroController::class,'like'])->name('muro.like.url');
});

//RUTAS PARA LOS PREMIOS
Route::prefix('/premios')->group(function(){
    //Apartado de los Premios
    Route::middleware(['auth:admin', 'state'])->get('index',[PremioController::class,'index'])->name('premio.index');
    Route::middleware(['auth:admin', 'state'])->get('/list', [PremioController::class, 'premios'])->name('premio.listado');
    Route::middleware(['auth:admin', 'state'])->get('/edit/{id}',[PremioController::class,'editPremio'])->name('premio.edit');
    Route::middleware(['auth:admin', 'state'])->get('/confirm/{id}',[PremioController::class,'deletePremio'] )->name('premio.confirm');

    Route::middleware(['auth:admin', 'state', 'store'])->post('/store',[PremioController::class,'store'])->name('premio.store');
    Route::middleware(['auth:admin', 'state', 'update'])->post('/update/{id}',[PremioController::class,'update'])->name('premio.update');
    Route::middleware(['auth:admin', 'state', 'destroy'])->delete('/delete/{id}',[PremioController::class,'destroy'])->name('premio.destroy');
    Route::get('/getImage/{filename?}', [PremioController::class, 'getImage'])->name('premio.icon');

    //Apartado de la Importación con excel de Premios
    Route::middleware(['auth:admin', 'state'])->get('/import',[PremioController::class,'importIndex'])->name('premio.import');
    Route::middleware(['auth:admin', 'state'])->get('/import/tabla',[PremioController::class,'importTabla'])->name('premio.import.tabla');
    Route::middleware(['auth:admin', 'state', 'store'])->post('/import/csv',[PremioController::class,'importCsv'])->name('premio.import.csv');
    Route::middleware(['auth:admin', 'state', 'store'])->post('/import/form',[PremioController::class,'importForm'])->name('premio.import.form');
    Route::middleware(['auth:admin', 'state', 'store'])->post('/import/store',[PremioController::class,'importStore'])->name('premio.import.store');

    //Apartado de las categorias
    Route::middleware(['auth:admin', 'state'])->get('/categorias',[PremioController::class,'categorias'])->name('premio.categoria.list');
    Route::middleware(['auth:admin', 'state', 'store'])->post('/categorias/store',[PremioController::class,'storeCategoria'])->name('premio.categoria.store');
    Route::middleware(['auth:admin', 'state', 'update'])->post('/categorias/update',[PremioController::class,'updateCategoria'])->name('premio.categoria.update');
    Route::middleware(['auth:admin', 'state', 'destroy'])->post('/categorias/delete/{id?}',[PremioController::class,'destroyCategoria'])->name('premio.categoria.destroy');
    Route::get('/categorias/getImage/{filename?}', [PremioController::class, 'getImage'])->name('premio.categoria.icon');
});

//AJAX PARA SOLICITAR DATOS DE QUANTUM
Route::prefix('/quantum')->group(function(){
    Route::middleware(['auth:admin', 'state'])->get('/marcas',[QuantumController::class,'QuantumBrands'])->name('quantum.marcas');
    Route::middleware(['auth:admin', 'state'])->get('/productos/{id?}', [QuantumController::class, 'QuantumProducts'])->name('quantum.productos');
    Route::get('/documento/{filename?}', [QuantumController::class, 'QuantumDocument'])->name('quantum.pdf');
});

//RUTAS PARA LOS PREMIOS CANJEADOS
Route::prefix('/canjeados')->group(function(){
    Route::middleware(['auth:admin', 'state'])->get('/list/{pendiente?}', [PremioController::class, 'canjeos'])->name('canjeado.index');
    Route::middleware(['auth:admin', 'state'])->get('/edit/{id}',[PremioController::class,'editCanjeado'])->name('canjeado.edit');
    Route::middleware(['auth:admin', 'state'])->get('/entrega/{id}',[PremioController::class,'entrega'])->name('canjeado.datos');
    Route::middleware(['auth:admin', 'state', 'update'])->post('/update/{id}',[PremioController::class,'updateCanjeado'])->name('canjeado.update');
});

//RUTAS PARA LAS CAPACITACIONES
Route::prefix('/capacitaciones')->group(function(){
    Route::middleware(['auth:admin', 'state'])->get('/general/',[CapacitacionController::class,'index'])->name('capacitacion.index');
    Route::middleware(['auth:admin', 'state'])->get('/general/listado/{id_producto?}/{id_patologia?}',[CapacitacionController::class,'capacitaciones'])->name('capacitacion.list');
    Route::middleware(['auth:admin', 'state'])->get('/general/entrenamiento/{id}',[CapacitacionController::class,'entrenamientos'])->name('capacitacion.entrenamiento');
    Route::middleware(['auth:admin', 'state'])->post('/trivias/cliente/{id?}',[CapacitacionController::class,'clienteDetail'])->name('capacitacion.cliente');

    Route::middleware(['auth:admin', 'state'])->get('/general/edit/{id}',[CapacitacionController::class,'edit'])->name('capacitacion.edit');
    Route::middleware(['auth:admin', 'state'])->get('/general/delete/{id}',[CapacitacionController::class,'delete'] )->name('capacitacion.delete');
    
    Route::middleware(['auth:admin', 'state', 'store'])->post('/store',[CapacitacionController::class,'store'])->name('capacitacion.store');
    Route::middleware(['auth:admin', 'state', 'update'])->post('/update/{id}',[CapacitacionController::class,'update'])->name('capacitacion.update');
    Route::middleware(['auth:admin', 'state', 'destroy'])->delete('/destroy/{id}',[CapacitacionController::class,'destroy'])->name('capacitacion.destroy');

    Route::get('/getArchive/{filename?}', [CapacitacionController::class, 'getArchive'])->name('capacitacion.archive');
    Route::get('/video/{filename?}',[CapacitacionController::class,'getVideo'])->name('stream.video');

    //Entrenamientos por Produtcos
    Route::middleware(['auth:admin', 'state'])->get('/productos/listado',[CapacitacionController::class,'capProductos'])->name('capacitacion.producto');

    //Entrenamientos por Patologias
    Route::middleware(['auth:admin', 'state'])->get('/patologias/listado',[CapacitacionController::class,'capPatologias'])->name('capacitacion.patologia');

    //Visual de cuantos usuarios las han visto
    Route::middleware(['auth:admin', 'state'])->get('/usuarios/listado',[CapacitacionController::class,'usuarios'])->name('capacitacion.usuario');
    Route::middleware(['auth:admin', 'state'])->post('/usuarios/cuestionarios/{id?}',[CapacitacionController::class,'usuarioDetail'])->name('capacitacion.cliente');
}); 

/*CONTROLADORES REDIRECCIONAR VISTAS*/
    Route::get('/reportes',[RutasController::class,'reportes'])->name('reportes.index');
    Route::get('/reportes/filtros',[RutasController::class,'filtros'])->name('reportes.filtros');
/*FIN CONTROLADORES*/
