 <!-- SIDEBAR -->
 @include('layouts.admin.components.head')
 <!-- FIN SIDEBAR -->

 <body class="g-sidenav-show  bg-gray-100">

     <!-- SIDEBAR -->
     @include('layouts.admin.components.sidebar')
     <!-- FIN SIDEBAR -->
     <!-- NAVBAR -->
     @include('layouts.admin.components.navbar')
     <!-- FIN NAVBAR -->
     <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">



         <div class="container p-0 my-5">


             <!-- CONTENIDO PRINCIPAL -->
             @yield('content')

         </div>

     </main>


     <!--   Core JS Files   -->
     <script src="{{ url('js/app.js') }}"></script>

     <!-- END (SCRIPTS) -->
     @include('layouts.admin.components.end')
     <!-- FIN END (SCRIPTS) -->

 </body>

 </html>
