<nav class="navbar navbar-main navbar-expand-lg px-0 shadow-none border-radius-xl bg-gray-10 bg-gray-100" id="navbarBlur"
            navbar-scroll="true" style="z-index: 100000; width: 80%; margin-left: 15%">
            <div class="container-fluid py-1 px-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                        <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Panel</a>
                        </li>
                        <li class="breadcrumb-item text-sm text-dark active" aria-current="page">
                            @if(request()->is('portal')) Inicio
                            @elseif(request()->is('perfil')) Perfil
                            @elseif(request()->is('usuarios/' , 'admin/')) Administración de Usuarios
                            @elseif(request()->is('notificaciones/*')) Panel de Notificaciones
                            @elseif(request()->is('informativo/inicio')) Panel Informativo
                            @elseif(request()->is('informativo/inicio/listbanners')) Panel de Banners
                            @elseif(request()->is('noticia/*')) Panel de Noticias
                            @elseif(request()->is('facturas/*')) Panel de Facturas
                            @elseif(request()->is('training/*')) Panel de Capacitaciones
                            @elseif(request()->is('trivias/*')) Panel de Trivias
                            @elseif(request()->is('encuestas/*')) Panel de Encuestas
                            @elseif(request()->is('premios/*')) Panel de Premios
                            @endif
                        </li>
                    </ol>
                    <h6 class="font-weight-bolder mb-0">
                        @if(request()->is('portal')) Inicio
                        @elseif(request()->is('perfil')) Perfil
                        @elseif(request()->is('usuarios/' , 'admin/')) Administración de Usuarios
                        @elseif(request()->is('notificaciones/*')) Panel de Notificaciones
                        @elseif(request()->is('informativo/inicio')) Panel Informativo
                        @elseif(request()->is('informativo/inicio/listbanners')) Panel de Banners
                        @elseif(request()->is('noticia/*')) Panel de Noticias
                        @elseif(request()->is('facturas/*')) Panel de Facturas
                        @elseif(request()->is('training/*')) Panel de Capacitaciones
                        @elseif(request()->is('trivias/*')) Panel de Trivias
                        @elseif(request()->is('encuestas/*')) Panel de Encuestas
                        @elseif(request()->is('premios/*')) Panel de Premios
                        @endif
                    </h6>
                </nav>
                <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                    <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                        <div class="input-group ">
                            <span class="input-group-text text-body"><i class="fas fa-search"
                                    aria-hidden="true"></i></span>
                            <input type="text" class="form-control px-3" placeholder="Buscar">
                        </div>
                    </div>
                    <ul class="navbar-nav  justify-content-end">
                        <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
                            <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                                <div class="sidenav-toggler-inner">
                                    <i class="sidenav-toggler-line"></i>
                                    <i class="sidenav-toggler-line"></i>
                                    <i class="sidenav-toggler-line"></i>
                                </div>
                            </a>
                        </li>
                       
                        
                        <li class="nav-item dropdown pe-2 d-flex align-items-center mx-3">
                            <a href="javascript:;" class="nav-link text-body p-0" id="dropdownMenuButton"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-bell cursor-pointer" style="font-size: 25px"></i>
                            </a>
                            <ul class="dropdown-menu  dropdown-menu-end  px-2 py-3 me-sm-n4"
                                aria-labelledby="dropdownMenuButton">
                                <li class="mb-2">
                                    <a class="dropdown-item border-radius-md" href="javascript:;">
                                        <div class="d-flex py-1">
                                            <div class="my-auto">
                                                <img src="../assets/img/team-2.jpg" class="avatar avatar-sm  me-3 ">
                                            </div>
                                            <div class="d-flex flex-column justify-content-center">
                                                <h6 class="text-sm font-weight-normal mb-1">
                                                    <span class="font-weight-bold">New message</span> from Laur
                                                </h6>
                                                <p class="text-xs text-secondary mb-0">
                                                    <i class="fa fa-clock me-1"></i>
                                                    13 minutes ago
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown pe-2 d-flex align-items-center">
                            <a href="javascript:;" class="nav-link text-body p-0" id="dropdownMenuButton"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                <div class="row-reverse">
                                    <div class="col d-flex justify-content-center">
                                        <img src="https://www.imbanaco.com/wp-content/uploads/2017/04/avatar-especialista-900x900.png" alt="" width="50" height="50">
                                    </div>
                                    <div class="col d-flex justify-content-center"><span>Admin</span></div>
                                </div>
                            </a>
                            <ul class="dropdown-menu  dropdown-menu-end  px-2 py-3 me-sm-n4"
                                aria-labelledby="dropdownMenuButton">
                                <li class="mb-1">
                                    <a class="dropdown-item border-radius-md" href="{{route('dashboard.perfil')}}">
                                        <div class="d-flex justify-content-center py-1">
                                            <span>Mi Perfil</span>
                                        </div>
                                    </a>
                                </li>
                                <hr>
                                @if(auth()->user()->roles->nombre == "Super Admin")
                                <li class="mb-1">
                                    <a class="dropdown-item border-radius-md" href="{{-- route('configuracion.index') --}}javascript:;">
                                        <div class="d-flex justify-content-center py-1">
                                            <span>Ajustes</span>
                                        </div>
                                    </a>
                                </li>
                                @endif
                                <hr>
                                <li class="mb-1">
                                    <a class="dropdown-item border-radius-md" href="javascript:;">
                                        <div class="d-flex justify-content-center py-1">
                                            <span>Cerrar Sesión</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <style>
            @media screen and (max-width: 1199px){
                #navbarBlur{
                    width: 100vw !important;
                    margin: 0% !important;
                    z-index: 0 !important;
                }
            }

            @media screen and (min-width: 1200px) and (max-width: 1800px){
                #navbarBlur{
                    width: 75vw !important;
                    margin-left: 23% !important;
                }
            }
        </style>