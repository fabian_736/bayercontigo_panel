@extends('layouts.admin.app')

@section('content')
    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        @if (Session::has('edit'))
            <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                {{ Session::get('edit') }}
            </div>
        @endif

        <div class="row mb-5 mx-auto">
            <!-- Editar Noticia -->
            <div class="col-md-12 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">


                <div class="card-header mt-3" style="background-color: #e7344c;">
                    <h5 class="text-white">Editar Noticia</h5>
                </div>


                <form action="{{ route('noticias.update',$noticia->id_noticia) }}" method="post" enctype="multipart/form-data">

                    @csrf
                    <div class="row col-md-12 my-4">
                        <div class="col">
                            <label for="name_user">Título:</label>
                            <input name="titulo" id="" type="text" placeholder="Título de la Noticia" value="{{ old('titulo', $noticia->titulo) }}"
                                class="form-control @error('titulo') is-invalid @enderror">
                            @error('titulo')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 mb-3">
                        <input type="hidden" name="selectedTipo" id="selectedTipo">
                        <div class="col-md-6">
                            <label for="name_user">Tipo de Noticia:</label>
                            <input id="ntipo" type="text" name="" disabled value="{{ $noticia->tipo->nombre }}" class="form-control">
                            <input type="hidden" name="FK_id_tipo" value="{{ $noticia->FK_id_tipo }}">
                            @error('FK_id_tipo')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Categoría:</label>
                            <input name="categoria" id="" type="text" placeholder="Categoría de la Noticia" value="{{ old('categoria', $noticia->categoria) }}"
                                class="form-control @error('categoria') is-invalid @enderror">
                            @error('categoria')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 mb-3">
                        <div class="col">
                            <label for="name_user">Descripción de la Noticia:</label>
                            <textarea class="form-control @error('descripcion') is-invalid @enderror" 
                            name="descripcion" id="" type="text" >{{ old('descripcion', $noticia->descripcion) }}</textarea>
                            @error('descripcion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 mb-3">
                        <div class="col">
                            <label>Contenido de la Noticia (Opcional):</label>
                            <textarea class="form-control @error('contenido') is-invalid @enderror" 
                            name="contenido" id="" type="text">{{ old('contenido', $noticia->contenido) }}</textarea>
                            @error('contenido')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 mb-3">
                        <div class="col-md-6">
                            <label for="">Imagen Preview de Noticia:</label>
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input @error('imagen') is-invalid @enderror" 
                                id="customImage" name="imagen" accept=".jpg,.jpeg,.png" value="">
                                <label class="custom-file-label" for="customImage">Escoger una Imagen</label>
                                @error('imagen')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">Subir Documento (imagen, pdf o video) si posee:</label>
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input @error('documento') is-invalid @enderror" 
                                id="customFile" name="documento" accept=".jpg,.jpeg,.png,.pdf,.mp4" value="">
                                <label class="custom-file-label" for="customFile">Escoger un Documento</label>
                                @error('documento')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row col-md-12 mb-3">
                        <div class="col">
                            <label for="">Estado de la Noticia:</label><br>
                            <div class="form-check">
                                <input value="1" class="" type="radio" name="estado" id="flexRadioDefault1"
                                    {{ old('estado', $noticia->estado) ? "checked" : "" }}>
                                <label class="form-check-label" for="flexRadioDefault1">
                                    Noticia Activada
                                </label>
                            </div>
                            <div class="form-check">
                                <input value="0" class="" type="radio" name="estado" id="flexRadioDefault2"
                                    {{ old('estado', $noticia->estado) ? "" : "checked" }}>
                                <label class="form-check-label" for="flexRadioDefault2">
                                    Noticia Desactivada
                                </label>
                            </div>
                        </div>
                    </div>

                    @error('ingredientes')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                    @error('directorio')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                    <!-- DIRECTORIO -->
                    <div id="portafolio" class="d-none">
                        @if(!$profesionales->isEmpty())
                            @if(!$noticia->directorios->isEmpty())
                                <label for="" style="color: #51A2A7; font-weight: bold">Directorio</label>
                                <ul class="text-dark">
                                    @foreach($noticia->directorios as $persona)
                                        <li>{{ $persona->profesional->nombre.' '.$persona->profesional->apellido.' | '.$persona->profesional->especialidad }}</li>
                                    @endforeach
                                </ul>

                                <label for="">Deseas Modificar el Directorio?</label>
                                <div class="form-check">
                                    <input value="1" class="" type="radio" name="selectDirect" id="flexRD1">
                                    <label class="form-check-label" for="flexRD1">
                                        Si
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input value="0" class="" type="radio" name="selectDirect" id="flexRD2" checked>
                                    <label class="form-check-label" for="flexRD2">
                                        No
                                    </label>
                                </div>
                            @endif

                            <div id="editDirectorios" class="{{ !$noticia->directorios->isEmpty() ? 'd-none' : '' }}">

                                <label for="" style="color: #51A2A7; font-weight: bold">Posee Directorios Anclado?</label>
                                <div class="form-check form-check-radio form-check-inline ml-3 mr-3">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="portafolio"
                                            id="inlineRadio1" value="1" {{ $noticia->directorios->isEmpty() ? "" : "checked" }}> Si
                                        <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                                <div class="form-check form-check-radio form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="portafolio" id="inlineRadio1"
                                            value="0" {{ $noticia->directorios->isEmpty() ? "checked" : "" }}> No
                                        <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>

                                <!-- PROFESIONALES -->
                                <div class="d-none my-3" id="directorios">
                                    <label for="" style="color: #51A2A7; font-weight: bold">Selecciona al Profesional</label>
                                    <div class="form-row">
                                        <div class="col">
                                            <select id="persona" class="form-control">
                                                @foreach($profesionales as $pro)
                                                    <option value="{{ $pro->id_profesional }}">
                                                        {{ $pro->nombre.' '.$pro->apellido.' | '.$pro->especialidad }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <input style="background-color: #51A2A7" class="btn btn-sm" type="button" title="Agregar Producto" Value="+" id="agregar">
                                            <input style="background-color: #51A2A7" class="btn btn-sm" type="button" title="Borrar Producto" Value="-" id="borrar">
                                        </div>
                                    </div> 

                                    <div class="form-row">
                                        <?php $inc = 0 ?>
                                        <div class="col" id="addPersona">
                                        @if($noticia->directorios)
                                            @foreach($noticia->directorios as $persona)
                                                <?php $inc++ ?>
                                                <div id='per{{$inc}}'>
                                                    <select readonly name='directorio[]' class='form-control'>
                                                        <option value='{{$persona->profesional->id_profesional}}' selected>
                                                            {{ $persona->profesional->nombre.' '.$persona->profesional->apellido.' | '.$persona->profesional->especialidad }}
                                                        </option>
                                                    </select>
                                                </div>
                                            @endforeach
                                        @endif
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
                        @endif
                    </div>
                    <!-- FIN DIRECTORIO -->

                    <!-- INGREDIENTES -->
                    <div id="ingredientes" class="d-none">
                        <label for="" style="color: #51A2A7; font-weight: bold">Ingredientes</label>
                        <ul class="text-dark">
                            @foreach($noticia->items as $item)
                                <li>{{ $item->item }}</li>
                            @endforeach
                        </ul>
                        
                        <label for="">Deseas Modificar los Ingredientes?</label>
                        <div class="form-check">
                            <input value="1" class="" type="radio" name="selectIngrediente" id="flexRI1">
                            <label class="form-check-label" for="flexRI1">
                                Si
                            </label>
                        </div>
                        <div class="form-check">
                            <input value="0" class="" type="radio" name="selectIngrediente" id="flexRI2" checked>
                            <label class="form-check-label" for="flexRI2">
                                No
                            </label>
                        </div>

                        <div id="editIngredientes" class="d-none">
                            <label for="" style="color: #51A2A7; font-weight: bold">Ingresa los Ingredientes</label>
                            <div class="form-row my-3">
                                <div class="col">
                                    <input type="text" id="in_ingrediente" class="form-control" placeholder="Ingrediente - Cantidad">
                                    <input style="background-color: #51A2A7" class="btn btn-sm" type="button" title="Agregar Producto" Value="+" id="agregar-ingrediente">
                                    <input style="background-color: #51A2A7" class="btn btn-sm" type="button" title="Borrar Producto" Value="-" id="borrar-ingrediente">
                                </div>
                            </div> 

                            <div class="form-row">
                                <?php $increment = 0 ?>
                                <div class="col" id="addIngredient">
                                @if($noticia->items)
                                    @foreach($noticia->items as $item)
                                        <?php $increment++ ?>
                                        <input id='ing{{$increment}}' readonly name='ingredientes[]' class='form-control' value='{{ $item->item }}'/>
                                    @endforeach
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- FIN INGREDIENTES -->

                    <hr>
                    <div class="row col-md-12 my-3">
                        <div class="col-md-6 mx-auto">
                            <button type="submit" class="form-control text-white" style="background-color: #64C2C8">Editar Noticia</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
    <input type="hidden" id="incDirectorio" value="{{ $inc }}">
    <input type="hidden" id="incItem" value="{{ $increment }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {

        $("#ntipo").change(function(){
            var selectValue = $("#ntipo option:selected").text();
            $("#selectedTipo").val(selectValue);

            if(selectValue == "Salud"){
                $("#portafolio").removeClass("d-none");
                $("#ingredientes").addClass("d-none");
            }
            else if(selectValue == "Receta"){
                $("#portafolio").addClass("d-none");
                $("#ingredientes").removeClass("d-none");
            }
            else if(selectValue == "Tip de Moda"){
                $("#portafolio").addClass("d-none");
                $("#ingredientes").addClass("d-none");
            }
        });

        /* PROCESO DE INGRESAR O NO DIRECTORIO */
            $("input[name=selectDirect]").change(function(){
                var value = $("input:radio[name=selectDirect]:checked").val();
                value == 1 ? $("#editDirectorios").removeClass("d-none") : $("#editDirectorios").addClass("d-none");
            });

            var directInitial = $("input[name='portafolio']:checked").val();
            if(directInitial == 1)
                $("#directorios").removeClass("d-none");
            else
                $("#directorios").addClass("d-none");

            $("input[name='portafolio']").change(function(){
                var radioValue = $("input[name='portafolio']:checked").val();
                if(radioValue == 1)
                    $("#directorios").removeClass("d-none");
                else
                    $("#directorios").addClass("d-none");
            });
            
            var inc = $("#incDirectorio").val();
            $('#agregar').click(function(){
                inc++;
                var id = $('#persona').val();
                var value = $("#persona option:selected").text();
                $('#addPersona').append("<div id='per"+inc+"'><select readonly name='directorio[]' class='form-control'><option value='"+id+"' selected>"+value+"</option></select></div>");
            });

            $('#borrar').click(function(){
                if(inc > 0){
                    console.log('delete');
                    $('#per'+inc).remove();
                    inc--;
                }
            });
        /* FIN PROCESO DE INGRESAR O NO DIRECTORIO */

        /* PROCESO DE INGRESAR DINAMICAMENTE RECETAS */
            $("input[name=selectIngrediente]").change(function(){
                var value = $("input:radio[name=selectIngrediente]:checked").val();
                value == 1 ? $("#editIngredientes").removeClass("d-none") : $("#editIngredientes").addClass("d-none");
            });

            var inc_ing = $("#incItem").val();
            $('#agregar-ingrediente').click(function(){
                inc_ing++;
                var value = $('#in_ingrediente').val();
                if(value)
                    $('#addIngredient').append("<input id='ing"+inc_ing+"' readonly name='ingredientes[]' class='form-control' value='"+value+"'/>");
            });

            $('#borrar-ingrediente').click(function(){
                if(inc_ing > 0){
                    console.log('delete');
                    $('#ing'+inc_ing).remove();
                    inc_ing--;
                }
            });
        /* FIN PROCESO DE INGRESAR DINAMICAMENTE RECETAS */

        /* SETUP INICIAL POR SI DEVUELVE VALIDACIONES */
            var selectValue = $("#ntipo").val();
            $("#selectedTipo").val(selectValue);

            if(selectValue == "Salud"){
                $("#portafolio").removeClass("d-none");
                $("#ingredientes").addClass("d-none");
            }
            else if(selectValue == "Receta"){
                $("#portafolio").addClass("d-none");
                $("#ingredientes").removeClass("d-none");
            }
            else if(selectValue == "Tip de Moda"){
                $("#portafolio").addClass("d-none");
                $("#ingredientes").addClass("d-none");
            }
        /* FIN SETUP INICIAL */

        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }
    });
</script>