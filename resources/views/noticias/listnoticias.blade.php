@extends('layouts.admin.app')

@section('content')

@if (Session::has('error'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('error') }}
    </div>
@endif

@if (Session::has('create'))
    <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('create') }}
    </div>
@endif

@if (Session::has('delete'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('delete') }}
    </div>
@endif

@if (Session::has('edit'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        {{ Session::get('edit') }}
    </div>
@endif

<label for="" class="lead mx-auto" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #51A2A7">Todas las noticias</label>


<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table id="example" class="table table-striped " cellspacing="0" width="100%">
                <thead style="background-color: #FFFFFF; color: #51A2A7; font-weight: bold">
                    <tr>
                        <th>Imagen</th>
                        <th>Likes</th>
                        <th>Título</th>
                        <th>Categoría</th>
                        <th>Tipo</th>
                        <th>Estado</th>
                        <th>Detallado</th>
                        <th>Documento</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody class="table-bordered">
                    @foreach($noticias as $noticia)
                    <tr>
                        <td class="mx-auto text-center" > 
                            <img style="width:100px;max-height:100px;" 
                                src="{{ route('noticias.image', ['filename' => basename($noticia->imagen)]) }}" >
                        </td>
                        <td>
                            <i class="fas fa-thumbs-up mx-1 my-auto" style="font-size: 20px; color: #51A2A7;"></i>
                            <span class="font-weight-bold">{{ $noticia->likes->count() }}</span>
                        </td>
                        <td>{{ $noticia->titulo }}</td>
                        <td>{{ $noticia->categoria }}</td>
                        <td>{{ $noticia->tipo->nombre }}</td>
                        <td>
                            <span class='{{ $noticia->estado ? "text-success" : "text-danger" }}'>
                                {{ $noticia->estado ? "Activa" : "Inactiva" }}
                            </span>
                        </td>
                        <td class="text-center">
                            <a id="{{ route('noticias.detail', $noticia->id_noticia) }}"
                                class="btn btn-sm btn-danger mx-auto w_open_detail text-white">
                                Ver Detallado <i class="fas fa-eye"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            @if($noticia->documento)
                                <a id="{{ route('noticias.document', ['filename' => basename($noticia->documento)]) }}" 
                                    target="_blank" class="w_open btn btn-sm btn-danger mx-auto text-white">
                                    @switch(pathinfo($noticia->documento)['extension'])
                                        @case('jpg') @case('jpeg') @case('png') <i class="fas fa-image"></i> @break
                                        @case('pdf') <i class="fa fa-file-pdf-o"></i> @break
                                        @case('mp4') @case('wm') @case('wmv') <i class="fas fa-video"></i> @break
                                    @endswitch
                                </a>
                            @else() 
                                <span>No posee...</span>
                            @endif
                        </td>
                        <td class="d-flex justify-content-around">
                            <a href="{{route('noticias.edit',$noticia->id_noticia)}}" style="color: #51A2A7;"> <i class="far fa-edit"></i></a>
                            <a href="{{route('noticias.confirm',$noticia->id_noticia)}}" style="color: #51A2A7;"><i class="far fa-trash-alt"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <button class="btn btn-round" style="background-color: #51A2A7" data-toggle="modal"
                data-target=".bd-example-modal-lg">
                <i class="material-icons">add</i>CREAR NOTICIA
            </button>

    <!-- MODAL CREAR NOTICIA -->
    <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                        Creacion de noticias</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('noticias.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="col">
                                <input name="titulo" id="" type="text" placeholder="Título de la Noticia" value="{{ old('titulo') }}"
                                    class="form-control @error('titulo') is-invalid @enderror">
                                @error('titulo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <input type="hidden" name="selectedTipo" id="selectedTipo">
                            <div class="col">
                                <select name="FK_id_tipo" id="ntipo" class="form-control @error('FK_id_tipo') is-invalid @enderror">
                                    <option value="" selected disabled>Seleccione tipo de Noticia...</option>
                                    @foreach($tipos as $tipo)
                                        <option value="{{ $tipo->id_noticia_tipo }}" {{ old('FK_id_tipo') == $tipo->id_noticia_tipo ? "selected" : "" }}>{{ $tipo->nombre }}</option>
                                    @endforeach
                                </select>
                                @error('FK_id_tipo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col">
                                <input name="categoria" id="" type="text" placeholder="Categoría de la Noticia" value="{{ old('categoria') }}"
                                    class="form-control @error('categoria') is-invalid @enderror">
                                @error('categoria')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <div class="col-12 mb-2">
                                <label>Descripción de la Noticia:</label>
                                <textarea class="form-control @error('descripcion') is-invalid @enderror" 
                                name="descripcion" id="" type="text" >{{ old('descripcion') }}</textarea>
                                @error('descripcion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col">
                                <label>Contenido de la Noticia (Opcional):</label>
                                <textarea class="form-control @error('contenido') is-invalid @enderror" 
                                name="contenido" id="" type="text">{{ old('contenido') }}</textarea>
                                @error('contenido')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <div class="col">
                                <label for="">Imagen Preview de Noticia:</label>
                                <input type="file" name="imagen" accept=".jpg,.jpeg,.png" required
                                    class="form-control @error('imagen') is-invalid @enderror" id="">
                                @error('imagen')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col">
                                <label for="">Subir Documento (imagen, pdf o video) si posee:</label>
                                <input type="file" name="documento" accept=".jpg,.jpeg,.png,.pdf,.mp4"
                                    class="form-control @error('documento') is-invalid @enderror" id="">
                                @error('documento')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <div class="col">
                                <label for="">Estado:</label><br>
                                <div class="form-check form-check-radio form-check-inline ml-3 mr-3">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="estado" checked
                                            id="inlineRadio1" value="1"> Noticia Activada
                                        <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                                <div class="form-check form-check-radio form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="estado" id="inlineRadio1"
                                            value="0"> Noticia Desactivada
                                        <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                        @error('ingredientes')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                        @error('directorio')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                        <!-- DIRECTORIO -->
                        <div id="portafolio" class="d-none">
                            @if(!$profesionales->isEmpty())
                                <label for="" style="color: #51A2A7; font-weight: bold">Posee Directorios Anclado?</label>
                                <div class="form-check form-check-radio form-check-inline ml-3 mr-3">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="portafolio"
                                            id="inlineRadio1" value="1"> Si
                                        <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                                <div class="form-check form-check-radio form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="portafolio" id="inlineRadio1"
                                            value="0"> No
                                        <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>

                                <!-- PROFESIONALES -->
                                <div class="d-none my-3" id="directorios">
                                    <label for="" style="color: #51A2A7; font-weight: bold">Selecciona al Profesional</label>
                                    <div class="form-row">
                                        <div class="col">
                                            <select id="persona" class="form-control">
                                                @foreach($profesionales as $pro)
                                                    <option value="{{ $pro->id_profesional }}">
                                                        {{ $pro->nombre.' '.$pro->apellido.' | '.$pro->especialidad }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <input style="background-color: #51A2A7" class="btn btn-sm" type="button" title="Agregar Producto" Value="+" id="agregar">
                                            <input style="background-color: #51A2A7" class="btn btn-sm" type="button" title="Borrar Producto" Value="-" id="borrar">
                                        </div>
                                    </div> 

                                    <div class="form-row">
                                        <div class="col" id="addPersona"></div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <!-- FIN DIRECTORIO -->

                        <!-- INGREDIENTES -->
                        <div id="ingredientes" class="d-none">
                            <label for="" style="color: #51A2A7; font-weight: bold">Ingresa los Ingredientes</label>
                            <div class="form-row my-3">
                                <div class="col">
                                    <input type="text" id="in_ingrediente" class="form-control" placeholder="Ingrediente - Cantidad">
                                    <input style="background-color: #51A2A7" class="btn btn-sm" type="button" title="Agregar Producto" Value="+" id="agregar-ingrediente">
                                    <input style="background-color: #51A2A7" class="btn btn-sm" type="button" title="Borrar Producto" Value="-" id="borrar-ingrediente">
                                </div>
                            </div> 

                            <div class="form-row">
                                <div class="col" id="addIngredient"></div>
                            </div>
                        </div>
                        <!-- FIN INGREDIENTES -->
                </div>

                <div class="modal-footer">
                    <input class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;"
                        type="submit" value="CREAR NOTICIA">
                </div>
                </form>
            </div>
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        $(".w_open").click(function() {
            var id = $(this).attr("id");
            window.open(id, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=800,height=800");
        });

        $(".w_open_detail").click(function() {
            var id = $(this).attr("id");
            window.open(id, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=600,height=500");
        });

        $("#ntipo").change(function(){
            var selectValue = $("#ntipo option:selected").text();
            $("#selectedTipo").val(selectValue);

            if(selectValue == "Salud"){
                $("#portafolio").removeClass("d-none");
                $("#ingredientes").addClass("d-none");
            }
            else if(selectValue == "Receta"){
                $("#portafolio").addClass("d-none");
                $("#ingredientes").removeClass("d-none");
            }
            else if(selectValue == "Tip de Moda"){
                $("#portafolio").addClass("d-none");
                $("#ingredientes").addClass("d-none");
            }
        });

        /* PROCESO DE INGRESAR O NO DIRECTORIO */
            $("input[name='portafolio']").change(function(){
                var radioValue = $("input[name='portafolio']:checked").val();
                if(radioValue == 1)
                    $("#directorios").removeClass("d-none");
                else
                    $("#directorios").addClass("d-none");
            });
            
            var inc = 0;
            $('#agregar').click(function(){
                inc++;
                var id = $('#persona').val();
                var value = $("#persona option:selected").text();
                $('#addPersona').append("<div id='per"+inc+"'><select readonly name='directorio[]' class='form-control'><option value='"+id+"' selected>"+value+"</option></select></div>");
            });

            $('#borrar').click(function(){
                if(inc > 0){
                    console.log('delete');
                    $('#per'+inc).remove();
                    inc--;
                }
            });
        /* FIN PROCESO DE INGRESAR O NO DIRECTORIO */

        /* PROCESO DE INGRESAR DINAMICAMENTE RECETAS */
            var inc_ing = 0;
            $('#agregar-ingrediente').click(function(){
                inc_ing++;
                var value = $('#in_ingrediente').val();
                if(value)
                    $('#addIngredient').append("<input id='ing"+inc_ing+"' readonly name='ingredientes[]' class='form-control' value='"+value+"'/>");
            });

            $('#borrar-ingrediente').click(function(){
                if(inc_ing > 0){
                    console.log('delete');
                    $('#ing'+inc_ing).remove();
                    inc_ing--;
                }
            });
        /* FIN PROCESO DE INGRESAR DINAMICAMENTE RECETAS */

        /* SETUP INICIAL POR SI DEVUELVE VALIDACIONES */
            var selectValue = $("#ntipo option:selected").text();
            $("#selectedTipo").val(selectValue);

            if(selectValue == "Salud"){
                $("#portafolio").removeClass("d-none");
                $("#ingredientes").addClass("d-none");
            }
            else if(selectValue == "Receta"){
                $("#portafolio").addClass("d-none");
                $("#ingredientes").removeClass("d-none");
            }
            else if(selectValue == "Tip de Moda"){
                $("#portafolio").addClass("d-none");
                $("#ingredientes").addClass("d-none");
            }
        /* FIN SETUP INICIAL */

        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
            $('#exampleModal').modal('show');
        }
    });
</script>