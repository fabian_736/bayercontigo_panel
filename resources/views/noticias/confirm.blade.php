@extends('layouts.admin.app')
@section('content')
    <div class="container py-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        <h1 class="mb-5">¿Deseas eliminar la siguiente noticia? </h1>
        <h4>Título: </h4>
        <p>{{ $noticia->titulo }}</p>
        <h4>Tipo: </h4>
        <p>{{ $noticia->tipo->nombre }}</p>
        <h4>Categoría: </h4>
        <p>{{ $noticia->categoria }}</p>
        <h4>Descripción: </h4>
        <p>{{ $noticia->descripcion }}</p>
        <h4>Imagen Preview: </h4>
        <p><img style="width:200px;max-height:200px;" src="{{ route('noticias.image', ['filename' => basename($noticia->imagen)]) }}" ></p>
        <h4>Documento: </h4>
        <p>
            @if($noticia->documento)
                <a href="{{ route('noticias.document', ['filename' => basename($noticia->documento)]) }}" target="_blank" class="btn btn-sm btn-danger mx-auto">
                    @switch(pathinfo($noticia->documento)['extension'])
                        @case('jpg') @case('jpeg') @case('png') <i class="fas fa-image"></i> @break
                        @case('pdf') <i class="fa fa-file-pdf-o"></i> @break
                        @case('mp4') @case('wm') @case('wmv') <i class="fas fa-video"></i> @break
                    @endswitch
                </a>
            @else() 
                <span>No posee...</span>
            @endif
        </p>

        <form method="post" enctype="multipart/form-data" action="{{ route('noticias.destroy', $noticia->id_noticia) }}">
            @method('DELETE')
            @csrf
            <div class="row col-md-12 mt-5 mx-auto">
                <button type="submit" class="redondo btn btn-danger col-md-4 mx-auto">
                    <i class="fas fa-trash-alt"></i> Eliminar
                </button>
                <a href="{{route('noticias.list')}}" class="btn btn-primary col-md-4 mx-auto">Volver</a>
            </div>
        </form>
    </div>
@endsection
