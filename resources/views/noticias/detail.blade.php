<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/x-icon" href="{{url('img/favicon.ico')}}" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>


    <title>Bayer Contigo</title>

    <link rel="stylesheet" href="{{url('css/app.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css"  href="{{url('DataTables/DataTables-1.10.24/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('DataTables/Buttons-1.7.0/css/buttons.bootstrap4.min.css')}}">

</head>

<body id="page-top">
<div class="row">
    <div class="modal-dialog modal-lg w-100">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    Datos de la Noticia<br>
                    {{ $noticia->titulo }}
                </h5>
            </div>
            <div class="modal-body">
            
                <div class="col-md-12 my-3">
                    <div class="col">
                        <label for="name_user">Descripción:</label>
                        <p>{{ $noticia->descripcion }}</p>
                    </div>
                </div>

                <div class="col-md-12 my-3">
                    <div class="col">
                        <label for="name_user">Contenido:</label>
                        <p>{{ $noticia->contenido }}</p>
                    </div>
                </div>

                @if(!$noticia->items->isEmpty())
                    <div class="col-md-12 my-3">
                        <div class="col">
                            <label for="" style="color: #51A2A7; font-weight: bold">Ingredientes</label>
                            <ul>
                            @foreach($noticia->items as $item)
                                <li>{{ $item->item }}</li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                @endif

                @if(!$noticia->directorios->isEmpty())
                    <div class="col-md-12 my-3">
                        <div class="col">
                            <label for="" style="color: #51A2A7; font-weight: bold">Directorio</label>
                            @foreach($noticia->directorios as $directorio)
                                <div class="card m-0 mb-2 p-2">
                                    <span class="font-weight-bold">Nombre:</span>{{ $directorio->profesional->nombre.' '.$directorio->profesional->apellido }}
                                    <span class="font-weight-bold">Especialidad:</span>{{ $directorio->profesional->especialidad }}
                                    <span class="font-weight-bold">Teléfono:</span>{{ $directorio->profesional->telefono }}
                                    <span class="font-weight-bold">Dirección:</span>
                                    <p>{{ $directorio->profesional->direccion }}</p>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>

        </div>
    </div>
</div>
</body>
