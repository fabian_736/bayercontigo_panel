@extends('layouts.admin.app')
@section('content')
    <div class="container py-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        @if(auth()->user()->FK_id_adminrol == 1)
            <h1 class="mb-5">¿Deseas eliminar el siguiente usuario? </h1>
            <h4>Nombre: </h4>
            <p>{{ $admin->nombre }}</p>
            <h4>Apellido: </h4>
            <p>{{ $admin->apellido }}</p>
            <h4>Rol: </h4>
            <p>{{ $admin->roles->nombre }}</p>

            <form method="post" enctype="multipart/form-data" action="{{ route('admin.destroy', $admin->id_useradmin) }}">
                @method('DELETE')
                @csrf
                <div class="row col-md-12 mt-5 mx-auto">
                    <button type="submit" class="redondo btn btn-danger col-md-4 mx-auto">
                        <i class="fas fa-trash-alt"></i> Eliminar
                    </button>
                    <a href="{{ route('admin.listadmin') }}" class="btn btn-primary col-md-4 mx-auto">Volver</a>
                </div>
            </form>
        @endif

    </div>
@endsection
