@extends('layouts.admin.app')

@section('content')

    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif

    @if (Session::has('create'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('create') }}
        </div>
    @endif

    <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #F28D8D">Todos los
        administradores</label>

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="example" class="table table-striped " cellspacing="0" width="100%">
                    <thead style="background-color: #F28D8D; color: #fff">
                        <tr>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Correo</th>
                            <th>Estado</th>
                            <th>Rol</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody class="table-bordered">
                        @foreach ($admins as $admin)
                            @if ($admin->id_useradmin != auth()->user()->id_useradmin)
                                <tr>
                                    <td>{{ $admin->nombre }}</td>
                                    <td>{{ $admin->apellido }}</td>
                                    <td>{{ $admin->email }}</td>
                                    <td>
                                        <span class='{{ $admin->estado ? "text-success" : "text-danger" }}'>
                                            {{ $admin->estado ? "Activo" : "Descativo" }}
                                        </span>
                                    </td>
                                    <td>{{ $admin->roles->nombre }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('admin.edit', $admin->id_useradmin) }}" style="color: #F28D8D;"> <i
                                                class="far fa-edit"></i></a>
                                        <a href="{{ route('admin.confirm', $admin->id_useradmin) }}"
                                            style="color: #F28D8D;"><i class="far fa-trash-alt"></i></a>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
                <button class="btn btn-round" style="background-color: #F28D8D" data-toggle="modal"
                    data-target=".bd-example-modal-lg">
                    <i class="material-icons">add</i>AGREGAR ADMINISTRADOR
                </button>

                <!-- MODAL ADMIN-->
                <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel" style="color: #F28D8D; font-weight: bold">
                                    Crear administrador</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="{{ route('admin.store') }}" method="post">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-row">
                                        <div class="col">
                                            <input name="nombre" id="categoria" type="text" placeholder="Nombre"
                                                class="form-control @error('nombre') is-invalid @enderror" value="{{ old('nombre') }}">
                                            @error('nombre')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col">
                                            <input name="apellido" id="categoria" type="text" placeholder="Apellido"
                                                class="form-control @error('apellido') is-invalid @enderror" value="{{ old('apellido') }}">
                                            @error('apellido')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row my-3">
                                        <div class="col">
                                            <input name="email" id="categoria" type="email" placeholder="Correo electronico"
                                                class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col">
                                            <input name="password" id="SUBCATEGORIA" type="password" placeholder="Contraseña"
                                                class="form-control @error('password') is-invalid @enderror">
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row my-3">
                                        <div class="col">
                                            <select name="estado" id="" class="form-control @error('estado') is-invalid @enderror">
                                                <option value="" selected disabled>Seleccione su estado</option>
                                                <option value="1" {{ old('estado') == 1 ? "selected" : "" }}>Activo</option>
                                                <option value="0" {{ old('estado') == 0 ? "selected" : "" }}>Desactivo</option>
                                            </select>
                                            @error('estado')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col">
                                            <select name="FK_id_adminrol" id="user_rol" class="form-control">
                                                @foreach ($rol as $i => $r)
                                                    <option value="{{ $r->id_adminrol }}" {{ old('FK_id_adminrol') == $r->id_adminrol ? "selected" : "" }}>
                                                        {{ $r->nombre }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row my-3">
                                        <div class="col-md-6 d-none" id="base_permisos">
                                            <label for="name_user">Permisos de Base de Datos:</label>
                                            <div class="form-check">
                                                <input class="" type="checkbox" value="1" id="p_store" name="p_store">
                                                <label class="form-check-label" for="p_store">
                                                    Creación de datos
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="" type="checkbox" value="1" id="p_update" name="p_update">
                                                <label class="form-check-label" for="p_update">
                                                    Actualización de datos
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="" type="checkbox" value="1" id="p_destroy" name="p_destroy">
                                                <label class="form-check-label" for="p_destroy">
                                                    Eliminación de datos
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-md-6 d-none" id="modulos_permisos">
                                            <label for="name_user">Permisos de Módulos:</label>
                                            <div class="form-check">
                                                <input class="" type="checkbox" value="1" id="p_productos" name="p_productos">
                                                <label class="form-check-label" for="p_productos">
                                                    Productos
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="" type="checkbox" value="1" id="p_premios" name="p_premios">
                                                <label class="form-check-label" for="p_premios">
                                                    Premios
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="" type="checkbox" value="1" id="p_facturas" name="p_facturas">
                                                <label class="form-check-label" for="p_facturas">
                                                    Facturas
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="" type="checkbox" value="1" id="p_noticias" name="p_noticias">
                                                <label class="form-check-label" for="p_noticias">
                                                    Noticias
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="" type="checkbox" value="1" id="p_capacitaciones" name="p_capacitaciones">
                                                <label class="form-check-label" for="p_capacitaciones">
                                                    Capacitaciones
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="" type="checkbox" value="1" id="p_trivia" name="p_trivia">
                                                <label class="form-check-label" for="p_trivia">
                                                    Trivias y Encuestas
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input class="btn mx-auto col-md-6" style="background-color: #F28D8D; border-radius: 40px;" 
                                    type="submit" value="CREAR USUARIO">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        $("#user_rol").change(function(){
            var selectValue = $(this).val();

            if(selectValue == 1){
                $("#base_permisos").addClass("d-none");
                $("#modulos_permisos").addClass("d-none");
            }
            else if(selectValue == 2){
                $("#base_permisos").removeClass("d-none");
                $("#modulos_permisos").removeClass("d-none");
            }
            else{
                $("#base_permisos").addClass("d-none");
                $("#modulos_permisos").removeClass("d-none");
            }
        });

        var optionValue = $('#user_rol').val();
        if(optionValue == 1){
            $("#base_permisos").addClass("d-none");
            $("#modulos_permisos").addClass("d-none");
        }
        else if(optionValue == 2){
            $("#base_permisos").removeClass("d-none");
            $("#modulos_permisos").removeClass("d-none");
        }
        else{
            $("#base_permisos").addClass("d-none");
            $("#modulos_permisos").removeClass("d-none");
        }

        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
            $('.bd-example-modal-lg').modal('show');
        }
    });
</script>


