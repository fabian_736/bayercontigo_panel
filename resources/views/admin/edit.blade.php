@extends('layouts.admin.app')

@section('content')

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">

                @if (Session::has('error'))
                    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                        {{ Session::get('error') }}
                    </div>
                @endif

                @if (Session::has('edit'))
                    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                        {{ Session::get('edit') }}
                    </div>
                @endif

                    <form action="{{ route('admin.update', $admin->id_useradmin) }}" method="post" id="asesor">

                        @csrf
                        <h3 class="mb-5">Editar usuarios</h3>

                        <div class="row col-md-12 my-3">
                            <div class="col-md-6">
                                <label for="name_user">Nombre:</label>
                                <input name="nombre" id="categoria" type="text" class="form-control @error('nombre') is-invalid @enderror"
                                       value="{{ old('nombre', $admin->nombre) }}">
                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="name_user">Apellido:</label>
                                <input name="apellido" id="categoria" type="text" class="form-control @error('apellido') is-invalid @enderror"
                                       value="{{ old('apellido', $admin->apellido) }}">
                                @error('apellido')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row col-md-12">
                            <div class="col-md-12">
                                <label for="name_user">Correo:</label>
                                <input name="email" id="categoria" type="text" class="form-control @error('email') is-invalid @enderror"
                                       value="{{ old('email', $admin->email) }}">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row col-md-12 my-3">
                            <div class="col-md-6">
                                <label for="name_user">Estado:</label>
                                <select name="estado" id="" class="form-control @error('estado') is-invalid @enderror">
                                    <option value="1" {{ old('estado', $admin->estado) ? "selected" : "" }} >Activo</option>
                                    <option value="0" {{ old('estado', $admin->estado) ? "" : "selected" }} >Inactivo</option>
                                </select>
                                @error('estado')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="name_user">Rol:</label>
                                <select name="FK_id_adminrol" id="user_rol" class="form-control">
                                    @foreach ($rol  as  $i => $a)
                                        <option  value="{{ $a->id_adminrol }}" {{ old('FK_id_adminrol', $admin->FK_id_adminrol) == $a->id_adminrol ? "selected" : "" }}>
                                            {{ $a->nombre }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row col-md-12 my-3">
                            <div class="col-md-6 d-none" id="base_permisos">
                                <label for="name_user">Permisos de Base de Datos:</label>
                                <div class="form-check">
                                    <input class="" type="checkbox" value="1" id="p_store" name="p_store"
                                        {{ $admin->p_store ? "checked" : "" }}>
                                    <label class="form-check-label" for="p_store">
                                        Creación de datos
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="" type="checkbox" value="1" id="p_update" name="p_update"
                                        {{ $admin->p_update ? "checked" : "" }}>
                                    <label class="form-check-label" for="p_update">
                                        Actualización de datos
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="" type="checkbox" value="1" id="p_destroy" name="p_destroy"
                                        {{ $admin->p_destroy ? "checked" : "" }}>
                                    <label class="form-check-label" for="p_destroy">
                                        Eliminación de datos
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-6 d-none" id="modulos_permisos">
                                <label for="name_user">Permisos de Módulos:</label>
                                <div class="form-check">
                                    <input class="" type="checkbox" value="1" id="p_productos" name="p_productos"
                                        {{ $admin->p_productos ? "checked" : "" }}>
                                    <label class="form-check-label" for="p_productos">
                                        Productos
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="" type="checkbox" value="1" id="p_premios" name="p_premios"
                                        {{ $admin->p_premios ? "checked" : "" }}>
                                    <label class="form-check-label" for="p_premios">
                                        Premios
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="" type="checkbox" value="1" id="p_facturas" name="p_facturas"
                                        {{ $admin->p_facturas ? "checked" : "" }}>
                                    <label class="form-check-label" for="p_facturas">
                                        Facturas
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="" type="checkbox" value="1" id="p_noticias" name="p_noticias"
                                        {{ $admin->p_noticias ? "checked" : "" }}>
                                    <label class="form-check-label" for="p_noticias">
                                        Noticias y Banners
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="" type="checkbox" value="1" id="p_capacitaciones" name="p_capacitaciones"
                                        {{ $admin->p_capacitaciones ? "checked" : "" }}>
                                    <label class="form-check-label" for="p_capacitaciones">
                                        Capacitaciones
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="" type="checkbox" value="1" id="p_trivia" name="p_trivia"
                                        {{ $admin->p_trivia ? "checked" : "" }}>
                                    <label class="form-check-label" for="p_trivia">
                                        Trivias y Encuestas
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row col-md-12">
                            <input class="btn btn-success my-3 form-control col-md-8 mx-auto" type="submit"
                                   value="Actualizar Usuario">
                        </div>
                    </form>

            </div>
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        $("#user_rol").change(function(){
            var selectValue = $(this).val();
            //1 Super Admin
            //2 Call Center
            //3 Básico

            //base_permisos
            //modulos_permisos

            if(selectValue == 1){
                $("#base_permisos").addClass("d-none");
                $("#modulos_permisos").addClass("d-none");
            }
            else if(selectValue == 2){
                $("#base_permisos").removeClass("d-none");
                $("#modulos_permisos").removeClass("d-none");
            }
            else{
                $("#base_permisos").addClass("d-none");
                $("#modulos_permisos").removeClass("d-none");
            }
        });

        var rol = $("#user_rol").val();
        if(rol == 1){
            $("#base_permisos").addClass("d-none");
            $("#modulos_permisos").addClass("d-none");
        }
        else if(rol == 2){
            $("#base_permisos").removeClass("d-none");
            $("#modulos_permisos").removeClass("d-none");
        }
        else{
            $("#base_permisos").addClass("d-none");
            $("#modulos_permisos").removeClass("d-none");
        }

        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }
    });
</script>