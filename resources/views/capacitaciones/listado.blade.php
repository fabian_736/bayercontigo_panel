@extends('layouts.admin.app')

@section('content')


<div class="col-md-12 mx-auto" >

    @if($id_producto || $id_patologia)
        <div>
            <a href="{{ route('capacitacion.list') }}">
                <button class="btn" style="background-color: #e7344c;">
                    Ver Todos los Entrenamientos
                </button>
            </a>
        </div>

        <h5 class="font-weight-bold my-3" style="color: #51A2A7">Entrenamientos del Producto / Patología Seleccionada</h5>
    @else
        <h5 class="font-weight-bold my-3" style="color: #51A2A7">Todos los Entrenamientos | Retos</h5>
    @endif

    <table class="table table-striped " id="example">
        <thead style="background-color: #51A2A7; color: #fff">
            <tr>
                <th>Capacitación</th>
                <th>Título</th>
                <th>Descripción</th>
                <th>N° de Ejercicios</th>
                <th>Ver Ejercicios</th>
                <th>Cuestionario - N° de preguntas</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody class="table-light" style="background-color: #fff; color: #000">
            @foreach ($capacitaciones as $capacitacion)
                <tr class="text-center">
                    <td class="align-middle">
                        {{ 
                            $capacitacion->FK_id_patologia ? 
                            $capacitacion->patologia->nombre : 
                            $capacitacion->producto->nombre 
                        }}
                    </td>
                    <td class="align-middle">{{ $capacitacion->titulo }}</td>
                    <td class="align-middle">{{ $capacitacion->descripcion }}</td>
                    <td class="align-middle">{{ $capacitacion->entrenamientos->count() }}</td>
                    <td class="align-middle">
                        <a href="{{ route('capacitacion.entrenamiento', $capacitacion->id_capacitacion) }}"
                            class="btn btn-sm btn-danger mx-auto text-white">
                            Ver <i class="fas fa-dumbbell"></i>
                        </a>
                    </td>
                    <td class="align-middle">
                        {{ $capacitacion->cuestionario ? $capacitacion->cuestionario->titulo.' | ('.$capacitacion->cuestionario->preguntas->count().')' : "No posee" }}
                    </td>
                    <td class="align-middle">
                        <form action="" method="post">
                            <div class="row text-center">
                                <div class="col-12 mb-1">
                                    <a href="{{ route('capacitacion.edit', $capacitacion->id_capacitacion) }}"
                                        class="btn btn-sm mx-auto" style="background-color: #51A2A7">
                                        <i class="far fa-edit"></i>
                                    </a>
                                </div>
                                <div class="col-12">
                                    <a href="{{ route('capacitacion.delete', $capacitacion->id_capacitacion) }}"
                                        class="btn btn-sm btn-danger mx-auto">
                                        <i class="far fa-trash-alt"></i>
                                    </a>
                                </div>
                            </div>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection