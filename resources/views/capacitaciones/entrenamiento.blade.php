@extends('layouts.admin.app')

@section('content')


<div class="col-md-12 mx-auto" >

    <div>
        <a href="{{ route('capacitacion.list') }}">
            <button class="btn" style="background-color: #e7344c;">
                Volver a Capacitaciones
            </button>
        </a>
    </div>

    <h5 class="font-weight-bold my-3" style="color: #51A2A7">
        {{ $capacitacion->titulo }} de {{ $capacitacion->FK_id_patologia ? $capacitacion->patologia->nombre : $capacitacion->producto->nombre }}
    </h5>
    <label for="" class="">
        {{ $capacitacion->descripcion }}
    </label>

    <table class="table table-striped mt-2" id="example">
        <thead style="background-color: #51A2A7; color: #fff">
            <tr>
                <th>
                    {{ $capacitacion->FK_id_patologia ? "Patologia" : "Producto" }}
                </th>
                <th>Ejercicio</th>
                <th>Documento</th>
                <th>Video</th>
            </tr>
        </thead>
        <tbody class="table-light" style="background-color: #fff; color: #000">
            @foreach ($capacitacion->entrenamientos as $entrenamiento)
                <tr class="">
                    <td class="align-middle">
                        {{ 
                            $capacitacion->FK_id_patologia ? 
                            $capacitacion->patologia->nombre : 
                            $capacitacion->producto->nombre 
                        }}
                    </td>
                    <td class="align-middle">Ejercicio COD: {{ $entrenamiento->id_capacitacion_documento }}</td>
                    <td class="align-middle">
                        @if ($entrenamiento->documento)
                            <a href="{{ route('capacitacion.archive', ['filename' => basename($entrenamiento->documento)]) }}"
                                target="_blank" class="btn btn-sm btn-danger mx-auto">
                                <i class="fas fa-file-pdf"></i>
                            </a>
                        @else()
                            <span>No posee...</span>
                        @endif
                    </td>
                    <td class="align-middle">
                        @if ($entrenamiento->video)
                            <a href="{{ route('capacitacion.archive', ['filename' => basename($entrenamiento->video)]) }}"
                                target="_blank" class="btn btn-sm btn-danger mx-auto">
                                <i class="fas fa-video"></i>
                            </a>
                        @else()
                            <span>No posee...</span>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection