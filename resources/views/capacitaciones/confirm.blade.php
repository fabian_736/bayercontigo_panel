@extends('layouts.admin.app')
@section('content')
    <div class="container py-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        <h1 class="mb-5">¿Deseas eliminar el siguiente entrenamiento? </h1>
        <h4>Título: </h4>
        <p>{{ $capacitacion->titulo }}</p>
        <h4>Descripcion: </h4>
        <p>{{ $capacitacion->descripcion }}</p>
        <h4>!Tenga en cuenta que al eliminarla estaremos eliminando todos sus ejercicios! </h4>
        
        <form method="post" enctype="multipart/form-data" action="{{ route('capacitacion.destroy', $capacitacion->id_capacitacion) }}">
            @method('DELETE')
            @csrf
            <div class="row col-md-12 mt-5 mx-auto">
                <button type="submit" class="redondo btn btn-danger col-md-4 mx-auto">
                    <i class="fas fa-trash-alt"></i> Eliminar
                </button>
                <a href="{{route('capacitacion.list')}}" class="btn btn-primary col-md-4 mx-auto">Volver</a>
            </div>
        </form>
    </div>
@endsection
