@extends('layouts.admin.app')

@section('content')

@if (Session::has('error'))
<div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('error') }}
</div>
@endif

@if (Session::has('create'))
<div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('create') }}
</div>
@endif

@if (Session::has('delete'))
<div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('delete') }}
</div>
@endif

@if (Session::has('edit'))
<div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
    {{ Session::get('edit') }}
</div>
@endif

<?php $cuestionarios = collect(); ?>
<div class="row" style="margin-top: 5%">

    <!-- CAPACITACIONES -->
        <div class="col-lg-6 col-md-6">
            <div class="row mx-auto">
                <label for="" class="lead" style="color: #51A2A7; font-weight: bold">Capacitaciones</label>
                <a href="{{route('capacitacion.list')}}" class="ml-auto text-secondary">Ver todos</a>
            </div>
            @forelse($capacitaciones as $capacitacion)
                @if($capacitacion->cuestionario)
                    <?php $cuestionarios->push($capacitacion->cuestionario); ?>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="row ">
                            <div class="col-md-8">
                                <h4 class="card-title" style="color: #51A2A7; font-weight: bold">
                                    {{ $capacitacion->titulo }}
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-0 px-4">
                        @if($capacitacion->producto)
                            <p class="text-left">
                                Producto: 
                                <span style="color: #51A2A7;">{{ $capacitacion->producto->nombre }}</span>
                            </p>
                        @else
                            <p class="text-left">
                                Patología: 
                                <span style="color: #51A2A7;">{{ $capacitacion->patologia->nombre }}</span>
                            </p>
                        @endif
                        <p>{{ $capacitacion->descripcion }}</p>
                        <div class="row ml-auto mx-auto mb-2">
                            @if($capacitacion->entrenamientos)
                                <p class="mx-2 ml-auto my-auto" style="font-size: 15px; color: #51A2A7; cursor: pointer">
                                    <a style="color: #51A2A7" href="{{ route('capacitacion.entrenamiento', $capacitacion->id_capacitacion) }}">
                                        Ver Entrenamientos ({{ $capacitacion->entrenamientos->count() }})
                                    </a>
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
            @empty
                <div class="card">
                    <div class="card-header">
                        <div class="row ">
                            <div class="col-md-8">
                                <h4 class="card-title" style="color: #51A2A7; font-weight: bold">
                                    No hay capacitaciones creadas
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            @endforelse
            
            <button class="btn btn-round" style="background-color: #51A2A7" data-toggle="modal" data-target=".bd-example-modal-lg">
                <i class="material-icons">add</i>CREAR CAPACITACIÓN
            </button>

            <!-- PAGINACION -->
            <nav aria-label="Page navigation example">
                <?php $limite_paginas = 10; ?>

                @if ($capacitaciones->lastPage() > 1)
                    <ul class="pagination justify-content-end">
                        <li class="page-item {{ ($capacitaciones->currentPage() == 1) ? ' disabled' : '' }}">
                            <a class="page-link" href="{{ $capacitaciones->url(1) }}">Primera</a>
                        </li>
                        @for ($i = 1; $i <= $capacitaciones->lastPage(); $i++)
                            <?php
                            $half_total_links = floor($limite_paginas / 2);
                            $from = $capacitaciones->currentPage() - $half_total_links;
                            $to = $capacitaciones->currentPage() + $half_total_links;
                            if ($capacitaciones->currentPage() < $half_total_links)
                                $to += $half_total_links - $capacitaciones->currentPage();

                            if ($capacitaciones->lastPage() - $capacitaciones->currentPage() < $half_total_links)
                                $from -= $half_total_links - ($capacitaciones->lastPage() - $capacitaciones->currentPage()) - 1;
                            ?>
                            @if ($from < $i && $i < $to) <li class="page-item {{ ($capacitaciones->currentPage() == $i) ? 'active disabled' : '' }}">
                                <a class="page-link" href="{{ $capacitaciones->url($i) }}">{{ $i }}</a>
                                </li>
                            @endif
                        @endfor
                        <li class="page-item {{ ($capacitaciones->currentPage() == $capacitaciones->lastPage()) ? ' disabled' : '' }}">
                            <a class="page-link" href="{{ $capacitaciones->url($capacitaciones->lastPage()) }}">Ultima</a>
                        </li>
                    </ul>
                @endif
            </nav>
        </div>
    <!-- FIN CAPACITACIONES -->

    <!-- CUESTIONARIOS -->
        <div class="col-md-6">
            <div class="row mx-auto">
                <label for="" class="lead" style="color: #F28D8D; font-weight: bold">Cuestionarios</label>
                <a href="{{route('capacitacion.index')}}" class="ml-auto text-secondary">Ver todos</a>
            </div>
            @forelse($cuestionarios as $cuestionario)
                <div class="card card-body">
                    <div class="row d-flex justify-content-between ">
                        <div class="col-md-9">
                            <div class="col">
                                <label for="" style="color: #F28D8D; font-weight: bold">Cuestionario - {{ $cuestionario->titulo }}</label>
                            </div>
                            <div class="col">
                                <label for="" style="color: #F28D8D;">Número de preguntas ({{ $cuestionario->preguntas->count() }})</label>
                            </div>
                            <div class="col">
                                <label for="">Información: <br>{{ $cuestionario->descripcion }}</label>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="card-title" style="color: #F28D8D; font-weight: bold">
                                    No hay Cuestionarios creados
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            @endforelse
        </div>
    <!-- FIN CUESTIONARIOS -->

</div>

<!-- MODAL STORE CAPACITACION-->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                    Creación de Capacitación</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="tstore" action="{{ route('capacitacion.store') }}" method="post" enctype="multipart/form-data">
                    @csrf

                <!-- CAPACITACION -->
                    <div class="form-row">
                        <div class="col">
                            <label for="">Título de Reto | Entrenamiento</label>
                            <input name="titulo" id="" type="text" class="form-control @error('titulo') is-invalid @enderror">
                            @error('titulo')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col">
                            <label for="">Descripción:</label>
                            <textarea name="descripcion" id="" type="text" 
                                class="form-control @error('descripcion') is-invalid @enderror"></textarea>
                            @error('descripcion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col">
                            <label for="">Tipo de Capacitación:</label>
                            <div class="form-check form-check-radio form-check-inline ml-3 mr-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="tipo" id="inlineRadio1" value="producto" required> Prodcuto
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                            <div class="form-check form-check-radio form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="tipo" id="inlineRadio1" value="patologia"> Patología
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                            <!-- PATOLOGIAS -->
                            <select name="FK_id_patologia" id="selectPat" class="form-control d-none">
                                <option value="" selected disabled>Seleccionar...</option>
                                @foreach($patologias as $patologia)
                                    <option value="{{ $patologia->id_patologia }}">{{ $patologia->nombre }}</option>
                                @endforeach
                            </select>

                            <!-- PRODUCTOS -->
                            <select name="FK_id_producto" id="selectPro" class="form-control d-none">
                                <option value="" selected disabled>Seleccionar...</option>
                                @foreach($produtos as $producto)
                                    <option value="{{ $producto->id_producto }}">{{ $producto->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                
                    <!-- ENTRENAMIENTOS | EJERCICIOS | RETOS -->
                    <div class="ejercicios">
                        <div class="form-row my-3">
                            <input type="hidden" id="input-1" value="1">
                            <div class="col-12">
                                <h5 class="" style="color: #51A2A7; font-weight: bold">
                                    Ejercicio/Reto 1
                                </h5>
                            </div>
                            <div class="col">
                                <label for="">Cargar PDF:</label>
                                <input type="file" id="pdf1" name="pdf[1]" accept=".pdf" class="loadfile form-control" required>
                            </div>
                            <div class="col">
                                <label for="">Cargar Video:</label>
                                <input type="file" id="video1" name="video[1]" accept=".mp4" class="loadfile form-control" required>
                            </div>
                        </div>
                    </div>

                    <div class="row ml-2 d-flex justify-content">
                        <a id="agregar" class="mr-2 my-3" style="color: #51A2A7" title="Add field"><i class="fas fa-plus mr-2"></i></a>
                        <a id="borrar" class="mr-2 my-3" style="color: #51A2A7" title="Add field"><i class="fas fa-trash mr-2"></i></a>
                    </div>

                    <div class="progress my-5">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%" role="progressbar">
                        </div>
                    </div>
                    <div id="uploadStatus" style="display:none"></div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;">CREAR CAPACITACION</button>
            </div>
            </form>
        </div>
    </div>
</div>

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
<input type="hidden" id="redirect" value="{{ route('capacitacion.index') }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {

        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
            $('#exampleModal').modal('show');
        }
        
        $('input[name="tipo"]').change(function(){
            var value = $(this).val();
            console.log(value);
            if(value == "producto"){
                $('#selectPat').addClass('d-none').removeAttr('required');
                $('#selectPro').addClass('d-none').removeAttr('required');

                $('#selectPro').removeClass('d-none').attr("required", true );
                $('#selectPat').val(null);
            }
            else if(value == "patologia"){
                $('#selectPat').addClass('d-none').removeAttr('required');
                $('#selectPro').addClass('d-none').removeAttr('required');

                $('#selectPat').removeClass('d-none').attr("required", true );
                $('#selectPro').val(null);
            }
        });

        //PROCESO DE ANEXAR O ELIMINAR EJERCICIOS O RETOS
        var inc = 1;
        $('#agregar').click(function(){
            console.log(inc);
            inc++;

            var html = "";
            html += '<div class="form-row my-3">'+
                        '<input type="hidden" id="input-'+inc+'" value="'+inc+'">'+
                        '<div class="col-12">'+
                            '<h5 class="" style="color: #51A2A7; font-weight: bold">'+
                                'Ejercicio/Reto '+inc+
                            '</h5>'+
                        '</div>'+
                        '<div class="col">'+
                            '<label for="">Cargar PDF:</label>'+
                            '<input type="file" id="pdf'+inc+'" name="pdf['+inc+']" accept=".pdf" class="loadfile form-control" required>'+
                        '</div>'+
                        '<div class="col">'+
                            '<label for="">Cargar Video:</label>'+
                            '<input type="file" id="video'+inc+'" name="video['+inc+']" accept=".mp4" class="loadfile form-control" required>'+
                        '</div>'+
                    '</div>';
            $('.ejercicios').append(html);
        });

        $('#borrar').click(function(){
            if(inc > 1){
                console.log(inc);
                $('#input-'+inc).parent().remove();
                inc--;
            }
        });

        $("#tstore").on('submit', function(e) {
            e.preventDefault();
            var type = $(this).attr('method');
            var url = $(this).attr('action');
            $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = ((evt.loaded / evt.total) * 100);
                            $(".progress-bar").width(percentComplete + '%');
                        }
                    }, false);
                    return xhr;
                },
                type: type,
                url: url,
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".progress-bar").width('0%');
                    jQuery('#uploadStatus').hide();
                    jQuery('#uploadStatus').html('');
                },
                error: function() {
                    $('#uploadStatus').html('<span class="invalid-feedback d-block" role="alert"><strong>No se pudo cargar de forma exitosa la capacitación, error de conexión</strong></span>');
                },
                success: function(resp) {
                    console.log(resp);
                    jQuery('#uploadStatus').show();
                    jQuery.each(resp.errors, function(key, value) {
                        jQuery('#uploadStatus').append('<p><span class="invalid-feedback d-block" role="alert"><strong>' + value + '</strong></span></p>');
                    });

                    if (resp.success) {
                        $('#uploadStatus').append('<p><span style="color:#28A74B;" class="invalid-feedback d-block" role="alert"><strong>' + resp.success + '</strong></span></p>');
                        alert(resp.success);
                        location.href = $('#redirect').val();
                    }

                    if (resp.fallo)
                        jQuery('#uploadStatus').append('<p><span class="invalid-feedback d-block" role="alert"><strong>' + resp.fallo + '</strong></span></p>');
                },
            });
        });
    });
</script>