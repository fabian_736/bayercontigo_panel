@extends('layouts.admin.app')

@section('content')
    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        @if (Session::has('edit'))
            <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                {{ Session::get('edit') }}
            </div>
        @endif

        <div class="row mb-5 mx-auto">
            <!-- Editar Capacitación -->
            <div class="col-md-12 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">


                <div class="card-header mt-3" style="background-color: #e7344c;">
                    <h5 class="text-white my-auto">Editar Entrenamiento | Reto</h5>
                </div>

                <form id="tupdate" action="{{ route('capacitacion.update',$capacitacion->id_capacitacion) }}" method="post" enctype="multipart/form-data">

                    @csrf
                    <div class="row col-md-12 my-4">
                        <div class="col">
                            <label for="name_user">Título de Reto | Entrenamiento:</label>
                            <input name="titulo" id="" type="text" value="{{ old('titulo', $capacitacion->titulo) }}"
                                class="form-control @error('titulo') is-invalid @enderror">
                            @error('titulo')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12">
                        <div class="col">
                            <label for="name_user">Descripción:</label>
                            <textarea name="descripcion" class="form-control @error('descripcion') is-invalid @enderror">{{ old('descripcion', $capacitacion->descripcion) }}</textarea>
                            @error('descripcion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col">
                            <label for="">Tipo de Capacitación:</label>
                            <div class="form-check form-check-radio form-check-inline ml-3 mr-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="tipo" id="inlineRadio1" value="producto" required  {{ $capacitacion->FK_id_producto ? "checked" : "" }}> Producto
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                            <div class="form-check form-check-radio form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="tipo" id="inlineRadio1" value="patologia" {{ $capacitacion->FK_id_patologia ? "checked" : "" }}> Patología
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                            <!-- PATOLOGIAS -->
                            <select name="FK_id_patologia" id="selectPat" class="form-control d-none">
                                <option value="" selected disabled>Seleccionar...</option>
                                @foreach($patologias as $patologia)
                                    <option value="{{ $patologia->id_patologia }}">{{ $patologia->nombre }}</option>
                                @endforeach
                            </select>

                            <!-- PRODUCTOS -->
                            <select name="FK_id_producto" id="selectPro" class="form-control d-none">
                                <option value="" selected disabled>Seleccionar...</option>
                                @foreach($produtos as $producto)
                                    <option value="{{ $producto->id_producto }}">{{ $producto->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <hr>

                    <div class="row col-md-12 mt-3 mb-2">
                        <div class="col-md-12 text-center">
                            <h4 for="" style="color: #51A2A7; font-weight: bold">Ejercicios</h4>
                        </div>
                    </div>

                    <?php $cantidad = 1; ?>
                    @foreach($capacitacion->entrenamientos as $entrenamiento)

                        <!-- DATOS PARA ELIMINAR SATISFACTORIAMENTE -->
                        <input type="hidden" name="id_docs[{{$cantidad}}]" value="{{ $entrenamiento->id_capacitacion_documento }}">
                        <input type="hidden" id="delete-{{ $entrenamiento->id_capacitacion_documento }}" 
                            name="delete_doc[{{$cantidad}}]" value="">

                        <div class="row col-md-12 my-3" id="div-{{ $entrenamiento->id_capacitacion_documento }}">
                            <input type="hidden" id="input-1" value="1">
                            <div class="col-12">
                                <h5 class="" style="color: #51A2A7; font-weight: bold">
                                    Ejercicio COD: {{ $entrenamiento->id_capacitacion_documento }}
                                    <a id="{{ $entrenamiento->id_capacitacion_documento }}" class="eEjercicio" style="color: #51A2A7; cursor:pointer"> <i class="far fa-edit"></i></a>
                                    <a id="{{ $entrenamiento->id_capacitacion_documento }}" class="dEjercicio" style="color: #51A2A7; cursor:pointer"><i class="far fa-trash-alt"></i></a>
                                    <a id="{{ 'ret'.$entrenamiento->id_capacitacion_documento }}" type="{{ $entrenamiento->id_capacitacion_documento }}"
                                        class="rEjercicio d-none" style="color: #51A2A7; cursor:pointer">Retornar</a>
                                </h5>
                                @if ($entrenamiento->documento)
                                    <a href="{{ route('capacitacion.archive', ['filename' => basename($entrenamiento->documento)]) }}"
                                        target="_blank" class="btn btn-sm btn-danger mx-auto">
                                        <i class="fas fa-file-pdf"></i>
                                    </a>
                                @else()
                                    <span>No posee...</span>
                                @endif
                                @if ($entrenamiento->video)
                                    <a href="{{ route('capacitacion.archive', ['filename' => basename($entrenamiento->video)]) }}"
                                        target="_blank" class="btn btn-sm btn-danger mx-auto">
                                        <i class="fas fa-video"></i>
                                    </a>
                                @else()
                                    <span>No posee...</span>
                                @endif
                            </div>

                            <div class="col d-none" id="editPdf-{{ $entrenamiento->id_capacitacion_documento }}">
                                <label for="">Cargar PDF:</label>
                                <input type="file" id="pdf{{$cantidad}}" name="pdf[{{$cantidad}}]" accept=".pdf" class="loadfile form-control">
                            </div>
                            <div class="col d-none" id="editVideo-{{ $entrenamiento->id_capacitacion_documento }}">
                                <label for="">Cargar Video:</label>
                                <input type="file" id="video{{$cantidad}}" name="video[{{$cantidad}}]" accept=".mp4" class="loadfile form-control">
                            </div>
                        </div>
                        <?php $cantidad++; ?>
                    @endforeach

                    <!-- ENTRENAMIENTOS | EJERCICIOS | RETOS NUEVOS QUE SE AGREGUEN -->
                    <div class="row col-md-12 my-3">
                        <div class="col">
                            <label for="">Deseas añadir nuevos ejercicios / retos:</label>
                            <div class="form-check form-check-radio form-check-inline ml-3 mr-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="exercise" id="radioNew1" value="si"> SI
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                            <div class="form-check form-check-radio form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="exercise" id="radioNew1" value="no" checked> NO
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="ejercicios d-none">
                        <input type="hidden" id="addExercise" name="addExercise" value="false">
                        <div class="row col-md-12 my-3">
                            <input type="hidden" id="input-1" value="1">
                            <div class="col-12">
                                <h5 class="" style="color: #51A2A7; font-weight: bold">
                                    Nuevo Ejercicio/Reto 1
                                </h5>
                            </div>
                            <div class="col">
                                <label for="">Cargar PDF:</label>
                                <input type="file" id="pdf1" name="newpdf[1]" accept=".pdf" class="newloadfile form-control">
                            </div>
                            <div class="col">
                                <label for="">Cargar Video:</label>
                                <input type="file" id="video1" name="newvideo[1]" accept=".mp4" class="newloadfile form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row ml-2 botonese d-none">
                        <a id="agregar" class="mr-2 my-3" style="color: #51A2A7" title="Add field"><i class="fas fa-plus mr-2"></i></a>
                        <a id="borrar" class="mr-2 my-3" style="color: #51A2A7" title="Add field"><i class="fas fa-trash mr-2"></i></a>
                    </div>

                    <div class="row col-md-12 mx-auto d-block">
                        <div class="progress mb-3">
                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" 
                                aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%"  role="progressbar">
                            </div>
                        </div>
                        <div id="uploadStatus" style="display:none"></div>
                    </div>

                    <hr>
                    <div class="row col-md-12 my-3">
                        <div class="col-md-6 mx-auto">
                            <button type="submit" class="form-control text-white" style="background-color: #64C2C8">Editar Capacitación</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
    <input type="hidden" id="redirect" value="{{ Request::url() }}">
    <input type="hidden" id="idProduct" value="{{ $capacitacion->FK_id_producto }}">
    <input type="hidden" id="idPatology" value="{{ $capacitacion->FK_id_patologia }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {

        //Inicializar y darle un valor a los checked de producto o patologia y cuando cambie
            var input = $('input[name="tipo"]:checked').val();
            if(input == "producto"){
                $('#selectPat').addClass('d-none').removeAttr('required');
                $('#selectPro').addClass('d-none').removeAttr('required');

                $('#selectPro').removeClass('d-none').attr("required", true );
                
                //Set de la opcion
                var option = $('#idProduct').val();
                $('#selectPro').val(option);
            }
            else if(input == "patologia"){
                $('#selectPat').addClass('d-none').removeAttr('required');
                $('#selectPro').addClass('d-none').removeAttr('required');

                $('#selectPat').removeClass('d-none').attr("required", true );
                
                //set de la opcion
                var option = $('#idPatology').val();
                $('#selectPat').val(option);
            }

            $('input[name="tipo"]').change(function(){
                var value = $(this).val();
                console.log(value);
                if(value == "producto"){
                    $('#selectPat').addClass('d-none').removeAttr('required');
                    $('#selectPro').addClass('d-none').removeAttr('required');

                    $('#selectPro').removeClass('d-none').attr("required", true );
                    $('#selectPat').val(null);
                }
                else if(value == "patologia"){
                    $('#selectPat').addClass('d-none').removeAttr('required');
                    $('#selectPro').addClass('d-none').removeAttr('required');

                    $('#selectPat').removeClass('d-none').attr("required", true );
                    $('#selectPro').val(null);
                }
            });

        //Estado de los ejercicios al querer cambiar o eliminar los existentes o crear unos nuevos
            $('.eEjercicio').click(function(){
                var id = $(this).attr("id");

                $("#editPdf-"+id).removeClass('d-none');
                $("#editVideo-"+id).removeClass('d-none');
                $('#ret'+id).removeClass('d-none');
            });

            $('.rEjercicio').click(function(){
                var id = $(this).attr("type");

                $("#editPdf-"+id).addClass('d-none');
                $("#editVideo-"+id).addClass('d-none');
                $(this).addClass('d-none');

                $("#editPdf-"+id+" input").val(null);
                $("#editVideo-"+id+" input").val(null);
            });

            $('.dEjercicio').click(function(){
                var id = $(this).attr("id");

                $("#div-"+id).remove();

                $("#editPdf-"+id+" input").val(null);
                $("#editVideo-"+id+" input").val(null);
                $("#delete-"+id).val(true);
            });

        //Nuevos documentos a cargar JavaScript
            $('input[name="exercise"]').change(function(){
                var value = $(this).val();
                
                if(value == "si"){
                    $('.ejercicios').removeClass('d-none');
                    $('.botonese').removeClass('d-none');
                    $('.newloadfile').attr('required',true);
                    $('#addExercise').val(true);
                }
                else if(value == "no"){
                    $('.ejercicios').addClass('d-none');
                    $('.botonese').addClass('d-none');
                    $('.newloadfile').attr('required',false);
                    $('#addExercise').val(false);
                }
            });

            var inc = 1;
            $('#agregar').click(function(){
                console.log(inc);
                inc++;

                var html = "";
                html += '<div class="row col-md-12 my-3">'+
                            '<input type="hidden" id="input-'+inc+'" value="'+inc+'">'+
                            '<div class="col-12">'+
                                '<h5 class="" style="color: #51A2A7; font-weight: bold">'+
                                    'Nuevo Ejercicio/Reto '+inc+
                                '</h5>'+
                            '</div>'+
                            '<div class="col">'+
                                '<label for="">Cargar PDF:</label>'+
                                '<input type="file" id="pdf'+inc+'" name="newpdf['+inc+']" accept=".pdf" class="newloadfile form-control" required>'+
                            '</div>'+
                            '<div class="col">'+
                                '<label for="">Cargar Video:</label>'+
                                '<input type="file" id="video'+inc+'" name="newvideo['+inc+']" accept=".mp4" class="newloadfile form-control" required>'+
                            '</div>'+
                        '</div>';
                $('.ejercicios').append(html);
            });

            $('#borrar').click(function(){
                if(inc > 1){
                    console.log(inc);
                    $('#input-'+inc).parent().remove();
                    inc--;
                }
            });

        //Enviar a Almacenar el Edit
            $("#tupdate").on('submit', function(e){
                e.preventDefault();
                var type = $(this).attr('method');
                var url = $(this).attr('action');
                $.ajax({
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = ((evt.loaded / evt.total) * 100);
                                $(".progress-bar").width(percentComplete + '%');
                            }
                        }, false);
                        return xhr;
                    },
                    type: type,
                    url: url,
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function(){
                        $(".progress-bar").width('0%');
                        jQuery('#uploadStatus').hide();
                        jQuery('#uploadStatus').html('');
                    },
                    error:function(){
                        $('#uploadStatus').html('<span class="invalid-feedback d-block" role="alert"><strong>No se pudo editar de forma exitosa la capacitación, error de conexión</strong></span>');
                    },
                    success: function(resp){
                        console.log(resp);
                        jQuery('#uploadStatus').show();
                        jQuery.each(resp.errors, function(key, value){
                            jQuery('#uploadStatus').append('<p><span class="invalid-feedback d-block" role="alert"><strong>'+value+'</strong></span></p>');
                        });

                        if(resp.success){
                            $('#uploadStatus').append('<p><span style="color:#28A74B;" class="invalid-feedback d-block" role="alert"><strong>'+resp.success+'</strong></span></p>');
                            alert(resp.success);
                            location.href = $('#redirect').val();
                        }

                        if(resp.fallo)
                            jQuery('#uploadStatus').append('<p><span class="invalid-feedback d-block" role="alert"><strong>'+resp.fallo+'</strong></span></p>');
                    },
                });
            });
    });
</script>