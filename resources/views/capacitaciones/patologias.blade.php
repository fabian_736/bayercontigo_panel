@extends('layouts.admin.app')

@section('content')


<div class="col-md-12 mx-auto" >

    <h5 class="font-weight-bold my-3" style="color: #51A2A7">Listado de Productos con Capacitaciones</h5>

    <table class="table table-striped " id="example">
        <thead style="background-color: #51A2A7; color: #fff">
            <tr class="text-center">
                <th class="font-weight-bold">Código</th>
                <th class="font-weight-bold">Nombre</th>
                <th class="font-weight-bold">Número de Pacientes</th>
                <th class="font-weight-bold">Número de Entrenamientos</th>
                <th class="font-weight-bold">Fecha de Creación</th>
                <th class="font-weight-bold">Ver Capacitaciones</th>
            </tr>
        </thead>
        <tbody class="table-light" style="background-color: #fff; color: #000">
            @foreach ($patologias as $patologia)
                @if(!$patologia->capacitaciones->isEmpty())
                    <tr class="text-center" id="row{{ $patologia->id_patologia }}">
                        <td>{{ $patologia->id_patologia }}</td>
                        <td id="nom{{ $patologia->id_patologia }}">{{ $patologia->nombre }}</td>
                        <td>
                            {{ $patologia->pacientes->count() }} 
                            <i class="fas fa-users" style="color: #51A2A7"></i>
                        </td>
                        <td>
                            {{ $patologia->capacitaciones->count() }} 
                            <i class="fas fa-clipboard" style="color: #51A2A7"></i>
                        </td>
                        <td>{{ date_format(date_create($patologia->created_at), "F j, Y") }}</td>
                        <td class="text-center">
                            <a href="{{ route('capacitacion.list', ['id_patologia' => $patologia->id_patologia, 'id_producto' => 0]) }}" style="color: #51A2A7; cursor: pointer">
                                <i class="far fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>
@endsection