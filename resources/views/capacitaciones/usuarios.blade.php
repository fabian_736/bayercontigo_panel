@extends('layouts.admin.app')
@section('content')


    <div class="row mx-auto my-5">
        <div class="col">
            <label for="" class="lead" style="color: #51A2A7; font-weight: bold">Lista de usuarios</label>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive shadow p-3 mb-5 rounded">
                <table id="example" class="table table-striped" cellspacing="0" width="100%">
                    <thead style="background-color: #51A2A7; color: #fff;">
                        <tr>
                            <th>Nombre</th>
                            <th>País</th>
                            <th>Tipo</th>
                            <th>Correo</th>
                            <th>Capacitaciones realizadas</th>
                            <th>Capacitaciones exitosas</th>
                            <th>Capacitaciones fallidas</th>
                            <th>Puntos Acumulados</th>
                            <th>Ver detalles</th>
                        </tr>
                    </thead>
                    <tbody class="table-light" style="background-color: #fff; color: #000">
                        @foreach($usuarios as $user)
                            <?php 
                                //Evaluar cuando tuvo al menos una respuesta exitosa
                                $exitosa = 0;
                                $fallida = 0;
                                $cantidad = 0;
                                $estrellas = 0;
                                $nopaso = true;
                                foreach ($user->cuestionarios as $cuestionario) {
                                    foreach ($trivia->respuestas as $respuesta) {
                                        if($respuesta->correcta){
                                            $exitosa++;
                                            $nopaso = false;
                                            break;
                                        }
                                    }
                                    
                                    $cantidad ++;

                                    if($nopaso) $fallida++;
                                }

                                if($exitosa + $fallida != $cantidad)
                                    $fallida = $cantidad - $exitosa;
                            ?>
                            <tr>
                                <td>
                                    @if($user->tipo == 'paciente')
                                        {{
                                            $user->paciente ? 
                                            $user->paciente->nombre . ' ' . $user->paciente->apellido : "" 
                                        }}
                                    @else
                                        {{
                                            $user->profesional ? 
                                            $user->profesional->nombre . ' ' . $user->profesional->apellido : ""
                                        }}
                                    @endif
                                </td>
                                <td>{{ $user->pais }}</td>
                                <td>{{ $user->tipo }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $cantidad }}</td>
                                <td>{{ $exitosa }}</td>
                                <td>{{ $fallida }}</td>
                                <td>{{ $user->total_puntos }}</td>
                                <td class="text-center">
                                    <a id="{{ $user->id }}" class="openModal" style="color: #51A2A7; cursor: pointer"> 
                                        <i class="far fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="exampleModalLabel" style="color: #51A2A7">DETALLES DE
                        USUARIO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="div_trivias">
                    <!-- DINAMICAMENTE AGREGO LO DE LAS TRIVIAS -->
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="urlModal" value="{{ route('capacitacion.cliente') }}">

@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        //ABRIR MODAL CON LA DATA RECIBIDA POR AJAX
        $('.openModal').click(function(){
            var id = $(this).attr('id');
            var url = $('#urlModal').val();

            $.ajax({
                url: url+'/'+id,
                data: {
                    "_token": $("meta[name='csrf-token']").attr("content")
                },
                type: 'POST',
                success: function(response){
                    if(response.trivias){
                        $("#div_trivias").empty();
                        var count = 1;
                        var correctas = 0;
                        var incorrectas = 0;

                        for (let trivia of response.trivias) {
                            correctas = 0;
                            incorrectas = 0;
                            console.log(trivia);
                            for (let respuesta of trivia.respuestas) {
                                if(respuesta["correcta"])
                                    correctas++;
                                else
                                    incorrectas++;
                            }

                            html =  '<div class="row-reverse p-0 m-0 mb-3" >'+
                                        '<div class="col" style="background: #51A2A7">'+
                                            '<label for="" class="font-weight-bold text-white lead p-2">Capacitacion #'+count+'</label>'+
                                        '</div>'+
                                        '<div class="col m-0 p-0">'+
                                            '<ul style="width: 100% !important" class="p-0 m-0">'+
                                                '<li style="border-style: solid; border-color: #e0e0e0; list-style:none; padding: 5px">Trivia: '+trivia["nombre"]+'</li>'+
                                                '<li style="border-style: solid; border-color: #e0e0e0; list-style:none; padding: 5px">Preguntas Incorrectas: '+incorrectas+'</li>'+
                                                '<li style="border-style: solid; border-color: #e0e0e0; list-style:none; padding: 5px">Preguntas Correctas: '+correctas+'</li>'+
                                                '<li style="border-style: solid; border-color: #e0e0e0; list-style:none; padding: 5px">Puntos Obtenidos: '+trivia["numero_estrellas"]+'</li>'+
                                            '</ul>'+
                                        '</div>'+
                                    '</div>';
                            $("#div_trivias").append(html);
                            count++;
                        }
                            
                        /* Swal.fire(
                            response.message,
                            'Presiona el boton para cerrar el modal',
                            'success'
                        ) */
                        $('#exampleModal').modal('show')
                    }
                    else{
                        Swal.fire(
                            response.message,
                            'Presiona el boton para cerrar el modal',
                            'warning'
                        )
                    }
                }
            });   
        }); 
    });
</script>