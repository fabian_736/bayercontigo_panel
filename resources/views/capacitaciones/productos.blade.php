@extends('layouts.admin.app')

@section('content')


<div class="col-md-12 mx-auto" >

    <h5 class="font-weight-bold my-3" style="color: #51A2A7">Listado de Productos con Capacitaciones</h5>

    <table class="table table-striped " id="example">
        <thead style="background-color: #51A2A7; color: #fff">
            <tr class="text-center">
                <th class="font-weight-bold">Código</th>
                <th class="font-weight-bold">Imagen</th>
                <th class="font-weight-bold">Nombre</th>
                <th class="font-weight-bold">Descripción</th>
                <th class="font-weight-bold">Número de Entrenamientos</th>
                <th class="font-weight-bold">Fecha de Creación</th>
                <th class="font-weight-bold">Ver Capacitaciones</th>
            </tr>
        </thead>
        <tbody class="table-light" style="background-color: #fff; color: #000">
            @foreach ($productos as $producto)
                @if(!$producto->capacitaciones->isEmpty())
                    <tr class="text-center" id="row{{ $producto->id_producto }}">
                        <td>{{ $producto->id_producto }}</td>
                        <td class="mx-auto text-center" > 
                            <img style="width:100px;max-height:100px;" id="image{{ $producto->id_producto }}"
                                src="{{ route('producto.icon', ['filename' => basename($producto->imagen)]) }}" >
                        </td>
                        <td id="nom{{ $producto->id_producto }}">{{ $producto->nombre }}</td>
                        <td id="desc{{ $producto->id_producto }}">{{ $producto->descripcion }}</td>
                        <td>
                            {{ $producto->capacitaciones->count() }} 
                            <i class="fas fa-clipboard" style="color: #51A2A7"></i>
                        </td>
                        <td>{{ date_format(date_create($producto->created_at), "F j, Y") }}</td>
                        <td class="text-center">
                            <a href="{{ route('capacitacion.list', ['id_producto' => $producto->id_producto]) }}" style="color: #51A2A7; cursor: pointer">
                                <i class="far fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>
@endsection