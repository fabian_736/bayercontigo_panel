@extends('layouts.auth.app')
@section('content')

<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center ">

        <div class="col-xl-10 col-lg-12 col-md-12 my-3">

            <div class="o-hidden border-0 shadow-lg">
                <div class="card-body p-0 " >
                    <!-- Nested Row within Card Body -->
                    <div class="row ">
                        <div class="col-lg-7 d-none d-lg-block bg-login-image" style="align-self: center;">
                            <div class="row p-3"  >
                                <div class="col-md-10 col-lg-10 mx-auto"  style="background:url('img/dentapp/logo.png');background-size:cover;height: 100px;width: 100px; align-self: center;"></div>
                            </div>
                        </div>

                        @if (Session::has('error'))
                            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                                {{ Session::get('error') }}
                            </div>
                        @endif

                        @if (Session::has('edit'))
                            <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                                {{ Session::get('edit') }}
                            </div>
                        @endif

                        @if($passwordReset)
                            <div class="col-lg-5" style="background-color: rgba(245, 245, 245, 0.7);">
                                <div class="p-5" style="margin-top: 10px; margin-bottom: 50px;">
                                    <div class="text-center ">
                                        <img src="{{url('img/logo_login.png')}}" alt="" class="w-50" style="margin-bottom: 30px;">
                                    </div>

                                    <h1 class="h4 text-gray-900 mb-2">Restablecer Contraseña</h1>
                                    <label class="text-center my-4" style="color: #053F72 !important;" for="">
                                        Por favor digita la siguiente informacion
                                    </label>

                                    <form class="user" method="POST" action="{{ route('password.update') }}">
                                        @csrf

                                        <input type="hidden" name="token" value="{{ $passwordReset->token }}">
                                        <input type="hidden" name="tipo" value="{{ $passwordReset->tipo }}">

                                        <div class="form-group">
                                            <input type="email" name="email"
                                                class="form-control form-control-user @error('email') is-invalid @enderror"
                                                id="email" required
                                                placeholder="Correo electronico" value="{{ $passwordReset->email }}" readonly> 
                                            
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <input type="password" name="password"
                                                class="form-control form-control-user @error('password') is-invalid @enderror"
                                                id="password" required
                                                placeholder="Nueva Contraseña"> 
                                            
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <input type="password" name="password_confirmation"
                                                class="form-control form-control-user @error('password_confirmation') is-invalid @enderror"
                                                id="password_confirmation" required
                                                placeholder="Confirmar Contraseña"> 
                                            
                                            @error('password_confirmation')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    
                                        <input type="submit" value="ENVIAR CÓDIGO"
                                                class="btn btn-user btn-block mt-5"
                                                style="border-radius: 40px; background-color: #AEDC5A; color: #053F72; font-weight: bold;">
                                    </form>
                                    <div class="text-center">
                                        <a class="text-white" href="{{ route('login') }}" style="font-size: 13px; color: #053F72 !important;">Volver al login</a>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="col-lg-5" style="background-color: rgba(245, 245, 245, 0.7);">
                                <div class="p-5" style="margin-top: 10px; margin-bottom: 50px;">
                                    <p class="mb-4">{{ $mensaje }}</p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>


@endsection

<style>::placeholder {
  color: blue !important;
}</style>