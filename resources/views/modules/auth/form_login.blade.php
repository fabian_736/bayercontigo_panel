@extends('layouts.auth.app')
@section('content')

    <div class="container position-sticky z-index-sticky top-0">
        <div class="row">
            <div class="col-12">
            </div>
        </div>
    </div>
    <main class="main-content  mt-0">
        <section>
            <div class="page-header min-vh-75">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-4 col-lg-5 col-md-6 d-flex flex-column mx-auto">
                            <div class="card card-plain mt-8">
                                <div class="card-header pb-0 text-left bg-transparent">
                                    <div class="row-reverse ">
                                        <div class="col d-flex justify-content-center">
                                            <img src="{{ url('img/logo_login.png') }}" alt="" width="100">
                                        </div>
                                        <div class="col d-flex justify-content-center">
                                            <p class="mb-0" style="color: #053F72; font-weight: bold"> <i>Panel
                                                    administrativo</i> </p>
                                        </div>
                                    </div>

                                </div>
                                <div class="card-body">
                                    <form class="user" method="post" action="{{ route('login.auth') }}">
                                        @csrf

                                        @if (Session::has('message'))
                                            <div
                                                style="border-radius: 5px; padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 3%;">
                                                {{ Session::get('message') }}
                                            </div>
                                        @endif

                                        @if (Session::has('edit'))
                                            <div
                                                style="border-radius: 5px; padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                                                {{ Session::get('edit') }}
                                            </div>
                                        @endif

                                        <label>Correo electronico:</label>
                                        <div class="mb-3">
                                            <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Correo electronico"
                                            id="exampleInputEmail" aria-describedby="emailHelp"
                                            value="{{ old('email') }}" autocomplete="email" autofocus
                                            placeholder="Correo electronico" name="email" required>
                                        </div>

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror


                                        <label>Contraseña:</label>
                                        <div class="mb-3">
                                            <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Contraseña"
                                                id="exampleInputPassword" placeholder="Contraseña" name="password" required
                                                autocomplete="current-password">
                                        </div>

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror


                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" id="rememberMe" checked="">
                                            <label class="form-check-label" for="rememberMe">Recordar Sesión</label>
                                        </div>
                                        <div class="text-center">
                                          <input type="submit" value="INICIAR SESION"
                                          class="btn w-100 mt-4 mb-0 text-white" 
                                                style="border-radius: 40px; background-color: #AEDC5A; color: #053F72; font-weight: bold;">
                                        </div>
                                    </form>
                                </div>
                                <div class="card-footer text-center pt-0 px-lg-2 px-1">
                                    <a href="{{ route('password.forgot') }}" class="mb-4 text-sm mx-auto">
                                        ¿Olvidaste tu contraseña?
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="oblique position-absolute top-0 h-100 d-md-block d-none me-n8">
                                <div class="oblique-image bg-cover position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6"
                                    style="background-image:url('https://cdn.mensagenscomamor.com/content/images/m000506738.jpg?v=1')">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <!-- -------- START FOOTER 3 w/ COMPANY DESCRIPTION WITH LINKS & SOCIAL ICONS & COPYRIGHT ------- -->
    <footer class="footer py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mb-4 mx-auto text-center">

                    <a href="javascript:;" target="_blank" class="text-secondary me-xl-5 me-3 mb-sm-0 mb-2">
                        Acerca de nosotros
                    </a>
                    <a href="javascript:;" target="_blank" class="text-secondary me-xl-5 me-3 mb-sm-0 mb-2">
                        Blog
                    </a>
                    <a href="javascript:;" target="_blank" class="text-secondary me-xl-5 me-3 mb-sm-0 mb-2">
                        Licencia
                    </a>
                </div>
                <div class="col-lg-8 mx-auto text-center mb-4 mt-2">
                    <a href="javascript:;" target="_blank" class="text-secondary me-xl-4 me-4">
                        <i class="fab fa-facebook"></i>
                    </a>
                    <a href="javascript:;" target="_blank" class="text-secondary me-xl-4 me-4">
                        <span class="text-lg fab fa-twitter"></span>
                    </a>
                    <a href="javascript:;" target="_blank" class="text-secondary me-xl-4 me-4">
                        <span class="text-lg fab fa-instagram"></span>
                    </a>
                    <a href="javascript:;" target="_blank" class="text-secondary me-xl-4 me-4">
                        <span class="text-lg fab fa-pinterest"></span>
                    </a>
                    <a href="javascript:;" target="_blank" class="text-secondary me-xl-4 me-4">
                        <span class="text-lg fab fa-github"></span>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-8 mx-auto text-center mt-1">
                    <p class="mb-0 text-secondary">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> <a href="https://peoplemarketing.com/inicio/"
                            class="font-weight-bold">People Marketing S.A.S</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>

@endsection

<style>
    ::placeholder {
        color: gray !important;
    }

</style>
