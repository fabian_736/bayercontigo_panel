@extends('layouts.admin.app')

@section('content')


    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    <div class="container-fluid">
        <form action="{{ route('update.perfil') }}" method="post" enctype="multipart/form-data" class="col-md-12">
            @csrf
            <div class="page-header min-height-300 border-radius-xl mt-4"
                style="background: radial-gradient(54.06% 54.06% at 18.84% 26.21%, #AEDC5A 0%, #02718B 100%);">
                <span class="mask  opacity-6"></span>
            </div>
            <div class="card card-body blur shadow-blur mx-4 mt-n6 overflow-hidden">
                <div class="row gx-4">
                    <div class="col-auto">
                        <div class="avatar avatar-xl position-relative">
                            <img src="{{ route('admin.icon', ['filename' => basename(auth()->user()->avatar)]) }}"
                                alt="profile_image" class="w-100 border-radius-lg shadow-sm">
                        </div>
                    </div>
                    <div class="col-auto my-auto">
                        <div class="h-100">
                            <h5 class="mb-1">
                                {{ $admin->nombre . ' ' . $admin->apellido }}
                            </h5>
                            <p class="mb-0 font-weight-bold text-sm">
                                {{ $admin->roles->nombre }} <br>
                                {{ $admin->estado ? 'Activo' : 'Inactivo' }}
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
                        <div class="nav-wrapper position-relative end-0">
                            <ul class="nav nav-pills nav-fill p-1 bg-transparent" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link mb-0 px-0 py-1 " data-bs-toggle="tab" href="javascript:;" role="tab"
                                        aria-selected="false">
                                        <svg class="text-dark" width="16px" height="16px" viewBox="0 0 40 40"
                                            version="1.1" xmlns="http://www.w3.org/2000/svg"
                                            xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <title>settings</title>
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <g transform="translate(-2020.000000, -442.000000)" fill="#FFFFFF"
                                                    fill-rule="nonzero">
                                                    <g transform="translate(1716.000000, 291.000000)">
                                                        <g transform="translate(304.000000, 151.000000)">
                                                            <polygon class="color-background" opacity="0.596981957"
                                                                points="18.0883333 15.7316667 11.1783333 8.82166667 13.3333333 6.66666667 6.66666667 0 0 6.66666667 6.66666667 13.3333333 8.82166667 11.1783333 15.315 17.6716667">
                                                            </polygon>
                                                            <path class="color-background"
                                                                d="M31.5666667,23.2333333 C31.0516667,23.2933333 30.53,23.3333333 30,23.3333333 C29.4916667,23.3333333 28.9866667,23.3033333 28.48,23.245 L22.4116667,30.7433333 L29.9416667,38.2733333 C32.2433333,40.575 35.9733333,40.575 38.275,38.2733333 L38.275,38.2733333 C40.5766667,35.9716667 40.5766667,32.2416667 38.275,29.94 L31.5666667,23.2333333 Z"
                                                                opacity="0.596981957"></path>
                                                            <path class="color-background"
                                                                d="M33.785,11.285 L28.715,6.215 L34.0616667,0.868333333 C32.82,0.315 31.4483333,0 30,0 C24.4766667,0 20,4.47666667 20,10 C20,10.99 20.1483333,11.9433333 20.4166667,12.8466667 L2.435,27.3966667 C0.95,28.7083333 0.0633333333,30.595 0.00333333333,32.5733333 C-0.0583333333,34.5533333 0.71,36.4916667 2.11,37.89 C3.47,39.2516667 5.27833333,40 7.20166667,40 C9.26666667,40 11.2366667,39.1133333 12.6033333,37.565 L27.1533333,19.5833333 C28.0566667,19.8516667 29.01,20 30,20 C35.5233333,20 40,15.5233333 40,10 C40,8.55166667 39.685,7.18 39.1316667,5.93666667 L33.785,11.285 Z">
                                                            </path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                        <a class="ms-1" href="javascript:;" onclick="one()">Editar Perfil</a>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-frame my-5">
                <div class="card-body">
                    <div id="primero">
                        <div class="row mb-3">
                            <div class="col">
                                <label for="">Correo electronico</label>
                                <input type="email" name="email" class="form-control" placeholder="Correo electronico"
                                    value="{{ $admin->email }}" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="exampleInputPassword" class="pl-3">Contraseña</label>
                                <input type="password" class="form-control" id="exampleInputPassword1"
                                    placeholder="Contraseña" value="{{ $admin->password }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div id="segundo" style="display: none">
                        <div class="row">
                            <div class="col d-flex justify-content-center">
                                <label for="" class="h4">Editar Perfil</label>
                            </div>
                        </div>
                        <div class="row-reverse">
                            <div class="col">
                                <div class="form-group col-md-6 my-5 mx-auto">
                                    <label for="upload" class="pl-3">Cambiar Avatar</label>
                                    <input type="file" class="form-control @error('foto') is-invalid @enderror" id="upload"
                                        placeholder="Contraseña" name="avatar" accept=".png,.jpg,.jpeg">
                                    @error('foto')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group col-md-6 my-5 mx-auto">
                                    <label for="exampleInputPassword" class="pl-3"">Nueva
                                        contraseña (Opcional)</label>
                                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                                        id="exampleInputPassword1" placeholder="Contraseña" name="password">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group col-md-6 my-5 mx-auto">
                                    <label for="exampleInputPassword" class="pl-3">Repite
                                        contraseña</label>
                                    <input type="password" class="form-control @error('confirmacion') is-invalid @enderror"
                                        id="exampleInputPassword1" placeholder="Contraseña" name="confirmacion">
                                    @error('confirmacion')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col d-flex justify-content-center">
                                <button class="btn col-md-6 text-white" style="background-color: #AEDC5A;" type="submit">
                                    ACTUALIZAR PERFIL
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>


    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
crossorigin="anonymous"></script>

<script>
    function one() {
        $('#primero').hide();
        $('#segundo').show();
    }
</script>


<script>
    $(document).ready(function() {
        $('#pass2').keyup(function() {
            var pass1 = $('#pass1').val();
            var pass2 = $('#pass2').val();
            if (pass1 == pass2) {

                $('#check').removeClass('fas fa-exclamation').addClass('fas fa-check')
            } else {

                $('#check').removeClass('fas fa-check').addClass('fas fa-exclamation')
            }
            if (pass1 === '' || pass2 === '') {
                $('#check').removeClass('fas fa-check').removeClass('fas fa-exclamation')
            }
        });

        //Para enviar el preview de la imagen al cargar
        function readImage(input) {
            var url = input.value;
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

            if (input.files && input.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#previewImage').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            } else {
                $('#previewImage').attr('src', null);
                input.value = null;
            }
        }

        $("#upload").change(function() {
            readImage(this);
        });

        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }
    });

</script>