<!-- SIDEBAR -->
@include('layouts.admin.components.head')
<!-- FIN SIDEBAR -->

<body class="g-sidenav-show  bg-gray-100">

    <!-- SIDEBAR -->
    @include('layouts.admin.components.sidebar')
    <!-- FIN SIDEBAR -->
    <!-- NAVBAR -->
    @include('layouts.admin.components.navbar')
    <!-- FIN NAVBAR -->
    <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">



        <div class="container p-0 my-5">

           <div class="row mb-5">
               <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
                 <div class="card">
                   <div class="card-body p-3">
                     <div class="row">
                       <div class="col-8">
                         <div class="numbers">
                           <p class="text-sm mb-0 text-capitalize font-weight-bold">Usuarios registrados</p>
                           <h5 class="font-weight-bolder mb-0">
                             24
                           </h5>
                         </div>
                       </div>
                       <div class="col-4 text-end">
                         <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                           <i class="fas fa-users"></i>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
               </div>
               <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
                 <div class="card">
                   <div class="card-body p-3">
                     <div class="row">
                       <div class="col-8">
                         <div class="numbers">
                           <p class="text-sm mb-0 text-capitalize font-weight-bold">Premios disponibles</p>
                           <h5 class="font-weight-bolder mb-0">
                             770
                           </h5>
                         </div>
                       </div>
                       <div class="col-4 text-end">
                         <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                           <i class="fas fa-gifts"></i>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
               </div>
               <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
                 <div class="card">
                   <div class="card-body p-3">
                     <div class="row">
                       <div class="col-8">
                         <div class="numbers">
                           <p class="text-sm mb-0 text-capitalize font-weight-bold">Visitas al aplicativo</p>
                           <h5 class="font-weight-bolder mb-0">
                             53
                             <span class="text-danger text-sm font-weight-bolder">-15%</span>
                           </h5>
                         </div>
                       </div>
                       <div class="col-4 text-end">
                         <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                           <i class="fas fa-eye"></i>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
               </div>
               <div class="col-xl-3 col-sm-6">
                 <div class="card">
                   <div class="card-body p-3">
                     <div class="row">
                       <div class="col-8">
                         <div class="numbers">
                           <p class="text-sm mb-0 text-capitalize font-weight-bold">Capacitaciones realizadas</p>
                           <h5 class="font-weight-bolder mb-0">
                             4
                             <span class="text-success text-sm font-weight-bolder">+5%</span>
                           </h5>
                         </div>
                       </div>
                       <div class="col-4 text-end">
                         <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                           <i class="fas fa-atlas"></i>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
               </div>
           </div>



            <div class="row">
                <label for="" class="h4 mb-5">Usuarios agregados recientemente <br> <hr style="background: #053F72; height: 2px; opacity: 1"></label>
               <div class="col">
                   <div class="card">
                       <div class="card-header p-0 mx-3 mt-3 position-relative z-index-1">
                           <a href="javascript:;" class="d-block d-flex justify-content-center">
                               <img src="https://s.france24.com/media/display/b1108fa4-f255-11e8-8346-005056bff430/bayer.jpg" class="img-fluid rounded-circle border-radius-lg" style="height: 200px !important" width="200">
                           </a>
                       </div>

                       <div class="card-body pt-2">
                           <span
                               class="text-gradient text-primary text-uppercase text-xs font-weight-bold my-2">Rol</span>
                           <a href="javascript:;" class="card-title h5 d-block text-darker">
                               Nombre del usuario
                           </a>
                           <p class="card-description mb-4">
                               ( <i>Descripcion de la especialidad</i> ) <br>
                               Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere laborum rem iusto hic qui eveniet cumque dignissimos, labore, necessitatibus ducimus quas itaque maiores expedita ratione harum, assumenda facilis ex saepe.
                           </p>
                           <div class="author align-items-center">
                               <img src="https://www.imbanaco.com/wp-content/uploads/2017/04/avatar-especialista-900x900.png" alt="..." class="avatar shadow">
                               <div class="name ps-3">
                                   <span>Admin</span>
                                   <div class="stats">
                                       <small>Publicado el 13 de octubre 2021</small>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
               <div class="col">
                   <div class="card">
                       <div class="card-header p-0 mx-3 mt-3 position-relative z-index-1">
                           <a href="javascript:;" class="d-block d-flex justify-content-center">
                               <img src="https://s.france24.com/media/display/b1108fa4-f255-11e8-8346-005056bff430/bayer.jpg" class="img-fluid rounded-circle border-radius-lg" style="height: 200px !important" width="200">
                           </a>
                       </div>

                       <div class="card-body pt-2">
                           <span
                               class="text-gradient text-primary text-uppercase text-xs font-weight-bold my-2">Rol</span>
                           <a href="javascript:;" class="card-title h5 d-block text-darker">
                               Nombre del usuario
                           </a>
                           <p class="card-description mb-4">
                               ( <i>Descripcion de la especialidad</i> ) <br>
                               Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere laborum rem iusto hic qui eveniet cumque dignissimos, labore, necessitatibus ducimus quas itaque maiores expedita ratione harum, assumenda facilis ex saepe.
                           </p>
                           <div class="author align-items-center">
                               <img src="https://www.imbanaco.com/wp-content/uploads/2017/04/avatar-especialista-900x900.png" alt="..." class="avatar shadow">
                               <div class="name ps-3">
                                   <span>Admin</span>
                                   <div class="stats">
                                       <small>Publicado el 13 de octubre 2021</small>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
               <div class="col">
                   <div class="card">
                       <div class="card-header p-0 mx-3 mt-3 position-relative z-index-1">
                           <a href="javascript:;" class="d-block d-flex justify-content-center">
                               <img src="https://s.france24.com/media/display/b1108fa4-f255-11e8-8346-005056bff430/bayer.jpg" class="img-fluid rounded-circle border-radius-lg" style="height: 200px !important" width="200">
                           </a>
                       </div>

                       <div class="card-body pt-2">
                           <span
                               class="text-gradient text-primary text-uppercase text-xs font-weight-bold my-2">Rol</span>
                           <a href="javascript:;" class="card-title h5 d-block text-darker">
                               Nombre del usuario
                           </a>
                           <p class="card-description mb-4">
                               ( <i>Descripcion de la especialidad</i> ) <br>
                               Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere laborum rem iusto hic qui eveniet cumque dignissimos, labore, necessitatibus ducimus quas itaque maiores expedita ratione harum, assumenda facilis ex saepe.
                           </p>
                           <div class="author align-items-center">
                               <img src="https://www.imbanaco.com/wp-content/uploads/2017/04/avatar-especialista-900x900.png" alt="..." class="avatar shadow">
                               <div class="name ps-3">
                                   <span>Admin</span>
                                   <div class="stats">
                                       <small>Publicado el 13 de octubre 2021</small>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
            </div>

            <div class="row my-5">
               <label for="" class="h4 mb-5">Ultimos premios <br> <hr style="background: #053F72; height: 2px; opacity: 1"></label>
              <div class="col">
                  <div class="card">
                      <div class="card-header p-0 mx-3 mt-3 position-relative z-index-1">
                          <a href="javascript:;" class="d-block">
                              <img src="https://s.france24.com/media/display/b1108fa4-f255-11e8-8346-005056bff430/bayer.jpg" class="img-fluid border-radius-lg" width="500">
                          </a>
                      </div>

                      <div class="card-body pt-2">
                          <span
                              class="text-gradient text-primary text-uppercase text-xs font-weight-bold my-2">House</span>
                          <a href="javascript:;" class="card-title h5 d-block text-darker">
                              Shared Coworking
                          </a>
                          <p class="card-description mb-4">
                              Use border utilities to quickly style the border and border-radius of an element. Great
                              for images, buttons.
                          </p>
                          <div class="author align-items-center">
                              <img src="https://www.imbanaco.com/wp-content/uploads/2017/04/avatar-especialista-900x900.png" alt="..." class="avatar shadow">
                              <div class="name ps-3">
                                  <span>Admin</span>
                                  <div class="stats">
                                      <small>Posted on 28 February</small>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
               <div class="col">
                  <div class="card">
                      <div class="card-header p-0 mx-3 mt-3 position-relative z-index-1">
                          <a href="javascript:;" class="d-block">
                              <img src="https://s.france24.com/media/display/b1108fa4-f255-11e8-8346-005056bff430/bayer.jpg" class="img-fluid border-radius-lg" width="500">
                          </a>
                      </div>

                      <div class="card-body pt-2">
                          <span
                              class="text-gradient text-primary text-uppercase text-xs font-weight-bold my-2">House</span>
                          <a href="javascript:;" class="card-title h5 d-block text-darker">
                              Shared Coworking
                          </a>
                          <p class="card-description mb-4">
                              Use border utilities to quickly style the border and border-radius of an element. Great
                              for images, buttons.
                          </p>
                          <div class="author align-items-center">
                              <img src="https://www.imbanaco.com/wp-content/uploads/2017/04/avatar-especialista-900x900.png" alt="..." class="avatar shadow">
                              <div class="name ps-3">
                                  <span>Admin</span>
                                  <div class="stats">
                                      <small>Posted on 28 February</small>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col">
                  <div class="card">
                      <div class="card-header p-0 mx-3 mt-3 position-relative z-index-1">
                          <a href="javascript:;" class="d-block">
                              <img src="https://img.lalr.co/cms/2019/02/01090028/Bayer.jpg?size=md" class="img-fluid border-radius-lg" width="500" >
                          </a>
                      </div>

                      <div class="card-body pt-2">
                          <span
                              class="text-gradient text-primary text-uppercase text-xs font-weight-bold my-2">House</span>
                          <a href="javascript:;" class="card-title h5 d-block text-darker">
                              Shared Coworking
                          </a>
                          <p class="card-description mb-4">
                              Use border utilities to quickly style the border and border-radius of an element. Great
                              for images, buttons.
                          </p>
                          <div class="author align-items-center">
                              <img src="https://www.imbanaco.com/wp-content/uploads/2017/04/avatar-especialista-900x900.png" alt="..." class="avatar shadow">
                              <div class="name ps-3">
                                  <span>Admin</span>
                                  <div class="stats">
                                      <small>Posted on 28 February</small>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
           </div>

           <div class="row">
               <label for="" class="h4 mb-5">Ultimas novedades <br> <hr style="background: #053F72; height: 2px; opacity: 1"></label>
              <div class="col">
                  <div class="card">
                      <div class="card-header p-0 mx-3 mt-3 position-relative z-index-1">
                          <a href="javascript:;" class="d-block">
                              <img src="https://s.france24.com/media/display/b1108fa4-f255-11e8-8346-005056bff430/bayer.jpg" class="img-fluid border-radius-lg" width="500">
                          </a>
                      </div>

                      <div class="card-body pt-2">
                          <span
                              class="text-gradient text-primary text-uppercase text-xs font-weight-bold my-2">House</span>
                          <a href="javascript:;" class="card-title h5 d-block text-darker">
                              Shared Coworking
                          </a>
                          <p class="card-description mb-4">
                              Use border utilities to quickly style the border and border-radius of an element. Great
                              for images, buttons.
                          </p>
                          <div class="author align-items-center">
                              <img src="https://www.imbanaco.com/wp-content/uploads/2017/04/avatar-especialista-900x900.png" alt="..." class="avatar shadow">
                              <div class="name ps-3">
                                  <span>Admin</span>
                                  <div class="stats">
                                      <small>Posted on 28 February</small>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
               <div class="col">
                  <div class="card">
                      <div class="card-header p-0 mx-3 mt-3 position-relative z-index-1">
                          <a href="javascript:;" class="d-block">
                              <img src="https://s.france24.com/media/display/b1108fa4-f255-11e8-8346-005056bff430/bayer.jpg" class="img-fluid border-radius-lg" width="500">
                          </a>
                      </div>

                      <div class="card-body pt-2">
                          <span
                              class="text-gradient text-primary text-uppercase text-xs font-weight-bold my-2">House</span>
                          <a href="javascript:;" class="card-title h5 d-block text-darker">
                              Shared Coworking
                          </a>
                          <p class="card-description mb-4">
                              Use border utilities to quickly style the border and border-radius of an element. Great
                              for images, buttons.
                          </p>
                          <div class="author align-items-center">
                              <img src="https://www.imbanaco.com/wp-content/uploads/2017/04/avatar-especialista-900x900.png" alt="..." class="avatar shadow">
                              <div class="name ps-3">
                                  <span>Admin</span>
                                  <div class="stats">
                                      <small>Posted on 28 February</small>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col">
                  <div class="card">
                      <div class="card-header p-0 mx-3 mt-3 position-relative z-index-1">
                          <a href="javascript:;" class="d-block">
                              <img src="https://img.lalr.co/cms/2019/02/01090028/Bayer.jpg?size=md" class="img-fluid border-radius-lg" width="500" >
                          </a>
                      </div>

                      <div class="card-body pt-2">
                          <span
                              class="text-gradient text-primary text-uppercase text-xs font-weight-bold my-2">House</span>
                          <a href="javascript:;" class="card-title h5 d-block text-darker">
                              Shared Coworking
                          </a>
                          <p class="card-description mb-4">
                              Use border utilities to quickly style the border and border-radius of an element. Great
                              for images, buttons.
                          </p>
                          <div class="author align-items-center">
                              <img src="https://www.imbanaco.com/wp-content/uploads/2017/04/avatar-especialista-900x900.png" alt="..." class="avatar shadow">
                              <div class="name ps-3">
                                  <span>Admin</span>
                                  <div class="stats">
                                      <small>Posted on 28 February</small>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
           </div>

           




        </div>

    </main>

    
    <!--   Core JS Files   -->
    <script src="{{ url('js/app.js') }}"></script>

    <!-- END (SCRIPTS) -->
    @include('layouts.admin.components.end')
    <!-- FIN END (SCRIPTS) -->

</body>

</html>
