@extends('layouts.admin.app')

@section('content')

<div class="container mt-5">
    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif
    
    @if ($estado)
        <div class="row ">
            <div class="col-md-12">
                <a href="{{ route('canjeado.index') }}" class="btn text-white mb-3" style="background-color: #e7344c;">
                    <i class="fas fa-undo-alt"></i>
                    Ver Cualquier Estado
                </a>
            </div>
        </div>
    @endif

    <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 2%; color: #51A2A7">
        Lista de premios canjeados {{ $estado ? "( ".$estado." )" : "" }}
    </label>

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="example" class="table table-striped " cellspacing="0" width="100%">
                    <thead style="background-color: #FFFFFF; color: #51A2A7; font-weight: bold">
                        <tr class="text-center">
                            <th>Cliente</th>
                            <th>Cédula</th>
                            <th>Premio</th>
                            <th>Marca</th>
                            <th>Cantidad</th>
                            <th>Puntos</th>
                            <th>Fecha Redención</th>
                            <th>Estado</th>
                            <th>Posible Entrega</th>
                            <th>Datos de Entrega</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="table-bordered" style="background-color: #fff; color: #000">
                        @foreach($canjeados as $canjeado)
                            <tr class="text-center">
                                <td class="align-middle">
                                    {{ $canjeado->usuario->paciente ? 
                                        $canjeado->usuario->paciente->nombre.' '.$canjeado->usuario->paciente->apellido :
                                        $canjeado->usuario->profesional->nombre.' '.$canjeado->usuario->profesional->apellido  
                                    }}
                                </td>
                                <td class="align-middle">{{ $canjeado->entrega->cedula }}</td>
                                <td class="align-middle">{{ $canjeado->nombre }}</td>
                                <td class="align-middle">{{ $canjeado->marca }}</td>
                                <td class="align-middle">{{ $canjeado->cantidad }}</td>
                                <td class="align-middle">{{ $canjeado->total_puntos }}</td>
                                <td class="align-middle">
                                    {{ date_format(date_create($canjeado->created_at), "F j, Y") }}
                                </td>
                                <td class="align-middle">
                                    <span class="alert text-white" style="background-color: #51A2A7;">
                                        {{ $canjeado->entrega->estado }}
                                    </span>
                                </td>
                                <td class="align-middle">
                                    @if($canjeado->entrega->estado != "entregado")
                                        {{ $canjeado->entrega->tiempo ? $canjeado->entrega->tiempo : "Falta Actualizar" }}
                                    @else
                                        Recibido por el Cliente
                                    @endif
                                </td>
                                <td class="align-middle">
                                    <a id="{{ route('canjeado.datos', $canjeado->id_canjeado) }}" 
                                        class="btn btn-sm btn-danger mx-auto w_open text-white">
                                        Ver Datos <i class="fas fa-eye"></i>
                                    </a>
                                </td>
                                <td class="align-middle">
                                    @if($canjeado->entrega->estado != "entregado")
                                        <a href="{{ route('canjeado.edit', $canjeado->id_canjeado) }}" style="color: #51A2A7;"> <i class="far fa-edit"></i></a>
                                    @endif
                                </td>
                                <input type="hidden" id="tipo{{ $canjeado->id_canjeado }}" value="{{ $canjeado->entrega->tipo }}">
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        $( ".w_open" ).click(function() {
            var id = $(this).attr("id");
            window.open(id, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=200,left=500,width=600,height=500");
        });
    });
</script>
