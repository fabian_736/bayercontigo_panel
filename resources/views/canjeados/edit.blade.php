@extends('layouts.admin.app')

@section('content')
<div class="container mt-5">
    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    <div class="row mb-5 mx-auto">

        <div>
            <a href="{{ route('canjeado.index') }}">
                <button class="btn" style="background-color: #e7344c;">
                    Volver a Premios Canjeados
                </button>
            </a>
        </div>

        <!-- Editar premios -->
        <div class="col-md-12 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">


            <div class="card-header mt-3" style="background-color: #e7344c;">
                <h5 class="text-white my-auto">Editar Datos y Estado de Entrega</h5>
            </div>

            <form action="{{ route('canjeado.update',$canjeado->id_canjeado) }}" method="post" enctype="multipart/form-data">
                @csrf
                @if($canjeado->entrega->tipo == "Domicilio")
                    <div class="row col-md-12 my-4">
                        <div class="col">
                            <label for="name_user">Nombre del Receptor:</label>
                            <input name="receptor" id="" type="text" value="{{ old('receptor', $canjeado->entrega->receptor) }}"
                                class="form-control @error('receptor') is-invalid @enderror" required>
                            @error('receptor')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="name_user">Teléfono:</label>
                            <input name="telefono" id="" type="text" value="{{ old('telefono', $canjeado->entrega->telefono) }}" 
                                class="form-control @error('marca') is-invalid @enderror" required>
                            @error('telefono')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col">
                            <label for="name_user">Dirección de Entrega:</label>
                            <input name="direccion" id="" type="text" value="{{ old('direccion', $canjeado->entrega->direccion) }}" 
                                class="form-control @error('marca') is-invalid @enderror" required>
                            @error('direccion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="name_user">Cédula de Entrega:</label>
                            <input name="cedula" id="" type="text" value="{{ old('cedula', $canjeado->entrega->cedula) }}" 
                                class="form-control @error('marca') is-invalid @enderror" required>
                            @error('cedula')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 my-4">
                        <div class="col-md-12">
                            <label for="name_user">Información Adicional:</label>
                            <textarea name="adicional" id="" type="text" required
                                class="form-control @error('adicional') is-invalid @enderror">{{ old('descripcion', $canjeado->entrega->adicional) }}</textarea>
                            @error('adicional')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                @else
                    <div class="row col-md-12 my-4">
                        <div class="col">
                            <label for="name_user">Correo Electrónico de Envío:</label>
                            <input name="correo" id="" type="text" value="{{ old('correo', $canjeado->entrega->correo) }}"
                                class="form-control @error('correo') is-invalid @enderror" required>
                            @error('correo')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="name_user">Teléfono:</label>
                            <input name="telefono" id="" type="text" value="{{ old('telefono', $canjeado->entrega->telefono) }}" 
                                class="form-control @error('marca') is-invalid @enderror" required>
                            @error('telefono')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12">
                        <div class="col-md-6">
                            <label for="name_user">Cédula de Entrega:</label>
                            <input name="cedula" id="" type="text" value="{{ old('cedula', $canjeado->entrega->cedula) }}"
                                class="form-control @error('cedula') is-invalid @enderror" required>
                            @error('cedula')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                @endif

                <hr>
                <!-- ESTADO DE LA ENTREGA Y DÍA APROXIMADO DE ENTREGA -->
                <div class="row col-md-12">
                    <div class="col">
                        <label for="" class="mb-1" style="font-weight: bold; color: #51A2A7">
                            Status de la Entrega
                        </label>
                    </div>
                </div>

                <div class="row col-md-12 my-4">
                    <div class="col">
                        <label for="name_user">Estado de la Entrega:</label>
                        <select name="estado" class="form-control @error('estado') is-invalid @enderror">
                            <option value="pendiente" {{ old('estado', $canjeado->entrega->estado) == "pendiente" ? "selected" : "" }}>Pendiente</option>
                            <option value="empacando" {{ old('estado', $canjeado->entrega->estado) == "empacando" ? "selected" : "" }}>Empacando</option>
                            <option value="saliendo" {{ old('estado', $canjeado->entrega->estado) == "saliendo" ? "selected" : "" }}>Saliendo</option>
                            <option value="entregado" {{ old('estado', $canjeado->entrega->estado) == "entregado" ? "selected" : "" }}>Entregado</option>
                        </select>
                        @error('estado')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col">
                        <label for="name_user">Fecha Aproximada de Entrega:</label>
                        <input name="tiempo" id="" type="text" value="{{ old('tiempo', $canjeado->entrega->tiempo) }}" 
                            class="form-control @error('marca') is-invalid @enderror mt-1">
                        @error('tiempo')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <hr>
                <div class="row col-md-12 my-3">
                    <div class="col-md-6 mx-auto">
                        <button type="submit" class="form-control text-white" style="background-color: #64C2C8">Editar Datos</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }
    });
</script>