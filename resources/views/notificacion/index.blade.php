@extends('layouts.admin.app')

@section('content')


    @if (Session::has('create'))
        <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('create') }}
        </div>
    @endif

    @if (Session::has('delete'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('delete') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    @if ($notifications->isEmpty() && $readnotificaciones->isEmpty())
        No hay notificaciones disponibles
    @else
        <a href="{{ route('notificacion.deleteall') }}" id="delete-all" class="d-block my-5">
            Eliminar todas las notificaciones
        </a>

        @forelse($notifications as $not)
            <div class="card" style="background-color: #51A2A7" role="alert">
                <div class="row px-5 py-3 text-white d-flex align-items-center">
                         <!-- INSERT -->
                         @if ($not->notificacion->tipo == 'Insert')
                         <div class="icon-circle mx-2">
                             <i class="fas fa-book text-white"></i>
                         </div>
                         <!-- UPDATE -->
                     @elseif ($not->notificacion->tipo == "Update")
                         <div class="icon-circle mx-2">
                             <i class="fas fa-pencil-alt text-white"></i>
                         </div>
                         <!-- DELETE -->
                     @elseif ($not->notificacion->tipo == "Delete")
                         <div class="icon-circle mx-2">
                             <i class="fas fa-trash-alt text-white"></i>
                         </div>
                     @endif
                        {{ date_format(date_create($not->notificacion->created_at), 'F j, Y') }}
                        <strong class="ml-2">{{ $not->notificacion->nombre }}</strong> / ({{ $not->notificacion->descripcion }}).
                        <div class="ml-auto">
                        <a href="{{ route('notificacion.marcar', $not->notificacion->id_notificaciones) }}"
                            class="text-white mx-1">
                            Marcar como leida
                        </a>
                        <a href="{{ route('notificacion.delete', $not->notificacion->id_notificaciones) }}"
                      ><i class="far fa-trash-alt text-white" style="font-size: 16px"></i></a>
                        </div>
                </div>
            </div>

            @if ($loop->last)
                <a href="{{ route('notificacion.marcartodas') }}" id="mark-all">
                    Marcar todas como leidas
                </a>
            @endif
        @empty
            No hay nuevas notificaciones sin leer
        @endforelse

        @foreach ($readnotificaciones as $not)
            <div class="alert alert-secondary d-flex align-items-center" role="alert">
                <div class="mr-3">
                    <!-- INSERT -->
                    @if ($not->notificacion->tipo == 'Insert')
                        <div class="icon-circle bg-success">
                            <i class="fas fa-book text-white"></i>
                        </div>
                        <!-- UPDATE -->
                    @elseif ($not->notificacion->tipo == "Update")
                        <div class="icon-circle bg-primary">
                            <i class="fas fa-pencil-alt text-white"></i>
                        </div>
                        <!-- DELETE -->
                    @elseif ($not->notificacion->tipo == "Delete")
                        <div class="icon-circle bg-danger">
                            <i class="fas fa-trash-alt text-white"></i>
                        </div>
                    @endif
                </div>
                <div>
                    {{ date_format(date_create($not->notificacion->created_at), 'F j, Y') }}
                    <strong>{{ $not->notificacion->nombre }}</strong> / ({{ $not->notificacion->descripcion }}).

                    <a href="{{ route('notificacion.delete', $not->notificacion->id_notificaciones) }}"
                        class="btn btn-sm btn-danger mx-auto"><i class="far fa-trash-alt"></i></a>
                </div>
            </div>
        @endforeach
    @endif

@endsection
