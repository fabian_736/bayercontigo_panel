@extends('layouts.admin.app')

@section('content')

@if (Session::has('create'))
    <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('create') }}
    </div>
@endif

@if (Session::has('delete'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('delete') }}
    </div>
@endif

@if (Session::has('edit'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        {{ Session::get('edit') }}
    </div>
@endif

@if (Session::has('errors'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        Fallo en los datos enviados, por favor abre nuevamente el modal y corrige los errores que hayas realizado
    </div>
@endif

<div class="row mb-3 mx-auto" style="display: flex; align-items: center; justify-content: space-between !important">
    <div class="col">
        <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 2%; color: #51A2A7">
            Productos dentro del Sistema
        </label>
    </div>
    <div class="col" style="display: flex;justify-content: flex-end !important">

        <button class="btn btn-round" style="background-color: #51A2A7" data-toggle="modal" data-target="#storeModal">
            <i class="material-icons">add</i>AGREGAR PRODUCTO
        </button>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table id="example" class="table table-striped " cellspacing="0" width="100%">
                <thead style="background-color: #FFFFFF; color: #51A2A7; font-weight: bold">
                    <tr>
                        <th class="font-weight-bold">Código</th>
                        <th class="font-weight-bold">Imagen</th>
                        <th class="font-weight-bold">Nombre</th>
                        <th class="font-weight-bold">Descripción</th>
                        <th class="font-weight-bold">Número de Entrenamientos</th>
                        <th class="font-weight-bold">Fecha de Creación</th>
                        <th class="font-weight-bold">Acciones</th>
                    </tr>
                </thead>
                <tbody class="table-bordered" style="background-color: #fff; color: #000"> 
                    @foreach ($productos as $producto)
                        <tr class="text-center" id="row{{ $producto->id_producto }}">
                            <td>{{ $producto->id_producto }}</td>
                            <td class="mx-auto text-center" > 
                                <img style="width:100px;max-height:100px;" id="image{{ $producto->id_producto }}"
                                    src="{{ route('producto.icon', ['filename' => basename($producto->imagen)]) }}" >
                            </td>
                            <td id="nom{{ $producto->id_producto }}">{{ $producto->nombre }}</td>
                            <td id="desc{{ $producto->id_producto }}">{{ $producto->descripcion }}</td>
                            <td>
                                {{ $producto->capacitaciones->count() }} 
                                <i class="fas fa-clipboard" style="color: #51A2A7"></i>
                            </td>
                            <td>{{ date_format(date_create($producto->created_at), "F j, Y") }}</td>
                            <td class="text-center">
                                <a id="{{ $producto->id_producto }}" class="editRow" data-toggle="modal" data-target="#editModal" style="color: #51A2A7; cursor: pointer">
                                    <i class="far fa-edit"></i>
                                </a>
                                <a id="{{ $producto->id_producto }}" style="color: #51A2A7;  cursor: pointer" class="deleteRow">
                                    <i class="far fa-trash-alt"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- MODAL DE AGREGAR PRODUCTO -->
<div class="modal fade" id="storeModal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                    Añadir Producto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="{{ route('producto.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="col">
                        <label for="">Nombre del Producto:</label>
                        <input name="nombre" id="" type="text" placeholder="Nombre del Producto..." value="{{ old('nombre') }}"
                            class="form-control @error('nombre') is-invalid @enderror">
                        @error('nombre')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row my-3">
                    <div class="col">
                        <label for="">Descripción del Producto:</label>
                        <textarea name="descripcion" id="" class="form-control @error('descripcion') is-invalid @enderror">{{ old('descripcion') }}</textarea>
                        @error('descripcion')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row my-3">
                    <div class="col">
                        <label for="">Imagen del Producto:</label>
                        <div class="custom-file mb-3">
                            <input type="file" class="custom-file-input @error('imagen') is-invalid @enderror" 
                            id="customImage" name="imagen" accept=".jpg,.jpeg,.png" value="">
                            <label class="custom-file-label" for="customImage">Escoger una Imagen</label>
                            @error('imagen')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="d-none" id="eraseImage">
                            <label style="cursor: pointer;">
                                Quitar Imagen 
                                <i class="fas fa-click" style="font-size: 20px; color: #F28D8D"></i>
                            </label>
                        </div>
                    </div>
                    <div class="col">
                        <img class="img w-100" id="previewImage"
                        src="{{ route('producto.icon', ['filename' => basename(null)]) }}">
                    </div>
                </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;">
                CREAR PRODUCTO
            </button>
        </div>
            </form>
        </div>
    </div>
</div>
<!-- FIN MODAL -->

<!-- MODAL DE EDITAR PRODUCTO -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                    Edición de Producto - <span id="editCOD"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('producto.update') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row">
                        <div class="col">
                            <label for="">Nombre del Producto:</label>
                            <input name="nombreUD" id="productoNom" type="text" placeholder="Nombre del Producto..." 
                                value="{{ old('nombreUD') }}" class="form-control @error('nombreUD') is-invalid @enderror">
                            @error('nombreUD')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col">
                            <label for="">Descripción del Producto:</label>
                            <textarea name="descripcionUD" id="productoDesc" class="form-control @error('descripcionUD') is-invalid @enderror"></textarea>
                            @error('descripcionUD')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col-12">
                            <label for="">Imagen del Producto (si no la vas a modificar no cambies nada):</label>
                        </div>
                        <div class="col">
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input @error('imagenUD') is-invalid @enderror" 
                                id="customUpdateImage" name="imagenUD" accept=".jpg,.jpeg,.png" value="">
                                <label class="custom-file-label" for="customImage">Escoger una Imagen</label>
                                @error('imagenUD')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col">
                            <img class="img w-100" id="previewUpdateImage"
                            src="{{ route('producto.icon', ['filename' => basename(null)]) }}">
                        </div>
                    </div>
                    <input type="hidden" name="id_producto" id="editID" value="">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;">
                    EDITAR PRODUCTO
                </button>
            </div>
                </form>
        </div>
    </div>
</div>
<!-- FIN MODAL DE EDITAR PRODUCTO -->

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
<input type="hidden" id="urlDelete" value="{{ route('producto.destroy') }}">

<style>
    .file-input>[type='file'] {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
        z-index: 10;
        cursor: pointer;
    }

    .file-input>.button {
        display: inline-block;
        cursor: pointer;
        background: #eee;
        padding: 4px 12px;
        border-radius: 2px;
        margin-right: 8px;
    }

    .file-input:hover>.button {
        background: dodgerblue;
        color: white;
    }

    .file-input>.label {
        color: #333;
        white-space: nowrap;
        opacity: .3;
    }

    .file-input.-chosen>.label {
        opacity: 1;
    }

</style>

<script>
    // Also see: https://www.quirksmode.org/dom/inputfile.html

    var inputs = document.querySelectorAll('.file-input')

    for (var i = 0, len = inputs.length; i < len; i++) {
        customInput(inputs[i])
    }

    function customInput(el) {
        const fileInput = el.querySelector('[type="file"]')
        const label = el.querySelector('[data-js-label]')

        fileInput.onchange =
            fileInput.onmouseout = function() {
                if (!fileInput.value) return

                var value = fileInput.value.replace(/^.*[\\\/]/, '')
                el.className += ' -chosen'
                label.innerText = value
            }
    }

</script>

@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }

        //ABRIR MODAL DE EDITAR Y SETTEAR VALORES
        $('.editRow').click(function(){
            var id = $(this).attr('id');
            
            //Recogo los Valores de la Tabla
            var nombre = $('#nom'+id).text().trim();
            var descripcion = $('#desc'+id).text().trim();
            var src = $('#image'+id).attr("src");

            //Setteo los valores en el modal
            $('#editID').val(id);
            $('#productoNom').val(nombre);
            $('#productoDesc').val(descripcion);
            $('#previewUpdateImage').attr("src", src);
            $('#editCOD').text(id);
        });

        //ELIMINAR EL DATO POST SWAL ALERT
        $('.deleteRow').click(function(){
            var id = $(this).attr('id');
            var url = $('#urlDelete').val();

            Swal.fire({
                icon: 'warning',
                title: '¿Esta seguro de borrar este producto?',
                text: 'Si algun entrenamiento esta bajo este producto las mismas seran eliminadas',
                type: 'warning',
                confirmButtonText: 'Si, Eliminar!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: url+'/'+id,
                        data: {
                            "_token": $("meta[name='csrf-token']").attr("content")
                        },
                        type: 'POST',
                        success: function(response){
                            if(response.delete){
                                $("#row"+id).remove();
                                Swal.fire(
                                    response.message,
                                    'Presiona el boton para cerrar el modal',
                                    'success'
                                )
                            }
                            else{
                                Swal.fire(
                                    response.message,
                                    'Presiona el boton para cerrar el modal',
                                    'warning'
                                )
                            }
                        }
                    });
                }
            });
        });

        //CAMBIO O AÑADIDO DEL ICONO DE LA CATEGORIA EN STORE
            function readImage(input) {
                var url = input.value;
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

                if (input.files && input.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#previewImage').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);

                    $('#eraseImage').removeClass('d-none');
                } else {
                    $('#previewImage').attr('src', null);
                    input.value = null;
                    $('#eraseImage').addClass('d-none');
                }
            }

            $("#customImage").change(function() {
                readImage(this);
            });

            $('#eraseImage').click(function (){
                $('#previewImage').attr('src', null);
                $("#customImage").val(null);
                $(this).addClass('d-none');
            });

        //CAMBIO O AÑADIDO DEL ICONO DE LA CATEGORIA EN UPDATE
            function readUpdateImage(input) {
                var url = input.value;
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

                if (input.files && input.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#previewUpdateImage').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);

                    $('#eraseUpdateImage').removeClass('d-none');
                } else {
                    $('#previewUpdateImage').attr('src', null);
                    input.value = null;
                    $('#eraseUpdateImage').addClass('d-none');
                }
            }

            $("#customUpdateImage").change(function() {
                readUpdateImage(this);
            });

            $('#eraseUpdateImage').click(function (){
                $('#previewUpdateImage').attr('src', null);
                $("#customUpdateImage").val(null);
                $(this).addClass('d-none');
            });
    });
</script>