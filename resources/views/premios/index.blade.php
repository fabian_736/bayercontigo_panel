@extends('layouts.admin.app')

@section('content')


@if (Session::has('create'))
<div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('create') }}
</div>
@endif

@if (Session::has('delete'))
<div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('delete') }}
</div>
@endif

@if (Session::has('edit'))
<div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
    {{ Session::get('edit') }}
</div>
@endif


<div class="row" style="margin-top: 1%">
    <div class="col">
        <a href="{{route('premio.categoria.list')}}" class="btn btn-round font-weight-bold" style="background-color: #fff; color: #51A2A7; border-width: 1px;
                    border-style: solid; border-color: #51A2A7;">
            <div class="row">
                <div class="col">
                    CONTROL CATEGORIAS DE PREMIOS
                </div>
                <div class="col" style="left: 20%">
                    <i class="material-icons my-auto">arrow_forward_ios</i>
                </div>
            </div>
        </a>
    </div>
</div>

<div class="row" style="margin-top: 3%">
    <!-------------------------------- CARDs PREMIOS / NIVELES --------------------------------->
    <div class="col-lg-6 col-md-6">
        <div class="row mx-auto">
            <label class="lead" for="" style="color: #51A2A7; font-weight: bold">Control de premios</label>
            <a href="{{route('premio.listado')}}" class="ml-auto text-secondary">Ver todos</a>
        </div>
        @forelse ($premios as $premio)
        <div class="card my-3">
            <div class="row">
                <div class="col-md-4 p-3 m-auto text-center">
                    <div style="width: 100%;">
                        @if( $premio->distribuidor == "N/A" )
                            <img src="{{ route('premio.icon', ['filename' => basename($premio->imagen)]) }}" width="100%" height="100%" alt="">
                        @else
                            <img src="{{ $premio->imagen }}" width="100%" height="100%" alt="">
                        @endif
                    </div>
                </div>
                <div class="col-md-8 p-3">
                    <p style="color: #51A2A7; font-weight: bold" class="mb-0">{{ $premio->nombre }}</p>
                    <p>
                        Categoria: 
                        <span style="color: #51A2A7; font-weight: bold">
                            {{ $premio->categoria ? $premio->categoria->categoria : "No posee" }}
                        </span>
                    </p>
                    
                    <p class="mb-0">Marca - <span class="font-weight-bold">{{ $premio->marca }}</span></p>
                    <div class="row">
                        <div class="col-12">
                            <p class="mb-0">
                                Inventario Disponible: <span class="font-weight-bold">{{ $premio->stock }}</span>
                            </p>
                        </div>
                        <div class="col-12">
                            <p>
                                Tipo de Entrega: <span class="font-weight-bold">{{ $premio->entrega }}</span>
                            </p>
                        </div>
                        <div class="col-12">
                            <p class="mb-0">
                                <span class="font-weight-bold">Carácteristicas:</span><br>
                                <ul>
                                    @forelse($premio->caracteristicas as $caracteristica)
                                        <li>{{ $caracteristica->caracteristica }}</li>
                                    @empty
                                        <li>No tiene carácteristicas adjuntadas</li>
                                    @endforelse
                                </ul>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <a href="{{route('premio.edit',$premio->id_premio)}}" class="float-right mr-3" style="color: #51A2A7">
                                Ver detalle
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @empty
        <div class="card">
            <div class="card-header">
                <div class="row mx-auto">
                    <h4 class="card-title" style="color: #51A2A7; font-weight: bold">
                        No hay Premios Registrados
                    </h4>
                </div>
            </div>
        </div>
        @endforelse
        <button class="btn btn-round" style="background-color: #51A2A7" data-toggle="modal" data-target=".bd-example-modal-lg2">
            <i class="material-icons">add</i>CREAR PREMIOS
        </button>
    </div>

    <!-- MODAL DE CREAR PREMIO -->
        <div class="modal fade bd-example-modal-lg2" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                            Creación de Premio</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('premio.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="col">
                                    <input name="nombre" id="nombrePremio" type="text" placeholder="Nombre" value="{{ old('nombre') }}" class="form-control @error('nombre') is-invalid @enderror">
                                    @error('nombre')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col">
                                    <input name="marca" id="marcaPremio" type="text" placeholder="Marca" value="{{ old('marca') }}" class="form-control @error('marca') is-invalid @enderror">
                                    @error('marca')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row my-3">
                                <div class="col">
                                    <label for="categoria">Categoría del Premio:</label>
                                    <select name="FK_categoria" id="" class="form-control @error('FK_categoria') is-invalid @enderror">
                                        <option value="">Selecciona una Categoría...</option>
                                        @foreach($categorias as $categoria)
                                            <option value="{{ $categoria->id_premio_cat }}">{{ $categoria->categoria }}</option>
                                        @endforeach
                                    </select>
                                    @error('FK_categoria')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col">
                                    <label for="categoria">Tipo de Entrega:</label>
                                    <select name="entrega" id="" class="form-control @error('entrega') is-invalid @enderror">
                                        <option value="">Selecciona una opción...</option>
                                        <option value="Domicilio">Domicilio</option>
                                        <option value="Online">Online</option>
                                    </select>
                                    @error('entrega')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row my-3">
                                <div class="col">
                                    <textarea name="descripcion" id="descripcionPremio" type="text" placeholder="Descripción del Premio..." class="form-control @error('descripcion') is-invalid @enderror">{{ old('descripcion') }}</textarea>
                                    @error('descripcion')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row ">
                                <div class="col">
                                    <input name="puntos" id="" type="number" min="0" placeholder="Puntos para redimir premio" class="form-control @error('puntos') is-invalid @enderror" value="{{ old('puntos') }}">
                                    @error('puntos')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col">
                                    <input name="stock" id="" type="number" min="0" placeholder="Cantidad en Stock" value="{{ old('stock') }}" class="form-control @error('stock') is-invalid @enderror">
                                    @error('stock')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row my-3">
                                <div class="col-12">
                                    <label for="">Carácteristicas del Premio (Opcional):</label>
                                </div>
                                <div class="col">
                                    <input id="caractName" type="text" placeholder="Añade la carácteristica" class="form-control flo">
                                </div>
                                <div class="col">
                                    <input style="background-color: #51A2A7" class="btn btn-sm" type="button" title="Agregar Producto" Value="+" id="agregar">
                                    <input style="background-color: #51A2A7" class="btn btn-sm" type="button" title="Borrar Producto" Value="-" id="borrar">
                                </div>
                            </div> 

                            <div class="form-row">
                                <div class="col" id="caracteristicas"></div>
                            </div>

                            <div class="form-row my-3">
                                <div class="col">
                                    <label for="">Premio es de Distribuidor Quantum?:</label><br>
                                    <div class="form-check form-check-radio form-check-inline ml-3 mr-3">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="tipo"
                                                id="inlineRadio1" value="no" required> No, es propio del sistema.
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-radio form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="tipo"
                                                id="inlineRadio1" value="si"> Si, es de dicho distribuidor.
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <input type="hidden" name="distribuidor" id="nameDistribuidor" value="N/A">
                            </div>

                            <!-- ESPACIO DE SI TIENE DISTRIBUIDOR DIFERENTE -->
                            <div id="formDist" class="d-none">
                                <div class="form-row my-3">
                                    <div id="marcasAPI" class="col d-none">
                                        <label for="">Seleccionar Marca</label>
                                        <select name="id_brand_dist" id="marcaselect" class="form-control @error('id_brand_dist') is-invalid @enderror">
                                            <option value="" selected disabled>Seleccionar marca...</option>
                                        </select>
                                        @error('id_brand_dist')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <!-- LISTA DE PREMIOS DE LA MARCA -->
                                <div class="form-row my-3" style="display: none;" id="itemlist">
                                    <div class="col-10 mx-auto">
                                        <a class="btn form-control" style="background-color: #51A2A7;" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="true" aria-controls="collapseExample">
                                            Lista de premios
                                        </a>

                                        <div class="collapse show" id="collapseExample">
                                            <div id="list" class="card p-3" style="background: #F5F5F5;">
                                                <div class="row d-flex justify-content-center mb-3" id="itemrow">
                                                    <!-- ESPACIO DONDE SALEN LOS PREMIOS CONSULTADOS -->

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row my-3" id="premioImage">
                                <div class="col">
                                    <label for="">Añadir Imagen del Premio:</label>
                                    <div class="custom-file mb-3">
                                        <input type="file" class="custom-file-input @error('imagen') is-invalid @enderror" 
                                        id="customImage" name="imagen" accept=".jpg,.jpeg,.png" value="">
                                        <label class="custom-file-label" for="customImage">Escoger una Imagen</label>
                                        @error('imagen')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="d-none" id="eraseImage">
                                        <label style="cursor: pointer;">
                                            Quitar Imagen 
                                            <i class="fas fa-click" style="font-size: 20px; color: #F28D8D"></i>
                                        </label>
                                    </div>
                                </div>
                                <div class="col">
                                    <img class="img w-100" id="previewImage"
                                    src="{{ route('premio.icon', ['filename' => basename(null)]) }}">
                                </div>
                            </div>

                            <div class="form-row my-3">
                                <div class="col">
                                    @error('imagen_quantum')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>Debes seleccionar un producto para almacenar la imagen</strong>
                                        </span>
                                    @enderror

                                    @error('id_producto_dist')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>Debes seleccionar un producto para almacenar su referencia</strong>
                                        </span>
                                    @enderror
                                </div>

                                <!-- INPUT HIDDEN TIPO DE IMAGEN | MARCA | DISTRIBUIDOR-->
                                <input type="hidden" id="imageQuantum" name="imagen_quantum" value="">
                                <input type="hidden" id="id_producto_dist" name="id_producto_dist" value="">
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;">CREAR PREMIO</button>
                    </div>
                        </form>
                </div>
            </div>
        </div>
    <!-- FIN MODAL DE CREAR PREMIO -->

    <!-------------------------------- CARDs CANJEADOS --------------------------------->
    <div class="col-lg-6 col-md-6">
        <div class="row mx-auto">
            <label for="" class="lead" style="color: #F28D8D; font-weight: bold">Premios canjeados</label>
            <a href="{{route('canjeado.index')}}" class="ml-auto text-secondary">Ver todos</a>
        </div>
        @forelse ($canjeados as $canjeado)
        <div class="card">
            <div class="card-header">
                <div class="row mx-auto">
                    <h4 class="card-title" style="color: #F28D8D; font-weight: bold">{{ $canjeado->nombre }}</h4>
                    <i class="material-icons ml-auto">star</i>{{ $canjeado->total_puntos }}
                </div>
                <p class="mb-0">
                    Categoria: <span style="color: #F28D8D; font-weight: bold">{{ $canjeado->categoria }}</span>
                </p>
            </div>
            <div class="card-body">
                <p class="mb-0">Cantidad: <span class="font-weight-bold">{{ $canjeado->cantidad }}</span></p>
                <p class="mb-0">Usuario: <span class="font-weight-bold">{{ $canjeado->usuario->email }}</span></p>
                <p class="mb-0">Fecha de rendicion: 
                    <span class="font-weight-bold">
                        {{ date_format(date_create($canjeado->created_at), "F j, Y") }}
                    </span>
                </p>
            </div>
        </div>
        @empty
        <div class="card">
            <div class="card-header">
                <div class="row mx-auto">
                    <h4 class="card-title" style="color: #F28D8D; font-weight: bold">
                        No hay Premios Canjeados
                    </h4>
                </div>
            </div>
        </div>
        @endforelse
    </div>
</div>
<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">

<!-- RUTAS PARA COGER LAS MARCAS Y PRODUCTOS -->
<input type="hidden" id="getMarcas" value="{{ route('quantum.marcas') }}">
<input type="hidden" id="getProductos" value="{{ route('quantum.productos') }}">

<style>
    .file-input>[type='file'] {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
        z-index: 10;
        cursor: pointer;
    }

    .file-input>.button {
        display: inline-block;
        cursor: pointer;
        background: #eee;
        padding: 4px 12px;
        border-radius: 2px;
        margin-right: 8px;
    }

    .file-input:hover>.button {
        background: dodgerblue;
        color: white;
    }

    .file-input>.label {
        color: #333;
        white-space: nowrap;
        opacity: .3;
    }

    .file-input.-chosen>.label {
        opacity: 1;
    }
</style>

<script>
    // Also see: https://www.quirksmode.org/dom/inputfile.html

    var inputs = document.querySelectorAll('.file-input')

    for (var i = 0, len = inputs.length; i < len; i++) {
        customInput(inputs[i])
    }

    function customInput(el) {
        const fileInput = el.querySelector('[type="file"]')
        const label = el.querySelector('[data-js-label]')

        fileInput.onchange =
            fileInput.onmouseout = function() {
                if (!fileInput.value) return

                var value = fileInput.value.replace(/^.*[\\\/]/, '')
                el.className += ' -chosen'
                label.innerText = value
            }
    }
</script>

@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
            $('#exampleModal').modal('show');
        }

        //SECCIÓN DE CARACTERISTICAS INSERTADAS PARA EL STORE DINAMICAMENTE
            var inc = 0;
            $('#agregar').click(function(){
                var value = $('#caractName').val();
                if(value){
                    inc++;
                    $('#caracteristicas').append("<input id='caract"+inc+"' type='text' name='caracteristicas[]' value='"+value+"' readonly class='form-control mt-1 col-md-6'>");
                }
            });

            $('#borrar').click(function(){
                $('#caract'+inc).remove();
                inc--;
            });

        //SECCIÓN DE TENER O NO DISTRIBUIDOR
            $("input[name='tipo']").change(function(){
                var radioValue = $("input[name='tipo']:checked").val();
                if(radioValue == "si"){
                    $('#nameDistribuidor').val("Quantum");

                    $("#formDist").removeClass("d-none");
                    $('#marcasAPI').removeClass('d-none');
                    $('#premioImage').addClass('d-none');
                    var length = $('#marcaselect').children('option').length;
                    if(length < 2)
                        marcas();
                }else{
                    $('#nameDistribuidor').val("N/A");

                    $("#formDist").addClass("d-none");
                    $('#marcasAPI').addClass('d-none');
                    $('#premioImage').removeClass('d-none');
                    $('#itemlist').hide();
                }
            });

        //Selección de marca y display de los premios
            $("#marcaselect").change(function() {
                var  id = $('#marcaselect').val();
                $('#itemrow').empty();
                
                //Borramos el id del product
                $('#id_producto_dist').val(null);

                //Arreglamos el input de marca
                var texto = $( "#marcaselect option:selected" ).text();
                $('#marcaPremio').val(texto);

                var urlProductos = $('#getProductos').val();
                $.ajax({
                    url: urlProductos+"/"+id,
                    type: 'GET',
                    success: function(response){
                        if(response.premios){
                            response.premios.forEach(function(element){
                                console.log(element);
                                
                                html = '<div class="col-3 card bg-secondary p-0 mr-3 mb-3" id="' + element.product_id + '" onclick="productTaken(this.id)">' +
                                            '<input class="product_cat" type="hidden" value="'+ element.catl1 +'">'+
                                            '<img class="product_image" src="' + element.image + '"style="width: 100%; height :100%"></img>' +
                                            '<div class="row-reverse w-100 mx-auto px-0 py-2" style="height: 40%; position: absolute; margin-top: 85%;background: rgb(0,150,146); background: linear-gradient(90deg, rgba(0,150,146,0.4990371148459384) 0%, rgba(9,94,121,0.49343487394957986) 35%);">' +
                                                '<div class="col">' +
                                                    '<label for="" class="text-white font-weight-bold product_name">' + element.name + '</label>' +
                                                '</div>' +
                                                '<div class="col">' +
                                                    '<label for="" class="text-white font-weight-bold product_desc">' + element.description + '</label>' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>';
                                $('#itemrow').append(html);
                            });
                            
                            $('#itemlist').show();
                        }
                        else{
                            Swal.fire({
                                title: response.message,
                                text: "Problemas de Conexión con Quantum, por favor intentalo más tarde",
                                icon: 'warning',
                                confirmButtonText: 'Aceptar'
                            })

                            $('#itemlist').hide();
                        }
                    }
                });
            });

        //CAMBIO O AÑADIDO DE IMAGEN DEL PREMIO EN STORE
            function readImage(input) {
                var url = input.value;
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

                if (input.files && input.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#previewImage').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);

                    $('#eraseImage').removeClass('d-none');
                } else {
                    $('#previewImage').attr('src', null);
                    input.value = null;
                    $('#eraseImage').addClass('d-none');
                }
            }

            $("#customImage").change(function() {
                readImage(this);
            });

            $('#eraseImage').click(function (){
                $('#previewImage').attr('src', null);
                $("#customImage").val(null);
                $(this).addClass('d-none');
            });
    });

    //Selección de producto y arreglo en el form
    function productTaken(id){
        var nombre = $("#"+id+" .product_name").text();
        var categoria = $("#"+id+" .product_cat").val();
        var imagen = $("#"+id+" .product_image").attr("src");
        var descripcion = $("#"+id+" .product_desc").text();

        $('#id_producto_dist').val(id);
        $('#nombrePremio').val(nombre);
        //$('#pre_cat').val(categoria);
        $('#imageQuantum').val(imagen);
        $('#descripcionPremio').text(descripcion);
    }

    function marcas(){
        
        var urlMarcas = $('#getMarcas').val();
        
        $.ajax({
            url: urlMarcas,
            type: 'GET',
            success: function(response){
                if(response.marcas){
                    response.marcas.forEach(function(element){
                        console.log(element);
                        var o = new Option(element.nombre, element.brand_id);
                        $(o).html(element.nombre);
                        $("#marcaselect").append(o);
                    });
                }
                else{
                    Swal.fire({
                        title: response.message,
                        text: "Problemas de Conexión con Quantum, por favor intentalo más tarde",
                        icon: 'warning',
                        allowOutsideClick: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Aceptar'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                /* var redirect = $('#redirectIndex').val();
                                location.href = redirect; */
                                $('#btnSubmit').attr('disabled', true);
                                $('#btnSubmit').text('No esta disponible');
                            }
                        }
                    )
                }
            }
        });
    }
</script>