@extends('layouts.admin.app')

@section('content')
<div class="container mt-5">
    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    <div class="row mb-5 mx-auto">

        <div>
            <a href="{{ route('premio.listado') }}">
                <button class="btn" style="background-color: #e7344c;">
                    Volver a Premios
                </button>
            </a>
        </div>

        <!-- Editar premios -->
        <div class="col-md-12 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">


            <div class="card-header mt-3" style="background-color: #e7344c;">
                <h5 class="text-white my-auto">Editar premio</h5>
            </div>

            <form action="{{ route('premio.update',$premio->id_premio) }}" method="post" enctype="multipart/form-data">

                @csrf
                <div class="row col-md-12 my-4">
                    <div class="col-md-6">
                        <label for="name_user">Nombre:</label>
                        <input name="nombre" id="nombrePremio" type="text" value="{{ old('nombre', $premio->nombre) }}"
                            class="form-control @error('nombre') is-invalid @enderror">
                        @error('nombre')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label for="name_user">Marca:</label>
                        <input name="marca" id="product_marca" type="text" value="{{ old('marca', $premio->marca) }}" 
                            class="form-control @error('marca') is-invalid @enderror">
                        @error('marca')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row col-md-12 my-4">
                    <div class="col-md-12">
                        <label for="name_user">Descripcion:</label>
                        <textarea name="descripcion" id="descripcionPremio" type="text" 
                            class="form-control @error('descripcion') is-invalid @enderror">{{ old('descripcion', $premio->descripcion) }}</textarea>
                        @error('descripcion')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row col-md-12 my-3">
                    <div class="col">
                        <label for="categoria">Categoría del Premio:</label>
                        <select name="FK_categoria" id="" class="form-control @error('FK_categoria') is-invalid @enderror">
                            <option value="">Selecciona una Categoría...</option>
                            @foreach($categorias as $categoria)
                                <option value="{{ $categoria->id_premio_cat }}" {{ old('FK_categoria', $premio->FK_categoria) == $categoria->id_premio_cat ? "selected" : "" }}>
                                    {{ $categoria->categoria }}
                                </option>
                            @endforeach
                        </select>
                        @error('FK_categoria')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col">
                        <label for="categoria">Tipo de Entrega:</label>
                        <select name="entrega" id="" class="form-control @error('entrega') is-invalid @enderror">
                            <option value="">Selecciona una opción...</option>
                            <option value="Domicilio" {{ old('entrega', $premio->entrega) == "Domicilio" ? "selected" : "" }}>Domicilio</option>
                            <option value="Online" {{ old('entrega', $premio->entrega) == "Online" ? "selected" : "" }}>Online</option>
                        </select>
                        @error('entrega')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row col-md-12 my-3">
                    <div class="col">
                        <label for="">Puntos para redención:</label>
                        <input name="puntos" id="" type="number" min="0" placeholder="Puntos para redimir premio" class="form-control @error('puntos') is-invalid @enderror" value="{{ old('puntos', $premio->puntos) }}">
                        @error('puntos')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col">
                        <label for="">Cantidad Disponible en Stock:</label>
                        <input name="stock" id="" type="number" min="0" placeholder="Cantidad en Stock" value="{{ old('stock', $premio->stock) }}" class="form-control @error('stock') is-invalid @enderror">
                        @error('stock')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row col-md-12 my-3">
                    <div class="col-12">
                        <label for="">Carácteristicas del Premio (Opcional*):</label>
                    </div>
                    <div class="col">
                        <input id="caractName" type="text" placeholder="Añade la carácteristica" class="form-control flo">
                    </div>
                    <div class="col">
                        <input style="background-color: #51A2A7" class="btn btn-sm" type="button" title="Agregar Producto" Value="+" id="agregar">
                        <input style="background-color: #51A2A7" class="btn btn-sm" type="button" title="Borrar Producto" Value="-" id="borrar">
                    </div>
                </div> 

                <?php $cantidad = 0; ?>
                <div class="row col-md-12 my-3">
                    <div class="col" id="caracteristicas">
                        @foreach($premio->caracteristicas as $caracteristica)
                            <?php $cantidad++; ?>
                            <input id='caract{{ $cantidad }}' type='text' name='caracteristicas[]' 
                                    value='{{ $caracteristica->caracteristica }}' readonly class='form-control mt-1 col-md-6'>
                        @endforeach
                    </div>
                </div>

                <div class="row col-md-12 my-3">
                    <div class="col">
                        <label for="">Premio es de Distribuidor Quantum:</label><br>
                        <div class="form-check form-check-radio form-check-inline ml-3 mr-3">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="tipo"
                                    id="inlineRadio1" value="no" required {{ old('tipo', $premio->distribuidor) == "N/A" || old('tipo') == "no" ? "checked" : "" }}> No, es propio del sistema.
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        <div class="form-check form-check-radio form-check-inline">
                            <label class="form-check-label">
                                <input class="form-check-input" type="radio" name="tipo"
                                    id="inlineRadio1" value="si" {{ old('tipo', $premio->distribuidor) != "N/A" || old('tipo') == "si" ? "checked" : "" }}> Si, es de dicho distribuidor.
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>
                    <input type="hidden" name="distribuidor" id="nameDistribuidor" value="{{ old('distribuidor', $premio->distribuidor) }}">
                </div>

                <!-- ESPACIO DE SI TIENE DISTRIBUIDOR DIFERENTE -->
                <div id="formDist" class="{{ $premio->distribuidor == 'N/A' ? 'd-none' : ''}}">
                    <div class="row col-md-12 my-3">
                        <div id="marcasAPI" class="col  {{ $premio->distribuidor == 'N/A' ? 'd-none' : '' }}">
                            <label for="">Seleccionar Marca</label>
                            <select name="id_brand_dist" id="marcaselect" class="form-control @error('id_brand_dist') is-invalid @enderror">
                                <option value="" selected disabled>Seleccionar marca...</option>
                            </select>
                            @error('id_brand_dist')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <!-- LISTA DE PREMIOS DE LA MARCA -->
                    <div class="row col-md-12 my-3" style="display: none;" id="itemlist">
                        <div class="col-10 mx-auto">
                            <a class="btn form-control" style="background-color: #51A2A7;" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="true" aria-controls="collapseExample">
                                Lista de premios
                            </a>

                            <div class="collapse show" id="collapseExample">
                                <div id="list" class="card p-3" style="background: #F5F5F5;">
                                    <div class="row d-flex justify-content-center mb-3" id="itemrow">
                                        <!-- ESPACIO DONDE SALEN LOS PREMIOS CONSULTADOS -->

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row col-md-12 my-3 {{ $premio->distribuidor != 'N/A' ? 'd-none' : '' }}" id="premioImage">
                    <div class="col">
                        <label for="">Editar Imagen del Premio (Opcional*):</label>
                        <div class="custom-file mb-3">
                            <input type="hidden" id="iBefore" name="iBefore" value="{{ $premio->imagen }}">
                            <input type="file" class="custom-file-input @error('imagen') is-invalid @enderror" 
                            id="customImage" name="imagen" accept=".jpg,.jpeg,.png" value="">
                            <label class="custom-file-label" for="customImage">Escoger una Imagen</label>
                            @error('imagen')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="{{ $premio->imagen ? '' : 'd-none' }}" id="eraseImage">
                            <label style="cursor: pointer;">
                                Quitar Imagen 
                                <i class="fas fa-click" style="font-size: 20px; color: #F28D8D"></i>
                            </label>
                        </div>
                    </div>
                    <div class="col">
                        @if($premio->distribuidor == "N/A")
                            <img class="img w-100" id="previewImage"
                            src="{{ route('premio.icon', ['filename' => basename($premio->imagen)]) }}">
                        @else
                            <img class="img w-100" id="previewImage" src="{{ $premio->imagen }}">
                        @endif
                    </div>
                </div>

                <div class="row col-md-12 my-3">
                    <div class="col">
                        @error('imagen_quantum')
                            <span class="invalid-feedback" role="alert">
                                <strong>Debes seleccionar un producto para almacenar la imagen</strong>
                            </span>
                        @enderror

                        @error('id_producto_dist')
                            <span class="invalid-feedback" role="alert">
                                <strong>Debes seleccionar un producto para almacenar su referencia</strong>
                            </span>
                        @enderror
                    </div>

                    <!-- INPUT HIDDEN TIPO DE IMAGEN | MARCA | DISTRIBUIDOR-->
                    <input type="hidden" id="imageQuantum" name="imagen_quantum" 
                        value="{{ $premio->distribuidor != 'N/A' ? $premio->imagen : '' }}">
                    <input type="hidden" id="id_producto_dist" name="id_producto_dist" 
                        value="{{ $premio->distribuidor != 'N/A' ? $premio->id_producto_dist : '' }}">
                </div>

                <hr>
                <div class="row col-md-12 my-3">
                    <div class="col-md-6 mx-auto">
                        <button type="submit" class="form-control text-white" style="background-color: #64C2C8">Editar Premio</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<input type="hidden" id="showCantidad" value="{{ $cantidad }}">
<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">

<!-- RUTAS PARA COGER LAS MARCAS Y PRODUCTOS -->
<input type="hidden" id="getBrand" value="{{ $premio->id_brand_dist }}">
<input type="hidden" id="getBrandName" value="{{ $premio->marca }}">
<input type="hidden" id="getMarcas" value="{{ route('quantum.marcas') }}">
<input type="hidden" id="getProductos" value="{{ route('quantum.productos') }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }

        //SECCIÓN DE CARACTERISTICAS INSERTADAS PARA EL STORE DINAMICAMENTE
            var inc = $('#showCantidad').val();
            $('#agregar').click(function(){
                var value = $('#caractName').val();
                if(value){
                    inc++;
                    $('#caracteristicas').append("<input id='caract"+inc+"' type='text' name='caracteristicas[]' value='"+value+"' readonly class='form-control mt-1 col-md-6'>");
                }
            });

            $('#borrar').click(function(){
                $('#caract'+inc).remove();
                inc--;
            });

        //SECCIÓN DE TENER O NO DISTRIBUIDOR
            var distribuidor = $('#distribuidor').val();
            if(distribuidor != 'N/A'){
                marcas();
                var brand = $('#getBrand').val();
                $("#marcaselect").val(brand);

                //Arreglamos el input de marca
                var texto = $( "#getBrandName" ).val();
                $('#product_marca').val(texto);
            }

            $("input[name='tipo']").change(function(){
                var radioValue = $("input[name='tipo']:checked").val();
                if(radioValue == "si"){
                    $('#nameDistribuidor').val("Quantum");

                    $("#formDist").removeClass("d-none");
                    $('#marcasAPI').removeClass('d-none');
                    $('#premioImage').addClass('d-none');
                    var length = $('#marcaselect').children('option').length;
                    if(length < 2)
                        marcas();
                }else{
                    $('#nameDistribuidor').val("N/A");

                    $("#formDist").addClass("d-none");
                    $('#marcasAPI').addClass('d-none');
                    $('#premioImage').removeClass('d-none');
                    $('#itemlist').hide();
                }
            });
        
        //SELECCIÓN DE MARCA Y DISPLAY DE PREMIOS
            $("#marcaselect").change(function() {
                var  id = $('#marcaselect').val();
                $('#itemrow').empty();
                
                //Borramos el id del product
                $('#id_producto_dist').val(null);

                //Arreglamos el input de marca
                var texto = $( "#marcaselect option:selected" ).text();
                $('#product_marca').val(texto);

                var urlProductos = $('#getProductos').val();
                $.ajax({
                    url: urlProductos+"/"+id,
                    type: 'GET',
                    success: function(response){
                        if(response.premios){
                            response.premios.forEach(function(element){
                                console.log(element);
                                
                                html = '<div class="col-3 card bg-secondary p-0 mr-3 mb-3" id="' + element.product_id + '" onclick="productTaken(this.id)">' +
                                            '<input class="product_cat" type="hidden" value="'+ element.catl1 +'">'+
                                            '<img class="product_image" src="' + element.image + '"style="width: 100%; height :100%"></img>' +
                                            '<div class="row-reverse w-100 mx-auto px-0 py-2" style="height: 40%; position: absolute; margin-top: 85%;background: rgb(0,150,146); background: linear-gradient(90deg, rgba(0,150,146,0.4990371148459384) 0%, rgba(9,94,121,0.49343487394957986) 35%);">' +
                                                '<div class="col">' +
                                                    '<label for="" class="text-white font-weight-bold product_name">' + element.name + '</label>' +
                                                '</div>' +
                                                '<div class="col">' +
                                                    '<label for="" class="text-white font-weight-bold product_desc">' + element.description + '</label>' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>';
                                $('#itemrow').append(html);
                            });
                            
                            $('#itemlist').show();
                        }
                        else{
                            Swal.fire({
                                title: response.message,
                                text: "Problemas de Conexión con Quantum, por favor intentalo más tarde",
                                icon: 'warning',
                                confirmButtonText: 'Aceptar'
                            })

                            $('#itemlist').hide();
                        }
                    }
                });
            });

        //CAMBIO O AÑADIDO DE IMAGEN DEL PREMIO EN STORE
            function readImage(input) {
                var url = input.value;
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

                if (input.files && input.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#previewImage').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);

                    $('#eraseImage').removeClass('d-none');
                } else {
                    $('#previewImage').attr('src', null);
                    input.value = null;
                    $('#eraseImage').addClass('d-none');
                }
            }

            $("#customImage").change(function() {
                readImage(this);
            });

            $('#eraseImage').click(function (){
                $('#previewImage').attr('src', null);
                $("#iBefore").val(null);
                $("#customImage").val(null);
                $(this).addClass('d-none');
            });
    });

    //Selección de producto y arreglo en el form
    function productTaken(id){
        var nombre = $("#"+id+" .product_name").text();
        var categoria = $("#"+id+" .product_cat").val();
        var imagen = $("#"+id+" .product_image").attr("src");
        var descripcion = $("#"+id+" .product_desc").text();

        $('#id_producto_dist').val(id);
        $('#nombrePremio').val(nombre);
        //$('#pre_cat').val(categoria);
        $('#imageQuantum').val(imagen);
        $('#descripcionPremio').text(descripcion);
    }

    function marcas(){
        var urlMarcas = $('#getMarcas').val();
        $.ajax({
            url: urlMarcas,
            type: 'GET',
            success: function(response){
                if(response.marcas){
                    response.marcas.forEach(function(element){
                        console.log(element);
                        var o = new Option(element.nombre, element.brand_id);
                        $(o).html(element.nombre);
                        $("#marcaselect").append(o);
                    });
                }
                else{
                    Swal.fire({
                        title: response.message,
                        text: "Problemas de Conexión con Quantum, por favor intentalo más tarde",
                        icon: 'warning',
                        allowOutsideClick: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Aceptar'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                /* var redirect = $('#redirectIndex').val();
                                location.href = redirect; */
                                $('#btnSubmit').attr('disabled', true);
                                $('#btnSubmit').text('No esta disponible');
                            }
                        }
                    )
                }
            }
        });
    }
</script>