@extends('layouts.admin.app')
@section('content')
    <div class="container py-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        <h1 class="mb-5">¿Deseas eliminar el siguiente premio? </h1>
        <h4>Nombre: </h4>
        <p>{{ $premio->nombre }}</p>
        <h4>Marca: </h4>
        <p>{{ $premio->marca }}</p>
        <h4>Tipo de Entrega: </h4>
        <p>{{ $premio->entrega }}</p>
        <h4>Descripción: </h4>
        <p>{{ $premio->descripcion }}</p>
        <h4>Carácteristicas: </h4>
        <ul>
            @forelse($premio->caracteristicas as $caracteristica)
                <li>{{ $caracteristica->caracteristica }}</li>
            @empty
                <li>No posee Carácteristicas adjuntadas</li>
            @endforelse
        </ul>
        <h4>Imagen: </h4>
        <p><img style="width:200px;max-height:200px;" src="{{ route('premio.icon', ['filename' => basename($premio->imagen)]) }}" ></p>
        
        <form method="post" enctype="multipart/form-data" action="{{ route('premio.destroy', $premio->id_premio) }}">
            @method('DELETE')
            @csrf
            <div class="row col-md-12 mt-5 mx-auto">
                <button type="submit" class="redondo btn btn-danger col-md-4 mx-auto">
                    <i class="fas fa-trash-alt"></i> Eliminar
                </button>
                <a href="{{route('premio.listado')}}" class="btn btn-primary col-md-4 mx-auto">Volver a Premios</a>
            </div>
        </form>
    </div>
@endsection
