@extends('layouts.admin.app')

@section('content')

@if (Session::has('create'))
    <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('create') }}
    </div>
@endif

@if (Session::has('delete'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('delete') }}
    </div>
@endif

@if (Session::has('edit'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        {{ Session::get('edit') }}
    </div>
@endif

<!-- ESTILOS -->
<style>
    .popover {
        background: #39A5DB;
    }

    .bg_rose{
        background-color: #EDB2AB !important;
        color: white;
    }

    .noborder{
        border: 0;
        outline: none !important;
        background-color: transparent !important;
    }
</style>

<!-- PASO SECCIÓN 2 -->
<div id="container_principal">
    <div class="row">
        <div class="col-md-6 ml-auto">
            <div class="row">
                <hr style="position:absolute; border:2px dotted #999999; width:70%; left: 5%; top: 25%" />
                <div class="col row-reverse">
                    <div class="d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; background-color: #51A2A7">
                        <i class="fas fa-check text-white"></i>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px; color: #51A2A7">Agregar<br>premios</label>
                    </div>
                </div>
                <div class="col row-reverse ">
                    <div class="d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; background-color: #51A2A7">
                        <label for="" class="font-weight-bold text-white" style="font-size: 20px">2</label>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px; color: #51A2A7">Revision<br>pendiente</label>
                    </div>
                </div>
                <div class="col row-reverse">
                    <div class="bg-white d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px">
                        <label for="" class="font-weight-bold " style="font-size: 20px">3</label>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px;">Creación y<br>estado</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row my-5">
        @if ($errors->any())
            <div class="alert alert-danger d-block w-100">
                <label class="text-white">Porfavor Corrige los siguientes elementos, toma en cuenta que su indice (dato.indice)
                    representa la fila de la tabla</label>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <ul class="list-unstyled">
            <li>Usted ha exportado ({{ count($import) }}) premios: </li>
            <li>
                <ul>
                    <li>(<span id="goodRows">0</span>) tienen todos los campos necesarios para su creación</li>
                    <li class="text-danger font-weight-bold">Se han encontrado (<span id="errorCol">0</span>) errores, por favor arreglelos o eliminelos</li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col table-responsive">
            <table class="w-100" id="example">
                <thead style="background: #FFFFFF; box-shadow: -3px 3px 6px rgba(0, 0, 0, 0.05);">
                    <tr style="color: #F28D8D;">
                        <th scope="col" style="font-weight: bold;">Fila</th>
                        <th scope="col" style="font-weight: bold;">Nombre</th>
                        <th scope="col" style="font-weight: bold;">Marca</th>
                        <th scope="col" style="font-weight: bold;">Descripción</th>
                        <th scope="col" style="font-weight: bold;">Entrega</th>
                        <th scope="col" style="font-weight: bold;">Stock</th>
                        <th scope="col" style="font-weight: bold;">Puntos</th>
                        <th scope="col" style="font-weight: bold;">Distribuidor*</th>
                        <th scope="col" style="font-weight: bold;">Producto ID*</th>
                        <th scope="col" style="font-weight: bold;">Brand ID*</th>
                    </tr>
                </thead>
                <tbody>

                    <?php $contador = 0; $gRow = 0; $eCol = 0;  ?>
                    @foreach ($import as $usuario)
                        <?php 
                            $contador++;
                            $e_global = false;
                            //TRUE ES CUANDO HAY ERROR

                            //ERRORES DE NOMBRE
                            if(isset($usuario["nombre"])) 
                                $e_nombre = false;
                            else{ 
                                $e_nombre = true; $eCol++;
                            }

                            //ERRORES DE MARCA
                            if(isset($usuario["marca"]))
                                $e_marca = false;
                            else{ 
                                $e_marca = true;  $eCol++;
                            }

                            //ERRORES DE DESCRIPCION
                            if(isset($usuario["descripcion"]))
                                $e_descripcion = false;
                            else{ 
                                $e_descripcion = true; $eCol++; 
                            }

                            //ERRORES DE ENTREGA
                            if(isset($usuario["entrega"])){
                                //if( strtolower($usuario["segmento"]) != "invest" && strtolower($usuario["segmento"]) != "focus" && strtolower($usuario["segmento"]) != "otro") {
                                if ( !in_array(strtolower($usuario["entrega"]), array('domicilio','online'), true ) ) {
                                    $e_entrega = true; $eCol++; 
                                }else
                                    $e_entrega = false;
                            }
                            else $e_entrega = true;

                            //ERRORES DE STOCK
                            if(isset($usuario["stock"])){
                                if(!is_numeric($usuario["stock"])){
                                    $e_stock = true; $eCol++; 
                                }else
                                    $e_stock = false;
                            }
                            else $e_stock = true;

                            //ERRORES DE PUNTOS
                            if(isset($usuario["puntos"])){
                                if(!is_numeric($usuario["puntos"])){
                                    $e_puntos = true; $eCol++; 
                                }else
                                    $e_puntos = false;
                            }
                            else $e_puntos = true;

                            //ERRORES DE DISTRIBUIDOR
                            if(isset($usuario["distribuidor"]))
                                $e_distribuidor = false;
                            else{ 
                                $e_distribuidor = true; $eCol++; 
                            }

                            //ERRORES DE PRODUCTO ID
                            if(isset($usuario["producto_id"])){
                                if(!is_numeric($usuario["producto_id"])){
                                    $e_producto_id = true; $eCol++; 
                                }else
                                    $e_producto_id = false;
                            }
                            else $e_producto_id = true;

                            //ERRORES DE BRAND ID
                            if(isset($usuario["marca_id"])){
                                if(!is_numeric($usuario["marca_id"])){
                                    $e_marca_id = true; $eCol++; 
                                }else
                                    $e_marca_id = false;
                            }
                            else $e_marca_id = true;

                            //CHEQUEO SI HAY ERROR GLOBAL ENTONCES EN ALGÚN PUNTO
                            if($e_nombre || $e_marca || $e_descripcion || $e_entrega || $e_stock || $e_puntos || $e_distribuidor || $e_producto_id || $e_marca_id)
                                $e_global = true;
                            else{
                                $gRow++;
                            }
                        ?>
                        <tr id="row-{{ $contador }}" class="{{ $e_global ? 'bg_rose' : '' }} table-bordered">
                            <td class="text-center">
                                {{ $contador - 1 }}
                            </td>
                            <td id="nombre-{{ $contador }}" class="{{ $e_nombre ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["nombre"]) ? $usuario["nombre"] : "" }}
                            </td>
                            <td id="marca-{{ $contador }}" class="{{ $e_marca ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["marca"]) ? $usuario["marca"] : "" }}
                            </td>
                            <td id="descripcion-{{ $contador }}" class="{{ $e_descripcion ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["descripcion"]) ? $usuario["descripcion"] : "" }}
                            </td>
                            <td id="entrega-{{ $contador }}" class="{{ $e_entrega ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["entrega"]) ? $usuario["entrega"] : "" }}
                            </td>
                            <td id="stock-{{ $contador }}" class="{{ $e_stock ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["stock"]) ? $usuario["stock"] : "" }}
                            </td>
                            <td id="puntos-{{ $contador }}" class="{{ $e_puntos ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["puntos"]) ? $usuario["puntos"] : "" }}
                            </td>
                            <td id="distribuidor-{{ $contador }}" class="{{ $e_distribuidor ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["distribuidor"]) ? $usuario["distribuidor"] : "" }}
                            </td>
                            <td id="producto_id-{{ $contador }}" class="{{ $e_producto_id ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["producto_id"]) ? $usuario["producto_id"] : "" }}
                            </td>
                            <td id="marca_id-{{ $contador }}" class="{{ $e_marca_id ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["marca_id"]) ? $usuario["marca_id"] : "" }}
                            </td>
                            <td class="text-center">
                                @if($e_global)
                                    <a id="eIcon-{{ $contador }}">
                                        <i class="fas fa-exclamation-triangle text-danger"></i>
                                    </a>
                                @endif
                                <a id="{{ $contador }}" class="editRow" data-toggle="modal" data-target=".bd-example-modal-lg-1">
                                    <i class="fas fa-edit mx-3" style="color: #F28D8D"></i>
                                </a>
                                <a id="{{ $contador }}" class="deleteRow">
                                    <i class="fas fa-trash text-secondary"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- FIN SECCIÓN 2 -->

<!-- MODAL DE EDITAR PREMIOS -->
<div class="modal fade bd-example-modal-lg-1" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                    Editar Premio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" id="formEdit">
                    <div class="form-row">
                        <div class="col">
                            <label for="">Nombre:</label>
                            <input id="editName" type="text" class="form-control" placeholder="Nombre*" required>
                        </div>
                        <div class="col">
                            <label for="">Marca:</label>
                            <input id="editMarca" type="text" class="form-control" placeholder="Marca*" required>
                        </div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col">
                            <label for="">Descripción:</label>
                            <textarea id="editDescripcion" class="form-control" required></textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <label for="">Entrega:</label>
                            <select id="editEntrega" class="form-control" required>
                                <option value="" selected disabled>Selecciona un metodo de Entrega</option>
                                <option value="Domicilio">Domicilio</option>
                                <option value="Online">Online</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col">
                            <label for="">Stock:</label>
                            <input id="editStock" type="number" min="0" class="form-control  mt-1" placeholder="Stock*" required>
                        </div>
                        <div class="col">
                            <label for="">Puntos:</label>
                            <input id="editPuntos" type="number" min="0" class="form-control  mt-1" placeholder="Puntos*" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <label for="">Distribuidor (Si no tiene colocar N/A):</label>
                            <input id="editDistribuidor" type="text" class="form-control" placeholder="N/A*" required>
                        </div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col">
                            <label for="">Producto ID (Si no tiene colocar 0):</label>
                            <input id="editProducto" type="number" min="0" class="form-control  mt-1" placeholder="0*" required>
                        </div>
                        <div class="col">
                            <label for="">Marca ID (Si no tiene colocar 0):</label>
                            <input id="editBrand" type="number" min="0" class="form-control  mt-1" placeholder="0*" required>
                        </div>
                    </div>
                    <input type="hidden" id="rowID">
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;">EDITAR USUARIO</button>
            </div>
            </form>

        </div>
    </div>
</div>
<!-- FIN MODAL DE EDITAR -->

<!-- PASO SECCIÓN 3 -->
<div id="container_principal_2" style="display: none;">
    <div class="row">
        <div class="col-md-6 ml-auto">
            <div class="row">
                <hr style="position:absolute; border:2px dotted #999999; width:70%; left: 5%; top: 25%" />
                <div class="col row-reverse">
                    <div class="d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; background-color: #51A2A7">
                        <i class="fas fa-check text-white"></i>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px; color: #51A2A7">Agregar<br>premios</label>
                    </div>
                </div>
                <div class="col row-reverse ">
                    <div class="d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; background-color: #51A2A7">
                        <i class="fas fa-check text-white"></i>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px; color: #51A2A7">Revision<br>pendiente</label>
                    </div>
                </div>
                <div class="col row-reverse">
                    <div class="d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; background-color: #51A2A7">
                        <label for="" class="font-weight-bold text-white" style="font-size: 20px">3</label>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px;">Creación y<br>estado</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col">
            <div class="row mb-2">
                <div class="col">
                    <label class="float-left">Selecciona una Categoría:</label>
                    <select class="form-control float-left" name="" id="categoriaSelect">
                        <option value="">Ninguna</option>
                        @foreach($categorias as $categoria)
                            <option value="{{ $categoria->id_premio_cat }}">{{ $categoria->categoria }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <table class="table">
                <thead style="background: #FFFFFF; box-shadow: -3px 3px 6px rgba(0, 0, 0, 0.05);">
                    <tr style="color: #F28D8D;">
                        <th scope="col" style="font-weight: bold;">Nombre</th>
                        <th scope="col" style="font-weight: bold;">Marca</th>
                        <th scope="col" style="font-weight: bold;">Descripción</th>
                        <th scope="col" style="font-weight: bold;">Entrega</th>
                        <th scope="col" style="font-weight: bold;">Stock</th>
                        <th scope="col" style="font-weight: bold;">Puntos</th>
                        <th scope="col" style="font-weight: bold;">Distribuidor*</th>
                        <th scope="col" style="font-weight: bold;">Producto ID*</th>
                        <th scope="col" style="font-weight: bold;">Brand ID*</th>
                    </tr>
                </thead>
                <tbody>
                    <form action="{{ route('premio.import.store') }}" method="post" id="formInputs">
                        @csrf
                        <input type="hidden" name="categoria" id="categoriaInput" value="">
                        <?php $contador = 0; ?>
                        @foreach ($import as $usuario)
                            <?php $contador++; ?>
                            <tr id="rowI-{{ $contador }}" class="table-bordered">
                                <td id="nombreI-{{ $contador }}">
                                    <input name="nombre[]" type="text" class="noborder" readonly value='{{ isset($usuario["nombre"]) ? $usuario["nombre"] : "" }}'>
                                </td>
                                <td id="marcaI-{{ $contador }}">
                                    <input name="marca[]" type="text" class="noborder" readonly value='{{ isset($usuario["marca"]) ? $usuario["marca"] : "" }}'>
                                </td>
                                <td id="descripcionI-{{ $contador }}">
                                    <input name="descripcion[]" type="text" class="noborder" readonly value='{{ isset($usuario["descripcion"]) ? $usuario["descripcion"] : "" }}'>
                                </td>
                                <td id="entregaI-{{ $contador }}">
                                    <input name="entrega[]" type="text" class="noborder" readonly value='{{ isset($usuario["entrega"]) ? $usuario["entrega"] : "" }}'>
                                </td>
                                <td id="stockI-{{ $contador }}">
                                    <input name="stock[]" type="text" class="noborder" readonly value='{{ isset($usuario["stock"]) ? $usuario["stock"] : "" }}'>
                                </td>
                                <td id="puntosI-{{ $contador }}">
                                    <input name="puntos[]" type="text" class="noborder" readonly value='{{ isset($usuario["puntos"]) ? $usuario["puntos"] : "" }}'>
                                </td>
                                <td id="distribuidorI-{{ $contador }}">
                                    <input name="distribuidor[]" type="text" class="noborder" readonly value='{{ isset($usuario["distribuidor"]) ? $usuario["distribuidor"] : "" }}'>
                                </td>
                                <td id="productI-{{ $contador }}">
                                    <input name="id_producto_dist[]" type="text" class="noborder" readonly value='{{ isset($usuario["producto_id"]) ? $usuario["producto_id"] : "" }}'>
                                </td>
                                <td id="brandI-{{ $contador }}">
                                    <input name="id_brand_dist[]" type="text" class="noborder" readonly value='{{ isset($usuario["marca_id"]) ? $usuario["marca_id"] : "" }}'>
                                </td>
                            </tr>
                        @endforeach
                    </form>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- FIN PASO DE SECCIÓN 3 -->

<div class="row mt-5">
    <div class="col-md-12">
        <hr style="height:2px;border-width:0;color:gray;background-color:#F28D8D">
    </div>
</div>

<!-- BOTONES PARA CAMBIOS DE SECCIÓN + ENVIO -->
<div class="row ">
    <div class="col d-flex justify-content-end">
        <button {{ $eCol > 0 ? "disabled" : "" }} class="btn btn-round" id="confirmar" onclick="container_principal();" style="background-color: #51A2A7">
            CONFIRMAR
        </button>
        <a href="javascript:;" onclick="sweetcheck()" id="aceptar" class="btn btn-round" style="background-color: #51A2A7; display: none">
            ACEPTAR Y PUBLICAR
        </a>
    </div>
</div>
<!-- FIN BOTONES -->

<input type="hidden" id="numRows" value="{{ $gRow }}">
<input type="hidden" id="numCols" value="{{ $eCol }}">

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<!-- SCRIPT DE CARGA INICIAL DEL DOM -->
<script>
    $(document).ready(function() {
        //SETTEO LOS VALORES DE BIEN Y ERRORES
        var rows = $('#numRows').val(); $('#goodRows').text(rows);
        var cols = $('#numCols').val(); $('#errorCol').text(cols);
        
        //SECCIÓN DE EDITAR FILAS
            //abrir modal
            $('.editRow').click(function(){
                var id = $(this).attr('id');
     
                //Recogo los Valores de la Tabla
                var nombre = $('#nombre-'+id).text().trim();
                var marca = $('#marca-'+id).text().trim();
                var descripcion = $('#descripcion-'+id).text().trim();
                var entrega = $('#entrega-'+id).text().trim();
                var stock = parseInt($('#stock-'+id).text().trim());
                var puntos = parseInt($('#puntos-'+id).text().trim());
                var distribuidor = $('#distribuidor-'+id).text().trim();
                var producto_id = parseInt($('#producto_id-'+id).text().trim());
                var marca_id = parseInt($('#marca_id-'+id).text().trim());

                //Setteo los valores en el modal
                $('#rowID').val(id);
                $('#editName').val(nombre);
                $('#editMarca').val(marca);
                $('#editDescripcion').val(descripcion);
                $('#editEntrega').val(entrega);
                $('#editStock').val(stock);
                $('#editPuntos').val(puntos);
                $('#editDistribuidor').val(distribuidor);
                $('#editProducto').val(producto_id);
                $('#editBrand').val(marca_id);
            });

            //Submit de Form
            $( "#formEdit" ).on('submit', function(e){
                e.preventDefault();

                //Cogemos los datos
                var id = $('#rowID').val();
                var nombre = $('#editName').val();
                var marca = $('#editMarca').val();
                var descripcion = $('#editDescripcion').val();
                var entrega = $('#editEntrega').val();
                var stock = $('#editStock').val();
                var puntos = $('#editPuntos').val();
                var distribuidor = $('#editDistribuidor').val();
                var producto_id = $('#editProducto').val();
                var marca_id = $('#editBrand').val();

                //Corregimos los datos el td y el tr
                $('#nombre-'+id).text(nombre); $('#nombreI-'+id+' input').val(nombre);
                $('#marca-'+id).text(marca); $('#marcaI-'+id+' input').val(marca);
                $('#descripcion-'+id).text(descripcion); $('#descripcionI-'+id+' input').val(descripcion);
                $('#entrega-'+id).text(entrega); $('#entregaI-'+id+' input').val(entrega);
                $('#stock-'+id).text(stock); $('#stockI-'+id+' input').val(stock);
                $('#puntos-'+id).text(puntos); $('#puntosI-'+id+' input').val(puntos);
                $('#distribuidor-'+id).text(distribuidor); $('#distribuidorI-'+id+' input').val(distribuidor);
                $('#producto_id-'+id).text(producto_id); $('#productI-'+id+' input').val(producto_id);
                $('#marca_id-'+id).text(marca_id); $('#brandI-'+id+' input').val(marca_id);

                $('#row-'+id).removeClass('bg_rose');
                $('#eIcon-'+id).remove();
                //$('#row-'+id+' td').removeClass('text-danger font-weight-bold');
                var error = 0;
                $('#row-'+id+' td').each(function(indice,elemento) {
                    $(elemento).hasClass('text-danger') ? error++ : "";
                    $(elemento).removeClass('text-danger font-weight-bold');
                });
                //Cerramos el Modal
                $('#editModal').modal('hide');

                //Arreglamos el número de errores por resolver
                rows++; cols -= error;
                $('#goodRows').text(rows<0 ? 0 : rows);
                $('#errorCol').text(cols<0 ? 0 : cols);

                if(cols < 1){
                    $('#confirmar').removeAttr('disabled');
                }
            });

        //SECCIÓN DE ELIMINAR FILA
            //Elimino fila con el click de la papelera
            $('.deleteRow').click(function(){
                var id = $(this).attr('id');

                Swal.fire({
                    icon: 'warning',
                    title: '¿Esta seguro de borrar este usuario?',
                    //type: 'warning',
                    confirmButtonText: 'Si, Eliminar!'
                }).then((result) => {
                    if (result.value) {
                        var error = 0;
                        $('#row-'+id+' td').each(function(indice,elemento) {
                            $(elemento).hasClass('text-danger') ? error++ : "";
                            $(elemento).removeClass('text-danger font-weight-bold');
                        });

                        Swal.fire({
                            title: 'Usuario a Importar Eliminado!',
                            icon: 'success',
                        });

                        cols -= error;
                        $('#errorCol').text(cols<0 ? 0 : cols);

                        if(cols < 1){
                            $('#confirmar').removeAttr('disabled');
                        }

                        $('#row-'+id).remove();
                        $('#rowI-'+id).remove();
                    }
                });
            });

        //SECCION DE CATEGORIA SELECCIONADA
            $("#categoriaSelect").change(function (){
                var categoria = $(this).val();
                $("#categoriaInput").val(categoria);
            });
    });
</script>

<!-- FUNCION UE PASA DE ESTADO 2 A 3 -->
<script>
    function container_principal() {
        $("#container_principal").css({
            'display': 'none'
        })
        $("#container_principal_2").css({
            'display': 'block'
        })
        $("#confirmar").css({
            'display': 'none'
        })
        $("#aceptar").css({
            'display': 'block'
        })
    }
</script>

<!-- ALERTAS SWEET DE ELIMINAR Y DE ACEPTAR CARGAR TODO -->
<script>
    function sweetcheck() {
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Los usuarios se estan subiendo',
            showConfirmButton: false,
        });
        $('#formInputs').submit();
    }
</script>
@endsection