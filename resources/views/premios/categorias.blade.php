@extends('layouts.admin.app')

@section('content')

@if (Session::has('create'))
    <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('create') }}
    </div>
@endif

@if (Session::has('delete'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('delete') }}
    </div>
@endif

@if (Session::has('edit'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        {{ Session::get('edit') }}
    </div>
@endif

@if (Session::has('errors'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        Fallo en los datos enviados, por favor abre nuevamente el modal y corrige los errores que hayas realizado
    </div>
@endif

<div class="row mb-3 mx-auto" style="display: flex; align-items: center; justify-content: space-between !important">
    <div class="col">
        <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 2%; color: #51A2A7">
            Categorías de los Premios
        </label>
    </div>
    <div class="col" style="display: flex;justify-content: flex-end !important">

        <button class="btn btn-round" style="background-color: #51A2A7" data-toggle="modal" data-target="#storeModal">
            <i class="material-icons">add</i>AGREGAR CATEGORÍA
        </button>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table id="example" class="table table-striped " cellspacing="0" width="100%">
                <thead style="background-color: #FFFFFF; color: #51A2A7; font-weight: bold">
                    <tr>
                        <th class="font-weight-bold">Código</th>
                        <th class="font-weight-bold">Icono</th>
                        <th class="font-weight-bold">Nombre</th>
                        <th class="font-weight-bold">Número de Premios</th>
                        <th class="font-weight-bold">Fecha de Actualización</th>
                        <th class="font-weight-bold">Acciones</th>
                    </tr>
                </thead>
                <tbody class="table-bordered" style="background-color: #fff; color: #000"> 
                    @foreach ($categorias as $categoria)
                        <tr class="text-center" id="row{{ $categoria->id_premio_cat }}">
                            <td>{{ $categoria->id_premio_cat }}</td>
                            <td class="mx-auto text-center" > 
                                <img style="width:100px;max-height:100px;" id="image{{ $categoria->id_premio_cat }}"
                                    src="{{ route('premio.categoria.icon', ['filename' => basename($categoria->imagen)]) }}" >
                            </td>
                            <td id="nom{{ $categoria->id_premio_cat }}">{{ $categoria->categoria }}</td>
                            <td>
                                {{ $categoria->premios->count() }} 
                                <i class="fas fa-gift" style="color: #51A2A7"></i>
                            </td>
                            <td>{{ date_format(date_create($categoria->updated_at), "F j, Y") }}</td>
                            <td class="text-center">
                                <a id="{{ $categoria->id_premio_cat }}" class="editRow" data-toggle="modal" data-target="#editModal" style="color: #51A2A7; cursor: pointer">
                                    <i class="far fa-edit"></i>
                                </a>
                                <a id="{{ $categoria->id_premio_cat }}" style="color: #51A2A7;  cursor: pointer" class="deleteRow">
                                    <i class="far fa-trash-alt"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- MODAL DE AGREGAR CATEGORIA -->
<div class="modal fade" id="storeModal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                    Añadir Categoría</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="{{ route('premio.categoria.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="col">
                        <input name="categoria" id="" type="text" placeholder="Nombre de la Categoría..." value="{{ old('categoria') }}"
                            class="form-control @error('categoria') is-invalid @enderror">
                        @error('categoria')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-row my-3">
                    <div class="col">
                        <label for="">Icono de la Categoría:</label>
                        <div class="custom-file mb-3">
                            <input type="file" class="custom-file-input @error('imagen') is-invalid @enderror" 
                            id="customImage" name="imagen" accept=".jpg,.jpeg,.png" value="">
                            <label class="custom-file-label" for="customImage">Escoger una Imagen</label>
                            @error('imagen')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="d-none" id="eraseImage">
                            <label style="cursor: pointer;">
                                Quitar Imagen 
                                <i class="fas fa-click" style="font-size: 20px; color: #F28D8D"></i>
                            </label>
                        </div>
                    </div>
                    <div class="col">
                        <img class="img w-100" id="previewImage"
                        src="{{ route('premio.categoria.icon', ['filename' => basename(null)]) }}">
                    </div>
                </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;">
                CREAR CATEGORÍA
            </button>
        </div>
            </form>
        </div>
    </div>
</div>
<!-- FIN MODAL -->

<!-- MODAL DE EDITAR CATEGORIA -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                    Edición de Categoría - <span id="editCOD"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('premio.categoria.update') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row">
                        <div class="col">
                            <input name="categoriaUD" id="categoriaNom" type="text" placeholder="Nombre de la Categoría..." 
                                value="{{ old('categoriaUD') }}" class="form-control @error('categoriaUD') is-invalid @enderror">
                            @error('categoriaUD')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col-12">
                            <label for="">Icono de la Categoría (si no la vas a modificar no cambies nada):</label>
                        </div>
                        <div class="col">
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input @error('imagenUD') is-invalid @enderror" 
                                id="customUpdateImage" name="imagenUD" accept=".jpg,.jpeg,.png" value="">
                                <label class="custom-file-label" for="customImage">Escoger una Imagen</label>
                                @error('imagenUD')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col">
                            <img class="img w-100" id="previewUpdateImage"
                            src="{{ route('premio.categoria.icon', ['filename' => basename(null)]) }}">
                        </div>
                    </div>
                    <input type="hidden" name="id_categoria" id="editID" value="">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;">
                    EDITAR CATEGORÍA
                </button>
            </div>
                </form>
        </div>
    </div>
</div>
<!-- FIN MODAL DE EDITAR CATEGORIA -->

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
<input type="hidden" id="urlDelete" value="{{ route('premio.categoria.destroy') }}">

<style>
    .file-input>[type='file'] {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
        z-index: 10;
        cursor: pointer;
    }

    .file-input>.button {
        display: inline-block;
        cursor: pointer;
        background: #eee;
        padding: 4px 12px;
        border-radius: 2px;
        margin-right: 8px;
    }

    .file-input:hover>.button {
        background: dodgerblue;
        color: white;
    }

    .file-input>.label {
        color: #333;
        white-space: nowrap;
        opacity: .3;
    }

    .file-input.-chosen>.label {
        opacity: 1;
    }

</style>

<script>
    // Also see: https://www.quirksmode.org/dom/inputfile.html

    var inputs = document.querySelectorAll('.file-input')

    for (var i = 0, len = inputs.length; i < len; i++) {
        customInput(inputs[i])
    }

    function customInput(el) {
        const fileInput = el.querySelector('[type="file"]')
        const label = el.querySelector('[data-js-label]')

        fileInput.onchange =
            fileInput.onmouseout = function() {
                if (!fileInput.value) return

                var value = fileInput.value.replace(/^.*[\\\/]/, '')
                el.className += ' -chosen'
                label.innerText = value
            }
    }

</script>

@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }

        //ABRIR MODAL DE EDITAR Y SETTEAR VALORES
        $('.editRow').click(function(){
            var id = $(this).attr('id');
            
            //Recogo los Valores de la Tabla
            var nombre = $('#nom'+id).text().trim();
            var src = $('#image'+id).attr("src");

            //Setteo los valores en el modal
            $('#editID').val(id);
            $('#previewUpdateImage').attr("src", src);
            $('#categoriaNom').val(nombre);
            $('#editCOD').text(id);
        });

        //ELIMINAR EL DATO POST SWAL ALERT
        $('.deleteRow').click(function(){
            var id = $(this).attr('id');
            var url = $('#urlDelete').val();

            Swal.fire({
                icon: 'warning',
                title: '¿Esta seguro de borrar esta categoría?',
                text: 'Si algún premio usaba esta categoría recuerde entrar y asignarle una nueva',
                type: 'warning',
                confirmButtonText: 'Si, Eliminar!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: url+'/'+id,
                        data: {
                            "_token": $("meta[name='csrf-token']").attr("content")
                        },
                        type: 'POST',
                        success: function(response){
                            if(response.delete){
                                $("#row"+id).remove();
                                Swal.fire(
                                    response.message,
                                    'Presiona el boton para cerrar el modal',
                                    'success'
                                )
                            }
                            else{
                                Swal.fire(
                                    response.message,
                                    'Presiona el boton para cerrar el modal',
                                    'warning'
                                )
                            }
                        }
                    });
                }
            });
        });

        //CAMBIO O AÑADIDO DEL ICONO DE LA CATEGORIA EN STORE
            function readImage(input) {
                var url = input.value;
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

                if (input.files && input.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#previewImage').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);

                    $('#eraseImage').removeClass('d-none');
                } else {
                    $('#previewImage').attr('src', null);
                    input.value = null;
                    $('#eraseImage').addClass('d-none');
                }
            }

            $("#customImage").change(function() {
                readImage(this);
            });

            $('#eraseImage').click(function (){
                $('#previewImage').attr('src', null);
                $("#customImage").val(null);
                $(this).addClass('d-none');
            });

        //CAMBIO O AÑADIDO DEL ICONO DE LA CATEGORIA EN UPDATE
            function readUpdateImage(input) {
                var url = input.value;
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

                if (input.files && input.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#previewUpdateImage').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);

                    $('#eraseUpdateImage').removeClass('d-none');
                } else {
                    $('#previewUpdateImage').attr('src', null);
                    input.value = null;
                    $('#eraseUpdateImage').addClass('d-none');
                }
            }

            $("#customUpdateImage").change(function() {
                readUpdateImage(this);
            });

            $('#eraseUpdateImage').click(function (){
                $('#previewUpdateImage').attr('src', null);
                $("#customUpdateImage").val(null);
                $(this).addClass('d-none');
            });
    });
</script>