@extends('layouts.admin.app')

@section('content')

@if (Session::has('create'))
    <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('create') }}
    </div>
@endif

@if (Session::has('delete'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('delete') }}
    </div>
@endif

@if (Session::has('edit'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        {{ Session::get('edit') }}
    </div>
@endif

<!-- PASOS 1 / 2 / 3 -->
<div class="row" id="pasos" style="display:block">
    <div class="col-md-6 ml-auto">
        <div class="row">
            <hr style="position:absolute; border:2px dotted #999999; width:70%; left: 5%; top: 25%" />
            <div class="col row-reverse">
                <div class="d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; background-color: #51A2A7">
                    <label for="" class="font-weight-bold text-white" style="font-size: 20px">1</label>
                </div>
                <div>
                    <label for="" class="text-center font-weight-bold" style="font-size: 14px; color: #51A2A7">Agregar<br>premios</label>
                </div>
            </div>
            <div class="col row-reverse ">
                <div class="bg-white d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; ">
                    <label for="" class="font-weight-bold" style="font-size: 20px">2</label>
                </div>
                <div>
                    <label for="" class="text-center font-weight-bold" style="font-size: 14px;">Revision<br>pendiente</label>
                </div>
            </div>
            <div class="col row-reverse">
                <div class="bg-white d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px">
                    <label for="" class="font-weight-bold " style="font-size: 20px">3</label>
                </div>
                <div>
                    <label for="" class="text-center font-weight-bold" style="font-size: 14px;">Creación y<br>estado</label>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- BOTON DE IMPORTAR O ECHAR PARA ATRAS -->
<div class="row">
    <a href="{{ route('premio.listado') }}">
        <button class="btn btn-round" id="Agregar" style="background-color: #F28D8D; color: white">
            <i class="fas fa-box-open mr-2"></i>Retornar a Premios
        </button>
    </a>
</div>

<!-- APARTADO DONDE APARECEN LOS ERRORES -->
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<!-- CONTENEDOR IMPORTAR USUARIOS -->
<div class="container mx-auto my-5 col-md-12 " id="contenedor2" style="display: block">
    <div class="row d-flex justify-content-center" style="margin-top: 15%;" id="contenedor_b_p">
        <div>
            <div class="pie" style="display: none">
                <div class="pie-holder">
                    <svg viewBox="-8 -5 115 110">
                        <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="#C1C1C1" stroke-width="5" fill-opacity="0" />
                        <path id="progress_animate1" d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="#F28D8D" stroke-width="5" fill-opacity="0" style="stroke-dasharray: 0, 295.351; stroke-dashoffset: 0;" />
                    </svg>

                    <span><strong id="score-1">0</strong>%</span>

                </div>
            </div>
            <form id="formImport" action="{{ route('premio.import.csv') }}" method="post" enctype="multipart/form-data">
                @csrf
                <span class="btn btn-round btn-file" style="background: #F28D8D;" id="botoncargue">
                    <span class="fileinput-new">SELECCIONE EL ARCHIVO</span>
                    <input type="file" name="file" accept=".xls,.csv,.xlsx" id="progress" required/>
                </span>
                @error('file')
                    <span class="invalid-feedback text-center" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </form>
        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <label for="" id="text3" style="display: none;">Su archivo se esta importando, espere unos minutos</label>
    </div>
    <div class="row d-flex justify-content-center mt-3">
        <label for="" id="text1">La primera línea del archivo xlsx o csv se tomará como cabecera</label>
    </div>
    <div class="row d-flex justify-content-center">
        <label for="" id="text2">El documento debe ser .xlsx o cvs</label>
    </div>
    <div class="row d-flex justify-content-center">
        <label for="" id="text4">Descarga la <a href="{{url('csv/premiosImport.xlsx')}}"  download="plantilla_premio.xlsx" download>Plantilla de ejemplo aqui</a></label>
    </div>
</div>

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
<!-- ESTA ES LA LIBRERIA PARA EL PROGRESS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    /* AQUI CAPTURAMOS CUANDO ESCOJA UN ARCHIVO PARA LANZAR EL PROGRESO */
    
    $(document).ready(function() {

        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }

        $('#progress').change(function(){
            
            /* SI NO ESCOGE UN ARCHIVO DA UN ERROR CON EL SWEET ALERT */
            var fileExtension = ['csv', 'xls', 'xlsx'];
            if (!$('#progress').val() || $.inArray($('#progress').val().split('.').pop().toLowerCase(), fileExtension) == -1){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'No has escogido ningun archivo o no cumple con el formato correcto (xls, xlsx, csv)!'
                });
                return;
            }
            
            /* SI ESCOGE UN ARCHIVO SIGO EL PROCESO */
            $(".pie").css({'display': 'block'});
            $("#botoncargue").css({'display': 'none'});
            $("#formSubmit").css({'display': 'none'});
            $("#text1").css({'display': 'none'});
            $("#text2").css({'display': 'none'});
            $("#text4").css({'display': 'none'});
            $("#text3").css({'display': 'block'});
            $("#contenedor_b_p").css({'margin-top': '0'});

            /* ESTE ES EL FUNCIONAMIENTO DEL PROGRESS */
            var game = { score: 0 },
            scoreDisplay = document.getElementById("score-1");

            function updateHandler() {
                scoreDisplay.innerHTML = game.score;
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Archivo adjuntado',
                    showConfirmButton: false,
                    timer: 1800
                }).then(function() {
                    //window.location.href = "{{route('premio.import.tabla')}}";
                    $("#formImport").submit();
                });
            }

            var c1Section3 = new TimelineLite();

            c1Section3
            .to("#progress_animate1", 3, {
                "stroke-dasharray": "300 200",
                ease: Expo.easeNone,
            }, 0)
            .to(game, 3, {
                score: "+100",
                roundProps: "score",
                //LLEGO A 100 EL LOADER
                onUpdate: updateHandler,
                ease: Expo.easeNone,
            }, 0);
        });
    });
</script>

<style>
    @media (min-width: 500px) and (max-width: 890px) {
        #pasos {
            margin-bottom: 10%;
        }
    }

    .pie {
        width: 100%;

    }

    .pie .pie-holder {
        position: relative;
        width: 350px;
        height: 350px;
        margin: 0 auto 0 auto;
        overflow: hidden;
        border-radius: 260px;
    }

    .pie .pie-holder span {
        position: absolute;
        top: 50%;
        transform: translateY(-100%);
        left: 0;
        width: 100%;
        text-align: center;
        font-weight: bold;
        color: #F28D8D;
        font-size: 2rem;

    }
</style>
@endsection
