@extends('layouts.admin.app')

@section('content')

    @if (Session::has('create'))
        <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('create') }}
        </div>
    @endif

    @if (Session::has('delete'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('delete') }}
        </div>
    @endif

    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    <div class="row" style="margin-top: 5%">

        <!-------------------------------- CARDs NOTICIAS --------------------------------->
        <div class="col-lg-6 col-md-6">
            <div class="row mx-auto">
                <label for="" class="lead" style="color: #51A2A7; font-weight: bold">Noticias</label>
                <a href="{{ route('noticias.list') }}" class="ml-auto text-secondary">Ver todos</a>
            </div>

            @forelse ($noticias as $noticia)
                <div class="card">
                    <div class="card-body p-0">
                        <div class="row container">
                            <div class="col-md-6">
                                <img src="{{ route('noticias.image', ['filename' => basename($noticia->imagen)]) }}"
                                    class="w-100" alt="">
                            </div>
                            <div class="col-md-6 my-auto">
                                <h4 class="card-title" style="color: #51A2A7; font-weight: bold">{{ $noticia->titulo }}</h4>
                                <p>                 
                                    <i class="fas fa-thumbs-up mx-1 my-auto" style="font-size: 20px; color: #51A2A7;"></i>
                                    <span class="font-weight-bold">{{ $noticia->likes->count() }}</span>           
                                    <br>
                                    Estado: (
                                    <span class='{{ $noticia->estado ? 'text-success' : 'text-danger' }}'>
                                        {{ $noticia->estado ? 'Activo' : 'Inactivo' }}
                                    </span>)
                                </p>
                                <p style="font-weight: bold" class="mb-0">Categoría: <span class="font-weight-normal">{{ $noticia->tipo->nombre }}</span></p>
                                <p style="font-weight: bold" class="mb-0">
                                    Fecha de publicación: <span class="font-weight-normal">{{ date_format(date_create($noticia->created_at), 'F j, Y') }}</span>
                                </p>
                                <p>{{ $noticia->descripcion }}</p>
                                  
                                <div class="row mx-auto">
                                    <p class="mr-2 my-auto" style="font-size: 15px; color: #51A2A7">
                                        <a style="color: #51A2A7" href="{{ route('noticias.edit', ['id' => $noticia->id_noticia]) }}">
                                        Ver / Editar Noticias</a>
                                    </p>
                                    <i class="far fa-edit mx-1 my-auto" style="font-size: 20px; color: #51A2A7"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="card">
                    <div class="card-header">
                        <div class="row mx-auto">
                            <h4 class="card-title" style="color: #51A2A7; font-weight: bold">No hay Noticias Registradas
                            </h4>
                        </div>
                    </div>
                </div>
            @endforelse

            <button class="btn btn-round" style="background-color: #51A2A7" data-toggle="modal"
                data-target=".bd-example-modal-lg">
                <i class="material-icons">add</i>CREAR NOTICIA
            </button>

            <!-- MODAL DE CREAR NOTICIA -->
            <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                                Creacion de noticias</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ route('noticias.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="col">
                                        <input name="titulo" id="" type="text" placeholder="Título de la Noticia" value="{{ old('titulo') }}"
                                            class="form-control @error('titulo') is-invalid @enderror">
                                        @error('titulo')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row my-3">
                                    <input type="hidden" name="selectedTipo" id="selectedTipo">
                                    <div class="col">
                                        <select name="FK_id_tipo" id="ntipo" class="form-control @error('FK_id_tipo') is-invalid @enderror">
                                            <option value="" selected disabled>Seleccione tipo de Noticia...</option>
                                            @foreach($tipos as $tipo)
                                                <option value="{{ $tipo->id_noticia_tipo }}" {{ old('FK_id_tipo') == $tipo->id_noticia_tipo ? "selected" : "" }}>{{ $tipo->nombre }}</option>
                                            @endforeach
                                        </select>
                                        @error('FK_id_tipo')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col">
                                        <input name="categoria" id="" type="text" placeholder="Categoría de la Noticia" value="{{ old('categoria') }}"
                                            class="form-control @error('categoria') is-invalid @enderror">
                                        @error('categoria')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row my-3">
                                    <div class="col-12 mb-2">
                                        <label>Descripción de la Noticia:</label>
                                        <textarea class="form-control @error('descripcion') is-invalid @enderror" 
                                        name="descripcion" id="" type="text" >{{ old('descripcion') }}</textarea>
                                        @error('descripcion')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col">
                                        <label>Contenido de la Noticia (Opcional):</label>
                                        <textarea class="form-control @error('contenido') is-invalid @enderror" 
                                        name="contenido" id="" type="text">{{ old('contenido') }}</textarea>
                                        @error('contenido')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row my-3">
                                    <div class="col">
                                        <label for="">Imagen Preview de Noticia:</label>
                                        <input type="file" name="imagen" accept=".jpg,.jpeg,.png" required
                                            class="form-control @error('imagen') is-invalid @enderror" id="">
                                        @error('imagen')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col">
                                        <label for="">Subir Documento (imagen, pdf o video) si posee:</label>
                                        <input type="file" name="documento" accept=".jpg,.jpeg,.png,.pdf,.mp4"
                                            class="form-control @error('documento') is-invalid @enderror" id="">
                                        @error('documento')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row my-3">
                                    <div class="col">
                                        <label for="">Estado:</label><br>
                                        <div class="form-check form-check-radio form-check-inline ml-3 mr-3">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="estado" checked
                                                    id="inlineRadio1" value="1"> Noticia Activada
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="form-check form-check-radio form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="estado" id="inlineRadio1"
                                                    value="0"> Noticia Desactivada
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                                @error('ingredientes')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                @error('directorio')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <!-- DIRECTORIO -->
                                <div id="portafolio" class="d-none">
                                    @if(!$profesionales->isEmpty())
                                        <label for="" style="color: #51A2A7; font-weight: bold">Posee Directorios Anclado?</label>
                                        <div class="form-check form-check-radio form-check-inline ml-3 mr-3">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="portafolio"
                                                    id="inlineRadio1" value="1"> Si
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="form-check form-check-radio form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="portafolio" id="inlineRadio1"
                                                    value="0"> No
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>

                                        <!-- PROFESIONALES -->
                                        <div class="d-none my-3" id="directorios">
                                            <label for="" style="color: #51A2A7; font-weight: bold">Selecciona al Profesional</label>
                                            <div class="form-row">
                                                <div class="col">
                                                    <select id="persona" class="form-control">
                                                        @foreach($profesionales as $pro)
                                                            <option value="{{ $pro->id_profesional }}">
                                                                {{ $pro->nombre.' '.$pro->apellido.' | '.$pro->especialidad }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    <input style="background-color: #51A2A7" class="btn btn-sm" type="button" title="Agregar Producto" Value="+" id="agregar">
                                                    <input style="background-color: #51A2A7" class="btn btn-sm" type="button" title="Borrar Producto" Value="-" id="borrar">
                                                </div>
                                            </div> 

                                            <div class="form-row">
                                                <div class="col" id="addPersona"></div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <!-- FIN DIRECTORIO -->

                                <!-- INGREDIENTES -->
                                <div id="ingredientes" class="d-none">
                                    <label for="" style="color: #51A2A7; font-weight: bold">Ingresa los Ingredientes</label>
                                    <div class="form-row my-3">
                                        <div class="col">
                                            <input type="text" id="in_ingrediente" class="form-control" placeholder="Ingrediente - Cantidad">
                                            <input style="background-color: #51A2A7" class="btn btn-sm" type="button" title="Agregar Producto" Value="+" id="agregar-ingrediente">
                                            <input style="background-color: #51A2A7" class="btn btn-sm" type="button" title="Borrar Producto" Value="-" id="borrar-ingrediente">
                                        </div>
                                    </div> 

                                    <div class="form-row">
                                        <div class="col" id="addIngredient"></div>
                                    </div>
                                </div>
                                <!-- FIN INGREDIENTES -->
                        </div>

                        <div class="modal-footer">
                            <input class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;"
                                type="submit" value="CREAR NOTICIA">
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-------------------------------- CARDs MUROS --------------------------------->
        <div class="col-lg-6 col-md-6">
            <div class="row mx-auto">
                <label class="lead" for="" style="color: #F28D8D; font-weight: bold">Muros</label>
                <a href="{{ route('muro.list') }}" class="ml-auto text-secondary
                            ">Ver Muros</a>
            </div>

            @forelse ($muros as $muro)
                <div class="card">
                    <div class="card-body p-0">
                        <div class="row container">
                            <div class="col py-3">
                                <div class="col">
                                    <img class="img-​thumbnail rounded-circle" style="width:40px; height:40px"
                                    src="{{ route('admin.icon', ['filename' => basename($muro->creador->avatar)]) }}">
                                    <span style="color: #F28D8D; font-weight: bold">{{ $muro->creador->nombre.' '.$muro->creador->apellido }}</span>
                                </div>
                                <div class="col">
                                    <span style="font-size: 12px">{{ date_format(date_create($muro->created_at), 'j F') }}</span>
                                </div>
                                @if($muro->imagen)
                                    <div class="col">
                                        <img src="{{ route('muro.image', ['filename' => basename($muro->imagen)]) }}"
                                            class="w-100" alt="" height="100">
                                    </div>
                                @endif
                                <div class="col">
                                    <p style="text-align: justify;">{{ $muro->contenido }}</p>
                                </div>
                                <div class="col">
                                    
                                    <?php $user_like = false; ?>
                                    @foreach($muro->likes as $like)
                                        @if($like->FK_id_useradmin == Auth::user()->id_useradmin)
                                            <?php $user_like = true; ?>
                                        @endif
                                    @endforeach

                                    @if($user_like)
                                        <i id="{{$muro->id_muro}}" class="fas fa-heart mx-1 my-auto text-danger btn-dislike" 
                                            style="font-size: 20px;"></i>
                                    @else
                                        <i id="{{$muro->id_muro}}" class="fas fa-heart mx-1 my-auto text-secondary btn-like" 
                                            style="font-size: 20px;"></i>
                                    @endif
                                    <span id="heartCount{{$muro->id_muro}}" class="font-weight-bold">{{ $muro->likes->count() }}</span><br>

                                    <button id="{{ route('muro.detail', ['id'=>$muro->id_muro]) }}" 
                                        class="btn btn-sm w_open" style="background-color: #F28D8D;">
                                        {{ $muro->comments->count() }} Comentarios
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="card">
                    <div class="card-header">
                        <div class="row mx-auto">
                            <h4 class="card-title" style="color: #F28D8D; font-weight: bold">No hay Muros Registrados</h4>
                        </div>
                    </div>
                </div>
            @endforelse
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
    <input type="hidden" id="URLPATH" value="{{ route('muro.like.url') }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script>
    $(document).ready(function() {

        $(".w_open").click(function() {
            var id = $(this).attr("id");
            window.open(id, "_blank",
                "toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=600,height=500");
        });

        $("#ntipo").change(function(){
            var selectValue = $("#ntipo option:selected").text();
            $("#selectedTipo").val(selectValue);

            if(selectValue == "Salud"){
                $("#portafolio").removeClass("d-none");
                $("#ingredientes").addClass("d-none");
            }
            else if(selectValue == "Receta"){
                $("#portafolio").addClass("d-none");
                $("#ingredientes").removeClass("d-none");
            }
            else if(selectValue == "Tip de Moda"){
                $("#portafolio").addClass("d-none");
                $("#ingredientes").addClass("d-none");
            }
        });

        /* PROCESO DE INGRESAR O NO DIRECTORIO */
            $("input[name='portafolio']").change(function(){
                var radioValue = $("input[name='portafolio']:checked").val();
                if(radioValue == 1)
                    $("#directorios").removeClass("d-none");
                else
                    $("#directorios").addClass("d-none");
            });
            
            var inc = 0;
            $('#agregar').click(function(){
                inc++;
                var id = $('#persona').val();
                var value = $("#persona option:selected").text();
                $('#addPersona').append("<div id='per"+inc+"'><select readonly name='directorio[]' class='form-control'><option value='"+id+"' selected>"+value+"</option></select></div>");
            });

            $('#borrar').click(function(){
                if(inc > 0){
                    console.log('delete');
                    $('#per'+inc).remove();
                    inc--;
                }
            });
        /* FIN PROCESO DE INGRESAR O NO DIRECTORIO */

        /* PROCESO DE INGRESAR DINAMICAMENTE RECETAS */
            var inc_ing = 0;
            $('#agregar-ingrediente').click(function(){
                inc_ing++;
                var value = $('#in_ingrediente').val();
                if(value)
                    $('#addIngredient').append("<input id='ing"+inc_ing+"' readonly name='ingredientes[]' class='form-control' value='"+value+"'/>");
            });

            $('#borrar-ingrediente').click(function(){
                if(inc_ing > 0){
                    console.log('delete');
                    $('#ing'+inc_ing).remove();
                    inc_ing--;
                }
            });
        /* FIN PROCESO DE INGRESAR DINAMICAMENTE RECETAS */

        /* SETUP INICIAL POR SI DEVUELVE VALIDACIONES */
            var selectValue = $("#ntipo option:selected").text();
            $("#selectedTipo").val(selectValue);

            if(selectValue == "Salud"){
                $("#portafolio").removeClass("d-none");
                $("#ingredientes").addClass("d-none");
            }
            else if(selectValue == "Receta"){
                $("#portafolio").addClass("d-none");
                $("#ingredientes").removeClass("d-none");
            }
            else if(selectValue == "Tip de Moda"){
                $("#portafolio").addClass("d-none");
                $("#ingredientes").addClass("d-none");
            }
        /* FIN SETUP INICIAL */

        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
            $('.bd-example-modal-lg').modal('show');
        }
    });
</script>
