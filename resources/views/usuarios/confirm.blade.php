@extends('layouts.admin.app')
@section('content')
    <div class="container py-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        @if(auth()->user()->FK_id_adminrol == 1)
            <h1 class="mb-5">¿Deseas eliminar el siguiente usuario? </h1>
            <h4 class="font-weight-bold">TIPO DE USUARIO {{ strtoupper($user->tipo) }}</h4>
            <p>
                <img style="width:200px;max-height:200px;" src="{{ route('user.icon', ['filename' => basename($user->avatar)]) }}" >
            </p>
            <h4 class="font-weight-bold">Correo Electrónico: </h4>
            <p>{{ $user->email }}</p>
            <h4 class="font-weight-bold">Nombre del Usuario: </h4>
            <p>{{ $datos->nombre." ".$datos->apellido }}</p>
            <h4 class="font-weight-bold">Documento de Identidad: </h4>
            <p>{{ $datos->identificacion }}</p>
            <h4 class="font-weight-bold">Dirección: </h4>
            <p>{{ $datos->direccion }}</p>
            <h4 class="font-weight-bold">Teléfono: </h4>
            <p>{{ $datos->telefono }}</p>

            <form method="post" enctype="multipart/form-data" action="{{ route('user.destroy', $user->id) }}">
                @csrf
                <div class="row col-md-12 mt-5 mx-auto">
                    <button type="submit" class="redondo btn btn-danger col-md-4 mx-auto">
                        <i class="fas fa-trash-alt"></i> Eliminar
                    </button>
                    <a href="{{ route('user.listuser') }}" class="btn btn-primary col-md-4 mx-auto">Volver</a>
                </div>
            </form>
        @endif

    </div>
@endsection
