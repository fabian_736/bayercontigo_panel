<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/x-icon" href="{{url('img/favicon.ico')}}" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>


    <title>Bayer Contigo</title>

    <link rel="stylesheet" href="{{url('css/app.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css"  href="{{url('DataTables/DataTables-1.10.24/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('DataTables/Buttons-1.7.0/css/buttons.bootstrap4.min.css')}}">

</head>

<body id="page-top">
<div class="row">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Datos del Usuario: {{ strtoupper($tipo) }}</h5>
            </div>
            <div class="modal-body">
            
                <div class="row col-md-12 my-3">
                    <div class="col-md-6">
                        <label for="name_user">Nombre:</label>
                        <input type="text" class="form-control" disabled value="{{ $datos->nombre }}">
                    </div>
                    <div class="col-md-6">
                        <label for="name_user">Apellido:</label>
                        <input type="text" class="form-control" disabled value="{{ $datos->apellido }}">
                    </div>
                </div>

                <div class="row col-md-12 my-3">
                    <div class="col-md-6">
                        <label for="name_user">Dirección:</label>
                        <input type="text" class="form-control" disabled value="{{ $datos->direccion }}">
                    </div>
                    <div class="col-md-6">
                        <label for="name_user">Telefono:</label>
                        <input type="text" class="form-control" disabled value="{{ $datos->telefono }}">
                    </div>
                </div>

                <div class="row col-md-12 my-3">
                    <div class="col-md-6">
                        <label for="name_user">Identificación:</label>
                        <input type="text" class="form-control" disabled value="{{ $datos->identificacion }}">
                    </div>
                    <div class="col-md-6">
                        <label for="name_user">Especialidad:</label>
                        <input type="text" class="form-control" disabled value="{{ $datos->especialidad }}">
                    </div>
                </div>

                <div class="row col-md-12 my-3">
                    <div class="col">
                        <label for="name_user">Descripción:</label>
                        <p>{{ $datos->descripcion }}</p>
                    </div>
                </div>

                @if($datos->patologias)
                    <div class="row col-md-12 my-3">
                        <div class="col">
                            <label for="" style="color: #51A2A7; font-weight: bold">Patologías</label>
                            @forelse($datos->patologias as $pat)
                                <input type="text" class="form-control" disabled value="{{ $pat->patologia->nombre }}">
                            @empty    
                                <label for="">No posee patologías afiliadas</label>
                            @endforelse
                        </div>
                    </div>
                @endif

                @if($datos->insignias)
                    <div class="row col-md-12">
                        <div class="col">
                            <label for="" style="color: #51A2A7; font-weight: bold">Insignias</label>
                            @forelse($datos->insignias as $ins)
                                <input type="text" class="form-control" disabled value="{{ $ins->insignia->nombre }}">
                            @empty
                                <label for="">No posee insignias afiliadas</label>
                            @endforelse
                        </div>
                    </div>
                @endif
            </div>

        </div>
    </div>
</div>
</body>
