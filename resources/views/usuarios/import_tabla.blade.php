@extends('layouts.admin.app')

@section('content')

@if (Session::has('create'))
    <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('create') }}
    </div>
@endif

@if (Session::has('delete'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('delete') }}
    </div>
@endif

@if (Session::has('edit'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        {{ Session::get('edit') }}
    </div>
@endif

<!-- ESTILOS -->
<style>
    .popover {
        background: #39A5DB;
    }

    .bg_rose{
        background-color: #EDB2AB !important;
        color: white;
    }

    .noborder{
        border: 0;
        outline: none !important;
        background-color: transparent !important;
    }
</style>

<!-- PASO SECCIÓN 2 -->
<div id="container_principal">
    <div class="row">
        <div class="col-md-6 ml-auto">
            <div class="row">
                <hr style="position:absolute; border:2px dotted #999999; width:70%; left: 5%; top: 25%" />
                <div class="col row-reverse">
                    <div class="d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; background-color: #51A2A7">
                        <i class="fas fa-check text-white"></i>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px; color: #51A2A7">Agregar<br>usuarios</label>
                    </div>
                </div>
                <div class="col row-reverse ">
                    <div class="d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; background-color: #51A2A7">
                        <label for="" class="font-weight-bold text-white" style="font-size: 20px">2</label>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px; color: #51A2A7">Revision<br>pendiente</label>
                    </div>
                </div>
                <div class="col row-reverse">
                    <div class="bg-white d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px">
                        <label for="" class="font-weight-bold " style="font-size: 20px">3</label>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px;">Creación y<br>estado</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row my-5">
        @if ($errors->any())
            <div class="alert alert-danger d-block w-100">
                <label class="text-white">Porfavor Corrige los siguientes elementos, toma en cuenta que su indice (dato.indice)
                    representa la fila de la tabla</label>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <ul class="list-unstyled">
            <li>Usted ha exportado ({{ count($import) }}) usuarios: </li>
            <li>
                <ul>
                    <li>(<span id="goodRows">0</span>) tienen todos los campos necesarios para su creación</li>
                    <li class="text-danger font-weight-bold">Se han encontrado (<span id="errorCol">0</span>) errores, por favor arreglelos o eliminelos</li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col table-responsive">
            <table class="w-100" id="example">
                <thead style="background: #FFFFFF; box-shadow: -3px 3px 6px rgba(0, 0, 0, 0.05);">
                    <tr style="color: #F28D8D;">
                        <th scope="col" style="font-weight: bold;">Fila</th>
                        <th scope="col" style="font-weight: bold;">Email</th>
                        <th scope="col" style="font-weight: bold;">País</th>
                        <th scope="col" style="font-weight: bold;">Tipo</th>
                        <th scope="col" style="font-weight: bold;">Nombre</th>
                        <th scope="col" style="font-weight: bold;">Apellido</th>
                        <th scope="col" style="font-weight: bold;">Dirección</th>
                        <th scope="col" style="font-weight: bold;">Identificación</th>
                        <th scope="col" style="font-weight: bold;">Teléfono</th>
                        <th scope="col" style="font-weight: bold;">Especialidad</th>
                    </tr>
                </thead>
                <tbody>

                    <?php $contador = 0; $gRow = 0; $eCol = 0;  ?>
                    @foreach ($import as $usuario)
                        <?php 
                            $contador++;
                            $e_global = false;
                            //TRUE ES CUANDO HAY ERROR

                            //ERRORES DE NOMBRE
                            if(isset($usuario["nombre"])) 
                                $e_nombre = false;
                            else{ 
                                $e_nombre = true; $eCol++;
                            }

                            //ERRORES DE APELLIDO
                            if(isset($usuario["apellido"]))
                                $e_apellido = false;
                            else{ 
                                $e_apellido = true;  $eCol++;
                            }

                            //ERRORES DE ESPECIALIDAD
                            if(isset($usuario["especialidad"]))
                                $e_especialidad = false;
                            else{ 
                                $e_especialidad = true; $eCol++; 
                            }

                            //ERRORES DE TIPO
                            if(isset($usuario["tipo"])){
                                if ( !in_array(strtolower($usuario["tipo"]), array('profesional','paciente'), true ) ) {
                                    $e_tipo = true; $eCol++; 
                                }else
                                    $e_tipo = false;
                            }
                            else $e_tipo = true;

                            //ERRORES DE TELEFONO
                            if(isset($usuario["telefono"])){
                                if(!is_numeric($usuario["telefono"])){
                                    $e_telefono = true; $eCol++; 
                                }else
                                    $e_telefono = false;
                            }
                            else $e_telefono = true;

                            //ERRORES DE EMAIL
                            if(isset($usuario["email"])){
                                if(!filter_var($usuario["email"], FILTER_VALIDATE_EMAIL)){
                                    if (!preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/', $usuario["email"])){
                                        $e_email = true; $eCol++; 
                                    }else
                                        $e_email = false;
                                }else
                                    $e_email = false;
                            }
                            else $e_email = true;

                            //ERRORES DE PAIS
                            if(isset($usuario["pais"]))
                                $e_pais = false;
                            else{ 
                                $e_pais = true; $eCol++; 
                            }

                            //ERRORES DE DIRECCION
                            if(isset($usuario["direccion"]))
                                $e_direccion = false;
                            else{ 
                                $e_direccion = true; $eCol++; 
                            }

                            //ERRORES DE IDENTIFICACION
                            if(isset($usuario["identificacion"])){
                                if(!is_numeric($usuario["identificacion"])){
                                    $e_identificacion = true; $eCol++; 
                                }else
                                    $e_identificacion = false;
                            }
                            else $e_identificacion = true;

                            //CHEQUEO SI HAY ERROR GLOBAL ENTONCES EN ALGÚN PUNTO
                            if($e_email || $e_nombre || $e_apellido || $e_tipo || $e_especialidad || $e_telefono || $e_pais || $e_direccion || $e_identificacion)
                                $e_global = true;
                            else{
                                $gRow++;
                            }
                        ?>
                        <tr id="row-{{ $contador }}" class="{{ $e_global ? 'bg_rose' : '' }} table-bordered">
                            <td class="text-center">
                                {{ $contador - 1 }}
                            </td>
                            <td id="email-{{ $contador }}" class="{{ $e_email ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["email"]) ? $usuario["email"] : "" }}
                            </td>
                            <td id="pais-{{ $contador }}" class="{{ $e_pais ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["pais"]) ? $usuario["pais"] : "" }}
                            </td>
                            <td id="tipo-{{ $contador }}" class="{{ $e_tipo ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["tipo"]) ? $usuario["tipo"] : "" }}
                            </td>
                            <td id="nombre-{{ $contador }}" class="{{ $e_nombre ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["nombre"]) ? $usuario["nombre"] : "" }}
                            </td>
                            <td id="apellido-{{ $contador }}" class="{{ $e_apellido ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["apellido"]) ? $usuario["apellido"] : "" }}
                            </td>
                            <td id="direccion-{{ $contador }}" class="{{ $e_direccion ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["direccion"]) ? $usuario["direccion"] : "" }}
                            </td>
                            <td id="identificacion-{{ $contador }}" class="{{ $e_identificacion ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["identificacion"]) ? $usuario["identificacion"] : "" }}
                            </td>
                            <td id="telefono-{{ $contador }}" class="{{ $e_telefono ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["telefono"]) ? $usuario["telefono"] : "" }}
                            </td>
                            <td id="especialidad-{{ $contador }}" class="{{ $e_especialidad ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["especialidad"]) ? $usuario["especialidad"] : "" }}
                            </td>
                            <td class="text-center">
                                @if($e_global)
                                    <a id="eIcon-{{ $contador }}">
                                        <i class="fas fa-exclamation-triangle text-danger"></i>
                                    </a>
                                @endif
                                <a id="{{ $contador }}" class="editRow" data-toggle="modal" data-target=".bd-example-modal-lg-1">
                                    <i class="fas fa-edit mx-3" style="color: #F28D8D"></i>
                                </a>
                                <a id="{{ $contador }}" class="deleteRow">
                                    <i class="fas fa-trash text-secondary"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- FIN SECCIÓN 2 -->

<!-- MODAL DE EDITAR USUARIOS -->
<div class="modal fade bd-example-modal-lg-1" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                    Editar Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" id="formEdit">
                    <div class="form-row">
                        <div class="col">
                            <label for="">Correo Electrónico:</label>
                            <input id="editEmail" type="email" class="form-control" placeholder="Correo electrónico*" required>
                        </div>
                        <div class="col">
                            <label for="">Tipo de Usuario:</label>
                            <select id="editTipo" class="form-control" required>
                                <option value="" selected disabled>Selecciona un Tipo</option>
                                <option value="paciente">Paciente</option>
                                <option value="profesional">Profesional</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col">
                            <label for="">País del Usuario:</label>
                            <select id="editPais" class="form-control" required>
                                <option value="" selected disabled>Seleccione el País</option>
                                <option value="Afganistán" id="AF">Afganistán</option>
                                <option value="Albania" id="AL">Albania</option>
                                <option value="Alemania" id="DE">Alemania</option>
                                <option value="Andorra" id="AD">Andorra</option>
                                <option value="Angola" id="AO">Angola</option>
                                <option value="Anguila" id="AI">Anguila</option>
                                <option value="Antártida" id="AQ">Antártida</option>
                                <option value="Antigua y Barbuda" id="AG">Antigua y Barbuda</option>
                                <option value="Antillas holandesas" id="AN">Antillas holandesas</option>
                                <option value="Arabia Saudí" id="SA">Arabia Saudí</option>
                                <option value="Argelia" id="DZ">Argelia</option>
                                <option value="Argentina" id="AR">Argentina</option>
                                <option value="Armenia" id="AM">Armenia</option>
                                <option value="Aruba" id="AW">Aruba</option>
                                <option value="Australia" id="AU">Australia</option>
                                <option value="Austria" id="AT">Austria</option>
                                <option value="Azerbaiyán" id="AZ">Azerbaiyán</option>
                                <option value="Bahamas" id="BS">Bahamas</option>
                                <option value="Bahrein" id="BH">Bahrein</option>
                                <option value="Bangladesh" id="BD">Bangladesh</option>
                                <option value="Barbados" id="BB">Barbados</option>
                                <option value="Bélgica" id="BE">Bélgica</option>
                                <option value="Belice" id="BZ">Belice</option>
                                <option value="Benín" id="BJ">Benín</option>
                                <option value="Bermudas" id="BM">Bermudas</option>
                                <option value="Bhután" id="BT">Bhután</option>
                                <option value="Bielorrusia" id="BY">Bielorrusia</option>
                                <option value="Birmania" id="MM">Birmania</option>
                                <option value="Bolivia" id="BO">Bolivia</option>
                                <option value="Bosnia y Herzegovina" id="BA">Bosnia y Herzegovina</option>
                                <option value="Botsuana" id="BW">Botsuana</option>
                                <option value="Brasil" id="BR">Brasil</option>
                                <option value="Brunei" id="BN">Brunei</option>
                                <option value="Bulgaria" id="BG">Bulgaria</option>
                                <option value="Burkina Faso" id="BF">Burkina Faso</option>
                                <option value="Burundi" id="BI">Burundi</option>
                                <option value="Cabo Verde" id="CV">Cabo Verde</option>
                                <option value="Camboya" id="KH">Camboya</option>
                                <option value="Camerún" id="CM">Camerún</option>
                                <option value="Canadá" id="CA">Canadá</option>
                                <option value="Chad" id="TD">Chad</option>
                                <option value="Chile" id="CL">Chile</option>
                                <option value="China" id="CN">China</option>
                                <option value="Chipre" id="CY">Chipre</option>
                                <option value="Ciudad estado del Vaticano" id="VA">Ciudad estado del Vaticano</option>
                                <option value="Colombia" id="CO">Colombia</option>
                                <option value="Comores" id="KM">Comores</option>
                                <option value="Congo" id="CG">Congo</option>
                                <option value="Corea" id="KR">Corea</option>
                                <option value="Corea del Norte" id="KP">Corea del Norte</option>
                                <option value="Costa del Marfíl" id="CI">Costa del Marfíl</option>
                                <option value="Costa Rica" id="CR">Costa Rica</option>
                                <option value="Croacia" id="HR">Croacia</option>
                                <option value="Cuba" id="CU">Cuba</option>
                                <option value="Dinamarca" id="DK">Dinamarca</option>
                                <option value="Djibouri" id="DJ">Djibouri</option>
                                <option value="Dominica" id="DM">Dominica</option>
                                <option value="Ecuador" id="EC">Ecuador</option>
                                <option value="Egipto" id="EG">Egipto</option>
                                <option value="El Salvador" id="SV">El Salvador</option>
                                <option value="Emiratos Arabes Unidos" id="AE">Emiratos Arabes Unidos</option>
                                <option value="Eritrea" id="ER">Eritrea</option>
                                <option value="Eslovaquia" id="SK">Eslovaquia</option>
                                <option value="Eslovenia" id="SI">Eslovenia</option>
                                <option value="España" id="ES">España</option>
                                <option value="Estados Unidos" id="US">Estados Unidos</option>
                                <option value="Estonia" id="EE">Estonia</option>
                                <option value="c" id="ET">Etiopía</option>
                                <option value="Ex-República Yugoslava de Macedonia" id="MK">Ex-República Yugoslava de Macedonia</option>
                                <option value="Filipinas" id="PH">Filipinas</option>
                                <option value="Finlandia" id="FI">Finlandia</option>
                                <option value="Francia" id="FR">Francia</option>
                                <option value="Gabón" id="GA">Gabón</option>
                                <option value="Gambia" id="GM">Gambia</option>
                                <option value="Georgia" id="GE">Georgia</option>
                                <option value="Georgia del Sur y las islas Sandwich del Sur" id="GS">Georgia del Sur y las islas Sandwich del Sur</option>
                                <option value="Ghana" id="GH">Ghana</option>
                                <option value="Gibraltar" id="GI">Gibraltar</option>
                                <option value="Granada" id="GD">Granada</option>
                                <option value="Grecia" id="GR">Grecia</option>
                                <option value="Groenlandia" id="GL">Groenlandia</option>
                                <option value="Guadalupe" id="GP">Guadalupe</option>
                                <option value="Guam" id="GU">Guam</option>
                                <option value="Guatemala" id="GT">Guatemala</option>
                                <option value="Guayana" id="GY">Guayana</option>
                                <option value="Guayana francesa" id="GF">Guayana francesa</option>
                                <option value="Guinea" id="GN">Guinea</option>
                                <option value="Guinea Ecuatorial" id="GQ">Guinea Ecuatorial</option>
                                <option value="Guinea-Bissau" id="GW">Guinea-Bissau</option>
                                <option value="Haití" id="HT">Haití</option>
                                <option value="Holanda" id="NL">Holanda</option>
                                <option value="Honduras" id="HN">Honduras</option>
                                <option value="Hong Kong R. A. E" id="HK">Hong Kong R. A. E</option>
                                <option value="Hungría" id="HU">Hungría</option>
                                <option value="India" id="IN">India</option>
                                <option value="Indonesia" id="ID">Indonesia</option>
                                <option value="Irak" id="IQ">Irak</option>
                                <option value="Irán" id="IR">Irán</option>
                                <option value="Irlanda" id="IE">Irlanda</option>
                                <option value="Isla Bouvet" id="BV">Isla Bouvet</option>
                                <option value="Isla Christmas" id="CX">Isla Christmas</option>
                                <option value="Isla Heard e Islas McDonald" id="HM">Isla Heard e Islas McDonald</option>
                                <option value="Islandia" id="IS">Islandia</option>
                                <option value="Islas Caimán" id="KY">Islas Caimán</option>
                                <option value="Islas Cook" id="CK">Islas Cook</option>
                                <option value="Islas de Cocos o Keeling" id="CC">Islas de Cocos o Keeling</option>
                                <option value="Islas Faroe" id="FO">Islas Faroe</option>
                                <option value="Islas Fiyi" id="FJ">Islas Fiyi</option>
                                <option value="Islas Malvinas Islas Falkland" id="FK">Islas Malvinas Islas Falkland</option>
                                <option value="Islas Marianas del norte" id="MP">Islas Marianas del norte</option>
                                <option value="Islas Marshall" id="MH">Islas Marshall</option>
                                <option value="Islas menores de Estados Unidos" id="UM">Islas menores de Estados Unidos</option>
                                <option value="Islas Palau" id="PW">Islas Palau</option>
                                <option value="Islas Salomón" d="SB">Islas Salomón</option>
                                <option value="Islas Tokelau" id="TK">Islas Tokelau</option>
                                <option value="Islas Turks y Caicos" id="TC">Islas Turks y Caicos</option>
                                <option value="Islas Vírgenes EE.UU." id="VI">Islas Vírgenes EE.UU.</option>
                                <option value="Islas Vírgenes Reino Unido" id="VG">Islas Vírgenes Reino Unido</option>
                                <option value="Israel" id="IL">Israel</option>
                                <option value="Italia" id="IT">Italia</option>
                                <option value="Jamaica" id="JM">Jamaica</option>
                                <option value="Japón" id="JP">Japón</option>
                                <option value="Jordania" id="JO">Jordania</option>
                                <option value="Kazajistán" id="KZ">Kazajistán</option>
                                <option value="Kenia" id="KE">Kenia</option>
                                <option value="Kirguizistán" id="KG">Kirguizistán</option>
                                <option value="Kiribati" id="KI">Kiribati</option>
                                <option value="Kuwait" id="KW">Kuwait</option>
                                <option value="Laos" id="LA">Laos</option>
                                <option value="Lesoto" id="LS">Lesoto</option>
                                <option value="Letonia" id="LV">Letonia</option>
                                <option value="Líbano" id="LB">Líbano</option>
                                <option value="Liberia" id="LR">Liberia</option>
                                <option value="Libia" id="LY">Libia</option>
                                <option value="Liechtenstein" id="LI">Liechtenstein</option>
                                <option value="Lituania" id="LT">Lituania</option>
                                <option value="Luxemburgo" id="LU">Luxemburgo</option>
                                <option value="Macao R. A. E" id="MO">Macao R. A. E</option>
                                <option value="Madagascar" id="MG">Madagascar</option>
                                <option value="Malasia" id="MY">Malasia</option>
                                <option value="Malawi" id="MW">Malawi</option>
                                <option value="Maldivas" id="MV">Maldivas</option>
                                <option value="Malí" id="ML">Malí</option>
                                <option value="Malta" id="MT">Malta</option>
                                <option value="Marruecos" id="MA">Marruecos</option>
                                <option value="Martinica" id="MQ">Martinica</option>
                                <option value="Mauricio" id="MU">Mauricio</option>
                                <option value="Mauritania" id="MR">Mauritania</option>
                                <option value="Mayotte" id="YT">Mayotte</option>
                                <option value="México" id="MX">México</option>
                                <option value="Micronesia" id="FM">Micronesia</option>
                                <option value="Moldavia" id="MD">Moldavia</option>
                                <option value="Mónaco" id="MC">Mónaco</option>
                                <option value="Mongolia" id="MN">Mongolia</option>
                                <option value="Montserrat" id="MS">Montserrat</option>
                                <option value="Mozambique" id="MZ">Mozambique</option>
                                <option value="Namibia" id="NA">Namibia</option>
                                <option value="Nauru" id="NR">Nauru</option>
                                <option value="Nepal" id="NP">Nepal</option>
                                <option value="Nicaragua" id="NI">Nicaragua</option>
                                <option value="Níger" id="NE">Níger</option>
                                <option value="Nigeria" id="NG">Nigeria</option>
                                <option value="Niue" id="NU">Niue</option>
                                <option value="Norfolk" id="NF">Norfolk</option>
                                <option value="Noruega" id="NO">Noruega</option>
                                <option value="Nueva Caledonia" id="NC">Nueva Caledonia</option>
                                <option value="Nueva Zelanda" id="NZ">Nueva Zelanda</option>
                                <option value="Omán" id="OM">Omán</option>
                                <option value="Panamá" id="PA">Panamá</option>
                                <option value="Papua Nueva Guinea" id="PG">Papua Nueva Guinea</option>
                                <option value="Paquistán" id="PK">Paquistán</option>
                                <option value="Paraguay" id="PY">Paraguay</option>
                                <option value="Perú" id="PE">Perú</option>
                                <option value="Pitcairn" id="PN">Pitcairn</option>
                                <option value="Polinesia francesa" id="PF">Polinesia francesa</option>
                                <option value="Polonia" id="PL">Polonia</option>
                                <option value="Portugal" id="PT">Portugal</option>
                                <option value="Puerto Rico" id="PR">Puerto Rico</option>
                                <option value="Qatar" id="QA">Qatar</option>
                                <option value="Reino Unido" id="UK">Reino Unido</option>
                                <option value="República Centroafricana" id="CF">República Centroafricana</option>
                                <option value="República Checa" id="CZ">República Checa</option>
                                <option value="República de Sudáfrica" id="ZA">República de Sudáfrica</option>
                                <option value="República Democrática del Congo Zaire" id="CD">República Democrática del Congo Zaire</option>
                                <option value="República Dominicana" id="DO">República Dominicana</option>
                                <option value="Reunión" id="RE">Reunión</option>
                                <option value="Ruanda" id="RW">Ruanda</option>
                                <option value="Rumania" id="RO">Rumania</option>
                                <option value="Rusia" id="RU">Rusia</option>
                                <option value="Samoa" id="WS">Samoa</option>
                                <option value="Samoa occidental" id="AS">Samoa occidental</option>
                                <option value="San Kitts y Nevis" id="KN">San Kitts y Nevis</option>
                                <option value="San Marino" id="SM">San Marino</option>
                                <option value="San Pierre y Miquelon" id="PM">San Pierre y Miquelon</option>
                                <option value="San Vicente e Islas Granadinas" id="VC">San Vicente e Islas Granadinas</option>
                                <option value="Santa Helena" id="SH">Santa Helena</option>
                                <option value="Santa Lucía" id="LC">Santa Lucía</option>
                                <option value="Santo Tomé y Príncipe" id="ST">Santo Tomé y Príncipe</option>
                                <option value="Senegal" id="SN">Senegal</option>
                                <option value="Serbia y Montenegro" id="YU">Serbia y Montenegro</option>
                                <option value="Sychelles" id="SC">Seychelles</option>
                                <option value="Sierra Leona" id="SL">Sierra Leona</option>
                                <option value="Singapur" id="SG">Singapur</option>
                                <option value="Siria" id="SY">Siria</option>
                                <option value="Somalia" id="SO">Somalia</option>
                                <option value="Sri Lanka" id="LK">Sri Lanka</option>
                                <option value="Suazilandia" id="SZ">Suazilandia</option>
                                <option value="Sudán" id="SD">Sudán</option>
                                <option value="Suecia" id="SE">Suecia</option>
                                <option value="Suiza" id="CH">Suiza</option>
                                <option value="Surinam" id="SR">Surinam</option>
                                <option value="Svalbard" id="SJ">Svalbard</option>
                                <option value="Tailandia" id="TH">Tailandia</option>
                                <option value="Taiwán" id="TW">Taiwán</option>
                                <option value="Tanzania" id="TZ">Tanzania</option>
                                <option value="Tayikistán" id="TJ">Tayikistán</option>
                                <option value="Territorios británicos del océano Indico" id="IO">Territorios británicos del océano Indico</option>
                                <option value="Territorios franceses del sur" id="TF">Territorios franceses del sur</option>
                                <option value="Timor Oriental" id="TP">Timor Oriental</option>
                                <option value="Togo" id="TG">Togo</option>
                                <option value="Tonga" id="TO">Tonga</option>
                                <option value="Trinidad y Tobago" id="TT">Trinidad y Tobago</option>
                                <option value="Túnez" id="TN">Túnez</option>
                                <option value="Turkmenistán" id="TM">Turkmenistán</option>
                                <option value="Turquía" id="TR">Turquía</option>
                                <option value="Tuvalu" id="TV">Tuvalu</option>
                                <option value="Ucrania" id="UA">Ucrania</option>
                                <option value="Uganda" id="UG">Uganda</option>
                                <option value="Uruguay" id="UY">Uruguay</option>
                                <option value="Uzbekistán" id="UZ">Uzbekistán</option>
                                <option value="Vanuatu" id="VU">Vanuatu</option>
                                <option value="Venezuela" id="VE">Venezuela</option>
                                <option value="Vietnam" id="VN">Vietnam</option>
                                <option value="Wallis y Futuna" id="WF">Wallis y Futuna</option>
                                <option value="Yemen" id="YE">Yemen</option>
                                <option value="Zambia" id="ZM">Zambia</option>
                                <option value="Zimbabue" id="ZW">Zimbabue</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <label for="">Nombre:</label>
                            <input id="editNombre" type="text" class="form-control" placeholder="Nombre*" required>
                        </div>
                        <div class="col">
                            <label for="">Apellido:</label>
                            <input id="editApellido" type="text" class="form-control" placeholder="Apellido*" required>
                        </div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col">
                            <label for="">Dirección:</label>
                            <textarea id="editDireccion" class="form-control" required></textarea>
                        </div>
                        <div class="col">
                            <label for="">Identificación:</label>
                            <input id="editIdentificacion" type="number" min="0" class="form-control  mt-1" placeholder="Cedula del Usuario*" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <label for="">Especialidad:</label>
                            <input id="editEspecialidad" type="text" class="form-control" required>
                        </div>
                        <div class="col">
                            <label for="">Teléfono:</label>
                            <input id="editTelefono" type="number" min="0" class="form-control  mt-1" placeholder="Teléfono del Usuario*" required>
                        </div>
                    </div>
                    <input type="hidden" id="rowID">
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;">EDITAR USUARIO</button>
            </div>
            </form>

        </div>
    </div>
</div>
<!-- FIN MODAL DE EDITAR -->

<!-- PASO SECCIÓN 3 -->
<div id="container_principal_2" style="display: none;">
    <div class="row">
        <div class="col-md-6 ml-auto">
            <div class="row">
                <hr style="position:absolute; border:2px dotted #999999; width:70%; left: 5%; top: 25%" />
                <div class="col row-reverse">
                    <div class="d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; background-color: #51A2A7">
                        <i class="fas fa-check text-white"></i>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px; color: #51A2A7">Agregar<br>usuarios</label>
                    </div>
                </div>
                <div class="col row-reverse ">
                    <div class="d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; background-color: #51A2A7">
                        <i class="fas fa-check text-white"></i>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px; color: #51A2A7">Revision<br>pendiente</label>
                    </div>
                </div>
                <div class="col row-reverse">
                    <div class="d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; background-color: #51A2A7">
                        <label for="" class="font-weight-bold text-white" style="font-size: 20px">3</label>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px;">Creación y<br>estado</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col">
            <div class="row mb-2">
                <div class="col">
                    <label class="float-left">Tome en cuenta que si el usuario es Paciente debe anexar luego sus patologías</label>
                </div>
            </div>
            <table class="table">
                <thead style="background: #FFFFFF; box-shadow: -3px 3px 6px rgba(0, 0, 0, 0.05);">
                    <tr style="color: #F28D8D;">
                        <th scope="col" style="font-weight: bold;">Email</th>
                        <th scope="col" style="font-weight: bold;">País</th>
                        <th scope="col" style="font-weight: bold;">Tipo</th>
                        <th scope="col" style="font-weight: bold;">Nombre</th>
                        <th scope="col" style="font-weight: bold;">Apellido</th>
                        <th scope="col" style="font-weight: bold;">Dirección</th>
                        <th scope="col" style="font-weight: bold;">Identificación</th>
                        <th scope="col" style="font-weight: bold;">Teléfono</th>
                        <th scope="col" style="font-weight: bold;">Especialidad</th>
                    </tr>
                </thead>
                <tbody>
                    <form action="{{ route('user.import.store') }}" method="post" id="formInputs">
                        @csrf
                        <?php $contador = 0; ?>
                        @foreach ($import as $usuario)
                            <?php $contador++; ?>
                            <tr id="rowI-{{ $contador }}" class="table-bordered">
                                <td id="emailI-{{ $contador }}">
                                    <input name="email[]" type="text" class="noborder" readonly value='{{ isset($usuario["email"]) ? $usuario["email"] : "" }}'>
                                </td>
                                <td id="paisI-{{ $contador }}">
                                    <input name="pais[]" type="text" class="noborder" readonly value='{{ isset($usuario["pais"]) ? $usuario["pais"] : "" }}'>
                                </td>
                                <td id="tipoI-{{ $contador }}">
                                    <input name="tipo[]" type="text" class="noborder" readonly value='{{ isset($usuario["tipo"]) ? $usuario["tipo"] : "" }}'>
                                </td>
                                <td id="nombreI-{{ $contador }}">
                                    <input name="nombre[]" type="text" class="noborder" readonly value='{{ isset($usuario["nombre"]) ? $usuario["nombre"] : "" }}'>
                                </td>
                                <td id="apellidoI-{{ $contador }}">
                                    <input name="apellido[]" type="text" class="noborder" readonly value='{{ isset($usuario["apellido"]) ? $usuario["apellido"] : "" }}'>
                                </td>
                                <td id="direccionI-{{ $contador }}">
                                    <input name="direccion[]" type="text" class="noborder" readonly value='{{ isset($usuario["direccion"]) ? $usuario["direccion"] : "" }}'>
                                </td>
                                <td id="identificacionI-{{ $contador }}">
                                    <input name="identificacion[]" type="text" class="noborder" readonly value='{{ isset($usuario["identificacion"]) ? $usuario["identificacion"] : "" }}'>
                                </td>
                                <td id="telefonoI-{{ $contador }}">
                                    <input name="telefono[]" type="text" class="noborder" readonly value='{{ isset($usuario["telefono"]) ? $usuario["telefono"] : "" }}'>
                                </td>
                                <td id="especialidadI-{{ $contador }}">
                                    <input name="especialidad[]" type="text" class="noborder" readonly value='{{ isset($usuario["especialidad"]) ? $usuario["especialidad"] : "" }}'>
                                </td>
                            </tr>
                        @endforeach
                    </form>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- FIN PASO DE SECCIÓN 3 -->

<div class="row mt-5">
    <div class="col-md-12">
        <hr style="height:2px;border-width:0;color:gray;background-color:#F28D8D">
    </div>
</div>

<!-- BOTONES PARA CAMBIOS DE SECCIÓN + ENVIO -->
<div class="row ">
    <div class="col d-flex justify-content-end">
        <button {{ $eCol > 0 ? "disabled" : "" }} class="btn btn-round" id="confirmar" onclick="container_principal();" style="background-color: #51A2A7">
            CONFIRMAR
        </button>
        <a href="javascript:;" onclick="sweetcheck()" id="aceptar" class="btn btn-round" style="background-color: #51A2A7; display: none">
            ACEPTAR Y PUBLICAR
        </a>
    </div>
</div>
<!-- FIN BOTONES -->

<input type="hidden" id="numRows" value="{{ $gRow }}">
<input type="hidden" id="numCols" value="{{ $eCol }}">

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<!-- SCRIPT DE CARGA INICIAL DEL DOM -->
<script>
    $(document).ready(function() {
        //SETTEO LOS VALORES DE BIEN Y ERRORES
        var rows = $('#numRows').val(); $('#goodRows').text(rows);
        var cols = $('#numCols').val(); $('#errorCol').text(cols);
        
        //SECCIÓN DE EDITAR FILAS
            //abrir modal
            $('.editRow').click(function(){
                var id = $(this).attr('id');
                
                //Recogo los Valores de la Tabla
                var email = $('#email-'+id).text().trim();
                var pais = $('#pais-'+id).text().trim();
                var tipo = $('#tipo-'+id).text().trim();
                var nombre = $('#nombre-'+id).text().trim();
                var apellido = $('#apellido-'+id).text().trim();
                var direccion = $('#direccion-'+id).text().trim();
                var identificacion = parseInt($('#identificacion-'+id).text().trim());
                var telefono = parseInt($('#telefono-'+id).text().trim());
                var especialidad = $('#especialidad-'+id).text().trim();

                //Setteo los valores en el modal
                $('#rowID').val(id);
                $('#editEmail').val(email);
                $('#editPais').val(pais);
                $('#editTipo').val(tipo);
                $('#editNombre').val(nombre);
                $('#editApellido').val(apellido);
                $('#editDireccion').val(direccion);
                $('#editIdentificacion').val(identificacion);
                $('#editTelefono').val(telefono);
                $('#editEspecialidad').val(especialidad);
            });

            //Submit de Form
            $( "#formEdit" ).on('submit', function(e){
                e.preventDefault();

                //Cogemos los datos
                var id = $('#rowID').val();
                var email = $('#editEmail').val();
                var pais = $('#editPais').val();
                var tipo = $('#editTipo').val();
                var nombre = $('#editNombre').val();
                var apellido = $('#editApellido').val();
                var direccion = $('#editDireccion').val();
                var identificacion = $('#editIdentificacion').val();
                var telefono = $('#editTelefono').val();
                var especialidad = $('#editEspecialidad').val();

                //Corregimos los datos el td y el tr
                $('#email-'+id).text(email); $('#emailI-'+id+' input').val(email);
                $('#pais-'+id).text(pais); $('#paisI-'+id+' input').val(pais);
                $('#tipo-'+id).text(tipo); $('#tipoI-'+id+' input').val(tipo);
                $('#nombre-'+id).text(nombre); $('#nombreI-'+id+' input').val(nombre);
                $('#apellido-'+id).text(apellido); $('#apellidoI-'+id+' input').val(apellido);
                $('#direccion-'+id).text(direccion); $('#direccionI-'+id+' input').val(direccion);
                $('#identificacion-'+id).text(identificacion); $('#identificacionI-'+id+' input').val(identificacion);
                $('#telefono-'+id).text(telefono); $('#telefonoI-'+id+' input').val(telefono);
                $('#especialidad-'+id).text(especialidad); $('#especialidadI-'+id+' input').val(especialidad);

                $('#row-'+id).removeClass('bg_rose');
                $('#eIcon-'+id).remove();
                //$('#row-'+id+' td').removeClass('text-danger font-weight-bold');
                var error = 0;
                $('#row-'+id+' td').each(function(indice,elemento) {
                    $(elemento).hasClass('text-danger') ? error++ : "";
                    $(elemento).removeClass('text-danger font-weight-bold');
                });
                //Cerramos el Modal
                $('#editModal').modal('hide');

                //Arreglamos el número de errores por resolver
                rows++; cols -= error;
                $('#goodRows').text(rows<0 ? 0 : rows);
                $('#errorCol').text(cols<0 ? 0 : cols);

                if(cols < 1){
                    $('#confirmar').removeAttr('disabled');
                }
            });

        //SECCIÓN DE ELIMINAR FILA
            //Elimino fila con el click de la papelera
            $('.deleteRow').click(function(){
                var id = $(this).attr('id');

                Swal.fire({
                    icon: 'warning',
                    title: '¿Esta seguro de borrar este usuario?',
                    //type: 'warning',
                    confirmButtonText: 'Si, Eliminar!'
                }).then((result) => {
                    if (result.value) {
                        var error = 0;
                        $('#row-'+id+' td').each(function(indice,elemento) {
                            $(elemento).hasClass('text-danger') ? error++ : "";
                            $(elemento).removeClass('text-danger font-weight-bold');
                        });

                        Swal.fire({
                            title: 'Usuario a Importar Eliminado!',
                            icon: 'success',
                        });

                        cols -= error;
                        $('#errorCol').text(cols<0 ? 0 : cols);

                        if(cols < 1){
                            $('#confirmar').removeAttr('disabled');
                        }

                        $('#row-'+id).remove();
                        $('#rowI-'+id).remove();
                    }
                });
            });
    });
</script>

<!-- FUNCION UE PASA DE ESTADO 2 A 3 -->
<script>
    function container_principal() {
        $("#container_principal").css({
            'display': 'none'
        })
        $("#container_principal_2").css({
            'display': 'block'
        })
        $("#confirmar").css({
            'display': 'none'
        })
        $("#aceptar").css({
            'display': 'block'
        })
    }
</script>

<!-- ALERTAS SWEET DE ELIMINAR Y DE ACEPTAR CARGAR TODO -->
<script>
    function sweetcheck() {
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Los usuarios se estan subiendo',
            showConfirmButton: false,
        });
        $('#formInputs').submit();
    }
</script>
@endsection