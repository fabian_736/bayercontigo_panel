@extends('layouts.admin.app')

@section('content')
    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        @if (Session::has('edit'))
            <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                {{ Session::get('edit') }}
            </div>
        @endif

        <div class="row mb-5 mx-auto">
            <!-- Editar Muro -->
            <div class="col-md-12 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">

                <div class="card-header mt-3" style="background-color: #e7344c;">
                    <h5 class="text-white">Editar Muro</h5>
                </div>


                <form action="{{ route('muro.update',$muro->id_muro) }}" method="post" enctype="multipart/form-data">

                    @csrf

                    <input type="hidden" name="tipo" value="{{ $muro->tipo }}">
                    <input type="hidden" name="FK_id_useradmin" value="{{ $muro->FK_id_useradmin }}">

                    <div class="row col-md-12 my-4">
                        <div class="col-12">
                            <img class="img-​thumbnail rounded-circle" style="width:40px; height:40px"
                            src="{{ route('admin.icon', ['filename' => basename($muro->creador->avatar)]) }}">
                            <span style="color: #F28D8D; font-weight: bold">{{ $muro->creador->nombre.' '.$muro->creador->apellido }}</span>
                        </div>
                        <div class="col-12 text-dark">
                            <span style="font-size: 12px">{{ date_format(date_create($muro->created_at), 'j F') }}</span>
                        </div>
                    </div>

                    <div class="row col-md-12 mb-3">
                        <div class="col">
                            <label for="name_user">Contenido:</label>
                            <textarea class="form-control @error('contenido') is-invalid @enderror" 
                            name="contenido" id="" type="text" >{{ old('contenido', $muro->contenido) }}</textarea>
                            @error('contenido')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 mb-3">
                        <div class="col-6">
                            <div class="col">
                                <label for="">{{ !$muro->imagen ? "Agregar" : "Editar"}} Imagen del Muro (Opcional):</label>
                                <div class="custom-file mb-3">
                                    <input type="file" class="custom-file-input @error('imagen') is-invalid @enderror" 
                                    id="customImage" name="imagen" accept=".jpg,.jpeg,.png" value="">
                                    <label class="custom-file-label" for="customImage">Escoger una Imagen</label>
                                    @error('imagen')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            @if($muro->imagen)
                                <div class="col">
                                    <label for="">Eliminar Imagen:</label><br>
                                    <div class="form-check">
                                        <input value="1" class="" type="radio" name="erase" id="flexRadioDefault1">
                                        <label class="form-check-label" for="flexRadioDefault1">
                                            Si
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input value="0" class="" type="radio" name="erase" id="flexRadioDefault2" checked>
                                        <label class="form-check-label" for="flexRadioDefault2">
                                            No
                                        </label>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <label for="">{{ $muro->imagen ? "Imagen Actual" : "Preview de Imagen"}}:</label>
                            <img class="img w-100" id="previewImage"
                            src="{{ route('muro.image', ['filename' => basename($muro->imagen)]) }}">
                        </div>
                    </div>

                    <hr>
                    <div class="row col-md-12 my-3">
                        <div class="col-md-6 mx-auto">
                            <button type="submit" class="form-control text-white" style="background-color: #64C2C8">Editar Muro</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }

        //Para enviar el preview de la imagen al cargar
        function readImage(input) {
            var url = input.value;
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

            if (input.files && input.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#previewImage').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            } else {
                $('#previewImage').attr('src', null);
                input.value = null;
            }
        }

        $("#customImage").change(function() {
            readImage(this);
        });
    });
</script>