@extends('layouts.admin.app')

@section('content')

@if (Session::has('error'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('error') }}
    </div>
@endif

@if (Session::has('create'))
    <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('create') }}
    </div>
@endif

@if (Session::has('delete'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('delete') }}
    </div>
@endif

@if (Session::has('edit'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        {{ Session::get('edit') }}
    </div>
@endif

<a href="{{ route('muro.list') }}">
    <button class="btn btn-round d-block" style="background-color: #F28D8D">
        VER OTROS MUROS
    </button>
</a>

<label for="" class="lead" style="font-weight: bold; margin-top: 5%; color: #F28D8D">Mis Muros de Blog</label>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive shadow p-3 mb-5 rounded">
            <table id="example" class="table table-striped " cellspacing="0" width="100%">
                <thead style="background-color: #F28D8D; color: #fff">
                    <tr>
                        <th>Fecha</th>
                        <th>Comentario</th>
                        <th>Imagen</th>
                        <th>Likes</th>
                        <th>Creador</th>
                        <th>Comentarios</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody class="table-bordered" style="background-color: #fff; color: #000">
                    @foreach($muros as $muro)
                    <tr>
                        <td>
                            {{ date_format(date_create($muro->created_at), "F j, Y") }}
                        </td>
                        <td style="text-align: justify;">{{ $muro->contenido }}</td>
                        <td class="mx-auto text-center" > 
                            @if($muro->imagen)
                                <img style="width:100px;max-height:100px;" 
                                src="{{ route('muro.image', ['filename' => basename($muro->imagen)]) }}" >
                            @else
                                No posee
                            @endif
                        </td>
                        <td>
                            <?php $user_like = false; ?>
                            @foreach($muro->likes as $like)
                                @if($like->FK_id_useradmin == Auth::user()->id_useradmin)
                                    <?php $user_like = true; ?>
                                @endif
                            @endforeach

                            @if($user_like)
                                <i id="{{$muro->id_muro}}" class="fas fa-heart mx-1 my-auto text-danger btn-dislike" 
                                    style="font-size: 20px;"></i>
                            @else
                                <i id="{{$muro->id_muro}}" class="fas fa-heart mx-1 my-auto text-secondary btn-like" 
                                    style="font-size: 20px;"></i>
                            @endif
                            <span id="heartCount{{$muro->id_muro}}" class="font-weight-bold">{{ $muro->likes->count() }}</span>
                        </td>
                        <td>{{ $muro->creador->nombre.' '.$muro->creador->apellido }}</td>
                        <td>
                            <a href="{{ route('muro.comments', ['id'=>$muro->id_muro]) }}">
                                <button class="btn btn-sm" style="background-color: #F28D8D;">
                                    {{ $muro->comments->count() }} Comentarios
                                </button>
                            </a>
                        </td>
                        <td class="d-flex justify-content-around">
                            <a href="{{route('muro.edit',$muro->id_muro)}}" style="color: #F28D8D;"> <i class="far fa-edit"></i></a>
                            <a href="{{route('muro.confirm',$muro->id_muro)}}" style="color: #F28D8D;"><i class="far fa-trash-alt"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <button class="btn btn-round" style="background-color: #F28D8D" data-toggle="modal"
                data-target=".bd-example-modal-lg">
                <i class="material-icons">add</i>CREAR MURO
            </button>
        </div>
    </div>
</div>

<!-- MODAL CREAR NOTICIA -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #F28D8D; font-weight: bold">
                    Creacion de Muro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('muro.store') }}" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="tipo" value="admin">
                    <input type="hidden" name="FK_id_useradmin" value="{{auth()->user()->id_useradmin}}">

                    <div class="form-row">
                        <div class="col">
                            <label for="name_user">Contenido:</label>
                            <textarea class="form-control @error('contenido') is-invalid @enderror" 
                            name="contenido" id="" type="text" >{{ old('contenido') }}</textarea>
                            @error('contenido')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col">
                            <label for="">Agregar Imagen del Muro (Opcional):</label>
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input @error('imagen') is-invalid @enderror" 
                                id="customImage" name="imagen" accept=".jpg,.jpeg,.png" value="">
                                <label class="custom-file-label" for="customImage">Escoger una Imagen</label>
                                @error('imagen')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="d-none" id="eraseImage">
                                <label style="cursor: pointer;">
                                    Quitar Imagen 
                                    <i class="fas fa-click" style="font-size: 20px; color: #F28D8D"></i>
                                </label>
                            </div>
                        </div>
                        <div class="col">
                            <img class="img w-100" id="previewImage"
                            src="{{ route('muro.image', ['filename' => basename(null)]) }}">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input class="btn mx-auto col-md-6" style="background-color: #F28D8D; border-radius: 40px;"
                        type="submit" value="CREAR MURO">
                </div>
            </form>
        </div>
    </div>
</div>

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
<!-- <input type="hidden" id="URLPATH" value="{{ public_path().'/informativo/muro' }}"> -->
<input type="hidden" id="URLPATH" value="{{ route('muro.like.url') }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script>
    $(document).ready(function() {
        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
            $('#exampleModal').modal('show');
        }

        //Para enviar el preview de la imagen al cargar
        function readImage(input) {
            var url = input.value;
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

            if (input.files && input.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#previewImage').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);

                $('#eraseImage').removeClass('d-none');
            } else {
                $('#previewImage').attr('src', null);
                input.value = null;
                $('#eraseImage').addClass('d-none');
            }
        }

        $("#customImage").change(function() {
            readImage(this);
        });

        $('#eraseImage').click(function (){
            $('#previewImage').attr('src', null);
            $("#customImage").val(null);
            $(this).addClass('d-none');
        });
    });
</script>