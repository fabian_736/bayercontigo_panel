@extends('layouts.admin.app')
@section('content')
    <div class="container py-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        <h1 class="mb-5">¿Deseas eliminar el siguiente muro? </h1>
        <div class="col-12">
            <img class="img-​thumbnail rounded-circle" style="width:40px; height:40px"
            src="{{ route('admin.icon', ['filename' => basename($muro->creador->avatar)]) }}">
            <span style="color: #F28D8D; font-weight: bold">{{ $muro->creador->nombre.' '.$muro->creador->apellido }}</span>
        </div>
        <div class="col-12 text-dark">
            <span style="font-size: 12px">{{ date_format(date_create($muro->created_at), 'j F') }}</span>
        </div>
        <h4>Contenido: </h4>
        <p>{{ $muro->contenido }}</p>
        @if($muro->imagen)
            <h4>Imagen Preview: </h4>
            <p><img style="width:200px;max-height:200px;" src="{{ route('muro.image', ['filename' => basename($muro->imagen)]) }}" ></p>
        @endif

        <form method="post" enctype="multipart/form-data" action="{{ route('muro.destroy', $muro->id_muro) }}">
            @method('DELETE')
            @csrf
            <div class="row col-md-12 mt-5 mx-auto">
                <button type="submit" class="redondo btn btn-danger col-md-4 mx-auto">
                    <i class="fas fa-trash-alt"></i> Eliminar
                </button>
                <a href="{{route('muro.index')}}" class="btn btn-primary col-md-4 mx-auto">Volver</a>
            </div>
        </form>
    </div>
@endsection
