<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/x-icon" href="{{url('img/favicon.ico')}}" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>


    <title>Bayer Contigo</title>

    <link rel="stylesheet" href="{{url('css/app.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css"  href="{{url('DataTables/DataTables-1.10.24/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('DataTables/Buttons-1.7.0/css/buttons.bootstrap4.min.css')}}">

</head>

<body id="page-top">
<div class="row">
    <div class="modal-dialog modal-lg w-100">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        @if (Session::has('create'))
            <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('create') }}
            </div>
        @endif

        @if (Session::has('delete'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('delete') }}
            </div>
        @endif

        @if (Session::has('edit'))
            <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                {{ Session::get('edit') }}
            </div>
        @endif

        <div class="modal-content">
            <div class="modal-header row container">
                <div class="col-12"> 
                    @if($muro->FK_id_useradmin == auth()->user()->id_useradmin)
                        <a href="{{ route('muro.index') }}">
                            <button class="btn btn-round text-white mb-2" style="background-color: #F28D8D">
                                Volver a Mis Muros
                            </button>
                        </a>
                    @else
                        <a href="{{ route('muro.list') }}">
                            <button class="btn btn-round text-white mb-2" style="background-color: #F28D8D">
                                Volver a Muros
                            </button>
                        </a>
                    @endif
                </div>

                <div class="col-12"> 
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        Comentarios
                    </h5>
                </div>
            </div>
            <div class="modal-body">
                <div class="col mb-3 d-block">
                    <div class="col">
                        <button class="btn btn-sm" style="color: #F28D8D;" id="addcoment">
                            Añadir Comentario <i class="far fa-plus ml-auto"></i>
                        </button>
                    </div>
                </div>
                
                <!-- ESPACIO DE AGREGAR COMENTARIO -->
                    <div class="col" style="display: none;" id="commentform">
                        <div class="col">
                            <span style="font-size: 12px">Escribe tu comentario en el recuadro de abajo (máximo 255 cáracteres).</span>
                            <br>
                            <form method="post" enctype="multipart/form-data" 
                                action="{{ route('muro.comment.store', $muro->id_muro) }}">
                                @csrf

                                <div class="form-row my-3">
                                    <div class="col">
                                        <div class="custom-file mb-3">
                                            <input type="text" class="form-control @error('comentario') is-invalid @enderror" 
                                                    required name="comentario">
                                            @error('comentario')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col">
                                        <button class="btn btn-sm" style="color: #F28D8D;" type="submit">
                                            Enviar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <!-- FIN ESPACIO DE AGREGAR COMENTARIO -->

                @foreach($muro->comments as $comentario)
                    <div class="col">
                        <div class="col">
                            @if($comentario->creadorUser)
                                <img class="img-​thumbnail rounded-circle" style="width:40px; height:40px"
                                src="{{ route('user.icon', ['filename' => basename($comentario->creadorUser->avatar)]) }}">
                                <span style="color: #F28D8D; font-weight: bold">
                                    {{ $comentario->creadorUser->paciente ? 
                                    $comentario->creadorUser->paciente->nombre.' '.$comentario->creadorUser->paciente->apellido :
                                    $comentario->creadorUser->profesional->nombre.' '.$comentario->creadorUser->profesional->apellido  }}
                                </span>
                            @else
                                <img class="img-​thumbnail rounded-circle" style="width:40px; height:40px"
                                src="{{ route('admin.icon', ['filename' => basename($comentario->creadorAdmin->avatar)]) }}">
                                <span style="color: #F28D8D; font-weight: bold">
                                    {{ $comentario->creadorAdmin->nombre.' '.$comentario->creadorAdmin->apellido }}
                                </span>
                            @endif
                        </div>
                        <div class="col">
                            <span style="font-size: 12px">{{ date_format(date_create($comentario->created_at), 'j F') }}</span>
                            <br>
                            <form method="post" enctype="multipart/form-data" 
                                action="{{ route('muro.comment.delete', $comentario->id_muro_comentario) }}">
                                @csrf
                                @if($muro->FK_id_useradmin == auth()->user()->id_useradmin || $comentario->FK_id_useradmin == auth()->user()->id_useradmin)        
                                    <button class="btn btn-sm" style="color: #F28D8D;" type="submit">
                                        Eliminar <i class="far fa-trash-alt ml-auto"></i>
                                    </button>
                                @endif
                            </form>
                        </div>
                        <div class="col">
                            <p style="text-align: justify;">{{ $comentario->comentario }}</p>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
</div>
</body>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
            $('#exampleModal').modal('show');
        }

        //Sacar el display de agregar comentario
        $("#addcoment").click(function(){
            var texto = $(this).text();

            if(texto == "Cerrar Comentario")
                $(this).text( 'Añadir Comentario +' );
            else
                $(this).text( "Cerrar Comentario" );

            $("#commentform").slideToggle(400);
        });
    });
</script>