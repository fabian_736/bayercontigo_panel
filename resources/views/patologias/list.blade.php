@extends('layouts.admin.app')

@section('content')

@if (Session::has('create'))
    <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('create') }}
    </div>
@endif

@if (Session::has('delete'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('delete') }}
    </div>
@endif

@if (Session::has('edit'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        {{ Session::get('edit') }}
    </div>
@endif

@if (Session::has('errors'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        Fallo en los datos enviados, por favor abre nuevamente el modal y corrige los errores que hayas realizado
    </div>
@endif

<div class="row mb-3 mx-auto" style="display: flex; align-items: center; justify-content: space-between !important">
    <div class="col">
        <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 2%; color: #51A2A7">
            Patologías dentro del Sistema
        </label>
    </div>
    <div class="col" style="display: flex;justify-content: flex-end !important">

        <button class="btn btn-round" style="background-color: #51A2A7" data-toggle="modal" data-target="#storeModal">
            <i class="material-icons">add</i>AGREGAR PATOLOGÍA
        </button>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table id="example" class="table table-striped " cellspacing="0" width="100%">
                <thead style="background-color: #FFFFFF; color: #51A2A7; font-weight: bold">
                    <tr>
                        <th class="font-weight-bold">Código</th>
                        <th class="font-weight-bold">Patología</th>
                        <th class="font-weight-bold">Número de Pacientes</th>
                        <th class="font-weight-bold">Número de Entrenamientos</th>
                        <th class="font-weight-bold">Ver Pacientes</th>
                        <th class="font-weight-bold">Fecha de Creación</th>
                        <th class="font-weight-bold">Acciones</th>
                    </tr>
                </thead>
                <tbody class="table-bordered" style="background-color: #fff; color: #000"> 
                    @foreach ($patologias as $patologia)
                        <tr class="text-center" id="row{{ $patologia->id_patologia }}">
                            <td>{{ $patologia->id_patologia }}</td>
                            <td id="nom{{ $patologia->id_patologia }}">{{ $patologia->nombre }}</td>
                            <td>
                                {{ $patologia->pacientes->count() }} 
                                <i class="fas fa-users" style="color: #51A2A7"></i>
                            </td>
                            <td>
                                {{ $patologia->capacitaciones->count() }} 
                                <i class="fas fa-clipboard" style="color: #51A2A7"></i>
                            </td>
                            <td>
                                <a id="{{ route('patologia.detail', ['id'=>$patologia->id_patologia]) }}"
                                    class="btn btn-sm btn-danger mx-auto openPacients text-white">
                                    ABRIR <i class="fas fa-eye"></i>
                                </a>
                            </td>
                            <td>{{ date_format(date_create($patologia->created_at), "F j, Y") }}</td>
                            <td class="text-center">
                                <a id="{{ $patologia->id_patologia }}" class="editRow" data-toggle="modal" data-target="#editModal" style="color: #51A2A7; cursor: pointer">
                                    <i class="far fa-edit"></i>
                                </a>
                                <a id="{{ $patologia->id_patologia }}" style="color: #51A2A7;  cursor: pointer" class="deleteRow">
                                    <i class="far fa-trash-alt"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- MODAL DE AGREGAR PATOLOGÍA -->
<div class="modal fade" id="storeModal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                    Añadir Patología</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="{{ route('patologia.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="col">
                        <input name="nombre" id="" type="text" placeholder="Nombre de la Patología Nueva..." value="{{ old('nombre') }}"
                            class="form-control @error('nombre') is-invalid @enderror" required>
                        @error('nombre')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;">
                CREAR PATOLOGÍA
            </button>
        </div>
            </form>
        </div>
    </div>
</div>
<!-- FIN MODAL -->

<!-- MODAL DE EDITAR PATOLOGÍA -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                    Edición de Patología - <span id="editCOD"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('patologia.update') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row">
                        <div class="col">
                            <input name="patologiaUD" id="patologiaNOM" type="text" placeholder="Nombre de la Patología..." required
                                value="{{ old('patologiaUD') }}" class="form-control @error('patologiaUD') is-invalid @enderror">
                            @error('patologiaUD')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <input type="hidden" name="id_patologia" id="editID" value="">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;">
                    EDITAR PATOLOGÍA
                </button>
            </div>
                </form>
        </div>
    </div>
</div>
<!-- FIN MODAL DE EDITAR CATEGORIA -->

<!-- MODAL DE VER PACIENTES BAJO DICHA PATOLOGÍA -->
<div class="modal fade" id="viewModal" tabindex="-1" role="dialog"
    aria-labelledby="viewModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                    Pacientes Bajo la Patología <span id="viewPat"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul id="patologiaPersona">
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- FIN MODAL DE EDITAR CATEGORIA -->

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
<input type="hidden" id="urlDelete" value="{{ route('patologia.destroy') }}">

<style>
    .file-input>[type='file'] {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
        z-index: 10;
        cursor: pointer;
    }

    .file-input>.button {
        display: inline-block;
        cursor: pointer;
        background: #eee;
        padding: 4px 12px;
        border-radius: 2px;
        margin-right: 8px;
    }

    .file-input:hover>.button {
        background: dodgerblue;
        color: white;
    }

    .file-input>.label {
        color: #333;
        white-space: nowrap;
        opacity: .3;
    }

    .file-input.-chosen>.label {
        opacity: 1;
    }

</style>

<script>
    // Also see: https://www.quirksmode.org/dom/inputfile.html

    var inputs = document.querySelectorAll('.file-input')

    for (var i = 0, len = inputs.length; i < len; i++) {
        customInput(inputs[i])
    }

    function customInput(el) {
        const fileInput = el.querySelector('[type="file"]')
        const label = el.querySelector('[data-js-label]')

        fileInput.onchange =
            fileInput.onmouseout = function() {
                if (!fileInput.value) return

                var value = fileInput.value.replace(/^.*[\\\/]/, '')
                el.className += ' -chosen'
                label.innerText = value
            }
    }

</script>

@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }

        //ABRIR MODAL DE EDITAR Y SETTEAR VALORES
        $('.editRow').click(function(){
            var id = $(this).attr('id');
            
            //Recogo los Valores de la Tabla
            var nombre = $('#nom'+id).text().trim();

            //Setteo los valores en el modal
            $('#editID').val(id);
            $('#patologiaNOM').val(nombre);
            $('#editCOD').text(id);
        });

        //PACIENTES BAJO DICHA PATOLOGIA
        $(".openPacients").click(function(){
            var url = $(this).attr("id");
            
            $.ajax({
                url: url,
                type: 'GET',
                success: function(response){
                    if(response.pacientes){
                        console.log(response.pacientes)
                        /* Swal.fire(
                            response.message,
                            'Presiona el boton para cerrar el modal',
                            'success'
                        ) */
                            
                        //Arreglamos el modal con los pacientes que se hayan leido
                        $('#patologiaPersona').empty();
                        for (let i = 0; i < response.pacientes.length; i++) {
                            html = "<li>" + response.pacientes[i] + "</li>";
                            $('#patologiaPersona').append(html);
                        }

                        $('#viewModal').modal("show");
                    }
                    else{
                        Swal.fire(
                            response.message,
                            'Presiona el boton para cerrar el modal',
                            'warning'
                        )
                    }
                }
            });
        });

        //ELIMINAR EL DATO POST SWAL ALERT
        $('.deleteRow').click(function(){
            var id = $(this).attr('id');
            var url = $('#urlDelete').val();

            Swal.fire({
                icon: 'warning',
                title: '¿Esta seguro de borrar esta patología?',
                text: 'Si algún usuario tiene registrada esta patología sera eliminada de sus registros como tambien sus entrenamiento',
                type: 'warning',
                confirmButtonText: 'Si, Eliminar!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: url+'/'+id,
                        data: {
                            "_token": $("meta[name='csrf-token']").attr("content")
                        },
                        type: 'POST',
                        success: function(response){
                            if(response.delete){
                                $("#row"+id).remove();
                                Swal.fire(
                                    response.message,
                                    'Presiona el boton para cerrar el modal',
                                    'success'
                                )
                            }
                            else{
                                Swal.fire(
                                    response.message,
                                    'Presiona el boton para cerrar el modal',
                                    'warning'
                                )
                            }
                        }
                    });
                }
            });
        });
    });
</script>