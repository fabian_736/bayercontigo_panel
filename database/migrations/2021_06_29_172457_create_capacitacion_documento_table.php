<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapacitacionDocumentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_capacitacion_documento', function (Blueprint $table) {
            Schema::create('bc_capacitacion_documento', function (Blueprint $table) {
                $table->id('id_capacitacion_documento');
                $table->string('nombre', 255);
                $table->string('documento', 255)->nullable();
                $table->string('video', 255)->nullable();
                $table->bigInteger('FK_id_capacitacion')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_capacitacion')->references('id_capacitacion')->on('bc_capacitacion');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_capacitacion_documento');
    }
}
