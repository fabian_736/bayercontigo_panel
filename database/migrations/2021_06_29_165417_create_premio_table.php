<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_premio', function (Blueprint $table) {
            Schema::create('bc_premio', function (Blueprint $table) {
                $table->id('id_premio');
                $table->string('nombre', 255);
                $table->string('marca', 255);
                $table->text('descripcion');
                $table->string('entrega', 255);
                $table->bigInteger('stock');
                $table->bigInteger('puntos')->nullable();
                $table->string('imagen')->nullable();

                //Datos del distribuidor
                $table->string('distribuidor', 255);  //o tien o es N/A
                $table->bigInteger('id_producto_dist')->nullable(); //Código del producto para relacionar
                $table->bigInteger('id_brand_dist')->nullable(); //Código de la marca para relacionar

                //Relación de la catgeoria que tiene
                $table->bigInteger('FK_categoria')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_categoria')->references('id_premio_cat')->on('bc_premio_categoria');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_premio');
    }
}
