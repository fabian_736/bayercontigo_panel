<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapacitacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_capacitacion', function (Blueprint $table) {
            Schema::create('bc_capacitacion', function (Blueprint $table) {
                $table->id('id_capacitacion');
                $table->string('titulo', 255);
                $table->text('descripcion');
                $table->bigInteger('FK_id_patologia')->nullable()->unsigned();
                $table->bigInteger('FK_id_producto')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_patologia')->references('id_patologia')->on('bc_patologia');
                $table->foreign('FK_id_producto')->references('id_producto')->on('bc_producto');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_capacitacion');
    }
}
