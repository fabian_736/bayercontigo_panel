<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMuroLikeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_muro_like', function (Blueprint $table) {
            Schema::create('bc_muro_like', function (Blueprint $table) {
                $table->id('id_muro_like');
                $table->bigInteger('FK_id_user')->nullable()->unsigned();
                $table->bigInteger('FK_id_useradmin')->nullable()->unsigned();
                $table->bigInteger('FK_id_muro')->nullable()->unsigned();
                $table->timestamps();
    
                $table->foreign('FK_id_user')->references('id')->on('users');
                $table->foreign('FK_id_useradmin')->references('id_useradmin')->on('bc_useradmin');
                $table->foreign('FK_id_muro')->references('id_muro')->on('bc_muro');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_muro_like');
    }
}
