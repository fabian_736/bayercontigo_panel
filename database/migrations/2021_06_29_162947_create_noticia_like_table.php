<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticiaLikeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_noticia_like', function (Blueprint $table) {
            Schema::create('bc_noticia_like', function (Blueprint $table) {
                $table->id('id_noticia_like');
                $table->bigInteger('FK_id_user')->nullable()->unsigned();
                $table->bigInteger('FK_id_noticia')->nullable()->unsigned();
                $table->timestamps();
    
                $table->foreign('FK_id_user')->references('id')->on('users');
                $table->foreign('FK_id_noticia')->references('id_noticia')->on('bc_noticia');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_noticia_like');
    }
}
