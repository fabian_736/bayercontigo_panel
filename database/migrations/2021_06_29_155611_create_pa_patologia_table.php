<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaPatologiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_pa_patologia', function (Blueprint $table) {
            Schema::create('bc_pa_patologia', function (Blueprint $table) {
                $table->id('id_pa_patologias');
                $table->bigInteger('FK_id_paciente')->nullable()->unsigned();
                $table->bigInteger('FK_id_patologia')->nullable()->unsigned();
                $table->timestamps();
    
                $table->foreign('FK_id_paciente')->references('id_paciente')->on('bc_paciente');
                $table->foreign('FK_id_patologia')->references('id_patologia')->on('bc_patologia');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_pa_patologia');
    }
}
