<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_paciente', function (Blueprint $table) {
            Schema::create('bc_paciente', function (Blueprint $table) {
                $table->id('id_paciente');
                $table->string('nombre');
                $table->string('apellido');
                $table->string('direccion');
                $table->text('descripcion')->nullable();
                $table->string('telefono');
                $table->string('identificacion')->unique();
                $table->string('especialidad');
                $table->timestamps();
                $table->bigInteger('FK_id_user')->unsigned();

                $table->foreign('FK_id_user')->references('id')->on('users');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_paciente');
    }
}
