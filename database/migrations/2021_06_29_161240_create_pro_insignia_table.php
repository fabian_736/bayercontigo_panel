<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProInsigniaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_pro_insignia', function (Blueprint $table) {
            Schema::create('bc_pro_insignia', function (Blueprint $table) {
                $table->id('id_pro_insignia');
                $table->bigInteger('FK_id_profesional')->nullable()->unsigned();
                $table->bigInteger('FK_id_insignia')->nullable()->unsigned();
                $table->timestamps();
    
                $table->foreign('FK_id_profesional')->references('id_profesional')->on('bc_profesional');
                $table->foreign('FK_id_insignia')->references('id_insignia')->on('bc_insignias');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_pro_insignia');
    }
}
