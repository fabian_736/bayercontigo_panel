<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_notificaciones', function (Blueprint $table) {
            Schema::create('bc_notificaciones', function (Blueprint $table) {
                $table->id('id_notificaciones');
                $table->string('nombre', 255);
                $table->string('tipo', 100);
                $table->string('descripcion', 1000);
                $table->bigInteger('FK_id_useradmin')->nullable()->unsigned();
                $table->timestamps();
    
                $table->foreign('FK_id_useradmin')->references('id_useradmin')->on('bc_useradmin');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_notificaciones');
    }
}
