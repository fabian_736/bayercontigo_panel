<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRespuestaOpcionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_respuesta_opcion', function (Blueprint $table) {
            Schema::create('bc_respuesta_opcion', function (Blueprint $table) {
                $table->id('id_opcion');
                $table->string('opcion', 255);
                $table->bigInteger('FK_id_pregunta')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_pregunta')->references('id_pregunta')->on('bc_pregunta');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_respuesta_opcion');
    }
}
