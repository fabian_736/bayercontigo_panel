<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistorialUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_historial_user', function (Blueprint $table) {
            Schema::create('bc_historial_user', function (Blueprint $table) {
                $table->id('id_historial_user');
                $table->string('accion', 255);
                $table->date('fecha_registro');
                $table->bigInteger('FK_id_user')->unsigned();
                $table->timestamps();
    
                $table->foreign('FK_id_user')->references('id')->on('users');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_historial_user');
    }
}
