<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuestionarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_cuestionario', function (Blueprint $table) {
            Schema::create('bc_cuestionario', function (Blueprint $table) {
                $table->id('id_cuestionario');
                $table->string('titulo', 255);
                $table->text('descripcion');
                $table->bigInteger('puntuacion')->nullable();
                $table->boolean('estado')->default(0);
                $table->bigInteger('FK_id_capacitacion')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_capacitacion')->references('id_capacitacion')->on('bc_capacitacion');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_cuestionario');
    }
}
