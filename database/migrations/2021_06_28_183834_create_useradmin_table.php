<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CreateUseradminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_useradmin', function (Blueprint $table) {
            Schema::create('bc_useradmin', function (Blueprint $table) {
                $table->id('id_useradmin');
                $table->string('nombre', 50);
                $table->string('apellido', 50);
                $table->string('email', 50);
                $table->string('password')->nullable();
                $table->timestamp('email_verified_at')->nullable();
                $table->string('email_code')->nullable();
                $table->boolean('estado')->default(1);
                $table->string('avatar')->nullable();
                //PERMISOS
                    $table->boolean('p_store')->default(1);
                    $table->boolean('p_update')->default(1);
                    $table->boolean('p_destroy')->default(1);
                //FIN PERMISOS
                $table->rememberToken();
                $table->bigInteger('FK_id_adminrol')->unsigned();

                $table->timestamps();
                $table->softDeletes();
                
                $table->foreign('FK_id_adminrol')->references('id_adminrol')->on('bc_adminrol');
            });
        });

        DB::table('bc_useradmin')->insert(
            array(
                'nombre' => 'Admin',
                'apellido' => 'Bayer Contigo',
                'email' => 'admin@admin.com',
                'estado' => 1,
                'password' => Hash::make('admin'), //la contraseña por defecto es admin
                'FK_id_adminrol' => 1,
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_useradmin');
    }
}
