<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateNoticiasTipoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_noticias_tipo', function (Blueprint $table) {
            Schema::create('bc_noticias_tipo', function (Blueprint $table) {
                $table->id('id_noticia_tipo');
                $table->string('nombre', 255);
                $table->timestamps();
            });
        });

        DB::table('bc_noticias_tipo')->insert(array(
            array(
                'nombre' => 'Receta',
            ),
            array(
                'nombre' => 'Tip de Moda',
            ),
            array(
                'nombre' => 'Salud',
            ),
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_noticias_tipo');
    }
}
