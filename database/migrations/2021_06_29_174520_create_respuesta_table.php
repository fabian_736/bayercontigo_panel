<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRespuestaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_respuesta', function (Blueprint $table) {
            Schema::create('bc_respuesta', function (Blueprint $table) {
                $table->id('id_respuesta');
                $table->string('respuesta', 255);
                $table->string('imagen', 255)->nullable();
                $table->boolean('correcta');
                $table->bigInteger('FK_id_pregunta')->nullable()->unsigned();
                $table->bigInteger('FK_id_opcion')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_pregunta')->references('id_pregunta')->on('bc_pregunta');
                $table->foreign('FK_id_opcion')->references('id_opcion')->on('bc_respuesta_opcion');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_respuesta');
    }
}
