<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRespuestaUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_respuesta_user', function (Blueprint $table) {
            Schema::create('bc_respuesta_user', function (Blueprint $table) {
                $table->id('id_respuesta_user');
                $table->string('respuesta', 255);
                $table->string('pregunta', 255);
                $table->string('opción', 255);
                $table->boolean('correcta');
                $table->timestamps();
                $table->bigInteger('FK_id_cuestionario_user')->nullable()->unsigned();
    
                $table->foreign('FK_id_cuestionario_user')->references('id_cuestionario_user')->on('bc_cuestionario_user');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_respuesta_user');
    }
}
