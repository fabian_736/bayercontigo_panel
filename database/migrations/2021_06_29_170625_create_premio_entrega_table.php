<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremioEntregaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_premio_entrega', function (Blueprint $table) {
            Schema::create('bc_premio_entrega', function (Blueprint $table) {
                $table->id('id_entrega');
                $table->string('tipo', 255);
                
                /* DATOS DE ENTREGA */
                $table->string('telefono', 255);
                $table->string('correo', 255)->nullable();
                $table->string('receptor', 255)->nullable();
                $table->string('cedula', 255)->nullable();
                $table->string('direccion', 255)->nullable();
                $table->text('adicional')->nullable();
                
                /* DATOS DE ESTATUS DE ENVIO */
                $table->string('estado', 255)->default('pendiente');
                $table->string('tiempo', 255)->nullable(); //dias restantes de entrega

                /* RELACIONES */
                $table->bigInteger('FK_id_canjeado')->nullable()->unsigned();
                $table->timestamps();
    
                $table->foreign('FK_id_canjeado')->references('id_canjeado')->on('bc_premio_canjeado');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_premio_entrega');
    }
}
