<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserNotificacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_user_notificaciones', function (Blueprint $table) {
            Schema::create('bc_user_notificaciones', function (Blueprint $table) {
                $table->id('id_user_notificaciones');
                $table->bigInteger('FK_id_user')->nullable()->unsigned();
                $table->bigInteger('FK_id_notificaciones')->nullable()->unsigned();
                $table->timestamp('read_at')->nullable();
                $table->timestamps();
    
                $table->foreign('FK_id_user')->references('id')->on('users');
                $table->foreign('FK_id_notificaciones')->references('id_notificaciones')->on('bc_notificaciones');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_user_notificaciones');
    }
}
