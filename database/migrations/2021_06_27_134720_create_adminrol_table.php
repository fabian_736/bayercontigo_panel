<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateAdminrolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_adminrol', function (Blueprint $table) {
            Schema::create('bc_adminrol', function (Blueprint $table) {
                $table->id('id_adminrol');
                $table->string('nombre', 20);
                $table->timestamps();
            });
        });

        DB::table('bc_adminrol')->insert(array(
            array(
                'nombre' => 'Super Admin',
            ),
            array(
                'nombre' => 'Call Center',
            ),
            array(
                'nombre' => 'Básico',
            ),
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_adminrol');
    }
}
