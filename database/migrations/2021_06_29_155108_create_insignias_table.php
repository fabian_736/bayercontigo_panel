<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsigniasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_insignias', function (Blueprint $table) {
            Schema::create('bc_insignias', function (Blueprint $table) {
                $table->id('id_insignia');
                $table->string('nombre');
                $table->string('imagen')->nullable();
                //CRITERIOS PARA GANARTELO VEREMOS COMO ES ESO
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_insignias');
    }
}
