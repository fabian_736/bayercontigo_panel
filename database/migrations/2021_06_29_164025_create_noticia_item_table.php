<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticiaItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_noticia_item', function (Blueprint $table) {
            Schema::create('bc_noticia_item', function (Blueprint $table) {
                $table->id('id_noticia_item');
                $table->string('item', 255);
                $table->bigInteger('FK_id_noticia')->nullable()->unsigned();
                $table->timestamps();
    
                $table->foreign('FK_id_noticia')->references('id_noticia')->on('bc_noticia');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_noticia_item');
    }
}
