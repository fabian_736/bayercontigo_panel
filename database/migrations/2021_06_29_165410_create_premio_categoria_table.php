<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremioCategoriaTable extends Migration
{
    public function up()
    {
        Schema::table('bc_premio_categoria', function (Blueprint $table) {
            Schema::create('bc_premio_categoria', function (Blueprint $table) {
                $table->id('id_premio_cat');
                $table->string('categoria', 255);
                $table->string('imagen')->nullable();
                $table->timestamps();
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('bc_premio_categoria');
    }
}
