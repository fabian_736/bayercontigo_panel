<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_noticia', function (Blueprint $table) {
            Schema::create('bc_noticia', function (Blueprint $table) {
                $table->id('id_noticia');
                $table->string('titulo', 255);
                $table->string('categoria',255);
                $table->string('descripcion',255);
                $table->text('contenido')->nullable();
                $table->boolean('estado');
                $table->date('fecha_inicio')->nullable();
                $table->date('fecha_fin')->nullable();
                $table->string('imagen', 255)->nullable();
                $table->string('documento', 255)->nullable();

                $table->bigInteger('FK_id_tipo')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_tipo')->references('id_noticia_tipo')->on('bc_noticias_tipo');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_noticia');
    }
}
