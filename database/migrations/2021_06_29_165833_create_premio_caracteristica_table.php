<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremioCaracteristicaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_premio_caracteristica', function (Blueprint $table) {
            Schema::create('bc_premio_caracteristica', function (Blueprint $table) {
                $table->id('id_caracteristica');
                $table->string('caracteristica', 255);
                $table->bigInteger('FK_id_premio')->nullable()->unsigned();
                $table->timestamps();
    
                $table->foreign('FK_id_premio')->references('id_premio')->on('bc_premio');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_premio_caracteristica');
    }
}
