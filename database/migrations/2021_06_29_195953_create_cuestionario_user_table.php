<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuestionarioUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_cuestionario_user', function (Blueprint $table) {
            Schema::create('bc_cuestionario_user', function (Blueprint $table) {
                $table->id('id_cuestionario_user');
                $table->string('titulo', 255);
                $table->text('descripcion');
                $table->string('tipo', 255);
                $table->integer('puntos');
                $table->bigInteger('FK_id_cuestionario')->nullable()->unsigned();
                $table->bigInteger('FK_id_user')->nullable()->unsigned();
                $table->timestamps();
    
                $table->foreign('FK_id_cuestionario')->references('id_cuestionario')->on('bc_cuestionario');
                $table->foreign('FK_id_user')->references('id')->on('users');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_cuestionario_user');
    }
}
