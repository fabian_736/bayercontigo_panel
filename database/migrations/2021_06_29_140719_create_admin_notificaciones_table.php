<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminNotificacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_admin_notificaciones', function (Blueprint $table) {
            Schema::create('bc_admin_notificaciones', function (Blueprint $table) {
                $table->id('id_admin_notificaciones');
                $table->bigInteger('FK_id_useradmin')->nullable()->unsigned();
                $table->bigInteger('FK_id_notificaciones')->nullable()->unsigned();
                $table->timestamp('read_at')->nullable();
                $table->timestamps();
    
                $table->foreign('FK_id_useradmin')->references('id_useradmin')->on('bc_useradmin');
                $table->foreign('FK_id_notificaciones')->references('id_notificaciones')->on('bc_notificaciones');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_admin_notificaciones');
    }
}
