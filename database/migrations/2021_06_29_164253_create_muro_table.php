<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMuroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_muro', function (Blueprint $table) {
            Schema::create('bc_muro', function (Blueprint $table) {
                $table->id('id_muro');
                $table->string('tipo', 255);
                $table->text('contenido');
                $table->string('imagen')->nullable();
                $table->bigInteger('FK_id_useradmin')->nullable()->unsigned();
                $table->timestamps();
    
                $table->foreign('FK_id_useradmin')->references('id_useradmin')->on('bc_useradmin');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_muro');
    }
}
