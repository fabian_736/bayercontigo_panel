<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreguntaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_pregunta', function (Blueprint $table) {
            Schema::create('bc_pregunta', function (Blueprint $table) {
                $table->id('id_pregunta');
                $table->string('pregunta', 255);
                $table->string('tipo', 255);
                $table->bigInteger('puntuacion')->nullable();
                $table->boolean('estado')->default(0);
                $table->bigInteger('FK_id_cuestionario')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_cuestionario')->references('id_cuestionario')->on('bc_cuestionario');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_pregunta');
    }
}
