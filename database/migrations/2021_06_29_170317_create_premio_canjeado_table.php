<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremioCanjeadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_premio_canjeado', function (Blueprint $table) {
            Schema::create('bc_premio_canjeado', function (Blueprint $table) {
                $table->id('id_canjeado');
                $table->string('nombre', 255);
                $table->string('marca', 255);
                $table->string('categoria', 255);
                $table->text('descripcion');
                $table->bigInteger('cantidad');
                $table->bigInteger('total_puntos');
                $table->bigInteger('FK_id_premio')->nullable()->unsigned();
                $table->bigInteger('FK_id_user')->nullable()->unsigned();
                $table->timestamps();
    
                $table->foreign('FK_id_premio')->references('id_premio')->on('bc_premio');
                $table->foreign('FK_id_user')->references('id')->on('users');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_premio_canjeado');
    }
}
