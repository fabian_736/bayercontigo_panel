<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapacitacionUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_capacitacion_user', function (Blueprint $table) {
            Schema::create('bc_capacitacion_user', function (Blueprint $table) {
                $table->id('id_capacitacion_user');
                $table->bigInteger('FK_id_user')->nullable()->unsigned();
                $table->bigInteger('FK_id_capacitacion_documento')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_user')->references('id')->on('users');
                $table->foreign('FK_id_capacitacion_documento')->references('id_capacitacion_documento')->on('bc_capacitacion_documento');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_capacitacion_user');
    }
}
