<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bc_producto', function (Blueprint $table) {
            Schema::create('bc_producto', function (Blueprint $table) {
                $table->id('id_producto');
                $table->string('nombre', 255);
                $table->text('descripcion');
                $table->string('imagen', 255)->nullable();
                $table->timestamps();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bc_producto');
    }
}
